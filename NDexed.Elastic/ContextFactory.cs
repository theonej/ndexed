﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;

using NDexed.Domain.Models.Documents;
using NDexed.Domain.Models.Regulations;
using NDexed.Domain.Models;

namespace NDexed.Elastic
{
    public static class ContextFactory
    {
        internal const string LOCAL_BASE_ADDRESS = "http://localhost:9200/";
        internal const string BASE_INDEX = "n-dexed";

        internal static string BASE_ADDRESS = ConfigurationManager.AppSettings["ElasticsearchBaseAddress"];

        private static Dictionary<Type, string> m_EndpointDictionary = new Dictionary<Type, string>
        {
            {typeof(DocumentSummary), string.Format("{0}/document", BASE_ADDRESS)},
            {typeof(DocumentInfo), string.Format("{0}/document", BASE_ADDRESS)},
            {typeof(RegulationInfo), string.Format("{0}/regulation", BASE_ADDRESS)},
            {typeof(LogInfo), string.Format("{0}/logs", BASE_ADDRESS)}
        };

        internal static HttpClient InitializeClient(Type type)
        {
            var client = new HttpClient();

            if (m_EndpointDictionary.ContainsKey(type))
            {
                client.BaseAddress = new Uri(m_EndpointDictionary[type]);
            }else
            {
                client.BaseAddress = new Uri(BASE_ADDRESS);
            }

            return client;
        }

        internal static HttpClient InitializeClient(string type)
        {
            var client = new HttpClient();

            client.BaseAddress = new Uri(string.Format("{0}/{1}", BASE_ADDRESS, type));

            return client;
        }

        internal static HttpClient InitializeClient(string index, string type)
        {
            var client = new HttpClient();

            client.BaseAddress = new Uri(string.Format("{0}/{1}/{2}", BASE_ADDRESS, index, type));

            return client;
        }
    }
}
