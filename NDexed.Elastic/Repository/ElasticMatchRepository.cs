﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Matching;
using NDexed.Messaging.Commands.Matching;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;

namespace NDexed.Elastic.Repository
{
    public class ElasticMatchRepository : ISearchableRepository<FindMatchesCommand, MatchInfo>
    {
        public IEnumerable<MatchInfo> Search(FindMatchesCommand criteria)
        {
            var returnValue = new List<MatchInfo>();

            dynamic results = GetResponse(criteria);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];
                var score = hit["_score"];

                var data = JsonConvert.DeserializeObject(fields.ToString());

                var match = new MatchInfo();
                match.Data = data;
                match.Rank = decimal.Parse(score.ToString());

                returnValue.Add(match);
            }

            return returnValue;
        }

        private dynamic GetResponse(FindMatchesCommand criteria)
        {
            var client = ContextFactory.InitializeClient(criteria.MatchType);

            using (client)
            {
                //get all
                var request = GetQuery(criteria);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}/_search?size={1}&from={2}", client.BaseAddress, criteria.PageSize, criteria.PageSize * criteria.PageNumber);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }

        private JObject GetQuery(FindMatchesCommand criteria)
        {
            var request = new JObject
            {
                {
                    "query", new JObject
                    {
                        {
                            "bool", new JObject
                            {
                                {
                                    "must", GetMatchArray(string.Join(" ", criteria.SearchTerms))
                                }
                            }
                        }
                    }
                }
            };

            return request;
        }

        private JArray GetMatchArray(string criteria)
        {
            var returnValue = new JArray
                                        {
                                            new JObject
                                            {
                                                {
                                                    "match",
                                                    new JObject
                                                    {
                                                        {"applicationContents", criteria}
                                                    }
                                                }
                                            }
                                        };

            return returnValue;
        }
    }
}
