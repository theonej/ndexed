﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Repositories;
using NDexed.DataAccess.Response;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Regulations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Elastic.Repository
{
    public class ElasticRegulationRepository : IRepository<Guid, RegulationInfo>,
                                               ISearchableRepository<RegulationSearchQuery, RegulationSearchResponse>
    {
        private readonly IRepository<Guid, TermInfo> m_TermRepo;

        public ElasticRegulationRepository(IRepository<Guid, TermInfo> termRepo)
        {
            Condition.Requires(termRepo).IsNotNull();

            m_TermRepo = termRepo;
        }

        public Guid Add(RegulationInfo item)
        {
            throw new NotImplementedException();
        }

        public RegulationInfo Get(Guid id)
        {
            var client = ContextFactory.InitializeClient(typeof(RegulationInfo));

            using (client)
            {
                //get all
                string resourcePath = string.Format("{0}/{1}/_source", client.BaseAddress.AbsoluteUri, id);
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }
                dynamic documentObject = JsonConvert.DeserializeObject(responseContent);
                var returnValue = JsonConvert.DeserializeObject<RegulationInfo>(responseContent);
                if (documentObject.Base64EncodedData != null)
                {
                    returnValue.Base64EncodedData = documentObject.Base64EncodedData.ToString();
                }
                if (documentObject.attachment != null)
                {
                    returnValue.Contents = documentObject.attachment.content;
                }

                return returnValue;
            }
        }

        public IEnumerable<RegulationInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Remove(RegulationInfo item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RegulationSearchResponse> Search(RegulationSearchQuery criteria)
        {
            var returnValue = new List<RegulationSearchResponse>();

            foreach (var code in criteria.UsCodes)
            {
                var response = new RegulationSearchResponse();
                response.UsCodeName = code;
                int totalDocuments = 0;
                response.Regulations = Search(criteria.SearchTerm, code, criteria.PageNumber, criteria.PageSize, out totalDocuments).ToList();
                response.TotalDocuments = totalDocuments;

                returnValue.Add(response);
            }
            return returnValue;
        }

        private IEnumerable<RegulationInfo> Search(string term, string regulationName, int pageNumber, int pageSize, out int totalResultsCount)
        {
            var returnValue = new List<RegulationInfo>();

            var client = ContextFactory.InitializeClient(typeof(RegulationInfo));

            using (client)
            {
                //get all
                var args = new object[]
                {
                    ContextFactory.BASE_ADDRESS,
                    pageNumber * pageSize,
                    pageSize
                };

                //var request = new JObject
                //{
                //    {
                //        "query", new JObject
                //        {
                //            {
                //                "bool", new JObject
                //                {
                //                    {
                //                        "must", new JArray
                //                        {
                //                            new JObject
                //                            {
                //                                {
                //                                    "match",
                //                                    new JObject
                //                                    {
                //                                        {"attachment.content", SearchTermUtilities.GetDistinctTerms(term, m_TermRepo)}
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //};

                dynamic request = new JObject();

                request["_source"] = new JObject();
                request["_source"].exclude = new JArray("Base64EncodedData");

                request.query = new JObject();
                request.query["bool"] = new JObject();
                request.query["bool"].must = new JObject();
                request.query["bool"].must.match = new JObject();
                request.query["bool"].must.match["attachment.content"] = new JObject();
                request.query["bool"].must.match["attachment.content"] = SearchTermUtilities.GetDistinctTerms(term, m_TermRepo);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                string resourcePath = string.Format("{0}/regulation/_search?from={1}&size={2}", args);
                HttpResponseMessage response = client.PostAsync(resourcePath, content).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }

                var responseObjects = (JObject)JsonConvert.DeserializeObject(responseContent);
                var hits = responseObjects["hits"];

                totalResultsCount = int.Parse(hits["total"].ToString());

                foreach (var hit in hits["hits"])
                {
                    var source = hit["_source"];
                    var regulation = JsonConvert.DeserializeObject<RegulationInfo>(source.ToString());

                    regulation.Id =Guid.Parse(hit["_id"].ToString());
                    regulation.Contents = source["attachment"]["content"].ToString();
                    regulation.Base64EncodedData = null;
                    returnValue.Add(regulation);
                }

                return returnValue;
            }
        }
    }
}
