﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NDexed.Elastic.Repository
{
    public class ElasticPatentApplicationRepository : ISearchableRepository<SearchInfo, PatentApplicationInfo>,
                                                      IRepository<string, PatentApplicationInfo>
    {
        public IEnumerable<PatentApplicationInfo> Search(SearchInfo criteria)
        { 
            var returnValue = new List<PatentApplicationInfo>();

            dynamic results = GetPatentSearchResponse(criteria);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];
                var score = hit["_score"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                var application = new PatentApplicationInfo();
                application.Description = data.text;
                application.Abstract = data.text;
                application.PatentNumber = data.patent_id;
                application.Link = string.Format("http://pdfpiw.uspto.gov/.piw?PageNum=0&docid={0}", application.PatentNumber);
                application.ApplicationNumber = GetPatentApplication(application.PatentNumber).ApplicationNumber;
                application.Cpc = GetCpc(data.patent_id.ToString());

                var patentResults = GetPatentResponse(application.PatentNumber);
                var patentInfo = (dynamic)((JArray)patentResults.hits.hits).FirstOrDefault();
                if (patentInfo != null)
                {
                    var source = patentInfo["_source"];
                    application.Title = source.title;
                    application.Date = source.date;
                    application.Abstract = source["abstract"];
                    application.Kind = source.kind;
                }


                returnValue.Add(application);
            }

            return returnValue;
        }

        private dynamic GetPatentResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed-patent", "patent");

            using (client)
            {
                var url = string.Format("{0}/_search?q=number:{1}", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }

        private List<CpcInfo> GetCpc(string patentNumber)
        {
            var returnValue = new List<CpcInfo>();

            dynamic results = GetCpcResponse(patentNumber);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                var cpc = new CpcInfo();
                cpc.PatentNumber = data.patent_id;
                cpc.Section = data.section_id;
                cpc.Subsection = data.subsection_id;
                cpc.Group = data.group_id;
                cpc.SubGroup = data.subgroup_id;
                cpc.Sequence = int.Parse(data.sequence.ToString());

                returnValue.Add(cpc);
            }

            return returnValue.OrderBy(item => item.Sequence).ToList();
        }

        private dynamic GetCpcResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed-cpc", "cpc");

            using (client)
            {
                var url = string.Format("{0}/_search?q=patent_id:{1}&size=1000", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }


        private dynamic GetPatentSearchResponse(SearchInfo criteria)
        {
            var client = ContextFactory.InitializeClient("n-dexed-application-text", "application-text");

            using (client)
            {
                var request = GetPatentApplicationQuery(criteria);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}/_search?size={1}&from={2}", client.BaseAddress, criteria.Count, criteria.Skip);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }
        private dynamic GetPatentApplicationQuery(SearchInfo criteria)
        {
            dynamic request = new JObject();

            request.query = new JObject();
            request.query.common = new JObject();
            request.query.common.text = new JObject();
            request.query.common.text.query = criteria.Query;
            request.query.common.text.cutoff_frequency = 0.001;

            return request;
        }

        private PatentApplicationInfo GetPatentApplication(string patentNumber)
        {
            var returnValue = new PatentApplicationInfo();

            dynamic results = GetPatentApplicationResponse(patentNumber);

            JArray hits = results.hits.hits;

            JToken hit = hits.FirstOrDefault();
            if(hit != null)
            {
                var fields = hit["_source"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                returnValue.ApplicationNumber = data.number;
                returnValue.PatentNumber = data.patent_id;
            }

            return returnValue;
        }
        private dynamic GetPatentApplicationResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed-application", "application");

            using (client)
            {
                var url = string.Format("{0}/_search?q=patent_id:{1}&size=1000", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }

        public PatentApplicationInfo Get(string id)
        {
            return GetPatentApplication(id);
        }

        public string Add(PatentApplicationInfo item)
        {
            throw new NotImplementedException();
        }

        public void Remove(PatentApplicationInfo item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PatentApplicationInfo> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
