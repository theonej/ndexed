﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NDexed.Elastic.Repository
{
    public class ElasticLogRepository : IRepository<Guid, LogInfo>
    {
        public Guid Add(LogInfo item)
        {
            //HttpClient client = ContextFactory.InitializeClient(typeof(LogInfo));
            //using (client)
            //{
            //    if(item.Id == Guid.Empty)
            //    {
            //        item.Id = Guid.NewGuid();
            //    }
            //    var itemObject = JObject.FromObject(item);

            //    var formatter = new JsonMediaTypeFormatter();
            //    var header = new MediaTypeHeaderValue("application/json");
            //    HttpContent content = new ObjectContent(typeof(JObject), itemObject, formatter, header);

            //    var url = string.Format("{0}/{1}", client.BaseAddress.AbsoluteUri, item.Id);
            //    var response = client.PutAsync(url, content).Result;
            //    var responseMessage = response.Content.ReadAsStringAsync().Result;
            //    if (!response.IsSuccessStatusCode)
            //    {
            //        throw new HttpException((int)response.StatusCode, responseMessage);
            //    }
            //}

            return item.Id;
        }

        public LogInfo Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LogInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Remove(LogInfo item)
        {
            throw new NotImplementedException();
        }
    }
}
