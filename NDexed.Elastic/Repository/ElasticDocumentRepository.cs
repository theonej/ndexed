﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Web;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Repositories;
using NDexed.DataAccess.Response;
using NDexed.Domain.Models.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NDexed.Domain.Models.Data;
using CuttingEdge.Conditions;
using System.Linq;

namespace NDexed.Elastic.Repository
{
    public class ElasticDocumentRepository : ISearchableRepository<Guid, DocumentSummary>,
                                             ISearchableRepository<Guid, DocumentInfo>,
                                             ISearchableRepository<DocumentSearchQuery, DocumentSearchResponse>,
                                             ISearchableRepository<IndexedDocumentQuery, DocumentInfo>,
                                             IRepository<Guid, DocumentInfo>
    {

        private readonly IRepository<Guid, TermInfo> m_TermRepo;

        public ElasticDocumentRepository(IRepository<Guid, TermInfo> termRepo)
        {
            Condition.Requires(termRepo).IsNotNull();

            m_TermRepo = termRepo;
        }

        public IEnumerable<DocumentSummary> Search(Guid criteria)
        {
            var client = ContextFactory.InitializeClient(typeof(DocumentSummary));

            using (client)
            {
                //get all
                string resourcePath = string.Format("/_search?q=CaseId:\"{0}\"", criteria);
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }

                var responseObjects = (JObject)JsonConvert.DeserializeObject(responseContent);
                JToken total = responseObjects["hits"]["total"];

                var documentSummary = new DocumentSummary
                {
                    DocumentCount = int.Parse(total.ToString())
                };

                var returnValue = new List<DocumentSummary> { documentSummary };

                return returnValue;
            }
        }

        public DocumentInfo Get(Guid id)
        {
            var client = ContextFactory.InitializeClient(typeof(DocumentSummary));

            using (client)
            {
                //get all
                string resourcePath = string.Format("{0}/{1}/_source", client.BaseAddress.AbsoluteUri, id);
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }
                dynamic documentObject = JsonConvert.DeserializeObject(responseContent);
                var returnValue = JsonConvert.DeserializeObject<DocumentInfo>(responseContent);
                if (documentObject.Base64EncodedData != null)
                {
                    returnValue.Base64EncodedData = documentObject.Base64EncodedData.ToString();
                }
                if (documentObject.attachment != null)
                {
                    returnValue.Contents = documentObject.attachment.content;
                }

                return returnValue;
            }
        }

        public Guid Add(DocumentInfo item)
        {
            HttpClient client = ContextFactory.InitializeClient(typeof(DocumentInfo));
            using (client)
            {
                var fileContent = item.Base64EncodedData;
                item.Contents = null;

                var itemObject = JObject.FromObject(item);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                HttpContent content = new ObjectContent(typeof(JObject), itemObject, formatter, header);

                var url = string.Format("{0}/{1}?pipeline=attachment", client.BaseAddress.AbsoluteUri, item.Id);
                var response = client.PutAsync(url, content).Result;
                var responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }
            }

            return item.Id;
        }

        public void Remove(DocumentInfo item)
        {
            var client = ContextFactory.InitializeClient(typeof(DocumentSummary));

            using (client)
            {
                //get all
                string resourcePath = string.Format("{0}/{1}", client.BaseAddress.AbsoluteUri, item.Id);
                HttpResponseMessage response = client.DeleteAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }
            }
        }

        public IEnumerable<DocumentInfo> GetAll()
        {
            var returnValue = new List<DocumentInfo>();

            var client = ContextFactory.InitializeClient(typeof(DocumentSummary));

            using (client)
            {
                //get all
                string resourcePath = string.Format("{0}/_search?filter_path=hits.hits._source&size=200", client.BaseAddress.AbsoluteUri);
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }
                dynamic results = JsonConvert.DeserializeObject(responseContent);
                JArray hits = results.hits.hits;
                results.TotalDocuments = results.hits.total;

                foreach (JToken hit in hits)
                {
                    var fields = hit["_source"];

                    dynamic documentObject = JsonConvert.DeserializeObject(fields.ToString());
                    var document = JsonConvert.DeserializeObject<DocumentInfo>(fields.ToString());
                    if (documentObject.Base64EncodedData != null)
                    {
                        document.Base64EncodedData = documentObject.Base64EncodedData.ToString();
                    }
                    if (documentObject.attachment != null)
                    {
                        document.Contents = documentObject.attachment.content;
                    }

                    returnValue.Add(document);
                }
            }

            return returnValue;
         }

        IEnumerable<DocumentInfo> ISearchableRepository<Guid, DocumentInfo>.Search(Guid criteria)
        {
            var client = ContextFactory.InitializeClient(typeof(DocumentSummary));

            using (client)
            {
                //get all
                string resourcePath = string.Format("/_search?q=CaseId:\"{0}\"&filter_path=hits.hits._source&_source=Id,CaseId,Name,Size,CreatedDateTime,CreatedBy&&sort=CreatedDateTime:desc&size=1000", criteria);
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }

                var returnValue = GetDocumentsFromResults(responseContent);

                return returnValue;
            }
        }

        public IEnumerable<DocumentSearchResponse> Search(DocumentSearchQuery criteria)
        {
            var returnValue = new DocumentSearchResponse();
            returnValue.Results = new List<DocumentInfo>();

            var client = ContextFactory.InitializeClient(typeof(DocumentSummary));

            using (client)
            {
                //get all
                var request = new JObject
                {
                    {
                        "query", new JObject
                        {
                            {
                                "bool", new JObject
                                {
                                    {
                                        "must", GetMustArray(criteria)
                                    },
                                    {
                                        "must_not", new JArray
                                        {
                                            new JObject
                                            {
                                                {
                                                    "match",
                                                    new JObject
                                                    {
                                                        {"CaseId", criteria.CaseId }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                };

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}/_search?size={1}&from={2}", client.BaseAddress, criteria.PageSize, criteria.PageSize * criteria.PageNumber);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);
                JArray hits = results.hits.hits;
                returnValue.TotalDocuments = results.hits.total;

                foreach (JToken hit in hits)
                {
                    var fields = hit["_source"];

                    var document = JsonConvert.DeserializeObject<DocumentInfo>(fields.ToString());
                    if (fields["attachment"] != null && fields["attachment"]["content"] != null)
                    {
                        document.Contents = fields["attachment"]["content"].ToString();
                    }
                    if (returnValue.Results.Where(doc => doc.Name == document.Name).Count() == 0)
                    {
                        returnValue.Results.Add(document);
                    }
                }

                return new List<DocumentSearchResponse> { returnValue };
            }
        }

        private JArray GetMustArray(DocumentSearchQuery criteria)
        {
            var returnValue = new JArray
                                        {
                                            new JObject
                                            {
                                                {
                                                    "match",
                                                    new JObject
                                                    {
                                                        {"attachment.content", SearchTermUtilities.GetDistinctTerms(criteria.Term, m_TermRepo)}
                                                    }
                                                }
                                            },new JObject
                                            {
                                                {
                                                    "match",
                                                    new JObject
                                                    {
                                                        {"OrganizationId", criteria.OrganizationId.ToString()}
                                                    }
                                                }
                                            }
                                        };

            if(criteria.Tags != null)
            {
                foreach(var tag in criteria.Tags)
                {
                    var tagMatch = new JObject
                    {
                        {
                            "match",
                            new JObject
                            {
                                {"Tags", tag }
                            }
                        }
                    };

                    returnValue.Add(tagMatch);
                }
            }
            return returnValue;
        }

        public IEnumerable<DocumentInfo> Search(IndexedDocumentQuery criteria)
        {
            var client = ContextFactory.InitializeClient(typeof(DocumentSummary));

            using (client)
            {
                //get all
                string resourcePath = string.Format("/_search?q=(CaseId:\"{0}\"%20AND%20Indexed:\"{1}\")", criteria.CaseId, criteria.Indexed.ToString().ToLower());
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }

                var returnValue = GetDocumentsFromResults(responseContent);

                return returnValue;
            }
        }

        private List<DocumentInfo> GetDocumentsFromResults(string responseContent)
        {
            var returnValue = new List<DocumentInfo>();

            dynamic results = JsonConvert.DeserializeObject(responseContent);
            if (results.hits != null)
            {
                JArray hits = results.hits.hits;
                foreach (JToken hit in hits)
                {
                    var fields = hit["_source"];

                    var document = new DocumentInfo
                    {
                        Id = Guid.Parse(fields["Id"].ToString()),
                        Name = fields["Name"].ToString(),
                        CaseId = Guid.Parse(fields["CaseId"].ToString()),
                        Size = int.Parse(fields["Size"].ToString()),
                        CreatedDateTime = DateTime.Parse(fields["CreatedDateTime"].ToString()),
                    };

                    if (fields["Indexed"] != null)
                    {
                        document.Indexed = bool.Parse(fields["Indexed"].ToString());
                    }

                    if (fields["Base64EncodedData"] != null)
                    {
                        document.Base64EncodedData = fields["Base64EncodedData"].ToString();
                    }


                    if (fields["attachment"] != null && fields["attachment"]["content"] != null)
                    {
                        document.Contents = fields["attachment"]["content"].ToString();
                    }

                    returnValue.Add(document);
                }
            }

            return returnValue;
        }
    }
}
