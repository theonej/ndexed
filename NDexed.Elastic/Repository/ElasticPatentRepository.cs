﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NDexed.Elastic.Repository
{
    public class ElasticPatentRepository : IRepository<string, PatentInfo>,
                                           ISearchableRepository<SearchInfo, PatentInfo>
    {
        public string Add(PatentInfo item)
        {
            throw new NotImplementedException();
        }

        public PatentInfo Get(string id)
        {
            var returnValue = GetPatent(id);

            var summaryResults = GetPatentSummaryResponse(id);
            var summaryInfo = (dynamic)((JArray)summaryResults.hits.hits).FirstOrDefault();

            if (summaryInfo != null)
            {
                returnValue.Description = summaryInfo["_source"].summary;
            }
            //create patent info object
            returnValue.Claims = GetClaims(id);
            returnValue.Citations = GetCitations(id);
            returnValue.Cpc = GetCpc(id);

            return returnValue; 
        }

        private PatentInfo GetPatent(string id)
        {
            var returnValue = new PatentInfo();
            returnValue.PatentNumber = id;
            returnValue.Link = string.Format("http://pdfpiw.uspto.gov/.piw?PageNum=0&docid={0}", id);

            returnValue.Source = PatentInfo.Sources.Internal;

            //set description by getting the summary out of the summary index
            var patentResults = GetPatentResponse(id);
            var patentInfo = (dynamic)((JArray)patentResults.hits.hits).FirstOrDefault();
            if (patentInfo != null)
            {
                var source = patentInfo["_source"];
                returnValue.Title = source.title;
                returnValue.Date = source.date;
                returnValue.Abstract = source["abstract"];
                returnValue.Kind = source.kind;
            }

            return returnValue;
        }

        public IEnumerable<PatentInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Remove(PatentInfo item)
        {
            throw new NotImplementedException();
        }

        private List<CpcInfo> GetCpc(string patentNumber)
        {
            var returnValue = new List<CpcInfo>();

            dynamic results = GetCpcResponse(patentNumber);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                var cpc = new CpcInfo();
                cpc.PatentNumber = data.patent_id;
                cpc.Section = data.section_id;
                cpc.Subsection = data.subsection_id;
                cpc.Group = data.group_id;
                cpc.SubGroup = data.subgroup_id;
                cpc.Sequence = int.Parse(data.sequence.ToString());

                returnValue.Add(cpc);
            }

            return returnValue.OrderBy(item=>item.Sequence).ToList();
        }

        private dynamic GetCpcResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed-cpc", "cpc");

            using (client)
            {
                var url = string.Format("{0}/_search?q=patent_id:{1}&size=1000", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }

        private List<ClaimInfo> GetClaims(string patentNumber)
        {
            var returnValue = new List<ClaimInfo>();

            dynamic results = GetClaimsResponse(patentNumber);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];
                var score = hit["_score"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());
 
                var claim = new ClaimInfo();
                claim.Claim = data.claim;
                claim.PatentNumber = data.patent_id;
                claim.Sequence = data["sequence"] == null ? "0.0" : float.Parse(data.sequence.ToString());

                returnValue.Add(claim);
            }

            return returnValue.OrderBy(claim=>claim.Sequence).ToList();
        }

        private List<CitationInfo> GetCitations(string patentNumber)
        {
            var returnValue = new List<CitationInfo>();

            dynamic results = GetCitationsResponse(patentNumber);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];
                var score = hit["_score"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                try
                {
                    var citedPatent = GetPatent(data.citation_id.ToString());

                    var citation = new CitationInfo();
                    citation.PatentNumber = citedPatent.PatentNumber;
                    citation.Title = citedPatent.Title;
                    citation.Abstract = citedPatent.Abstract;

                    citation.CitedPatentNumber = data.citation_id;
                    citation.Country = data.country;
                    citation.Name = data.name;
                    citation.Sequence = data.sequence;
                    citation.Category = data.category;

                    returnValue.Add(citation);
                }
                catch (HttpException) { 
                    //TODO: Remove me
                }
            }

            return returnValue.OrderBy(item=>item.Sequence).ToList();
        }

        private dynamic GetCitationsResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed-citations", "citations");

            using (client)
            {
                var url = string.Format("{0}/_search?q=patent_id:{1}&size=1000", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }
        
        private dynamic GetClaimsResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed", "claims");

            using (client)
            {
                //get all
                
                var url = string.Format("{0}/_search?q=patent_id:{1}&size=1000", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }

        public IEnumerable<PatentInfo> Search(SearchInfo criteria)
        {
            var returnValue = new List<PatentInfo>();

            dynamic results = GetPatentSearchResponse(criteria);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];
                var score = hit["_score"];
                var highlightSource = hit["highlight"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());
                
                var patent = new PatentInfo();
                patent.Description = data.summary;
                patent.PatentNumber = data.patent_id;
                patent.Link = string.Format("http://pdfpiw.uspto.gov/.piw?PageNum=0&docid={0}", patent.PatentNumber);
                patent.Citations = GetCitations(data.patent_id.ToString());
                patent.Claims = GetClaims(data.patent_id.ToString());
                patent.Cpc = GetCpc(data.patent_id.ToString());
                patent.Source = PatentInfo.Sources.Internal;

                var patentResults = GetPatentResponse(patent.PatentNumber);
                var patentInfo = (dynamic)((JArray)patentResults.hits.hits).FirstOrDefault();
                if (patentInfo != null)
                {
                    var source = patentInfo["_source"];
                    patent.Title = source.title;
                    patent.Date = source.date;
                    patent.Abstract = source["abstract"];
                    patent.Kind = source.kind;
                }

                returnValue.Add(patent);
            }
            
            return returnValue;
        }


        private dynamic GetPatentSearchResponse(SearchInfo criteria)
        {
            var client = ContextFactory.InitializeClient("n-dexed-summary", "summary");

            using (client)
            {
                var request = GetPatentQuery(criteria);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}/_search?size={1}&from={2}", client.BaseAddress, criteria.Count, criteria.Skip);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }
        private dynamic GetPatentResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed-patent", "patent");

            using (client)
            {
                var url = string.Format("{0}/_search?q=number:{1}", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }

        private dynamic GetPatentSummaryResponse(string patentNumber)
        {
            var client = ContextFactory.InitializeClient("n-dexed-summary", "summary");

            using (client)
            {
                var url = string.Format("{0}/_search?q=patent_id:{1}", client.BaseAddress, patentNumber);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }


        private dynamic GetPatentQuery(SearchInfo criteria)
        {
            switch (criteria.Type)
            {
                case (SearchInfo.SearchType.Terms):
                    return GetPatentTermsQuery(criteria);
                default:
                    return GetPatentSemanticQuery(criteria);
            }
        }

        private dynamic GetPatentSemanticQuery(SearchInfo criteria)
        {
            dynamic request = new JObject();

            request.query = new JObject();
            request.query.common = new JObject();
            request.query.common.summary = new JObject();
            request.query.common.summary.query = criteria.Query;
            request.query.common.summary.cutoff_frequency = 0.001;
           
            return request;
        }
        
        private dynamic GetPatentTermsQuery(SearchInfo criteria)
        {
            var included = criteria.Included
                                   .Split('\n')
                                   .Select(term=>term.Trim());
            var excluded = criteria.Excluded
                                   .Split('\n')
                                   .Select(term => term.Trim());

            /*
             * "query" : {
                    "terms" : {
                        "user" : {
                            "index" : "users",
                            "type" : "_doc",
                            "id" : "2",
                            "path" : "followers"
                        }
                    }
                }
             */
            dynamic request = new JObject();

            request.query = new JObject();
            request.query["bool"] = new JObject();

            request.query["bool"].must = new JArray();
            foreach (var include in included)
            {
                dynamic must = new JObject();
                must.term = new JObject();
                must.term.summary = new JObject();
                must.term.summary = include;

                request.query["bool"].must.Add(must);
            }

            request.query["bool"].must_not = new JArray();
            foreach (var exclude in excluded)
            {
                dynamic must_not = new JObject();
                must_not.term = new JObject();
                must_not.term.summary = new JObject();
                must_not.term.summary = exclude;

                request.query["bool"].must_not.Add(must_not);
            }


            return request;
        }

    }
}
