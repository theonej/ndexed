﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Repositories;
using NDexed.DataAccess.Response;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Opinions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NDexed.Elastic.Repository
{
    public class ElasticOpinionMatchingRepository : ISearchableRepository<OpinionSearchQuery, OpinionSearchResponse>
    {
        private readonly IRepository<Guid, TermInfo> m_TermRepo;

        public ElasticOpinionMatchingRepository(IRepository<Guid, TermInfo> termRepo)
        {
            Condition.Requires(termRepo).IsNotNull();

            m_TermRepo = termRepo;
        }

        public IEnumerable<OpinionSearchResponse> Search(OpinionSearchQuery criteria)
        {
            var returnValue = new List<OpinionSearchResponse>();

            var client = ContextFactory.InitializeClient(typeof(OpinionInfo));

            using (client)
            {
                //get all
                var request = new JObject
                {
                    {
                        "query", new JObject
                        {
                            {
                                "bool", new JObject
                                {
                                    {
                                        "must", new JArray
                                        {
                                            new JObject
                                            {
                                                {
                                                    "match",
                                                    new JObject
                                                    {
                                                        {"html", SearchTermUtilities.GetDistinctTerms(criteria.SearchTerm, m_TermRepo)}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }     
                };

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);


                    var searchResult = new OpinionSearchResponse();
                    searchResult.Opinions = new List<OpinionInfo>();
                    searchResult.Jurisdiction = string.Join(",", criteria.Jurisdictions);

                    var url = string.Format("{0}/opinions/_search?size={1}", ContextFactory.BASE_ADDRESS, criteria.PageSize);
                    HttpResponseMessage response = client.PostAsync(url, content).Result;
                    string responseMessage = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new HttpException((int)response.StatusCode, responseMessage);
                    }

                    dynamic results = JsonConvert.DeserializeObject(responseMessage);
                    JArray hits = results.hits.hits;
                    searchResult.TotalDocuments = results.hits.total;

                    foreach (JToken hit in hits)
                    {
                        var source = hit["_source"];

                        var opinion = JsonConvert.DeserializeObject<OpinionInfo>(source.ToString());

                        opinion.id = hit["_id"].ToString();

                        opinion.SetAdditionalFields();

                        searchResult.Opinions.Add(opinion);
                    }

                    returnValue.Add(searchResult);

                return returnValue;
            }
        }
    }
}
