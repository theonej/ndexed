﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NDexed.Elastic.Repository
{
    public class ElasticClaimsSearchRepository : ISearchableRepository<SearchInfo, ClaimInfo>
    {
        public IEnumerable<ClaimInfo> Search(SearchInfo criteria)
        {
            var returnValue = new List<ClaimInfo>();

            dynamic results = GetResponse(criteria);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];
                var score = hit["_score"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                var claim = new ClaimInfo();
                claim.Claim = data.claim;
                claim.PatentNumber = data.patent_id;
                claim.ClaimRank = float.Parse(score.ToString());
                claim.Sequence = data["sequence"] ==null ? "0.0" : float.Parse(data.sequence.ToString());
                returnValue.Add(claim);
            }

            return returnValue.OrderBy(claim => claim.Sequence).ToList();
        }
        private dynamic GetResponse(SearchInfo criteria)
        {
            var client = ContextFactory.InitializeClient("n-dexed", criteria.Endpoint);

            using (client)
            {
                //get all
                var request = GetQuery(criteria);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}/_search?size={1}&from={2}", client.BaseAddress, criteria.Count, criteria.Skip);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }

        private JObject GetQuery(SearchInfo criteria)
        {
            var request = new JObject
            {
                {
                    "query", new JObject
                    {
                        {
                            "common", new JObject
                            {
                                {
                                    "claim", new JObject
                                    {
                                        {"query", criteria.Query},
                                        {"cutoff_frequency", 0.001}
                                    }
                                }
                            }
                        }
                    }
                }
            };
            return request;
        }
        /*
        private JObject GetQuery(SearchInfo criteria)
        {
            var request = new JObject
            {
                {
                    "query", new JObject
                    {
                        {
                            "bool", new JObject
                            {
                                {
                                    "must", new JObject
                                            {
                                                {
                                                    "match",
                                                    new JObject
                                                    {
                                                        {"claim", criteria.Query}
                                                    }
                                                }

                                    }
                                }
                            }
                        }
                    }
                }
            };
            return request;
        }

    */
    }
}
