﻿
using System;
using System.Collections.Generic;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web;
using NDexed.Domain.Models.Search;
using System.Threading.Tasks;
using System.Linq;

namespace NDexed.Elastic.Repository
{
    public class ElasticCpcRepository : ISearchableRepository<SearchInfo, CpcInfo>,
                                        ISearchableRepository<CpcSearchInfo, Dictionary<string, string>>
    {
        delegate Dictionary<string, string> CpcTitleHandler(List<string> args);

        public IEnumerable<CpcInfo> Search(SearchInfo criteria)
        { 
            var returnValue = new List<CpcInfo>();

            dynamic results = GetCpcSearchResponse(criteria);

            JArray hits = results.hits.hits;

            foreach (JToken hit in hits)
            {
                var fields = hit["_source"];

                dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                var cpc = new CpcInfo();
                cpc.PatentNumber = data.patent_id;
                cpc.Section = data.section_id;
                cpc.Subsection = data.subsection_id;
                cpc.Group = data.group_id;
                cpc.SubGroup = data.subgroup_id;

                returnValue.Add(cpc);
            }

            return returnValue;
        }


        private dynamic GetCpcSearchResponse(SearchInfo criteria)
        {
            var client = ContextFactory.InitializeClient("n-dexed-cpc", "cpc");

            using (client)
            {
                var request = GetCpcQuery(criteria.Query);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}/_search?size={1}&from={2}", client.BaseAddress, criteria.Count, criteria.Skip);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                return results;
            }
        }



        private dynamic GetCpcQuery(string criteria)
        {
            dynamic request = new JObject();

            var criteriaParts = criteria.ToLower().Split('/');

            var fields = new string[] 
            {
                "section_id",
                "subsection_id",
                "group_id",
                "subgroup_id"
            };

            request.query = new JObject();
            request.query.constant_score = new JObject();
            request.query.constant_score.filter = new JObject();
            request.query.constant_score.filter["bool"] = new JObject();
            request.query.constant_score.filter["bool"].should = new JArray();

            for (var fieldIndex = 0; fieldIndex < fields.Length; fieldIndex++)
            {
                var field = fields[fieldIndex];

                request.query.constant_score.filter["bool"].should.Add(new JObject());
                request.query.constant_score.filter["bool"].should[fieldIndex]["bool"] = new JObject();
                request.query.constant_score.filter["bool"].should[fieldIndex]["bool"].must = new JArray();
                for (var partIndex = 0; partIndex < criteriaParts.Length; partIndex++)
                {
                    request.query.constant_score.filter["bool"].should[fieldIndex]["bool"].must.Add(new JObject());
                    request.query.constant_score.filter["bool"].should[fieldIndex]["bool"].must[partIndex].term = new JObject();
                    request.query.constant_score.filter["bool"].should[fieldIndex]["bool"].must[partIndex].term[field] = criteriaParts[partIndex];
                }
            }

            return request;
        }

        public IEnumerable<Dictionary<string, string>> Search(CpcSearchInfo criteria)
        {
            var returnValue = new List<Dictionary<string, string>>();

            var actions = new List<Action>();
            if (criteria.Subsection.Count > 0)
            {
                actions.Add(() =>
                {
                    returnValue.Add(GetCpcSubsectionTitles(criteria.Subsection));
                });
            }

            if (criteria.Group.Count > 0)
            {
                actions.Add(() =>
                {
                    returnValue.Add(GetCpcGroupTitles(criteria.Group));
                });
            }

            if (criteria.Subgroup.Count > 0)
            {
                actions.Add(() =>
                {
                    returnValue.Add(GetCpcSubgroupTitles(criteria.Subgroup));
                });
            }

            Parallel.Invoke(actions.ToArray());

            return returnValue;
        }

        private Dictionary<string, string> GetCpcSubsectionTitles(List<string> cpcSubsections)
        {
            var returnValue = new Dictionary<string, string>();

            var client = ContextFactory.InitializeClient("n-dexed-cpc-subsection", "cpc-subsection");

            var query = string.Join(",", cpcSubsections);
            using (client)
            {
                var url = string.Format("{0}/_search?q=id({1})&size=1000", client.BaseAddress, query);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                JArray hits = results.hits.hits;

                foreach (JToken hit in hits)
                {
                    var fields = hit["_source"];

                    dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                    returnValue.Add(data.id.ToString().ToLower(), data.title.ToString());
                }
                return returnValue;
            }
        }

        private Dictionary<string, string> GetCpcGroupTitles(List<string> cpcGroups)
        {
            var returnValue = new Dictionary<string, string>();

            var client = ContextFactory.InitializeClient("n-dexed-cpc-group", "cpc-group");
            
            var query = string.Join(",", cpcGroups);
            using (client)
            {
                var url = string.Format("{0}/_search?q=id({1})&size=1000", client.BaseAddress, query);
                HttpResponseMessage response = client.GetAsync(url).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                JArray hits = results.hits.hits;

                foreach (JToken hit in hits)
                {
                    var fields = hit["_source"];

                    dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                    returnValue.Add(data.id.ToString().ToLower(), data.title.ToString());
                }
                return returnValue;
            }
        }
        private Dictionary<string, string> GetCpcSubgroupTitles(List<string> cpcSubgroups)
        {
            var returnValue = new Dictionary<string, string>();

            var client = ContextFactory.InitializeClient("n-dexed-cpc-subgroup", "cpc-subgroup");

            dynamic request = new JObject();
            request.query = new JObject();
            request.query.constant_score = new JObject();
            request.query.constant_score.filter = new JObject();
            request.query.constant_score.filter.terms = new JObject();
            request.query.constant_score.filter.terms._id = new JArray();
            foreach(var subgroup in cpcSubgroups)
            {
                request.query.constant_score.filter.terms._id.Add(subgroup);
            }
            
            using (client)
            {
                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}/_search?size=1000", client.BaseAddress);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);

                JArray hits = results.hits.hits;

                foreach (JToken hit in hits)
                {
                    var fields = hit["_source"];

                    dynamic data = JsonConvert.DeserializeObject(fields.ToString());

                    returnValue.Add(data.id.ToString().ToLower(), data.title.ToString());
                }
                return returnValue;
            }
        }
        
    }
}
