﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Repositories;
using NDexed.DataAccess.Response;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Opinions;
using NDexed.Elastic.Domain;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web;

namespace NDexed.Elastic.Repository
{
    public class ElasticOpinionRepository : ISearchableRepository<OpinionSearchQuery, OpinionSearchResponse>,
                                            IReadRepository<OpinionDetailQuery, OpinionInfo>
    {
        private readonly IRepository<Guid, TermInfo> m_TermRepo;

        public ElasticOpinionRepository(IRepository<Guid, TermInfo> termRepo)
        {
            Condition.Requires(termRepo).IsNotNull();

            m_TermRepo = termRepo;
        }

        public OpinionInfo Get(OpinionDetailQuery query)
        {
            var client = ContextFactory.InitializeClient(typeof(OpinionInfo));

            using (client)
            {
                //get all
                string resourcePath = string.Format("{0}/opinions/{1}/_source", ContextFactory.BASE_ADDRESS, query.Id);
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }

                var returnValue = JsonConvert.DeserializeObject<OpinionInfo>(responseContent);
                returnValue.SetAdditionalFields();

                returnValue.id = query.Id;

                return returnValue;
            }
        }
        public IEnumerable<OpinionSearchResponse> Search(OpinionSearchQuery criteria)
        {
            var returnValue = new List<OpinionSearchResponse>();

            foreach(var jurisdiction in criteria.Jurisdictions)
            {
                var response = new OpinionSearchResponse();
                response.Jurisdiction = jurisdiction;
                int totalDocuments = 0;
                response.Opinions = Search(criteria.SearchTerm, jurisdiction, criteria.PageNumber, criteria.PageSize, out totalDocuments);
                response.TotalDocuments = totalDocuments;

                returnValue.Add(response);
            }
            return returnValue;
        }



        #region Private Methods

        private IList<OpinionInfo> Search(string term, string jurisdiction, int pageNumber, int pageSize, out int totalResultsCount)
        {
            var returnValue = new List<OpinionInfo>();

            var client = ContextFactory.InitializeClient(typeof(OpinionInfo));

            using (client)
            {
                //get all
                var args = new object[]
                {
                    ContextFactory.BASE_ADDRESS,
                    pageNumber * pageSize,
                    pageSize
                };

                var request = new JObject
                {
                    {
                        "query", new JObject
                        {
                            {
                                "bool", new JObject
                                {
                                    {
                                        "must", new JArray
                                        {
                                            new JObject
                                            {
                                                {
                                                    "match",
                                                    new JObject
                                                    {
                                                        {"html", SearchTermUtilities.GetDistinctTerms(term, m_TermRepo)}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                };

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                var content = new ObjectContent(typeof(JObject), request, formatter, header);

                string resourcePath = string.Format("{0}/opinions/_search?from={1}&size={2}", args);
                HttpResponseMessage response = client.PostAsync(resourcePath, content).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }

                var responseObjects = (JObject)JsonConvert.DeserializeObject(responseContent);
                var hits = responseObjects["hits"];

                totalResultsCount = int.Parse(hits["total"].ToString());

                foreach (var hit in hits["hits"])
                {
                    var source = hit["_source"];
                    var opinion = JsonConvert.DeserializeObject<OpinionInfo>(source.ToString());

                    opinion.id = hit["_id"].ToString();

                    opinion.SetAdditionalFields();

                    opinion.html = string.Empty;

                    returnValue.Add(opinion);
                }

                return returnValue;
            }
        }

        #endregion

        #region Not Implemented Methods


        public OpinionDetailQuery Add(OpinionInfo item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OpinionInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Remove(OpinionInfo item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OpinionInfo> Search(OpinionInfo criteria)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
