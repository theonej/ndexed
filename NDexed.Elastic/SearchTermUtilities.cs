﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Elastic
{
    public static class SearchTermUtilities
    {
        const int TERMS_TO_TAKE = 200;

        public static string GetDistinctTerms(string terms, IRepository<Guid, TermInfo> termRepo)
        {
            //order the words by length, then take top 100 to hopefully contain most relative terms
            var orderedTerms = GetDistinctTermsArray(terms, termRepo);

            var distinctTerm = string.Join(" ", orderedTerms.Take(TERMS_TO_TAKE));


            return distinctTerm;
        }

        public static string[] GetDistinctTermsArray(string terms, IRepository<Guid, TermInfo> termRepo)
        {
            if (termRepo == null)
            {
                throw new NullReferenceException("Term repo cannot be null");
            }

            var configuredTerms = termRepo.GetAll().ToList();

            StringBuilder builder = new StringBuilder();
            builder.Append(terms);

            foreach (var term in configuredTerms)
            {
                if (!term.Include)
                {
                    //we pad the term with spaces on each side so that we don't accidentaly remove parts of words
                    builder = builder.Replace(string.Format(" {0} ", term), " ");
                }
            }

            builder = builder.Replace("\n", " ");
            builder = builder.Replace("\\n", " ");
            builder = builder.Replace("\t", " ");
            builder = builder.Replace("\\t", " ");
            builder = builder.Replace("\r", " ");
            builder = builder.Replace("\\r", " ");
            builder = builder.Replace(@"\", " ");
            builder = builder.Replace("\\", " ");
            builder = builder.Replace("_", " ");
            builder = builder.Replace("...", " ");

            //order the words by length, then take top 100 to hopefully contain most relative terms
            var orderedTerms = builder.ToString().Split(' ').Distinct().OrderByDescending(word => word.Length).Take(TERMS_TO_TAKE).ToList();

            //add back in any terms that have been specified as important to include
            orderedTerms.Reverse();
            var includeTerms = configuredTerms.Where(item => item.Include).ToList();
            for (var termIndex = 0; termIndex < includeTerms.Count; termIndex++)
            {
                var term = includeTerms[termIndex];

                if (term.Include && terms.IndexOf(term.Value) > -1)
                {
                    orderedTerms.Add(term.Value);
                }
            }

            orderedTerms.Reverse();

            return orderedTerms.Take(TERMS_TO_TAKE).ToArray();
        }

    }
}
