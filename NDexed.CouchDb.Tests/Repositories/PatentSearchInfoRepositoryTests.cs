﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.CouchDb.Repositories;
using NDexed.Domain.Models.Search;
using NDexed.Domain.Models.Patents;
using System.Linq;
using System.Collections.Generic;

namespace NDexed.CouchDb.Tests.Repositories
{
    [TestClass]
    public class PatentSearchInfoRepositoryTests
    {
        [TestMethod]
        public void CreatePatentSearchThenGetThenSearch()
        {
            var patentSearchInfo = new PatentSearchInfo();
            patentSearchInfo.PatentSearchCriteria = new PatentInfo();
            patentSearchInfo.PatentSearchCriteria.Abstract = "this is a static patent search used for testing.  Do not remove.";
            patentSearchInfo.PatentSearchId = Guid.Empty;
            patentSearchInfo.UserId = Guid.NewGuid();
            patentSearchInfo.DocketNumber = "JH1976";

            var repo = new PatentSearchInfoRepository();
            var recordId = repo.Add(patentSearchInfo);

            Assert.AreEqual(recordId, patentSearchInfo.PatentSearchId);

            var foundRecord = repo.Get(recordId);
            Assert.AreEqual(foundRecord.PatentSearchId, patentSearchInfo.PatentSearchId);
            Assert.AreEqual(foundRecord.PatentSearchCriteria.Abstract, patentSearchInfo.PatentSearchCriteria.Abstract);
            Assert.AreEqual(foundRecord.UserId, patentSearchInfo.UserId);
            Assert.AreEqual(foundRecord.DocketNumber, patentSearchInfo.DocketNumber);

            var searchCriteria = patentSearchInfo.UserId;
            var patentSearchList = repo.Search(searchCriteria).ToList();

            Assert.AreEqual(1, patentSearchList.Count);
            Assert.AreEqual(patentSearchList[0].PatentSearchId, recordId);
            Assert.AreEqual(patentSearchList[0].UserId, searchCriteria);
            Assert.AreEqual(patentSearchList[0].PatentSearchCriteria.Abstract, patentSearchInfo.PatentSearchCriteria.Abstract);
        }

        [TestMethod]
        public void CreateSearchThenAddSearchResultsThenGet()
        {
            var patentSearchInfo = new PatentSearchInfo();
            patentSearchInfo.PatentSearchCriteria = new PatentInfo();
            patentSearchInfo.PatentSearchCriteria.Abstract = "this is a static patent search used for testing.  Do not remove.";
            patentSearchInfo.PatentSearchId = Guid.Empty;
            patentSearchInfo.UserId = Guid.NewGuid();

            var repo = new PatentSearchInfoRepository();
            var recordId = repo.Add(patentSearchInfo);

            Assert.AreEqual(recordId, patentSearchInfo.PatentSearchId);

            var foundRecord = repo.Get(recordId);
            Assert.IsNotNull(foundRecord);

            foundRecord.PatentSearchResults = new List<PatentInfo>();
            foundRecord.PatentSearchResults.Add(new PatentInfo());
            foundRecord.PatentSearchResults[0].Abstract = "a test search result";

            foundRecord.PatentSearchResults[0].Claims = new List<ClaimInfo>();
            foundRecord.PatentSearchResults[0].Claims.Add(new ClaimInfo());
            foundRecord.PatentSearchResults[0].Claims[0].Claim = "A test claim";

            foundRecord.PatentSearchResults[0].Citations = new List<CitationInfo>();
            foundRecord.PatentSearchResults[0].Citations.Add(new CitationInfo());
            foundRecord.PatentSearchResults[0].Citations[0].Name = "A test citation";

            foundRecord.PatentSearchResults[0].Cpc = new List<CpcInfo>();
            foundRecord.PatentSearchResults[0].Cpc.Add(new CpcInfo());
            foundRecord.PatentSearchResults[0].Cpc[0].SubGroup = "A test subgroup";

            repo.Add(foundRecord);

            var updatedRecord = repo.Get(recordId);
            Assert.IsNotNull(updatedRecord);

            Assert.AreEqual(foundRecord.PatentSearchResults.Count, updatedRecord.PatentSearchResults.Count);
            Assert.AreEqual(foundRecord.PatentSearchResults[0].Abstract, updatedRecord.PatentSearchResults[0].Abstract);

            Assert.AreEqual(foundRecord.PatentSearchResults[0].Claims.Count, updatedRecord.PatentSearchResults[0].Claims.Count);
            Assert.AreEqual(foundRecord.PatentSearchResults[0].Claims[0].Claim, updatedRecord.PatentSearchResults[0].Claims[0].Claim);

            Assert.AreEqual(foundRecord.PatentSearchResults[0].Citations.Count, updatedRecord.PatentSearchResults[0].Citations.Count);
            Assert.AreEqual(foundRecord.PatentSearchResults[0].Citations[0].Name, updatedRecord.PatentSearchResults[0].Citations[0].Name);

            Assert.AreEqual(foundRecord.PatentSearchResults[0].Cpc.Count, updatedRecord.PatentSearchResults[0].Cpc.Count);
            Assert.AreEqual(foundRecord.PatentSearchResults[0].Cpc[0].SubGroup, updatedRecord.PatentSearchResults[0].Cpc[0].SubGroup);
        }
    }
}
