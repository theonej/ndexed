﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.CouchDb.Repositories;
using NDexed.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.CouchDb.Tests.Repositories
{
    [TestClass]
    public class CouchUserRepositoryTests
    {
        [TestMethod]
        public void GetUserByCaseInsensitiveSearch()
        {
            var searchCriteria = new UserInfo()
            {
                EmailAddress = "jack.r.hEnry@GmaiL.com"
            };

            var userRepo = new CouchUserRepository();
            var user = userRepo.Search(searchCriteria);
            Assert.IsNotNull(user);
        }
    }
}
