﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.CouchDb.Repositories;
using NDexed.Domain.Models.Data;
using System.Collections.Generic;

namespace NDexed.CouchDb.Tests.Repositories
{
    [TestClass]
    public class CouchTermsRepositoryTests
    {
        [TestMethod]
        public void AddTermsThenGetAllTerms()
        {
            var excludeValues = new string[]
               {
                "a",
                "and",
                "I",
                "would",
                "like",
                "the",
                "him",
                "her",
                "no",
                "yes",
                "it",
                "at",
                "by",
                "from",
                "for",
                "where",
                "when",
                "as",
                "in",
                "out",
                "to",
                "it",
                "it's",
                "its",
                "that",
                "was",
                "is",
                "have",
                "has",
                "whos",
                "are",
                "enough",
                "which",
                "into",
                "question",
                "after",
                "some",
                "cases",
                "not",
                "against",
                "with",
                "raised",
                "court",
                "granted",
                "denied",
                "petitioners'",
                "petitioners",
                "petitioner",
                "respondents'",
                "respondents",
                "respondent",
                "litigant",
                "ordinarily",
                "claimants",
                "claimants'",
                "claimant",
                "upheld",
                "began",
                "begin",
                "entitled"
               };

            var repo = new CouchTermsRepository();

            foreach(var termValue in excludeValues)
            {
                var term = new TermInfo();
                term.Id = Guid.NewGuid();
                term.Value = termValue;

                repo.Add(term);
            }

            var includeValues = new string[]
            {
                "HIPAA",
                "Secretary",
                "HHS",
                "Disclosures",
                "Health",
                "Care",
                "Privacy",
                "Information",
                "Portability",
                "Department",
                "Guidance"
            };

            foreach (var termValue in includeValues)
            {
                var term = new TermInfo();
                term.Id = Guid.NewGuid();
                term.Value = termValue;
                term.Include = true;

                repo.Add(term);
            }
        }
    }
}
