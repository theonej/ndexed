﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Cases;
using System.Collections.Generic;
using NDexed.CouchDb.Repositories;
using System.Linq;
using NDexed.Messaging.Commands.CaseUsers;
using NDexed.Messaging.Handlers;
using NDexed.Domain.Models.User;

namespace NDexed.CouchDb.Tests.Repositories
{
    [TestClass]
    public class CouchCaseRepositoryTests
    {
        [TestMethod]
        public void CreateCaseThenAddUsersThenVerifyUsersthenRemoveUsers()
        {
            var @case = new CaseInfo()
            {
                Id = Guid.Parse("00000000-0000-0000-0000-000000000001"),
                UserIds = new List<Guid>()
            };

            var caseRepo = new CouchCaseRepository();
            caseRepo.Add(@case);

            var users = new List<UserInfo>()
            {
                new UserInfo() {
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000002")
                },
                new UserInfo() {
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000003")
                }
            };

            var userRepo = new CouchUserRepository();
            foreach(var user in users)
            {
                userRepo.Add(user);
            }

            var addCommand = new AddCaseUsersCommand();

            addCommand.CaseId = @case.Id;
            addCommand.UserIds.Add(users[0].Id);
            addCommand.UserIds.Add(users[1].Id);

            var handler = new CaseUsersCommandHandler(caseRepo, userRepo);

            handler.Handle(addCommand);

            var result = caseRepo.Get(@case.Id);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.UserIds.Count);

            var userId1 = result.UserIds.First(id=>id == addCommand.UserIds[0]);
            var user1 = userRepo.Get(users[0]);
            Assert.IsNotNull(user1);
            Assert.IsTrue(user1.CaseIds.Contains(@case.Id));

            var userId2 = result.UserIds.First(id => id == addCommand.UserIds[1]);
            var user2 = userRepo.Get(users[1]);
            Assert.IsNotNull(user2);
            Assert.IsTrue(user2.CaseIds.Contains(@case.Id));

            var deleteCommand = new DeleteCaseUsersCommand();
            deleteCommand.CaseId = @case.Id;
            deleteCommand.UserIds = addCommand.UserIds;

            handler.Handle(deleteCommand);

            result = caseRepo.Get(@case.Id);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.UserIds.Count);

            user1 = userRepo.Get(users[0]);
            Assert.IsNotNull(user1);
            Assert.IsFalse(user1.CaseIds.Contains(@case.Id));

            user2 = userRepo.Get(users[1]);
            Assert.IsNotNull(user2);
            Assert.IsFalse(user2.CaseIds.Contains(@case.Id));

            caseRepo.Remove(@case);
            foreach (var user in users)
            {
                userRepo.Remove(user);
            }
        }
    }
}
