﻿
using System;

namespace NDexed.DataAccess.Query
{
    public class DocumentSearchQuery
    {
        public string Term { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Guid CaseId { get; set; }
        public Guid OrganizationId { get; set; }

        public string[] Tags { get; set; }
    }
}
