﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.DataAccess.Query
{
    public class RegulationSearchQuery
    {
        public string SearchTerm { get; set; }
        public string[] UsCodes { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
