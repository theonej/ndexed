﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.DataAccess.Query
{
    public class OpinionSearchQuery
    {
        public string SearchTerm { get; set; }
        public string[] Jurisdictions { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
