﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.DataAccess.Query
{
    public class OpinionDetailQuery
    {
        public string Id { get; set; }
        public string Jurisdiction { get; set; }
    }
}
