﻿using System;

namespace NDexed.DataAccess.Query
{
    public class IndexedDocumentQuery
    {
        public bool Indexed { get; set; }
        public Guid CaseId { get; set; }
    }
}
