﻿using System.Collections.Generic;
using NDexed.Domain.Models.Documents;

namespace NDexed.DataAccess.Response
{
    public class DocumentSearchResponse
    {
        public int TotalDocuments { get; set; }
        public List<DocumentInfo> Results { get; set; }
    }
}
