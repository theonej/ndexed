﻿using NDexed.Domain.Models.Regulations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.DataAccess.Response
{
    public class RegulationSearchResponse
    {
        public int TotalDocuments { get; set; }
        public string UsCodeName { get; set; }
        public IList<RegulationInfo> Regulations { get; set; }
    }
}
