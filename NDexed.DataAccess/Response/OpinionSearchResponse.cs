﻿using NDexed.Domain.Models.Opinions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.DataAccess.Response
{
    public class OpinionSearchResponse
    {
        public int TotalDocuments { get; set; }
        public string Jurisdiction { get; set; }
        public IList<OpinionInfo> Opinions { get; set; }
    }
}
