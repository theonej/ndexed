﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Messaging.Commands.Applications;
using NDexed.Messaging.Handlers.Applications;
using NDexed.AWS.Messagers;
using NDexed.CouchDb.Repositories;

namespace Waitless.Messaging.Tests.Applications
{
    [TestClass]
    public class ApplicationCommandHandlerTests
    {
        [TestMethod]
        public void ImportApplicationThenDelete()
        {
            var applicationRepo = new CouchApplicationRepository();

            var command = new ImportApplicationCommand();
            command.Abstract = "An energy storage device includes a first conductor having a first surface and a second surface. The energy storage device also includes a second conductor and a separator assembly that encloses the first conductor and that is disposed between the first and second conductors. The separator assembly also includes a first portion that covers the first surface and a second portion that covers the second surface. The first and second portions are attached to one another  and at least one of the first and second portions includes a first sheet and a second sheet that are attached to one another. The first sheet includes a first material  and the second sheet includes a second material that is different from the first material.";
            command.ApplicationName = "Test Application From Test Command";
            command.EmailAddress = "J.Henry@n-dexed.com";
            command.Id = Guid.Empty;

            var handler = new ImportApplicationHandler(new AwsEmailMessenger());

            handler.Handle(command);

            var application = applicationRepo.Get(command.Id);
            Assert.IsNotNull(application);
        }
    }
}
