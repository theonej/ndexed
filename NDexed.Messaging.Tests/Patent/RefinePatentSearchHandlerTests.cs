﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Messaging.Commands;
using NDexed.Messaging.Handlers.Patent;
using NDexed.CouchDb.Repositories;
using NDexed.Elastic.Repository;
using NDexed.Messaging.Messages;
using NDexed.AWS.Messagers;

namespace Waitless.Messaging.Tests.Patent
{
    [TestClass]
    public class RefinePatentSearchHandlerTests
    {
        [TestMethod]
        public void RefineExistingSearch()
        {
            var command = new RefinePatentSearchCommand();
            command.PatentSearchInfoId = Guid.Parse("214ae888-7ac6-4f5a-b7dc-1af133f1b9ef");
            
            var searchHandler = new PatentSearchHandler(new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticClaimsSearchRepository(),
                                                  new CouchSearchHistoryRepository(),
                                                  new ElasticCpcRepository(),
                                                  new PatentSearchInfoRepository(),
                                                  new CouchUserRepository());

            var handler = new RefinePatentSearchHandler(new PatentSearchInfoRepository(), 
                                                        searchHandler, 
                                                        new CouchUserRepository(), 
                                                        new AwsEmailMessenger());
            var results = handler.Handle(command);

            Assert.IsNotNull(results);
        }
    }
}
