﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Documents;
using System.Collections.Generic;
using NDexed.Search.Analyzers;

namespace Waitless.Messaging.Tests.Patent
{
    [TestClass]
    public class DocumentSimilarityCommandTests
    {
        [TestMethod]
        public void GetSimilarityForMultipleDocuments()
        {
            var command = new DocumentSimilarityRequest();

            command.DocumentContents = new List<string>();
            command.DocumentContents.Add("Described herein are cannabidiol prodrugs, methods of making cannabidiol prodrugs, formulations comprising cannabidiol prodrugs and methods of using cannabidiols. One embodiment described herein relates to the transdermal or topical administration of a cannabidiol prodrug for treating and preventing diseases and/or disorders.");
            command.DocumentContents.Add("The method of claim 3 wherein the cannabidiol prodrug is administered transdermally. ");
            command.DocumentContents.Add("The method of claim 3 wherein the cannabidiol prodrug is administered topically.");
            command.DocumentContents.Add("The method of claim 1, wherein the cannabidiol compound is cannabidiol (CBD). ");
            command.DocumentContents.Add("The method of claim 1, wherein the cannabidiol compound is the dimethyl heptyl homolog of cannabidiol (CBD-DMH).");

            var results = DocumentSimilarityAnalyzer.GetDocumentsSimilarity(command);
            Assert.AreEqual(1.0000000000000, results[0][0]);
            Assert.AreEqual(0.40638393163681f, results[0][1]);
            Assert.AreEqual(0.40638393699281f, results[0][2]);
            Assert.AreEqual(0.252301582553377f, results[0][3]);
            Assert.AreEqual(0.188104813941385f, results[0][4]);

            Assert.AreEqual(0.40638393699281f, results[1][0]);
            Assert.AreEqual(1.0f, results[1][1]);
            Assert.AreEqual(0.7948433909215f, results[1][2]);
            Assert.AreEqual(0.289322683482009f, results[1][3]);
            Assert.AreEqual(0.215706096627007f, results[1][4]);
            
        }
    }
}
