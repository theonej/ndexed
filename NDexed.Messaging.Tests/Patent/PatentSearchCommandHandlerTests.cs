﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Search;
using NDexed.Messaging.Commands;
using NDexed.Messaging.Handlers.Patent;
using NDexed.Elastic.Repository;
using NDexed.CouchDb.Repositories;
using System.Linq;
using System.IO;

namespace Waitless.Messaging.Tests.Patent
{
    [TestClass]
    public class PatentSearchCommandHandlerTests
    {
        [TestMethod]
        public void SearchByPatentInfo()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 10;
            searchInfo.Endpoint = "claims";
            searchInfo.Query = "A method according to claim 14, wherein the foams are microcellular foams having average cell sizes of up to 10 .mu.m and a cell density equal to or greater than 10.sup.9 cells/g.";
            searchInfo.Skip = 0;
            searchInfo.PatentSearchId = Guid.NewGuid();
            searchInfo.AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o";
            searchInfo.ProviderId = "000375274410958331611:ealp1cwjuyo";

            var command = new PatentSearchCommand();
            command.Criteria = searchInfo;
            command.Id = Guid.NewGuid();

            var handler = new PatentSearchHandler(new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticClaimsSearchRepository(),
                                                  new CouchSearchHistoryRepository(),
                                                  new ElasticCpcRepository(),
                                                  new PatentSearchInfoRepository(),
                                                  new CouchUserRepository());

            var searchResults = handler.Handle(command);
            Assert.IsNotNull(searchResults);
            Assert.AreNotEqual(0, searchResults.PatentResults.Count);
            
        }

        [TestMethod]
        public void SearchByPatentInfoThenResearchForDifferentResults()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 10;
            searchInfo.Endpoint = "claims";
            searchInfo.Query = "A method according to claim 14, wherein the foams are microcellular foams having average cell sizes of up to 10 .mu.m and a cell density equal to or greater than 10.sup.9 cells/g.";
            searchInfo.Skip = 0;
            searchInfo.PatentSearchId = Guid.NewGuid();
            searchInfo.AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o";
            searchInfo.ProviderId = "000375274410958331611:ealp1cwjuyo";

            var command = new PatentSearchCommand();
            command.Criteria = searchInfo;
            command.Id = Guid.NewGuid();

            var handler = new PatentSearchHandler(new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticClaimsSearchRepository(),
                                                  new CouchSearchHistoryRepository(),
                                                  new ElasticCpcRepository(),
                                                  new PatentSearchInfoRepository(),
                                                  new CouchUserRepository());

            var searchResults = handler.Handle(command);
            Assert.IsNotNull(searchResults);
            Assert.AreNotEqual(0, searchResults.PatentResults.Count);

            var firstBatch = searchResults.PatentResults
                                          .Select(item => item.PatentNumber)
                                          .OrderByDescending(item=>item);

            command.Criteria.Skip = 10;
            searchResults = handler.Handle(command);
            Assert.IsNotNull(searchResults);
            Assert.AreNotEqual(0, searchResults.PatentResults.Count);

            var secondBatch = searchResults.PatentResults
                                           .Select(item => item.PatentNumber)
                                           .Where(number=>!firstBatch.Contains(number))
                                           .OrderByDescending(item => item);

            Assert.AreNotEqual(firstBatch, secondBatch);

        }

        [TestMethod]
        public void CreateSearchThenSave()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 10;
            searchInfo.Endpoint = "claims";
            searchInfo.Query = "A method according to claim 14, wherein the foams are microcellular foams having average cell sizes of up to 10 .mu.m and a cell density equal to or greater than 10.sup.9 cells/g.";
            searchInfo.Skip = 0;
            searchInfo.PatentSearchId = Guid.NewGuid();
            searchInfo.AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o";
            searchInfo.ProviderId = "000375274410958331611:ealp1cwjuyo";
            
            var command = new PatentSearchCommand();
            command.Criteria = searchInfo;
            command.Id = Guid.NewGuid();

            var handler = new PatentSearchHandler(new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticPatentRepository(),
                                                  new ElasticPatentApplicationRepository(),
                                                  new ElasticClaimsSearchRepository(),
                                                  new CouchSearchHistoryRepository(),
                                                  new ElasticCpcRepository(),
                                                  new PatentSearchInfoRepository(),
                                                  new CouchUserRepository());

            var searchResults = handler.Handle(command);
            Assert.IsNotNull(searchResults);
            Assert.AreNotEqual(0, searchResults.PatentResults.Count);

            var saveCommand = new SaveSearchInfoCommand();
            saveCommand.SearchInfo = new PatentSearchInfo();
            saveCommand.SearchInfo.PatentSearchId = Guid.Empty;
            saveCommand.SearchInfo.PatentSearchResults = searchResults.PatentResults;
            saveCommand.SearchInfo.UserId = Guid.Parse("f335d79f-bc69-42f3-8f56-a03dad5d2339");

            handler.Handle(saveCommand);
        }

        [TestMethod]
        public void SearchByAbandonedAbstracts()
        {
            //read files, search by each line, write out results
            var abandonedPatentsPath = @"C:\data\NDexed\ABANDONED.txt";
            var abandonedOutputPath = @"C:\data\NDexed\ABANDONED_RESULTS.txt";

            var lines = File.ReadAllLines(abandonedPatentsPath);

            foreach (var line in lines)
            {
                try
                {
                    var outputStream = File.AppendText(abandonedOutputPath);
                    using (outputStream)
                    {
                        var searchInfo = new SearchInfo();
                        searchInfo.Count = 10;
                        searchInfo.Query = line.Replace("/", string.Empty);
                        searchInfo.Skip = 0;
                        searchInfo.PatentSearchId = Guid.NewGuid();
                        searchInfo.AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o";
                        searchInfo.ProviderId = "000375274410958331611:ealp1cwjuyo";
                        searchInfo.Type = SearchInfo.SearchType.All;

                        var command = new PatentSearchCommand();
                        command.Criteria = searchInfo;
                        command.Id = Guid.NewGuid();

                        var handler = new PatentSearchHandler(new ElasticPatentRepository(),
                                                              new ElasticPatentApplicationRepository(),
                                                              new ElasticPatentRepository(),
                                                              new ElasticPatentApplicationRepository(),
                                                              new ElasticClaimsSearchRepository(),
                                                              new CouchSearchHistoryRepository(),
                                                              new ElasticCpcRepository(),
                                                              new PatentSearchInfoRepository(),
                                                              new CouchUserRepository());

                        var searchResults = handler.Handle(command);

                        var topScores = searchResults.PatentResults
                                                     .OrderByDescending(result=>result.DescriptionRank)
                                                     .Take(5)
                                                     .Select(result => result.DescriptionRank)
                                                     .ToList();

                        var averageDescriptionScore = searchResults.PatentResults
                                                                .Select(result => result.DescriptionRank)
                                                                .Average();

                        var topClaimScore = searchResults.PatentResults
                                                                .SelectMany(result => result.Claims)
                                                                .OrderByDescending(claim => claim.ClaimRank)
                                                                .Select(claim => claim.ClaimRank)
                                                                .FirstOrDefault();

                        var averageClaimScore = searchResults.PatentResults
                                                                .SelectMany(result => result.Claims)
                                                                .Select(claim => claim.ClaimRank)
                                                                .Average();


                        var args = new object[] {
                        topScores[0],
                        topScores[1],
                        topScores[2],
                        topScores[3],
                        topScores[4],
                        averageDescriptionScore,
                        topClaimScore,
                        averageClaimScore
                    };
                        var outputLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", args);

                        outputStream.WriteLine(outputLine);
                    }
                }
                catch { }
            }

        }


        [TestMethod]
        public void SearchByGrantedPatents()
        {
            //read files, search by each line, write out results
            var grantedPatentsPath = @"C:\data\NDexed\PENDING.txt";
            var grantedOutputPath = @"C:\data\NDexed\PENDING_RESULTS.txt";

            var lines = File.ReadAllLines(grantedPatentsPath);

            foreach (var line in lines)
            {
                try
                {
                    var outputStream = File.AppendText(grantedOutputPath);
                    using (outputStream)
                    {
                        var searchInfo = new SearchInfo();
                        searchInfo.Count = 10;
                        searchInfo.Query = line.Replace("/", string.Empty);
                        searchInfo.Skip = 0;
                        searchInfo.PatentSearchId = Guid.NewGuid();
                        searchInfo.AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o";
                        searchInfo.ProviderId = "000375274410958331611:ealp1cwjuyo";
                        searchInfo.Type = SearchInfo.SearchType.All;

                        var command = new PatentSearchCommand();
                        command.Criteria = searchInfo;
                        command.Id = Guid.NewGuid();

                        var handler = new PatentSearchHandler(new ElasticPatentRepository(),
                                                              new ElasticPatentApplicationRepository(),
                                                              new ElasticPatentRepository(),
                                                              new ElasticPatentApplicationRepository(),
                                                              new ElasticClaimsSearchRepository(),
                                                              new CouchSearchHistoryRepository(),
                                                              new ElasticCpcRepository(),
                                                              new PatentSearchInfoRepository(),
                                                              new CouchUserRepository());

                        var searchResults = handler.Handle(command);

                        var topScores = searchResults.PatentResults
                                                     .OrderByDescending(result => result.DescriptionRank)
                                                     .Take(5)
                                                     .Select(result => result.DescriptionRank)
                                                     .ToList();

                        var averageDescriptionScore = searchResults.PatentResults
                                                                .Select(result => result.DescriptionRank)
                                                                .Average();

                        var topClaimScore = searchResults.PatentResults
                                                                .SelectMany(result => result.Claims)
                                                                .OrderByDescending(claim => claim.ClaimRank)
                                                                .Select(claim => claim.ClaimRank)
                                                                .FirstOrDefault();

                        var averageClaimScore = searchResults.PatentResults
                                                                .SelectMany(result => result.Claims)
                                                                .Select(claim => claim.ClaimRank)
                                                                .Average();


                        var args = new object[] {
                        topScores[0],
                        topScores[1],
                        topScores[2],
                        topScores[3],
                        topScores[4],
                        averageDescriptionScore,
                        topClaimScore,
                        averageClaimScore
                    };
                        var outputLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", args);

                        outputStream.WriteLine(outputLine);
                    }
                }
                catch(Exception ex){ }
            }

        }
    }
}
