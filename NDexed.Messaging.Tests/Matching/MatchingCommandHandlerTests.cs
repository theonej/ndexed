﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Messaging.Commands.Matching;
using NDexed.Messaging.Handlers.Matching;
using NDexed.Elastic.Repository;
using NDexed.CouchDb.Repositories;

namespace Waitless.Messaging.Tests.Matching
{
    [TestClass]
    public class MatchingCommandHandlerTests
    {
        [TestMethod]
        public void FindMatchingPatents()
        {
            var searchRepo = new ElasticMatchRepository();
            var termRepo = new CouchTermsRepository();

            var handler = new MatchCommandHandler(searchRepo, termRepo);

            var command = new FindMatchesCommand();
            command.MatchType = "patents";
            command.PageNumber = 0;
            command.PageSize = 20;
            command.SearchTerms = new string[] 
            {
                "conveying",
                "segment",
                "by",
                "a",
                "pneumatic",
                "pickup",
                "A",
                "sorting",
                "machine",
                "according",
                "to",
                "claim"
            };

            var results = handler.Handle(command);
            Assert.AreNotEqual(0, results.Count);

        }

        [TestMethod]
        public void FindMatchingPatentsAndGetClassifications()
        {
            var searchRepo = new ElasticMatchRepository();
            var termRepo = new CouchTermsRepository();

            var handler = new MatchCommandHandler(searchRepo, termRepo);

            var command = new FindMatchesCommand();
            command.MatchType = "patents";
            command.PageNumber = 0;
            command.PageSize = 20;
            command.SearchTerms = new string[]
            {
                "conveying",
                "segment",
                "by",
                "a",
                "pneumatic",
                "pickup",
                "A",
                "sorting",
                "machine",
                "according",
                "to",
                "claim"
            };

            var results = handler.Handle(command);
            Assert.AreNotEqual(0, results.Count);

            foreach(var match in results)
            {
                var classifyCommand = new GetMatchClassificationCommand();
                classifyCommand.Match = match;

                var classifications = handler.Handle(classifyCommand);
                Assert.AreNotEqual(0, classifications.Count);
            }
        }

        [TestMethod]
        public void ExtractMatchTermsAndClassesTest()
        {
            var searchRepo = new ElasticMatchRepository();
            var termRepo = new CouchTermsRepository();

            var handler = new MatchCommandHandler(searchRepo, termRepo);

            var command = new ExtractMatchTermsAndClassesCommand();
            command.TotalRecordsToGet = 5000;
            command.ClassOutputFile = @"C:\code\personal\data\classification\test\classes.txt";
            command.SubClassOutputFile = @"C:\code\personal\data\classification\test\subclasses.txt";
            command.TermOutputFile = @"C:\code\personal\data\classification\test\terms.txt";

            handler.Handle(command);
        }
    }
}
