﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Messaging.Handlers;
using NDexed.Elastic.Repository;
using NDexed.Messaging.Commands.Document;
using NDexed.CouchDb.Repositories;
using NDexed.AWS.Repository;
using NDexed.AWS.Messagers;

namespace Waitless.Messaging.Tests
{
    /// <summary>
    /// Summary description for DocumentCommandHandlerTests
    /// </summary>
    [TestClass]
    public class DocumentCommandHandlerTests
    {
        [TestMethod]
        public void CreateZipFromKnownDocuments()
        {
            var termRepo = new CouchTermsRepository();
            var docRepo = new ElasticDocumentRepository(termRepo);
            var opinionRepo = new ElasticOpinionRepository(termRepo);
            var emailRepo = new S3Repository();
            var userRepo = new CouchUserRepository();
            var messenger = new AwsEmailMessenger();

            var handler = new DocumentCommandHandler(docRepo, opinionRepo, emailRepo, userRepo, messenger);

            var command = new CreateDocumentZipCommand();
            command.Id = Guid.NewGuid();
            command.OutputFileName = @"c:\files\documents.zip";
            command.DocumentIds = new List<Guid>();
            command.DocumentIds.Add(Guid.Parse("bad8dc2b-abcf-4aea-8b81-098b444b9171"));
            command.DocumentIds.Add(Guid.Parse("f9d4d7c2-8d7a-4d68-9730-7543019d28fb"));
            command.DocumentIds.Add(Guid.Parse("0f608ce2-77c6-4e7d-951c-ce1926afc936"));

            handler.Handle(command);
        }
    }
}
