﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Matching;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;

namespace NDexed.Search.Repositories
{
    public class GoogleRepository : ISearchableRepository<SearchInfo, PatentInfo>
    {
        private static string BASE_ADDRESS = ConfigurationManager.AppSettings["GoogleSearchBaseAddress"];

        public IEnumerable<PatentInfo> Search(SearchInfo criteria)
        {
            var returnValue = new List<PatentInfo>();

            var client = new HttpClient();
            using (client)
            {
                var args = new object[]
                {
                    BASE_ADDRESS,
                    criteria.AccountId,
                    criteria.ProviderId,
                    criteria.Query,
                    criteria.Count,
                    criteria.Skip
                };

                var url = string.Format("{0}?key={1}&cx={2}&q={3}&num={4}&startIndex={5}", args);

                var response = client.GetAsync(url).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    return returnValue;//TODO: Once we increase search limits, revert this
//                    throw new HttpRequestException(string.Format("{0} -- {1}", response.ReasonPhrase, content));
                }

                JObject result = (JObject)JsonConvert.DeserializeObject(content);
                JArray items = (JArray)result["items"];
                if (items != null)
                {
                    returnValue = items.Select(item => GetPatentFromItem(item))
                                       .Where(item=>item != null)               
                                       .ToList();
                }
                return returnValue;
            }
        }

        #region Private Methods

        private string GetMatchContents(JToken item)
        {
            var contents = item["snippet"].ToString();

            if(item["pagemap"] != null && item["pagemap"]["metatags"] != null)
            {
                var metatags = item["pagemap"]["metatags"];
                var description = metatags.First()["dc.description"];
                contents = description == null?contents:description.ToString();
            }

            return contents;
        }
        private PatentInfo GetPatentFromItem(dynamic item)
        {
            PatentInfo returnValue = null;
            if (item.pagemap != null)
            {
                returnValue = new PatentInfo();
                var metatag = item.pagemap.metatags[0];
                returnValue.PatentNumber = metatag.citation_patent_number;
                returnValue.ApplicationNumber = metatag.citation_patent_application_number;
                returnValue.Abstract = item["snippet"];
                returnValue.Title = metatag["dc.title"];
                returnValue.Link = metatag.citation_pdf_url != null ? metatag.citation_pdf_url : item.link;
                returnValue.Highlight = item["htmlSnippet"];
                returnValue.Source = PatentInfo.Sources.External;

                var contents = item["snippet"].ToString();

                if (item["pagemap"] != null && item["pagemap"]["metatags"] != null)
                {
                    var description = metatag["dc.description"];
                    contents = description == null ? contents : description.ToString();
                    returnValue.Description = contents;
                    returnValue.Date = metatag["dc.date"];
                }
            }
            return returnValue;
        }

        #endregion
    }
}
