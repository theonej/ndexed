﻿using HtmlAgilityPack;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Search.Repositories
{
    public class UsptoContentsRepository : ISearchableRepository<string, PatentInfo>
    {
        public IEnumerable<PatentInfo> Search(string criteria)
        {
            var url = string.Format("http://patft.uspto.gov/netacgi/nph-Parser?Sect2=PTO1&Sect2=HITOFF&p=1&u=/netahtml/PTO/search-bool.html&r=1&f=G&l=50&d=PALL&RefSrch=yes&Query=PN/{0}", criteria);

            var webParser = new HtmlWeb();
            var doc = webParser.Load(url);

            var abstractText = doc.DocumentNode
                                .Descendants("p")
                                .First()
                                .InnerHtml;

            var patentInfo = new PatentInfo();
            patentInfo.PatentNumber = criteria;
            patentInfo.Description = abstractText;

            return new List<PatentInfo>()
            {
                patentInfo
            };
        }
    }
}
