﻿using Google.Cloud.Vision.V1;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Matching;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Google.Repository
{
    public class VisionAPIRepository : ISearchableRepository<SearchInfo, MatchInfo>
    {
        private static string BASE_ADDRESS = ConfigurationManager.AppSettings["VisionAPIBaseAddress"];

        public IEnumerable<MatchInfo> Search(SearchInfo criteria)
        {
            var credsLocation = ConfigurationManager.AppSettings["VisionAPICredsLocation"];
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", credsLocation);

            var image = Image.FromFile(criteria.Query);
            var client = ImageAnnotatorClient.Create();

            var webContents = client.DetectWebInformation(image);

            var returnValue = new List<MatchInfo>();

            var httpClient = new HttpClient();
            using (httpClient)
            {
                var pageMatches = webContents.PagesWithMatchingImages.Select(item => new MatchInfo()
                {
                    MatchType = "Page",
                    Data = item.Url,
                    Rank = (decimal)item.Score,
                    Contents = GetImageContents(httpClient.GetStringAsync(item.Url).Result)
                }).ToList();

                returnValue.AddRange(pageMatches);
            }

            var fullImageMatches = webContents.FullMatchingImages.Select(item => new MatchInfo()
            {
                MatchType = "FullImage",
                Data = item.Url,
                Rank = (decimal)item.Score
            }).ToList();

            var partialImageMatches = webContents.PartialMatchingImages.Select(item => new MatchInfo()
            {
                MatchType = "PartialImage",
                Data = item.Url,
                Rank = (decimal)item.Score
            }).ToList();

            returnValue.AddRange(fullImageMatches);
            returnValue.AddRange(partialImageMatches);

            return returnValue;
        }

        #region Private Methods

        private string GetImageContents(string contents)
        {
            var imageStrings = new List<string>();

            var imageIndex = contents.IndexOf("<img");
            while(imageIndex > 0)
            {
                var endIndex = contents.IndexOf(">", imageIndex);
                if(endIndex > imageIndex)
                {
                    var imageString = contents.Substring(imageIndex, (endIndex - imageIndex) + 1);
                    if (imageString.IndexOf(".png") > 0 && imageString.IndexOf("thumbnail") > 0)
                    {
                        imageStrings.Add(imageString);
                    }
                    imageIndex = contents.IndexOf("<img", endIndex);
                }else
                {
                    break;
                }
            }
            return string.Join(" ", imageStrings);
        }
        #endregion
    }
}
