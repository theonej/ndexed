﻿using HtmlAgilityPack;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Matching;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;

namespace NDexed.Search.Repositories
{
    public class GoogleClaimsRepository : ISearchableRepository<SearchInfo, MatchInfo>
    {

        private static string BASE_ADDRESS = ConfigurationManager.AppSettings["GoogleSearchBaseAddress"];

        public IEnumerable<MatchInfo> Search(SearchInfo criteria)
        {
            var returnValue = new List<MatchInfo>();

            var client = new HttpClient();
            using (client)
            {
                var args = new object[]
                {
                    BASE_ADDRESS,
                    criteria.AccountId,
                    criteria.ProviderId,
                    criteria.Query,
                    criteria.Count,
                    criteria.Skip
                };

                var url = string.Format("{0}?key={1}&cx={2}&q={3}&num={4}&startIndex={5}", args);

                var response = client.GetAsync(url).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(string.Format("{0} -- {1}", response.ReasonPhrase, content));
                }

                JObject result = (JObject)JsonConvert.DeserializeObject(content);
                JArray items = (JArray)result["items"];
                if (items != null)
                {
                    returnValue = items.Select(item => new MatchInfo()
                    {
                        ContentEntries = GetClaimContents(item)
                    }).ToList();
                }
                return returnValue;
            }
        }

        #region Private Methods
        

        private List<string> GetClaimContents(JToken item)
        {
            var url = item["link"].ToString();

            var webParser = new HtmlWeb();
            var doc = webParser.Load(url);

            var claimsText = doc.DocumentNode
                                .Descendants("div")
                                .Where(div => div.HasClass("claim-text"))
                                .Select(div=>div.InnerText)
                                .ToList();

            return claimsText;
        }

        #endregion
    }
}
