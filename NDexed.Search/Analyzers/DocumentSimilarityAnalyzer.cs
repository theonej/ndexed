﻿using NDexed.Domain.Models.Documents;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Search.Analyzers
{
    public static class DocumentSimilarityAnalyzer
    {
        public static float[] GetDocumentSimilarity(DocumentSimilarityRequest command)
        {
            try
            {
                var url = string.Format(@"{0}/document/similarity", GetBaseAddress());

                var client = new HttpClient();
                using (client)
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                    new KeyValuePair<string, string>("document1", command.Document1Contents),
                    new KeyValuePair<string, string>("document2", command.Document2Contents)
                });
                    var response = client.PostAsync(url, formContent).Result;

                    var contents = response.Content.ReadAsStringAsync().Result;

                    var sections = contents.Replace("[", "").Replace("\n", "").Replace("]", "").Split(',');

                    var returnValue = sections.Select(item => float.Parse(item)).ToArray();

                    return returnValue;
                }
            }
            catch
            {
                return new float[] { 0, 500 };
            }
        }
        public static float[] GetSourceDocumentScores(List<string> sourceDocuments, List<string> targetDocs)
        {
            try
            {
                var url = string.Format(@"{0}/documents/score", GetBaseAddress());

                var client = new HttpClient();
                using (client)
                {
                    var documentContents = GetDocumentContents(sourceDocuments.Concat(targetDocs).ToList());
                    var documentValues = new KeyValuePair<string, string>("documents", documentContents);
                    var sourceDocumentCount = new KeyValuePair<string, string>("source_document_count", sourceDocuments.Count.ToString());

                    var formContent = new FormUrlEncodedContent(new KeyValuePair<string, string>[] { documentValues, sourceDocumentCount });
                    
                    var response = client.PostAsync(url, formContent).Result;

                    var contents = response.Content.ReadAsStringAsync().Result;

                    var sections = contents.Replace("[", "").Replace("\n", "").Replace("]", "").Split(',');

                    var returnValue = sections.Select(item => float.Parse(item)).ToArray();

                    return returnValue;
                }
            }
            catch(Exception ex)
            {
                return new float[] { 0, 500 };
            }
        }
        public static DocumentScoreInfo GetSourceDocumentsScores(List<string> sourceDocuments, List<string> targetDocs)
        {
            try
            {
                var url = string.Format(@"{0}/documents/scores", GetBaseAddress());

                var client = new HttpClient();
                using (client)
                {
                    var documentContents = GetDocumentContents(sourceDocuments.Concat(targetDocs).ToList());
    
                    var jsonContent = new JObject();
                    jsonContent.Add("documents", documentContents);
                    jsonContent.Add("source_document_count", sourceDocuments.Count.ToString());

                    var json = jsonContent.ToString();
                    var content = new StringContent(json);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var response = client.PostAsync(url, content).Result;
                    
                    var contents = response.Content.ReadAsStringAsync().Result;

                    var sections = contents.Replace("[", "").Replace("\n", "").Replace("]", "").Split(',');
                    float[] scores = sections.Select(item => float.Parse(item.Trim()))
                                                     .ToArray();

                    var returnValue = new DocumentScoreInfo();

                    returnValue.DocumentScores = scores.Take(targetDocs.Count).ToArray();
                    returnValue.Score = scores.Skip(targetDocs.Count).Take(1).First();

                    return returnValue;
                }
            }
            catch (Exception ex)
            {
                return new DocumentScoreInfo() {
                    Score=0,
                    DocumentScores = new float[0]
                };
            }
        }
        
        public static float[][] GetDocumentsSimilarity(DocumentSimilarityRequest command)
        {
            try
            {
                var url = string.Format(@"{0}/documents/similarity", GetBaseAddress());

                var client = new HttpClient();
                using (client)
                {
                    var documentContents = GetDocumentContents(command.DocumentContents);
                    var documentValues = new KeyValuePair<string, string>("documents", documentContents);

                    var formContent = new FormUrlEncodedContent(new KeyValuePair<string, string>[] { documentValues });
                    var response = client.PostAsync(url, formContent).Result;

                    var contents = response.Content.ReadAsStringAsync().Result;

                    var sections = contents.Replace("[", "").Replace("\n", "").Replace("]", "").Split(',');
                    float[] documentScores = sections.Select(item => float.Parse(item.Trim())).ToArray();

                    //group by count of documents
                    var vectorCount = command.DocumentContents.Count;
                    var vectors = new float[vectorCount][];
                    for(var index = 0; index < vectorCount; index++)
                    {
                        vectors[index] = documentScores.Skip(index * vectorCount).Take(vectorCount).ToArray();
                    }

                    return vectors;
                }
            }
            catch(Exception ex)
            {
                return new float[0][];
            }
        }


        public static List<SearchTermInfo> GetDocumentsTfIdf(List<string> sourceDocuments)
        {
            var returnValue = new List<SearchTermInfo>();

            if (sourceDocuments.Count > 0)
            {
                var url = string.Format(@"{0}/documents/tfidf", GetBaseAddress());

                var client = new HttpClient();
                using (client)
                {
                    var documentContents = GetDocumentContents(sourceDocuments);

                    var jsonContent = new JObject();
                    jsonContent.Add("documents", documentContents);

                    var json = jsonContent.ToString();
                    var content = new StringContent(json);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var response = client.PostAsync(url, content).Result;

                    var contents = response.Content.ReadAsStringAsync().Result;

                    returnValue = contents.Split('[')
                                        .Select(item => item.Replace("]", ""))
                                        .Select(item => item.Replace("\n", ""))
                                        .Select(item => item.Replace("\"", ""))
                                        .Select(item => item.Replace(" ", ""))
                                        .Where(item => item != string.Empty)
                                        .Select(item => item.Trim().Split(',').Take(3).ToArray())
                                        .Select(item => new SearchTermInfo() { Term = item[0], Stem = item[1], Score = float.Parse(item[2]) })
                                        .ToList();
                }
            }

            return returnValue;
        }


        public static List<SearchTermInfo> GetDocumentStems(List<string> sourceDocuments)
        {
            var url = string.Format(@"{0}/documents/tfidf", GetBaseAddress());

            var client = new HttpClient();
            using (client)
            {
                var documentContents = GetDocumentContents(sourceDocuments);

                var jsonContent = new JObject();
                jsonContent.Add("documents", documentContents);

                var json = jsonContent.ToString();
                var content = new StringContent(json);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.PostAsync(url, content).Result;

                var contents = response.Content.ReadAsStringAsync().Result;

                var items = contents.Split('[')
                                    .Select(item => item.Replace("]", ""))
                                    .Select(item => item.Replace("\n", ""))
                                    .Select(item => item.Replace("\"", ""))
                                    .Select(item => item.Replace(" ", ""))
                                    .Where(item => item != string.Empty)
                                    .Select(item => item.Trim().Split(',').Take(2).ToArray())
                                    .Select(item => new SearchTermInfo() { Term = item[0], Stem = item[1] })
                                    .ToList();

                return items;
            }
        }

        private static string GetDocumentContents(List<string> documents)
        {
            var builder = new StringBuilder();
            builder.Append("[");
            foreach (var document in documents)
            {
                var documentString = new string(document.Where(c => (
                                                                        !char.IsPunctuation(c) 
                                                                        && 
                                                                        char.IsLetterOrDigit(c)
                                                                        &&
                                                                        !char.IsSymbol(c)
                                                                        &&
                                                                        c < 127
                                                                     )
                                                                     ||
                                                                     char.IsWhiteSpace(c)
                                                                     ).ToArray()
                                                )
                                                .Replace("\"\"", "")
                                                .Replace("\n", "");
                if (string.IsNullOrEmpty(documentString))
                {
                    documentString = Guid.NewGuid().ToString();//should never match anything
                }

                builder.AppendFormat("\"{0}\",", documentString);
            }
            builder.Append("]");
            var documentContents = builder.ToString()
                                          .Replace(",]", "]");

            return documentContents.ToString();
        }

        private static string GetBaseAddress()
        {
            return ConfigurationManager.AppSettings["ScoreBaseAddress"];
        }
    }
}
