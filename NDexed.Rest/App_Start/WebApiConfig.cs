﻿using System;
using NDexed.CouchDb.Repositories;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Response;
using NDexed.Domain.Models;
using NDexed.Domain.Models.Cases;
using NDexed.Domain.Models.Documents;
using NDexed.Domain.Models.Payment;
using NDexed.Domain.Models.User;
using NDexed.Elastic.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dependencies;
using NDexed.AWS.Messagers;
using NDexed.AWS.Repository;
using NDexed.DataAccess.Repositories;
using NDexed.Messaging.Commands;
using NDexed.Messaging.Handlers;
using NDexed.Messaging.Messages;
using NDexed.Payments;
using NDexed.Payments.Stripe;
using NDexed.Rest.Registry;
using NDexed.Security;
using NDexed.Security.Encryptors;
using NDexed.Security.Hashers;
using NDexed.Security.Providers;
using NDexed.Rest.Filters;
using NDexed.Messaging.Commands.Queues;
using Waitless.Messaging.Commands;
using Waitless.Messaging.Handlers;
using NDexed.Domain.Models.Opinions;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Regulations;
using NDexed.Messaging.Commands.Document;
using NDexed.Messaging.Commands.CaseUsers;
using NDexed.Messaging.Queues;
using NDexed.AWS.SQS;
using NDexed.Domain.Models.Search;
using NDexed.Domain.Models.Matching;
using NDexed.Search.Repositories;
using NDexed.Domain.Models.Prediction;
using NDexed.Messaging.Commands.Applications;
using NDexed.Messaging.Handlers.Applications;
using NDexed.Rest.Common;
using NDexed.Domain.Models.Patents;
using NDexed.Messaging.Handlers.Patent;
using System.Collections.Generic;

namespace NDexed.Rest
{
    public static class WebApiConfig
    {
        internal static IDependencyResolver Container;

        public static void Register(HttpConfiguration config)
        {
           var corsConfig = new EnableCorsAttribute("*", "*", "*");
           corsConfig.Methods.Add("get");
            corsConfig.Methods.Add("post");
            corsConfig.Methods.Add("put");
            corsConfig.Methods.Add("options");
            corsConfig.Methods.Add("delete");
            config.EnableCors(corsConfig);

            config.DependencyResolver = GetContainer();
            Container = config.DependencyResolver;

            config.Routes.MapHttpRoute(
                name: "ServiceProviderResources",
                routeTemplate: "api/ServiceProvider/{serviceProviderId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "SearchRefinementResources",
                routeTemplate: "api/search/{patentSearchId}/refine/",
                defaults: new {
                                id = RouteParameter.Optional,
                                controller ="SearchRefinement"
                              }
            );
            config.Routes.MapHttpRoute(
                name: "UserSearchList",
                routeTemplate: "api/user/{userId}/search",
                defaults: new { controller = "Search" }
            );

            config.Routes.MapHttpRoute(
                name: "UserServicesWithAction",
                routeTemplate: "api/User/{action}/{id}",
                defaults: new { controller = "User", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "PaymentServicesWithAction",
                routeTemplate: "api/Payment/{action}/{id}",
                defaults: new { controller = "Payment", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ServiceInventoryResources",
                routeTemplate: "api/ServiceInventory/{serviceInventoryId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "OrganizationResources",
                routeTemplate: "api/Organization/{organizationId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "CaseUsersControllersWithActions",
                routeTemplate: "api/Case/{caseId}/CaseUsers/{action}/{id}",
                defaults: new { controller="CaseUsers", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "CaseResources",
                routeTemplate: "api/Case/{caseId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DataApi",
                routeTemplate: "api/Data/{type}/{id}",
                defaults: new { controller="Data" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            JsonMediaTypeFormatter jsonFormatter = config.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Filters.Add(new ExceptionFilter());
        }

        #region Private Methods

        private static IDependencyResolver GetContainer()
        {
            DependencyController container = new DependencyController();

            container.Register<IRepository<UserInfo, UserInfo>, CouchUserRepository>();
            container.Register<IRepository<Guid, CaseInfo>, CouchCaseRepository>();
            container.Register<IRepository<UserPaymentInfo, UserPaymentInfo>, DynamoUserPaymentRepository>();
            container.Register<IRepository<Guid, DocumentInfo>, ElasticDocumentRepository>();
            container.Register<IRepository<Guid, RegulationInfo>, ElasticRegulationRepository>();
            container.Register<IRepository<Guid, LogInfo>, FileLogRepository>();

            container.Register<IReadRepository<OpinionDetailQuery, OpinionInfo>, ElasticOpinionRepository>();
            container.Register<IRepository<Guid, TimeInfo>, CouchTimeRepository>();
            container.Register<IRepository<Guid, NoteInfo>, CouchNoteRepository>();
            container.Register<IRepository<Guid, ClientInfo>, CouchClientRepository>();
            container.Register<IRepository<Guid, Organization>, CouchOrganizationRepository>();
            container.Register<IRepository<Guid, TermInfo>, CouchTermsRepository>();
            container.Register<IRepository<DataRequestInfo, DataInfo>, S3Repository>();
            container.Register<IRepository<Guid, ApplicationInfo>, CouchApplicationRepository>();
            container.Register<IRepository<string, PatentInfo>, ElasticPatentRepository>();
            container.Register<IRepository<string, PatentApplicationInfo>, ElasticPatentApplicationRepository>();
            container.Register<IRepository<Guid, PatentSearchInfo>, PatentSearchInfoRepository>();
            container.Register<IRepository<Guid, SearchInfo>, CouchSearchHistoryRepository>();
            

            container.Register<ISearchableRepository<UserInfo, UserInfo>, CouchUserRepository>();
            container.Register<ISearchableRepository<Guid, CaseInfo>, CouchCaseRepository>();
            container.Register<ISearchableRepository<Guid, DocumentSummary>, ElasticDocumentRepository>();
            container.Register<ISearchableRepository<Guid, DocumentInfo>, ElasticDocumentRepository>();
            container.Register<ISearchableRepository<DocumentSearchQuery, DocumentSearchResponse>, ElasticDocumentRepository>();
            container.Register<ISearchableRepository<OpinionSearchQuery, OpinionSearchResponse>, ElasticOpinionRepository>();
            container.Register<ISearchableRepository<RegulationSearchQuery, RegulationSearchResponse>, ElasticRegulationRepository>();
            container.Register<ISearchableRepository<Guid, TimeInfo>, CouchTimeRepository>();
            container.Register<ISearchableRepository<Guid, NoteInfo>, CouchNoteRepository>();
            container.Register<ISearchableRepository<Guid, ClientInfo>, CouchClientRepository>();
            container.Register<ISearchableRepository<Guid, UserInfo>, CouchUserRepository>();
            container.Register<ISearchableRepository<PredictionService, PredictionInfo>, MLRepository>();
            container.Register<ISearchableRepository<SearchInfo, ClaimInfo>, ElasticClaimsSearchRepository>();
            container.Register<ISearchableRepository<SearchInfo, PatentInfo>, ElasticPatentRepository>();
            container.Register<ISearchableRepository<SearchInfo, PatentApplicationInfo>, ElasticPatentApplicationRepository>();
            container.Register<ISearchableRepository<Guid, PatentSearchInfo>, PatentSearchInfoRepository>();
            container.Register<ISearchableRepository<PatentSearchInfo, SearchInfo>, CouchSearchHistoryRepository>();
            container.Register<ISearchableRepository<SearchInfo, CpcInfo>, ElasticCpcRepository>();
            container.Register<ISearchableRepository<CpcSearchInfo, Dictionary<string, string>>, ElasticCpcRepository>();

            //security
            container.Register<IHashProvider, PublicPrivateKeyHasher>();
            container.Register<IEncryptor, RijndaelManagedEncryptor>();
            container.Register<IAuthorizationTokenProvider, HashAuthorizationTokenProvider>();

            //handlers
            container.Register<ICommandHandler<CreateOrganizationCommand>, OrganizationCommandHandler>();
            container.Register<ICommandHandler<CreateUserCommand>, UserCommandHandler>();
            container.Register<ICommandHandler<ResetPasswordCommand>, UserCommandHandler>();
            container.Register<ICommandHandler<SetPasswordCommand>, UserCommandHandler>();
            container.Register<ICommandHandler<AddQueueItemCommand>, QueueCommandHandler>();
            container.Register<ICommandHandler<UpdateQueueItemCommand>, QueueCommandHandler>();
            container.Register<ICommandHandler<SavePaymentInfoCommand>, PaymentCommandHandler>();
            container.Register<ICommandHandler<AddCaseUsersCommand>, CaseUsersCommandHandler>();
            container.Register<ICommandHandler<DeleteCaseUsersCommand>, CaseUsersCommandHandler>();
            container.Register<ICommandHandler<UpdateDocumentCommand>, DocumentCommandHandler>();
            container.Register<ICommandHandler<DownloadEmailCommand>, DocumentCommandHandler>();
            container.Register<ICommandHandler<CreateDocumentZipCommand>, DocumentCommandHandler>();
            container.Register<ICommandHandler<AddCaseDocumentCommand>, CaseDocumentCommandHandler>();
            container.Register<ICommandHandler<ImportApplicationCommand>, ImportApplicationHandler>();
            container.Register<ICommandHandler<SaveSearchInfoCommand>, PatentSearchHandler>();
            container.Register<IReturnCommandHandler <RefinePatentSearchCommand, List <PatentInfo>>, RefinePatentSearchHandler>();

            container.Register<IReturnCommandHandler<PatentSearchCommand, SearchResultInfo>, PatentSearchHandler>();

            //payments
            container.Register<IPaymentProvider, StripePaymentProvider>();

            container.Register<IMessager, AwsEmailMessenger>();

            container.Register<IMessageQueueProcessor<SqsMessage>, SqsMessageProcessor>();

            return container;
        }

        #endregion
    }
}
