﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Providers.Entities;
using Microsoft.Ajax.Utilities;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.User;
using NDexed.Rest.Resources;
using NDexed.Security;

namespace NDexed.Rest._extensions
{
    internal static class HttpRequestMessageExtensions
    {
        internal const string AUTH_TOKEN_NAME = "NDexedAuthToken";
        private const string AUTH_TOKEN_QUERY_STRING_NAME = "authToken";

        internal static string GetAuthToken(this HttpRequestMessage request)
        {
            try
            {
                var tokenHeader = request.Headers
                    .FirstOrDefault(header => header.Key == AUTH_TOKEN_NAME);

                string tokenValue = null;
                if (tokenHeader.Key != null)
                {
                    tokenValue = tokenHeader.Value.FirstOrDefault();
                }

                if (string.IsNullOrEmpty(tokenValue))
                {
                    tokenValue = GetTokenFromQueryString(request);
                    if (string.IsNullOrEmpty(tokenValue))
                    {
                        throw new HttpException(401, ErrorMessages.UserNotAuthorized);
                    }
                }
                return tokenValue;
            }
            catch (ArgumentNullException ex)
            {
                throw new HttpException(401, ErrorMessages.UserNotAuthorized);
            }
        }

        private static string GetTokenFromQueryString(HttpRequestMessage request)
        {
            string authToken = null;

            Dictionary<string, string> queryStrings = request
                        .GetQueryNameValuePairs()
                        .ToDictionary(pair => pair.Key, pair => pair.Value, StringComparer.OrdinalIgnoreCase);

            if (queryStrings.ContainsKey(AUTH_TOKEN_QUERY_STRING_NAME))
            {
                string base64EncodedString = queryStrings[AUTH_TOKEN_QUERY_STRING_NAME];
                byte[] buffer = Convert.FromBase64String(base64EncodedString);
                authToken = Encoding.UTF8.GetString(buffer);
            }

            return authToken;
        }
        internal static Guid GetUserId(this HttpRequestMessage request)
        {
            try
            {
                string tokenHeader = request.GetAuthToken();

                IAuthorizationTokenProvider tokenProvider = (IAuthorizationTokenProvider)WebApiConfig.Container.GetService(typeof(IAuthorizationTokenProvider));
                Guid userId = tokenProvider.ValidateAuthorizationToken(tokenHeader);

                return userId;
            }
            catch (Exception)
            {
                throw new HttpException(401, ErrorMessages.UserNotAuthorized);
            }
        }

        public static string GetHeaderValueAsString(this HttpRequestMessage request, string headerName)
        {
            string requestHeader = null;
            if (request.Headers.Contains(headerName))
                requestHeader = request.Headers.GetValues(headerName).FirstOrDefault();

            return requestHeader;
        }

        public static UserInfo GetRequestingUser(this HttpRequestMessage request)
        {
            var userId = request.GetUserId();

            var userRepo = (IRepository<UserInfo, UserInfo>)WebApiConfig.Container.GetService(typeof(IRepository<UserInfo, UserInfo>));

            var user = userRepo.Get(new UserInfo{Id = userId});

            return user;
        }
    }
}