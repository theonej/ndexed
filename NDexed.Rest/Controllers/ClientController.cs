﻿
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;

namespace NDexed.Rest.Controllers
{
    public class ClientController : ApiController
    {
        private readonly IRepository<Guid, ClientInfo> m_Repository;
        private readonly ISearchableRepository<Guid, ClientInfo> m_SearchRepository;

        public ClientController(IRepository<Guid, ClientInfo> repository,
                                ISearchableRepository<Guid, ClientInfo> searchRepository)
        {
            Condition.Requires(repository).IsNotNull();
            Condition.Requires(searchRepository).IsNotNull();

            m_Repository = repository;
            m_SearchRepository = searchRepository;
        }

        [HttpPost]
        [AuthorizationFilter]
        public HttpResponseMessage Add(ClientInfo client)
        {
            if (client.Id == Guid.Empty)
            {
                client.Id = Guid.NewGuid();
            }

            if (client.CreatedDateTime == DateTime.MinValue)
            {
                client.CreatedDateTime = DateTime.UtcNow;
            }
            client.CreatedBy = Request.GetUserId().ToString();

            m_Repository.Add(client);

            var response = Request.CreateResponse(HttpStatusCode.Created, client.Id);

            return response;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid organizationId)
        {
            var caseClients = m_SearchRepository.Search(organizationId);

            var response = Request.CreateResponse(HttpStatusCode.OK, caseClients);

            return response;
        }

        [HttpDelete]
        [AuthorizationFilter]
        public HttpResponseMessage Remove(Guid organizationId, Guid id)
        {
            var client = m_Repository.Get(id);
            m_Repository.Remove(client);

            var response = Request.CreateResponse(HttpStatusCode.OK, id);

            return response;
        }
    }
}
