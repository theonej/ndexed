﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http;
using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;

namespace NDexed.Rest.Controllers
{
    public class TimeController : ApiController
    {
        private readonly IRepository<Guid, TimeInfo> m_Repository;
        private readonly ISearchableRepository<Guid, TimeInfo> m_SearchRepository;

        public TimeController(IRepository<Guid, TimeInfo> repository,
                              ISearchableRepository<Guid, TimeInfo> searchRepository)
        {
            Condition.Requires(repository).IsNotNull();
            Condition.Requires(searchRepository).IsNotNull();

            m_Repository = repository;
            m_SearchRepository = searchRepository;
        }

        [HttpPost]
        [AuthorizationFilter]
        public HttpResponseMessage Add(TimeInfo time)
        {
            if (time.Id == Guid.Empty)
            {
                time.Id = Guid.NewGuid();
            }

            if (time.CreatedDateTime == DateTime.MinValue)
            {
                time.CreatedDateTime = DateTime.UtcNow;
            }
            time.CreatedBy = Request.GetUserId().ToString();

            m_Repository.Add(time);

            var response = Request.CreateResponse(HttpStatusCode.Created, time.Id);

            return response;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid caseId)
        {
            var user = Request.GetRequestingUser();
            if (user.CaseIds.IndexOf(caseId) < 0)
            {
                throw new AuthenticationException("You are not authorized to view this case");
            }

            var caseTimes = m_SearchRepository.Search(caseId);

            var response = Request.CreateResponse(HttpStatusCode.OK, caseTimes);

            return response;
        }

        [HttpDelete]
        [AuthorizationFilter]
        public HttpResponseMessage Remove(Guid caseId, Guid id)
        {
            var time = m_Repository.Get(id);
            m_Repository.Remove(time);

            var response = Request.CreateResponse(HttpStatusCode.OK, id);

            return response;
        }
    }
}
