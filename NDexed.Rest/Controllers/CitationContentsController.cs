﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using NDexed.Rest.Filters;
using NDexed.Search.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class CitationContentsController : ApiController
    {
        private readonly ISearchableRepository<string, PatentInfo> m_ContentsRepo;

        public CitationContentsController()
        {
            m_ContentsRepo = new UsptoContentsRepository();
        }

        [HttpGet]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Get(string id)
        {
            var contents = m_ContentsRepo.Search(id).FirstOrDefault();

            var response = Request.CreateResponse(HttpStatusCode.OK, contents);

            return response;
        }
    }
}
