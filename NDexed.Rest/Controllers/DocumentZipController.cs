﻿using CuttingEdge.Conditions;
using NDexed.Messaging.Commands.Document;
using NDexed.Messaging.Handlers;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class DocumentZipController : ApiController
    {
        private readonly ICommandHandler<CreateDocumentZipCommand> m_ZipHandler;

        public DocumentZipController(ICommandHandler<CreateDocumentZipCommand> zipHandler)
        {
            Condition.Requires(zipHandler).IsNotNull();

            m_ZipHandler = zipHandler;
        }

        [HttpPost]
        [AuthorizationFilter]
        public HttpResponseMessage CreateZip(CreateDocumentZipCommand command)
        {
            var fileName = command.OutputFileName;

            var tempFileName = Path.Combine(Path.GetTempPath(), command.OutputFileName);
            command.OutputFileName = tempFileName;

            m_ZipHandler.Handle(command);
            var fileContents = File.ReadAllBytes(command.OutputFileName);
            File.Delete(command.OutputFileName);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            var memoryStream = new MemoryStream(fileContents);

            memoryStream.Position = 0;
            response.Content = new StreamContent(memoryStream);
            response.Content.Headers.ContentLength = memoryStream.Length;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return response;
        }
    }
}
