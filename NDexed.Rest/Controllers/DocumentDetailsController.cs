﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using CuttingEdge.Conditions;

using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Documents;
using NDexed.Rest.Resources;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;

using Newtonsoft.Json;


namespace NDexed.Rest.Controllers
{
    public class DocumentDetailsController : ApiController
    {
        private const string DOCUMENT_SETTINGS_HEADER = "DocumentSettings";

        private readonly IRepository<Guid, DocumentInfo> m_DocumentRepository;
        private readonly ISearchableRepository<Guid, DocumentInfo> m_CaseDocumentRepository;

        public DocumentDetailsController(IRepository<Guid, DocumentInfo> documentRepository,
                                  ISearchableRepository<Guid, DocumentInfo> caseDocumentRepository)
        {
            Condition.Requires(documentRepository).IsNotNull();
            Condition.Requires(caseDocumentRepository).IsNotNull();

            m_DocumentRepository = documentRepository;
            m_CaseDocumentRepository = caseDocumentRepository;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid caseId, Guid id)
        {

            var user = Request.GetRequestingUser();
            //figure this out and re-enable it
            //if (user.CaseIds.IndexOf(caseId) < 0)
            //{
            //    throw new AuthenticationException("You are not authorized to view this case");
            //}

            var document = m_DocumentRepository.Get(id);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, document);
          
            return response;
        }
        
       
    }
}
