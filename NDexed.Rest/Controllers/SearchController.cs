﻿
using CuttingEdge.Conditions;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using NDexed.Messaging.Commands;
using NDexed.Rest.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using NDexed.Messaging.Handlers;
using System;
using NDexed.DataAccess.Repositories;
using System.Linq;

namespace NDexed.Rest.Controllers
{
    public class SearchController : ApiController
    {
        private readonly IReturnCommandHandler<PatentSearchCommand, SearchResultInfo> m_Handler;
        private readonly ISearchableRepository<Guid, PatentSearchInfo> m_SearchListRepo;
        private readonly IRepository<Guid, PatentSearchInfo> m_PatentSearchRepo;
        private readonly ICommandHandler<SaveSearchInfoCommand> m_SearchInfoSaveHandler;

        public SearchController(IReturnCommandHandler<PatentSearchCommand, SearchResultInfo> handler,
                                ISearchableRepository<Guid, PatentSearchInfo> searchListRepo,
                                IRepository<Guid, PatentSearchInfo> patentSearchRepo,
                                ICommandHandler<SaveSearchInfoCommand> searchInfoSaveHandler)
        {
            Condition.Requires(handler).IsNotNull();
            Condition.Requires(searchListRepo).IsNotNull();
            Condition.Requires(patentSearchRepo).IsNotNull();
            Condition.Requires(searchInfoSaveHandler).IsNotNull();

            m_Handler = handler;
            m_SearchListRepo = searchListRepo;
            m_PatentSearchRepo = patentSearchRepo;
            m_SearchInfoSaveHandler = searchInfoSaveHandler;
        }

        [HttpGet]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage GetByUserId(Guid userId)
        {
            var results = m_SearchListRepo.Search(userId);
            
            var response = Request.CreateResponse(HttpStatusCode.OK, results);

            return response;
        }

        [HttpGet]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Get(Guid id)
        {
            var results = m_PatentSearchRepo.Get(id);

            var response = Request.CreateResponse(HttpStatusCode.OK, results);

            return response;
        }

        [HttpPut]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Add(PatentSearchInfo patentSearchInfo)
        {
            if(patentSearchInfo.PatentSearchId == Guid.Empty)
            {
                patentSearchInfo.PatentSearchId = Guid.NewGuid();
            }

            var command = new SaveSearchInfoCommand();
            command.SearchInfo = patentSearchInfo;
            m_SearchInfoSaveHandler.Handle(command);

            var id = m_PatentSearchRepo.Add(patentSearchInfo);
            
            var response = Request.CreateResponse(HttpStatusCode.Accepted, patentSearchInfo.PatentSearchId);

            return response;
        }

        [HttpPost]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Search(SearchInfo searchInfo)
        {
            var command = new PatentSearchCommand();
            command.Criteria = searchInfo;
            
            var results = m_Handler.Handle(command);
            
            var response = Request.CreateResponse(HttpStatusCode.OK, results);

            return response;
        }
    }
}
