﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using NDexed.Domain.Models.User;
using NDexed.Messaging.Handlers;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;
using Waitless.Messaging.Commands;

namespace NDexed.Rest.Controllers
{
    public class OrganizationController : BaseController
    {
        private readonly ICommandHandler<CreateOrganizationCommand> m_CreateHandler;
        private readonly IRepository<Guid, Organization> m_Repository;
        private readonly ISearchableRepository<Guid, UserInfo> m_UsersRepo; 
 
        public OrganizationController(ICommandHandler<CreateOrganizationCommand> createHandler,
                                      IRepository<Guid, Organization> repository,
                                      ISearchableRepository<Guid, UserInfo> usersRepo)
        {
            Condition.Requires(createHandler).IsNotNull();
            Condition.Requires(repository).IsNotNull();
            Condition.Requires(usersRepo).IsNotNull();

            m_CreateHandler = createHandler;
            m_Repository = repository;
            m_UsersRepo = usersRepo;
        }

        [HttpGet]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Get(Guid id)
        {
            var organization = m_Repository.Get(id);
            if (organization != null)
            {
                organization.Users = m_UsersRepo.Search(id).ToList();
            }
            var response = Request.CreateResponse(HttpStatusCode.OK, organization);

            return response;
        }

        [HttpPut]
        [ExceptionFilter]
        [AuthorizationFilter]
        public HttpResponseMessage Update(Organization organization)
        {
            organization.UpdatedBy = Request.GetUserId().ToString();
            organization.UpdatedDateTime = DateTime.UtcNow;

            m_Repository.Add(organization);

            var response = Request.CreateResponse(HttpStatusCode.Accepted, organization.OrganizationId);

            return response;
        }

        [HttpPost]
        [ExceptionFilter]
        [AuthorizationFilter]
        public HttpResponseMessage Post(CreateOrganizationCommand command)
        {
            if (command.Id == Guid.Empty)
            {
                command.Id = Guid.NewGuid();
            }

            m_CreateHandler.Handle(command);

            var response = Request.CreateResponse(HttpStatusCode.Created, command.Id);

            return response;
        }
    }
}
