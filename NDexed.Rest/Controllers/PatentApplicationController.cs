﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class PatentApplicationController : ApiController
    {
        private readonly ISearchableRepository<SearchInfo, PatentApplicationInfo> m_SearchRepo;
        private readonly IRepository<Guid, SearchInfo> m_SearchHistoryRepository;
        public PatentApplicationController(ISearchableRepository<SearchInfo, PatentApplicationInfo> searchRepo,
                                           IRepository<Guid, SearchInfo> searchHistoryRepo)
        {
            Condition.Requires(searchRepo).IsNotNull();
            Condition.Requires(searchHistoryRepo).IsNotNull();

            m_SearchRepo = searchRepo;
            m_SearchHistoryRepository = searchHistoryRepo;
        }
        
        [HttpPost]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Get(SearchInfo searchInfo)
        {
            if (searchInfo.PatentSearchId == Guid.Empty)
            {
                throw new InvalidOperationException("You must specify the search Id for this query");
            }

            var results = m_SearchRepo.Search(searchInfo);

            m_SearchHistoryRepository.Add(searchInfo);

            var response = Request.CreateResponse(HttpStatusCode.OK, results);

            return response;
        }
    }
}
