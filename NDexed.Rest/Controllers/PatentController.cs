﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using NDexed.Messaging.Commands;
using NDexed.Messaging.Handlers;
using NDexed.Rest.Filters;
using NDexed.Search.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class PatentController : ApiController
    {
        private readonly IRepository<string, PatentInfo> m_Repo;
        private readonly IReturnCommandHandler<PatentSearchCommand, SearchResultInfo> m_SearchHandler;
        public PatentController(IRepository<string, PatentInfo> repo,
                                IReturnCommandHandler<PatentSearchCommand, SearchResultInfo> handler
                                )
        {
            Condition.Requires(repo).IsNotNull();
            Condition.Requires(handler).IsNotNull();

            m_Repo = repo;
            m_SearchHandler = handler;
        }

        [HttpGet]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Get(string id)
        {
            var patent = m_Repo.Get(id);
            
            var response = Request.CreateResponse(HttpStatusCode.OK, patent);

            return response;
        }

        [HttpPost]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Search(SearchInfo searchInfo)
        {
            searchInfo.AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o";
                searchInfo.ProviderId = "000375274410958331611:ealp1cwjuyo";

            var command = new PatentSearchCommand() {
                Criteria = searchInfo
            };

            var results = m_SearchHandler.Handle(command);

            var response = Request.CreateResponse(HttpStatusCode.OK, results);

            return response;
        }
    }
}
