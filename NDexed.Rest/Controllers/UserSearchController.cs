﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.User;
using NDexed.Rest._extensions;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class UserSearchController : ApiController
    {
        private ISearchableRepository<Guid, UserInfo> m_SearchRepo;

        public UserSearchController(ISearchableRepository<Guid, UserInfo> searchRepo)
        {
            Condition.Requires(searchRepo).IsNotNull();

            m_SearchRepo = searchRepo;
        }

        [HttpGet]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Search(string searchTerm)
        {
            var user = Request.GetRequestingUser();
            var orgUsers = m_SearchRepo.Search(user.OrganizationId);

            var filteredUsers = orgUsers.Where(item => item.EmailAddress.Contains(searchTerm) || item.UserName.Contains(searchTerm));

            var response = Request.CreateResponse(HttpStatusCode.OK, filteredUsers);

            return response;
        }
    }
}
