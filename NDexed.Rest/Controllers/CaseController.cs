﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http;
using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;

namespace NDexed.Rest.Controllers
{
    public class CaseController : ApiController
    {
        private readonly IRepository<Guid, CaseInfo> m_Repository;
        private readonly ISearchableRepository<Guid, CaseInfo> m_SearchRepository;

        public CaseController(IRepository<Guid, CaseInfo> repository,
                              ISearchableRepository<Guid, CaseInfo> searchRepository)
        {
            Condition.Requires(repository).IsNotNull();
            Condition.Requires(searchRepository).IsNotNull();

            m_Repository = repository;
            m_SearchRepository = searchRepository;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid id)
        {
            var user = Request.GetRequestingUser();
            if (user.CaseIds.IndexOf(id) < 0)
            {
                throw new AuthenticationException("You are not authorized to view this case");
            }
            var caseInfo = m_Repository.Get(id);

            var response = Request.CreateResponse(HttpStatusCode.OK, caseInfo);

            return response;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage GetOrganizationCases(Guid organizationId)
        {
            var user = Request.GetRequestingUser();

            var cases = m_SearchRepository.Search(organizationId).Where(item=>item.IsDeleted == false);

            cases = cases.Where(@case => user.CaseIds.Contains(@case.Id)).ToList();

            var response = Request.CreateResponse(HttpStatusCode.OK, cases);

            return response;
        }

        [HttpPost]
        [AuthorizationFilter]
        public HttpResponseMessage Create(CaseInfo caseInfo)
        {
            if (caseInfo.Id == Guid.Empty)
            {
                caseInfo.Id = Guid.NewGuid();
            }

            caseInfo.CreatedDateTime = DateTime.UtcNow;
            caseInfo.CreatedBy = Request.GetUserId().ToString();

            m_Repository.Add(caseInfo);

            var response = Request.CreateResponse(HttpStatusCode.Created, caseInfo.Id);

            return response;
        }

        [HttpPut]
        [AuthorizationFilter]
        public HttpResponseMessage Update(CaseInfo caseInfo)
        {
            if (caseInfo.Id == Guid.Empty)
            {
                throw new InvalidDataException("CaseInfo.Id is missing");
            }

            caseInfo.UpdatedDateTime = DateTime.UtcNow;
            caseInfo.UpdatedBy = Request.GetUserId().ToString();

            m_Repository.Add(caseInfo);

            var response = Request.CreateResponse(HttpStatusCode.Created, caseInfo.Id);

            return response;
        }
    }
}
