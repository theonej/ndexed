﻿using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    /// <summary>
    /// Acts as a pass-through for calling locally running Flask services
    /// </summary>
    public class FlaskController : ApiController
    {
        [HttpGet]
        [AuthorizationFilter]
        public async Task<HttpResponseMessage> Get(string endpoint)
        {
            var client = new HttpClient();
            using (client)
            {
                var response = await client.GetAsync(string.Format("http://127.0.0.1:5000/{0}", endpoint));

                return response;
            }
        }

        [HttpPost]
        [AuthorizationFilter]
        public async Task<HttpResponseMessage> Post(string endpoint)
        {
            var client = new HttpClient();
            using (client)
            {
                var uri = new Uri(string.Format("http://127.0.0.1:5000/{0}", endpoint));
                var response = await client.PostAsync(uri, Request.Content);

                return response;
            }
        }
    }
}
