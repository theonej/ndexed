﻿using CuttingEdge.Conditions;
using NDexed.AWS.Repository;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Prediction;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class PredictionController : ApiController
    {
        private readonly ISearchableRepository<PredictionService, PredictionInfo> m_PredictionRepository;

        public PredictionController(ISearchableRepository<PredictionService, PredictionInfo> predictionRepo)
        {
            Condition.Requires(predictionRepo).IsNotNull();

            m_PredictionRepository = predictionRepo;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(string modelId, string contents)
        {
            var searchInfo = new PredictionService("https://realtime.machinelearning.us-east-1.amazonaws.com", modelId);
            searchInfo.Features = new Dictionary<string, string>();
            searchInfo.Features.Add("Var1", contents);

            var predictionRepo = new MLRepository();
            var prediction = predictionRepo.Search(searchInfo).First();

            var response = Request.CreateResponse(HttpStatusCode.OK, prediction);

            return response;
        }
    }
}
