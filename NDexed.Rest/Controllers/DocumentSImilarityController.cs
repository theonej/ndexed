﻿
using NDexed.Domain.Models.Documents;
using NDexed.Rest.Filters;
using NDexed.Search.Analyzers;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class DocumentSimilarityController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(DocumentSimilarityRequest command)
        {
            var similarity = DocumentSimilarityAnalyzer.GetDocumentSimilarity(command);

            var response = Request.CreateResponse(HttpStatusCode.OK, similarity);
            return response;
        }
    }
}
