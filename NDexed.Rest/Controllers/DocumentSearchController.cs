﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CuttingEdge.Conditions;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Repositories;
using NDexed.DataAccess.Response;
using NDexed.Rest.Filters;
using NDexed.Rest.Common;
using System.Collections.Generic;
using System;
using NDexed.Rest._extensions;

namespace NDexed.Rest.Controllers
{
    public class DocumentSearchController : ApiController
    {
        private readonly ISearchableRepository<DocumentSearchQuery, DocumentSearchResponse> m_SearchRepo;

        public DocumentSearchController(ISearchableRepository<DocumentSearchQuery, DocumentSearchResponse> searchRepo)
        {
            Condition.Requires(searchRepo).IsNotNull();

            m_SearchRepo = searchRepo;
        }

        [HttpPost]
        [AuthorizationFilter]
        public HttpResponseMessage Search(DocumentSearchQuery query)
        {
            var user = Request.GetRequestingUser();
            query.OrganizationId = user.OrganizationId;

            var results = m_SearchRepo.Search(query);

            try
            {
                results = RankResponses(query.Term, query.PageSize, results);
            }catch(Exception ex)
            {
                //this means their are not enough documents to match our critera, but that's ok
            }
            var response = Request.CreateResponse(HttpStatusCode.OK, results);

            return response;
        }

        #region Private Methods

        private List<DocumentSearchResponse> RankResponses(string searchTerm, int maxCount, IEnumerable<DocumentSearchResponse> matches)
        {
            var rankedResponses = new List<DocumentSearchResponse>();

            var searchTerms = Utilities.GetDistinctTerms(searchTerm);

            foreach (var match in matches)
            {
                var dictionary = new List<string[]>();

                foreach (var document in match.Results)
                {
                    var terms = Utilities.GetDistinctTerms(document.Contents);
                    dictionary.Add(terms);
                }

                var rankings = ndexed.lsi.lsiRanker.rank(searchTerms, dictionary.ToArray());

                //rankings are assumed to be in the same order as the data we passed in
                for (var rankIndex = 0; rankIndex < rankings.Length; rankIndex++)
                {
                    var rank = rankings[rankIndex];
                    var document = match.Results[rankIndex];
                    var doubleString = string.Format("{0:0.00000}", rank);
                    document.Rank = double.Parse(doubleString);
                }

                var matchedDocuments = match.Results.ToList();

                matchedDocuments.Sort((a, b) => {
                        return b.Rank.CompareTo(a.Rank);
                });

                match.Results = matchedDocuments.Take(maxCount).ToList();

                rankedResponses.Add(match);
            }
            return rankedResponses;
        }

        #endregion
    }
}
