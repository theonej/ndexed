﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using CuttingEdge.Conditions;

using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Documents;
using NDexed.Rest.Resources;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;
using NDexed.DocumentProcessing.OfficeInterop;

using Newtonsoft.Json;
using NDexed.Domain.Models.Cases;
using NDexed.Domain.Models.Data;
using NDexed.Messaging.Commands.Document;
using NDexed.Messaging.Handlers;

namespace NDexed.Rest.Controllers
{
    public class DocumentController : ApiController
    {
        private const string DOCUMENT_SETTINGS_HEADER = "DocumentSettings";

        private readonly IRepository<Guid, DocumentInfo> m_DocumentRepository;
        private readonly ISearchableRepository<Guid, DocumentInfo> m_CaseDocumentRepository;
        private readonly IRepository<Guid, CaseInfo> m_CaseRepository;
        private readonly IRepository<Guid, TermInfo> m_TermRepo;
        private readonly ICommandHandler<UpdateDocumentCommand> m_UpdateCommandHandler;

        public DocumentController(IRepository<Guid, DocumentInfo> documentRepository,
                                  ISearchableRepository<Guid, DocumentInfo> caseDocumentRepository,
                                  IRepository<Guid, CaseInfo> caseRepository,
                                  IRepository<Guid, TermInfo> termRepo,
                                  ICommandHandler<UpdateDocumentCommand> updateHandler)
        {
            Condition.Requires(documentRepository).IsNotNull();
            Condition.Requires(caseDocumentRepository).IsNotNull();
            Condition.Requires(caseRepository).IsNotNull();
            Condition.Requires(termRepo).IsNotNull();
            Condition.Requires(updateHandler).IsNotNull();

            m_DocumentRepository = documentRepository;
            m_CaseDocumentRepository = caseDocumentRepository;
            m_CaseRepository = caseRepository;
            m_TermRepo = termRepo;
            m_UpdateCommandHandler = updateHandler;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid caseId)
        {
            var user = Request.GetRequestingUser();
            if (user.CaseIds.IndexOf(caseId) < 0)
            {
                throw new AuthenticationException("You are not authorized to view this case");
            }
            var documents = m_CaseDocumentRepository.Search(caseId);

            var response = Request.CreateResponse(HttpStatusCode.OK, documents);

            return response;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid caseId, Guid id, bool convertToPdf = false)
        {

            var user = Request.GetRequestingUser();

            var @case = m_CaseRepository.Get(caseId);
            if (@case.OrganizationId != user.OrganizationId)
            {
                throw new AuthenticationException("You are not authorized to view this organization's documents");
            }

            var document = m_DocumentRepository.Get(id);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (convertToPdf && (document.Name.IndexOf(".doc") > -1 || document.Name.IndexOf(".doc") > -1))
            {
                var converter = new DocumentConverter();
                document.Base64EncodedData = converter.ConvertBase64WordToBase64Pdf(document.Base64EncodedData);
            }
            var bytes = Convert.FromBase64String(document.Base64EncodedData);
            var memoryStream = new MemoryStream(bytes);

            memoryStream.Position = 0;
            response.Content = new StreamContent(memoryStream);
            response.Content.Headers.ContentLength = memoryStream.Length;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = document.Name
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return response;
        }

        [HttpPost]
        [AuthorizationFilter]
        public async Task<HttpResponseMessage> Post()
        {
            var response = Request.CreateResponse(HttpStatusCode.Accepted);

            var user = Request.GetRequestingUser();

            MultipartFormDataStreamProvider provider = await GetProvider(Request);

            var settings = Request.GetHeaderValueAsString(DOCUMENT_SETTINGS_HEADER);
            dynamic settingsObject = JsonConvert.DeserializeObject(settings);

            string documentName = provider.FormData[settingsObject["documentName"].ToString()];
           
            documentName = GetDocumentName(documentName);

            var documentType = GetDocumentType(settingsObject, provider);
            var tags = GetTags(settingsObject, provider);
            var caseId = Guid.Parse(provider.FormData[settingsObject["caseId"].ToString()]);
            var chunkNumber = int.Parse(provider.FormData[settingsObject["chunkNumber"].ToString()]);
            var isLastChunk = IsLastChunk(provider.FormData, settingsObject);

            WriteChunk(provider.FileData[0].LocalFileName, documentName, chunkNumber);

            if (isLastChunk)
            {
                var encodedData = EncodeDocumentContents(documentName);

                var document = new DocumentInfo
                {
                    Id = Guid.NewGuid(),
                    Name = documentName,
                    ContentType = MimeMapping.GetMimeMapping(documentName),
                    CaseId = caseId,
                    OrganizationId = user.OrganizationId,
                    Base64EncodedData = encodedData,
                    Size = GetSize(encodedData),
                    CreatedDateTime = DateTime.Now,
                    CreatedBy = Request.GetUserId().ToString(),
                    Type = documentType,
                    Tags = tags
                };

                m_DocumentRepository.Add(document);

                response = Request.CreateResponse(HttpStatusCode.Created, document.Id);
            }

            return response;
        }

        [HttpDelete]
        [AuthorizationFilter]
        public HttpResponseMessage Delete(Guid id)
        {
            var document = m_DocumentRepository.Get(id);
            if (document != null)
            {
                m_DocumentRepository.Remove(document);
            }

            var response = Request.CreateResponse(HttpStatusCode.Accepted, id);

            return response;
        }

        [HttpPut]
        [AuthorizationFilter]
        public HttpResponseMessage Update(UpdateDocumentCommand command)
        {
            var user = Request.GetRequestingUser();
            command.UpdatedByUser = user.UserName;

            m_UpdateCommandHandler.Handle(command);

            var response = Request.CreateResponse(HttpStatusCode.Accepted, command.DocumentId);

            return response;
        }

        /// <summary>
        /// Extracts the tags from the form data, and removes any terms that have been defined as 
        /// not relevant when classifying documents
        /// </summary>
        /// <param name="settingsObject"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        private string[] GetTags(dynamic settingsObject, MultipartFormDataStreamProvider provider)
        {
            string[] tags = new string[] { };

            var tagValues = provider.FormData[settingsObject["tags"].ToString()];
            if(tagValues != null)
            {
                tags = tagValues.ToString().Split(',');
            }

            var termsToRemove = m_TermRepo.GetAll().Select(term => term.Value);
            tags = tags.Where(tag => !termsToRemove.Contains(tag)).ToArray();

            return tags;
        }

        private string GetDocumentName(string documentName)
        {
            int length = documentName.Length > 50 ? 50 : documentName.Length;
            var name = documentName;
            if (length < documentName.Length)
            {
                var startIndex = documentName.LastIndexOf('.');
                if (startIndex < 0)
                {
                    startIndex = documentName.Length - 4;
                }

                var extension = documentName.Substring(startIndex, documentName.Length - startIndex);
                name = documentName.Substring(0, length);
                name = name + extension;
            }

            return name;
        }

        private DocumentTypes GetDocumentType(dynamic settingsObject, MultipartFormDataStreamProvider provider)
        {
            var documentType = DocumentTypes.Private;

            var typeData = settingsObject["documentType"];
               
            if (settingsObject["documentType"] != null)
            {
                if (typeData != null)
                {
                    documentType = (DocumentTypes)int.Parse(typeData.ToString());
                }
            }

            return documentType;
        }

        private static Task<MultipartFormDataStreamProvider> GetProvider(HttpRequestMessage request)
        {
            if (!request.Content.IsMimeMultipartContent())
            {
                throw new InvalidOperationException(ErrorMessages.FileExpected);
            }

            string tempPath = Path.GetTempPath();
           
            var provider = new MultipartFormDataStreamProvider(tempPath);
            return request.Content.ReadAsMultipartAsync(provider);
        }

        private static int GetSize(string encodedData)
        {
            var bytes = Convert.FromBase64String(encodedData);

            return bytes.Length;
        }

        private static string EncodeDocumentContents(string documentName)
        {
            var stream = ReadChunks(documentName);

            var bytes = stream.ToArray();

            var encodedData = Convert.ToBase64String(bytes);
            return encodedData;
        }

        private static MemoryStream ReadChunks(string documentName)
        {
            var byteStream = new MemoryStream();
            
            string tempPath = Path.GetTempPath();
            var directoryName = Path.Combine(tempPath, documentName);
            var directory = new DirectoryInfo(directoryName);

            FileInfo[] files = directory.GetFiles().OrderBy(file => file.Name).ToArray();
            foreach (var file in files)
            {
                byte[] buffer = File.ReadAllBytes(file.FullName);
                byteStream.Write(buffer, 0, buffer.Length);
                File.Delete(file.FullName);
            }

            return byteStream;
        }

        private static bool IsLastChunk(NameValueCollection formData, dynamic settingsObject)
        {
            bool isLastChunk = false;

            int totalChunks = int.Parse(formData[settingsObject["totalChunks"].ToString()]);
            int currentChunk = int.Parse(formData[settingsObject["chunkNumber"].ToString()]);

            if (currentChunk == totalChunks)
                isLastChunk = true;

            return isLastChunk;
        }

        private void WriteChunk(string localName, string documentName, int chunkNumber)
        {
            string tempPath = Path.GetTempPath();
            var directoryName = Path.Combine(tempPath, documentName);
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
            var filePath = Path.Combine(directoryName, chunkNumber.ToString(CultureInfo.InvariantCulture));

            var fileInfo = new FileInfo(localName);
            if (fileInfo.Exists)
            {
                File.Move(localName, filePath);
            }
        }
    }
}
