﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http;
using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Documents;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;

namespace NDexed.Rest.Controllers
{
    public class DocumentSummaryController : ApiController
    {
        private readonly ISearchableRepository<Guid, DocumentSummary> m_Repository;

        public DocumentSummaryController(ISearchableRepository<Guid, DocumentSummary> repo)
        {
            Condition.Requires(repo).IsNotNull();

            m_Repository = repo;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid caseId)
        {

            var user = Request.GetRequestingUser();
            if (user.CaseIds.IndexOf(caseId) < 0)
            {
                throw new AuthenticationException("You are not authorized to view this case");
            }
            var summary = m_Repository.Search(caseId).FirstOrDefault();

            var response = Request.CreateResponse(HttpStatusCode.OK, summary);

            return response;
        }
    }
}
