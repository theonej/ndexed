﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using NDexed.Rest._extensions;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class ClientDetailsController : ApiController
    {
        private readonly IRepository<Guid, ClientInfo> m_Repository;

        public ClientDetailsController(IRepository<Guid, ClientInfo> repository)
        {
            Condition.Requires(repository).IsNotNull();

            m_Repository = repository;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid id)
        {
            var clientDetails = m_Repository.Get(id);

            var response = Request.CreateResponse(HttpStatusCode.OK, clientDetails);

            return response;
        }

        [HttpPut]
        [AuthorizationFilter]
        public HttpResponseMessage Update(ClientInfo client)
        {
            if (client.UpdatedDateTime == DateTime.MinValue)
            {
                client.UpdatedDateTime = DateTime.UtcNow;
            }
            client.UpdatedBy = Request.GetUserId().ToString();

            m_Repository.Add(client);

            var response = Request.CreateResponse(HttpStatusCode.Accepted, client.Id);

            return response;
        }

    }
}
