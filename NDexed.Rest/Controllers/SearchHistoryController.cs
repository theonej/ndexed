﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Search;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class SearchHistoryController : ApiController
    {
        private readonly ISearchableRepository<PatentSearchInfo, SearchInfo> m_SearchRepository;
        private readonly IRepository<Guid, SearchInfo> m_HistoryRepository;
        public SearchHistoryController(ISearchableRepository<PatentSearchInfo, SearchInfo> searchRepo,
                                       IRepository<Guid, SearchInfo> historyRepo)
        {
            Condition.Requires(searchRepo).IsNotNull();
            Condition.Requires(historyRepo).IsNotNull();

            m_SearchRepository = searchRepo;
            m_HistoryRepository = historyRepo;
        }

        [HttpPost]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Search(Guid id)
        {
            var criteria = new PatentSearchInfo();
            criteria.PatentSearchId = id;

            var historyList = m_SearchRepository.Search(criteria).OrderByDescending(item=>item.CreatedDateTime);
            
            var response = Request.CreateResponse(HttpStatusCode.OK, historyList);

            return response;
        }

        [HttpGet]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Get(Guid id)
        {
            var history = m_HistoryRepository.Get(id);

            var response = Request.CreateResponse(HttpStatusCode.OK, history);

            return response;
        }
    }
}
