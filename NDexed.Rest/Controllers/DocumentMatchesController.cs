﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Repositories;
using NDexed.DataAccess.Response;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Documents;
using NDexed.Elastic;
using NDexed.Rest._extensions;
using NDexed.Rest.Common;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class DocumentMatchesController : ApiController
    {
        private readonly IRepository<Guid, DocumentInfo> m_DocumentRepository;
        private readonly ISearchableRepository<DocumentSearchQuery, DocumentSearchResponse> m_SearchRepo;
        private readonly IRepository<Guid, TermInfo> m_TermRepo;

        public DocumentMatchesController(IRepository<Guid, DocumentInfo> documentRepository,
                                         ISearchableRepository<DocumentSearchQuery, DocumentSearchResponse> searchRepo,
                                         IRepository<Guid, TermInfo> termRepo)
        {
            Condition.Requires(searchRepo).IsNotNull();
            Condition.Requires(documentRepository).IsNotNull();
            Condition.Requires(termRepo).IsNotNull();

            m_DocumentRepository = documentRepository;
            m_SearchRepo = searchRepo;
            m_TermRepo = termRepo;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid id, int resultCount = 10)
        {
            var document = m_DocumentRepository.Get(id);

            if (document == null)
            {
                throw new NullReferenceException(string.Format("No document was found for ID {0}", id));
            }

            var query = new DocumentSearchQuery();
            query.PageNumber = 0;
            //we will use the command.MaxCount to filter how many results we send, but from the initial
            //search, we want a larger set to cull down using the lsi ranker
            query.PageSize = 200;
            query.Term = Utilities.ConvertHtml(document.Contents);

            var user = Request.GetRequestingUser();
            query.OrganizationId = user.OrganizationId;

            var matches = m_SearchRepo.Search(query);

            var rankedMatches = RankResponses(query.Term, resultCount, matches);

            //now pass the 
            var response = Request.CreateResponse(HttpStatusCode.OK, rankedMatches);

            return response;
        }

        #region Private Methods

        private List<DocumentSearchResponse> RankResponses(string searchTerm, int maxCount, IEnumerable<DocumentSearchResponse> matches)
        {
            var rankedResponses = new List<DocumentSearchResponse>();

            var searchTerms = SearchTermUtilities.GetDistinctTermsArray(searchTerm, m_TermRepo);

            foreach (var match in matches)
            {
                var dictionary = new List<string[]>();


                //if documents are less than page size, add dummy documents so we have a square matrix 
                //var dummy = new DocumentInfo();
                //dummy.Contents = "";
                //while (match.Results.Count < 200)
                //{
                //    match.Results.Add(dummy);
                //}

                foreach (var document in match.Results)
                {
                    var terms = SearchTermUtilities.GetDistinctTermsArray(searchTerm, m_TermRepo);
                    dictionary.Add(terms);
                }

                try
                {
                    var rankings = ndexed.lsi.lsiRanker.rank(searchTerms, dictionary.ToArray());

                    //rankings are assumed to be in the same order as the data we passed in
                    for (var rankIndex = 0; rankIndex < rankings.Length; rankIndex++)
                    {
                        var rank = rankings[rankIndex];
                        var document = match.Results[rankIndex];
                        var doubleString = string.Format("{0:0.00000}", rank);
                        document.Rank = double.Parse(doubleString);
                    }

                    var matchedDocuments = match.Results.ToList();

                    matchedDocuments.Sort((a, b) => {
                        return b.Rank.CompareTo(a.Rank);
                    });

                    //we only want the top x docs, and only if their rank is greater than 0
                    match.Results = matchedDocuments.Take(maxCount).Where(doc => doc.Rank > 0).ToList();

                    rankedResponses.Add(match);
                }
                catch(Exception ex)
                {
                    if(ex.Message.Contains("must not be singular"))
                    {
                        return matches.ToList();
                    }else
                    {
                        throw;
                    }
                }
                
            }
            return rankedResponses;
        }

        #endregion
    }
}
