﻿using CuttingEdge.Conditions;
using NDexed.AWS.Repository;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Search;
using NDexed.Google.Repository;
using NDexed.Rest._extensions;
using NDexed.Rest.Filters;
using NDexed.Rest.Resources;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class ImageController : ApiController
    {
        private const string IMAGE_SETTINGS_HEADER = "ImageSettings";

        private VisionAPIRepository m_VisionAPIRepo;

        public ImageController()
        {
            m_VisionAPIRepo = new VisionAPIRepository();
        }

        [HttpPost]
        [AuthorizationFilter]
        public async Task<HttpResponseMessage> Create()
        {
            var response = Request.CreateResponse(HttpStatusCode.Accepted);

            var user = Request.GetRequestingUser();

            MultipartFormDataStreamProvider provider = await GetProvider(Request);

            var settings = Request.GetHeaderValueAsString(IMAGE_SETTINGS_HEADER);
            dynamic settingsObject = JsonConvert.DeserializeObject(settings);

            string imageName = provider.FormData[settingsObject["imageName"].ToString()];
            
            var chunkNumber = int.Parse(provider.FormData[settingsObject["chunkNumber"].ToString()]);
            var isLastChunk = IsLastChunk(provider.FormData, settingsObject);

            WriteChunk(provider.FileData[0].LocalFileName, imageName, chunkNumber);

            if (isLastChunk)
            {
                var imagePath = WriteImageContents(imageName);

                var search = new SearchInfo();
                search.Query = imagePath;

                var results = m_VisionAPIRepo.Search(search);

                //image is now in s3, delete from server
                File.Delete(imagePath);

                response = Request.CreateResponse(HttpStatusCode.Created, results);
            }

            return response;
        }
        private static bool IsLastChunk(NameValueCollection formData, dynamic settingsObject)
        {
            bool isLastChunk = false;

            int totalChunks = int.Parse(formData[settingsObject["totalChunks"].ToString()]);
            int currentChunk = int.Parse(formData[settingsObject["chunkNumber"].ToString()]);

            if (currentChunk == totalChunks)
                isLastChunk = true;

            return isLastChunk;
        }

        private static string WriteImageContents(string imageName)
        {
            var stream = ReadChunks(imageName);

            var bytes = stream.ToArray();

            var tempPath = Path.GetTempFileName();
            File.WriteAllBytes(tempPath, bytes);

            return tempPath;
        }

        private static MemoryStream ReadChunks(string documentName)
        {
            var byteStream = new MemoryStream();

            string tempPath = Path.GetTempPath();
            var directoryName = Path.Combine(tempPath, documentName);
            var directory = new DirectoryInfo(directoryName);

            FileInfo[] files = directory.GetFiles().OrderBy(file => file.Name).ToArray();
            foreach (var file in files)
            {
                byte[] buffer = File.ReadAllBytes(file.FullName);
                byteStream.Write(buffer, 0, buffer.Length);
                File.Delete(file.FullName);
            }

            return byteStream;
        }
        private static Task<MultipartFormDataStreamProvider> GetProvider(HttpRequestMessage request)
        {
            if (!request.Content.IsMimeMultipartContent())
            {
                throw new InvalidOperationException(ErrorMessages.FileExpected);
            }

            string tempPath = Path.GetTempPath();

            var provider = new MultipartFormDataStreamProvider(tempPath);
            return request.Content.ReadAsMultipartAsync(provider);
        }

        private void WriteChunk(string localName, string documentName, int chunkNumber)
        {
            string tempPath = Path.GetTempPath();
            var directoryName = Path.Combine(tempPath, documentName);
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
            var filePath = Path.Combine(directoryName, chunkNumber.ToString(CultureInfo.InvariantCulture));

            var fileInfo = new FileInfo(localName);
            if (fileInfo.Exists)
            {
                File.Move(localName, filePath);
            }
        }
    }
}
