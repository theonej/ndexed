﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using NDexed.Domain.Models.User;
using NDexed.Messaging.Commands.CaseUsers;
using NDexed.Messaging.Handlers;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class CaseUsersController : ApiController
    {
        private readonly ICommandHandler<AddCaseUsersCommand> m_AddHandler;
        private readonly ICommandHandler<DeleteCaseUsersCommand> m_DeleteHandler;

        public CaseUsersController(ICommandHandler<AddCaseUsersCommand> addHandler,
                                   ICommandHandler<DeleteCaseUsersCommand> deleteHandler)
        {
            Condition.Requires(addHandler).IsNotNull();
            Condition.Requires(deleteHandler).IsNotNull();

            m_AddHandler = addHandler;
            m_DeleteHandler = deleteHandler;
        }

        [HttpPost]
        [ActionName("Add")]
        [AuthorizationFilter]
        public HttpResponseMessage Add(AddCaseUsersCommand command)
        {
            m_AddHandler.Handle(command);

            var response = Request.CreateResponse(HttpStatusCode.Accepted, command.Id);

            return response;
        }


        [HttpPost]
        [ActionName("Remove")]
        [AuthorizationFilter]
        public HttpResponseMessage Remove(DeleteCaseUsersCommand command)
        {
            m_DeleteHandler.Handle(command);

            var response = Request.CreateResponse(HttpStatusCode.Accepted, command.Id);

            return response;
        }
    }
}
