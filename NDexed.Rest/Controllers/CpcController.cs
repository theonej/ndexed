﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Search;
using NDexed.Rest.Filters;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class CpcController : ApiController
    {
        private readonly ISearchableRepository<CpcSearchInfo, Dictionary<string, string>> m_SearchRepo;

        public CpcController(ISearchableRepository<CpcSearchInfo, Dictionary<string, string>> searchRepo)
        {
            Condition.Requires(searchRepo).IsNotNull();

            m_SearchRepo = searchRepo;
        }

        [AuthorizationFilter]
        [ExceptionFilter]
        [HttpPost]
        public HttpResponseMessage Search(CpcSearchInfo criteria)
        {
            var results = m_SearchRepo.Search(criteria);

            var response = Request.CreateResponse(HttpStatusCode.OK, results);

            return response;
        }
    }
}
