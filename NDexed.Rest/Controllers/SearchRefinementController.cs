﻿using CuttingEdge.Conditions;
using NDexed.Domain.Models.Patents;
using NDexed.Messaging.Commands;
using NDexed.Messaging.Handlers;
using NDexed.Rest.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NDexed.Rest.Controllers
{
    public class SearchRefinementController : ApiController
    {
        private readonly IReturnCommandHandler<RefinePatentSearchCommand, List<PatentInfo>> m_RefinementHandler;

        public SearchRefinementController(IReturnCommandHandler<RefinePatentSearchCommand, List<PatentInfo>> refinementHandler)
        {
            Condition.Requires(refinementHandler).IsNotNull();

            m_RefinementHandler = refinementHandler;
        }

        [HttpPut]
        [AuthorizationFilter]
        [ExceptionFilter]
        public HttpResponseMessage Put(Guid patentSearchId)
        {
            var command = new RefinePatentSearchCommand();
            command.PatentSearchInfoId = patentSearchId;

            Task.Factory.StartNew(()=> { m_RefinementHandler.Handle(command); });

            var response = Request.CreateResponse(HttpStatusCode.Accepted, patentSearchId);

            return response;
        }
    }
}
