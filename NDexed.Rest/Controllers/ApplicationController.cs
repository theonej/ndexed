﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using NDexed.Messaging.Commands.Applications;
using NDexed.Messaging.Handlers;
using NDexed.Rest.Filters;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NDexed.Rest._extensions;
using System.Threading.Tasks;
using System.Threading;

namespace NDexed.Rest.Controllers
{
    public class ApplicationController : ApiController
    {
        private readonly IRepository<Guid, ApplicationInfo> m_Repo;
        private readonly ICommandHandler<ImportApplicationCommand> m_Handler;

        public ApplicationController(IRepository<Guid, ApplicationInfo> repo,
                                     ICommandHandler<ImportApplicationCommand> handler)
        {
            m_Repo = repo;
            m_Handler = handler;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get()
        {
            var applications = m_Repo.GetAll();

            var response = Request.CreateResponse(HttpStatusCode.OK, applications);

            return response;
        }


        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid id)
        {
            var application = m_Repo.Get(id);

            var response = Request.CreateResponse(HttpStatusCode.OK, application);

            return response;
        }

        [HttpPost]
        [AuthorizationFilter]
        public HttpResponseMessage Post(ApplicationInfo application)
        {
            var command = new ImportApplicationCommand();
            command.Abstract = application.Abstract;
            command.ApplicationName = application.Name;
            command.EmailAddress = Request.GetRequestingUser().EmailAddress;
            command.Id = Guid.NewGuid();

            new Thread(() =>
            {
                m_Handler.Handle(command);
            }).Start();
            

            var response = Request.CreateResponse(HttpStatusCode.Created, command.Id);

            return response;
        }

        [HttpDelete]
        [AuthorizationFilter]
        public HttpResponseMessage Delete(Guid id)
        {
            var application = m_Repo.Get(id);
            if(application!= null)
            {
                m_Repo.Remove(application);
            }
            var response = Request.CreateResponse(HttpStatusCode.Accepted);

            return response;
        }
    }
}
