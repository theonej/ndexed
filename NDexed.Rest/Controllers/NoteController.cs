﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Web.Http;
using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using NDexed.Rest.Filters;
using NDexed.Rest._extensions;
using NDexed.Security;

namespace NDexed.Rest.Controllers
{
    public class NoteController : ApiController
    {
        private readonly IRepository<Guid, NoteInfo> m_Repository;
        private readonly ISearchableRepository<Guid, NoteInfo> m_SearchRepository;
        private readonly IEncryptor m_Encryptor;

        public NoteController(IRepository<Guid, NoteInfo> repository,
                              ISearchableRepository<Guid, NoteInfo> searchRepository,
                              IEncryptor encryptor)
        {
            Condition.Requires(repository).IsNotNull();
            Condition.Requires(searchRepository).IsNotNull();
            Condition.Requires(encryptor).IsNotNull();

            m_Repository = repository;
            m_SearchRepository = searchRepository;
            m_Encryptor = encryptor;
        }

        [HttpPost]
        [AuthorizationFilter]
        public HttpResponseMessage Add(NoteInfo note)
        {
            if (note.Id == Guid.Empty)
            {
                note.Id = Guid.NewGuid();
            }

            if (note.CreatedDateTime == DateTime.MinValue)
            {
                note.CreatedDateTime = DateTime.UtcNow;
            }
            note.CreatedBy = Request.GetUserId().ToString();
            note.Message = m_Encryptor.EncryptValue(note.Message);

            m_Repository.Add(note);

            var response = Request.CreateResponse(HttpStatusCode.Created, note.Id);

            return response;
        }

        [HttpGet]
        [AuthorizationFilter]
        public HttpResponseMessage Get(Guid caseId)
        {
            var user = Request.GetRequestingUser();
            if (user.CaseIds.IndexOf(caseId) < 0)
            {
                throw new AuthenticationException("You are not authorized to view this case");
            }

            var caseNotes = m_SearchRepository.Search(caseId).ToList();
            foreach (var note in caseNotes)
            {
                note.Message = m_Encryptor.DecryptValue(note.Message);
            }
            var response = Request.CreateResponse(HttpStatusCode.OK, caseNotes);

            return response;
        }

        [HttpDelete]
        [AuthorizationFilter]
        public HttpResponseMessage Remove(Guid caseId, Guid id)
        {
            var user = Request.GetRequestingUser();
            if (user.CaseIds.IndexOf(caseId) < 0)
            {
                throw new AuthenticationException("You are not authorized to view this case");
            }

            var note = m_Repository.Get(id);
            m_Repository.Remove(note);

            var response = Request.CreateResponse(HttpStatusCode.OK, id);

            return response;
        }
    }
}
