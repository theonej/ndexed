﻿using NDexed.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Security.Authentication;
using System.Web;
using System.Web.Http.Filters;
using NDexed.Rest._extensions;
using NDexed.Domain.Models.User;
using NDexed.DataAccess.Repositories;

namespace NDexed.Rest.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var exception = GetException(context.Exception);

            LogException(context);

            Type exceptionType = exception.GetType();
            if (exceptionType == typeof(HttpException))
            {
                HttpStatusCode code = (HttpStatusCode)((HttpException)exception).GetHttpCode();
                context.Response = context.Request.CreateErrorResponse(code, exception.Message);
            }
            else
            {
                context.Response = context.Request.CreateErrorResponse(System.Net.HttpStatusCode.InternalServerError, exception.Message);

                Dictionary<Type, HttpStatusCode> codes = new Dictionary<Type, HttpStatusCode>();
                codes.Add(typeof(FormatException), HttpStatusCode.Unauthorized);
                codes.Add(typeof(SecurityException), HttpStatusCode.Unauthorized);
                codes.Add(typeof(AuthenticationException), HttpStatusCode.Unauthorized);
                codes.Add(typeof(MissingFieldException), HttpStatusCode.NotFound);
                codes.Add(typeof(HttpException), context.Response.StatusCode);

                if (codes.ContainsKey(exceptionType))
                {
                    context.Response = context.Request.CreateErrorResponse(codes[exceptionType], exception.Message);
                }
            }
            base.OnException(context);
        }

        #region Private Methods

        private Exception GetException(Exception ex)
        {
            var returnValue = ex;
            if(ex.InnerException != null)
            {
                returnValue = GetException(ex.InnerException);
            }

            return returnValue;
        }

        private void LogException(HttpActionExecutedContext context)
        {
            var exception = context.Exception;
            string[] authExceptionMessages = new string[] 
            {
                "The authorization token is expired",
                "You are not authorized to perform this action",
                "The authorization token is malformed.  Cannot authorize user."
            };
            if (!authExceptionMessages.Contains(exception.Message))
            {
                var requestingUser = context.Request.GetRequestingUser();
                if (requestingUser == null)
                {
                    requestingUser = new UserInfo();
                }
                var logMessage = new LogInfo();
                logMessage.CreatedBy = requestingUser.UserName.ToString();
                logMessage.CreatedDateTime = DateTime.Now;
                logMessage.Message = string.Format("{0} --- {1}", context.Exception.Message, context.Exception.StackTrace);

                logMessage.OrganizationId = requestingUser.OrganizationId;
                logMessage.Source = context.Request.RequestUri.AbsoluteUri;
                logMessage.Timestamp = DateTime.Now;

                IRepository<Guid, LogInfo> logRepo = WebApiConfig.Container.GetService(typeof(IRepository<Guid, LogInfo>)) as IRepository<Guid, LogInfo>;
                if (logRepo != null)
                {
                    logRepo.Add(logMessage);
                }
            }
        }

        #endregion
    }
}