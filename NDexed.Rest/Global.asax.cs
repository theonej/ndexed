﻿using NDexed.AWS.SQS;
using NDexed.Messaging.Queues;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NDexed.Rest
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var logFilePath = ConfigurationManager.AppSettings["LogPath"];
            if(!string.IsNullOrEmpty(logFilePath))
            {
                var writer = File.AppendText(logFilePath);
                Console.SetOut(writer);
            }

            var messageProcessor = WebApiConfig.Container.GetService(typeof(IMessageQueueProcessor<SqsMessage>)) as IMessageQueueProcessor<SqsMessage>;
            if(messageProcessor == null)
            {
                throw new NullReferenceException("No message processor was registered. Cannot proceed.");
            }

            //var task = ProcessMessages(messageProcessor);
        }

        private async Task ProcessMessages(IMessageQueueProcessor<SqsMessage> messageProcessor)
        {
            var action = new Action(messageProcessor.ProcessNext);

            var task = new Task(action);
            task.Start();

            await task.ContinueWith<Task>(previous => { return ProcessMessages(messageProcessor); });
        }
    }
}