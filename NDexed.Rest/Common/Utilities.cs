﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace NDexed.Rest.Common
{
    public static class Utilities
    {
        public static string[] GetDistinctTerms(string terms, bool convertHtml = true)
        {
            var converted = convertHtml?ConvertHtml(terms): terms;

            var termRepo = new CouchDb.Repositories.CouchTermsRepository();
            
            var termstoRemove = termRepo.GetAll().Select(item => item.Value);

            StringBuilder builder = new StringBuilder();
            builder.Append(terms);

            foreach (var term in termstoRemove)
            {
                //we pad the term with spaces on each side so that we don't accidentaly remove parts of words
                builder = builder.Replace(string.Format(" {0} ", term), " ");
            }

            builder = builder.Replace("\n", " ");
            builder = builder.Replace("\\n", " ");
            builder = builder.Replace("\t", " ");
            builder = builder.Replace("\\t", " ");
            builder = builder.Replace("\r", " ");
            builder = builder.Replace("\\r", " ");
            builder = builder.Replace(@"\", " ");
            builder = builder.Replace("\\", " ");
            builder = builder.Replace("_", " ");
            
            //remove html tags
            var stringToSplit = Regex.Replace(builder.ToString(), "<.*?>", string.Empty);
            var orderedTerms = stringToSplit.Split(' ').Distinct().OrderByDescending(word => word.Length).Take(200).ToArray();
          //  var orderedTerms = stringToSplit.Split(' ').Distinct().Take(200).ToArray();
            return orderedTerms;
        }

        public static string ConvertHtml(string html)
        {
            try
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);

                StringWriter sw = new StringWriter();
                ConvertTo(doc.DocumentNode, sw);
                sw.Flush();
                return sw.ToString();
            }catch
            {
                return html;
            }
        }

        private static void ConvertTo(HtmlNode node, TextWriter outText)
        {
            string html;
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    html = ((HtmlTextNode)node).Text;

                    // is it in fact a special closing node output as text?
                    if (HtmlNode.IsOverlappedClosingElement(html))
                        break;

                    // check the text is meaningful and not a bunch of whitespaces
                    if (html.Trim().Length > 0)
                    {
                        outText.Write(HtmlEntity.DeEntitize(html));
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            outText.Write("\r\n");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }

        private static void ConvertContentTo(HtmlNode node, TextWriter outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }
        
    }
}