﻿const { graphql, buildSchema, GraphQLObjectType, GraphQLString, GraphQLSchema, GraphQLList } = require('graphql');

const searchClient = require('./searchClient');
const util = require('util');

const patentSearchResultType = new GraphQLObjectType({
    name: 'patentSearchResult',
    description: 'Representation of patent data for a search',
    fields: () => ({
        type: { type: GraphQLString },
        number: { type: GraphQLString },
        country: { type: GraphQLString },
        date: { type: GraphQLString },
        abstract: { type: GraphQLString },
        title: { type: GraphQLString },
        kind: { type: GraphQLString },
        num_claims: { type: GraphQLString },
        patentNumber: { type: GraphQLString },
        link: { type: GraphQLString },
        description: { type: GraphQLString },
        score: { type: GraphQLString },
        filename: { type: GraphQLString }
    })
});

const root = new GraphQLObjectType({
    name: 'searchQuery',
    description: 'The schema that defines searches',
    fields: () => ({
        crossReference: {
            type: GraphQLList(patentSearchResultType),
            description: 'cross references multiple searches',

            resolve: () => {
                const request = {
                    previous_searches: [
                        'personal computer that fits on your lap',
                        'lap dog'
                    ]
                };
                
               
                return new Promise((resolve, reject) => {
                    searchClient.crossReference(request, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result.results);
                        }
                    });
                });
            }
        },
        cpcSearch: {
            type: GraphQLList(patentSearchResultType),
            description: 'search within a set of cpcs',

            resolve: () => {
                const request = {
                    cpcs: ['C08L89/00'],
                    take: 10,
                    skip:0
                };


                return new Promise((resolve, reject) => {
                    searchClient.cpcSearch(request, (err, result) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        } else {
                            resolve(result.results);
                        }
                    });
                });
            }
        },
        termsSearch: {
            type: GraphQLList(patentSearchResultType),
            description: 'search using included and excluded terms',

            resolve: () => {
                const request = {
                    criteria: {
                        included: ['shifted'],
                        excluded: ['wavelength'],
                        take: 10,
                        skip: 0
                    }
                };


                return new Promise((resolve, reject) => {
                    searchClient.termsSearch(request, (err, result) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        } else {
                            resolve(result.results);
                        }
                    });
                });
            }
        }
    })
});

const schema = new GraphQLSchema({
    query: root
});

const query = `{
        termsSearch{
            patentNumber
            filename
            type
            description
        }
    }`;

graphql(schema, query, root).then((response) => {
    console.log(['response', response.errors || response.data]);
}).catch((err) => {
    console.error(err);
});
