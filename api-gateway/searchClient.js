﻿const PROTO_PATH = '../ndexed-protocols/search-service.proto';
const grpc = require('grpc');
const proto_descriptor = grpc.load(PROTO_PATH);
const search = proto_descriptor.search;

var client = new search('localhost:8000', grpc.credentials.createInsecure());

module.exports = {
    crossReference: function (request, callback) {
        client.crossReference(request, (err, response) => {
            callback(err, response);
        });
    },
    cpcSearch: function (request, callback) {
        
        client.cpcSearch(request, (err, response) => {
            callback(err, response);
        });
    },
    termsSearch: function (request, callback) {

        client.termsSearch(request, (err, response) => {
            callback(err, response);
        });
    }
}