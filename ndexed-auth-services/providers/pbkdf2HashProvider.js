﻿const crypto = require('crypto');
const q = require('q');

const secret = process.env.SECRET_KEY;

exports.generate_hash = function (value, salt) {
    var deferred = q.defer();

    crypto.pbkdf2(secret, salt, 100000, 64, 'sha512', (err, derivedKey) => {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(derivedKey.toString('hex'));
        }
    });

    return deferred.promise;
};
