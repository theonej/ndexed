﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Google.Repository;
using NDexed.Domain.Models.Search;
using System.Collections.Generic;
using NDexed.Domain.Models.Matching;
using System.Configuration;

namespace NDexed.Search.Tests.Repositories
{
    [TestClass]
    public class VisionAPIRepositoryTests
    {
        [TestMethod]
        public void SearchForKnownImage()
        {
            var imagePath = @"c:\data\1067110a-6c09-4d23-9092-0d905b3c064c";

            var repo = new VisionAPIRepository();

            var search = new SearchInfo();
            search.Query = imagePath;
            
            var result = repo.Search(search) as List<MatchInfo>;

            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count);
        }
    }
}
