﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Search;
using NDexed.Search.Repositories;
using System.Linq;

namespace NDexed.Search.Tests.Repositories
{
    [TestClass]
    public class GoogleClaimsRepositoryTests
    {
        [TestMethod]
        public void SearchForPatentsAndGetRelatedResults()
        {
            var searchInfo = new SearchInfo()
               {
                    AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o",
                    ProviderId = "000375274410958331611:ealp1cwjuyo",
                    Query = "Over-voltage protection device which includes a housing holding a first electrode and a second electrode having a first air breakdown spark gap established therebetween such that an arc, caused by a surge current, is formed in the first air breakdown spark gap, comprising:"
            };

            var repo = new GoogleClaimsRepository();
            var result = repo.Search(searchInfo);
            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count());
        }
    }
}
