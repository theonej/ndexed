﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Search;
using NDexed.Search.Repositories;
using System.Linq;

namespace NDexed.Search.Tests.Repositories
{
    [TestClass]
    public class GoogleRepositoryTests
    {
        [TestMethod]
        public void SearchForPatentsAndGetRelatedResults()
        {
            var searchInfo = new SearchInfo()
               {
                    AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o",
                    ProviderId = "000375274410958331611:ealp1cwjuyo",
                    Query = "Test"
            };

            var repo = new GoogleRepository();
            var result = repo.Search(searchInfo);
            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count());
        }
    }
}
