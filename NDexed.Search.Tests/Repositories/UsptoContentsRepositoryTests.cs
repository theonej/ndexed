﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Search.Repositories;
using System.Linq;

namespace NDexed.Search.Tests.Repositories
{
    [TestClass]
    public class UsptoContentsRepositoryTests
    {
        [TestMethod]
        public void GetPatentContentsFromNumber()
        {
            string patentNumber = "9009250";

            var repo = new UsptoContentsRepository();
            var patentContents = repo.Search(patentNumber).First();

            Assert.IsNotNull(patentContents.Description);

        }
    }
}
