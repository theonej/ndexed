﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using NDexed.Search.Analyzers;

namespace NDexed.Search.Tests
{
    [TestClass]
    public class DocumentSimilarityAnalyzerTests
    {
        [TestMethod]
        public void GetDocumentScoresAndSummaryScore()
        {
            var sourceDocuments = new string[] {
                                                    "Described herein are cannabidiol prodrugs, methods of making cannabidiol prodrugs, formulations comprising cannabidiol prodrugs and methods of using cannabidiols. One embodiment described herein relates to the transdermal or topical administration of a cannabidiol prodrug for treating and preventing diseases and/or disorders."
            }.ToList();

            var targetDocuments = new string[] {
                                                    "The method of claim 3 wherein the cannabidiol prodrug is administered transdermally. ",
                                                    "The method of claim 3 wherein the cannabidiol prodrug is administered topically.",
                                                    "The method of claim 1, wherein the cannabidiol compound is cannabidiol (CBD). ",
                                                    "The method of claim 1, wherein the cannabidiol compound is the dimethyl heptyl homolog of cannabidiol (CBD-DMH)."
            }.ToList();

            var documentScores = DocumentSimilarityAnalyzer.GetSourceDocumentsScores(sourceDocuments, targetDocuments);
            Assert.IsNotNull(documentScores);

            Assert.AreEqual(documentScores.Score, 0.31329356762009f);

            Assert.AreEqual(documentScores.DocumentScores[0], 0.40638393699281f);
            Assert.AreEqual(documentScores.DocumentScores[1], 0.40638393699281f);
            Assert.AreEqual(documentScores.DocumentScores[2], 0.252301582553377f);
            Assert.AreEqual(documentScores.DocumentScores[3], 0, 188104813941385f);
        }

        [TestMethod]
        public void GetDocumentTfIdfs()
        {
            var sourceDocuments = new string[] {
                                                    "The method of claim 3 wherein the cannabidiol prodrug is administered transdermally. ",
                                                    "The method of claim 3 wherein the cannabidiol prodrug is administered topically.",
                                                    "The method of claim 1, wherein the cannabidiol compound is cannabidiol (CBD). ",
                                                    "The method of claim 1, wherein the cannabidiol compound is the dimethyl heptyl homolog of cannabidiol (CBD-DMH)."
            }.ToList();

            var terms = DocumentSimilarityAnalyzer.GetDocumentsTfIdf(sourceDocuments);
            Assert.IsNotNull(terms);
            Assert.AreEqual(21, terms.Count);

            terms = terms.OrderByDescending(item => item.Score).ToList();
            var maxScore = terms.First().Score;
            var minScore = terms.Last().Score;
            var difference = maxScore - minScore;

            var topTenPercent = terms.Where(item=>item.Score >= (difference * .9f))
                                     .ToList();
            Assert.AreEqual(1, topTenPercent.Count);


            var topFiftyPercent = terms.Where(item => item.Score >= (difference * .5f))
                                     .ToList();
            Assert.AreEqual(6, topFiftyPercent.Count);
        }

        [TestMethod]
        public void GetDocumentStems()
        {
            var sourceDocuments = new string[] {
                                                    "The method of claim 3 wherein the cannabidiol prodrug is administered transdermally. ",
                                                    "The method of claim 3 wherein the cannabidiol prodrug is administered topically.",
                                                    "The method of claim 1, wherein the cannabidiol compound is cannabidiol (CBD). ",
                                                    "The method of claim 1, wherein the cannabidiol compound is the dimethyl heptyl homolog of cannabidiol (CBD-DMH)."
            }.ToList();

            var terms = DocumentSimilarityAnalyzer.GetDocumentStems(sourceDocuments);
            Assert.IsNotNull(terms);
            
        }
    }
}
