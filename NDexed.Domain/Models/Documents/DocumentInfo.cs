﻿using System;

namespace NDexed.Domain.Models.Documents
{
    public enum DocumentTypes
    {
        Private = 0,
        Public = 1
    }
    public class DocumentInfo : AuditInfo
    {
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }
        public Guid OrganizationId { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public string Base64EncodedData { get; set; }
        public int Size { get; set; }
        public string Contents { get; set; }
        public DocumentTypes Type { get; set; }

        public bool Indexed { get; set; }

        public double Rank { get; set; }
        public string[] Tags { get; set; }
    }
}
