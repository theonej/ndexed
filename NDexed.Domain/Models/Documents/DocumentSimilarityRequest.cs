﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Documents
{
    public class DocumentSimilarityRequest
    {
        public string Document1Contents { get; set; }
        public string Document2Contents { get; set; }

        public List<string> DocumentContents { get; set; }
    }
}
