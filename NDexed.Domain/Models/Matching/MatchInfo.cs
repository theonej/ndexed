﻿using System.Collections.Generic;

namespace NDexed.Domain.Models.Matching
{
    public class MatchInfo
    {
        public MatchInfo()
        {
            ContentEntries = new List<string>();
        }

        public string MatchType { get; set; }
        public dynamic Data { get; set; } 
        public decimal Rank { get; set; }
        public string Contents { get; set; }
        public List<string> ContentEntries { get; set; }
        
    }
}
