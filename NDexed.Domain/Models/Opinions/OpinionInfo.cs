﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Opinions
{
    public class OpinionInfo : AuditInfo
    {
        public string id { get; set; }
        public string jurdisdiction { get; set; }
        public string court { get; set; }
        public string judge { get; set; }
        public string summary { get; set; }
        public string parties{get;set;}
        public string date { get; set; }
        public string docket { get; set; }
        public string resource_uri { get; set; }
        public string absolute_url { get; set; }
        public string cluster { get; set; }
        public string author { get; set; }
        public List<object> joined_by { get; set; }
        public bool per_curiam { get; set; }
        public string date_created { get; set; }
        public string date_modified { get; set; }
        public string type { get; set; }
        public string sha1 { get; set; }
        public object page_count { get; set; }
        public string download_url { get; set; }
        public string local_path { get; set; }
        public string plain_text { get; set; }
        public string html { get; set; }
        public string html_lawbox { get; set; }
        public object html_columbia { get; set; }
        public string html_with_citations { get; set; }
        public bool extracted_by_ocr { get; set; }
        public List<string> opinions_cited { get; set; }

        public double rank { get; set; }

        #region Public Methods

        public void SetAdditionalFields()
        {
            parties = GetContents("<p class=\"parties\">", "</p>", html);
            court = GetContents("<p class=\"court\">", "</p>", html);
            docket = GetContents("<p class=\"docket\">", "</p>", html);
            judge = GetContents("<p class=\"judge\">", "</p>", html);
            date = GetContents("<p class=\"date\">", "</p>", html);
            
            summary = GetSummary(html);
        }

        #endregion

        #region Private Methods

        private string GetContents(string startTag, string endTag, string html)
        {
            string returnValue = string.Empty;

            int start = html.IndexOf(startTag);
            if (start >= 0)
            {
                int end = html.IndexOf(endTag, start);
                int length = end - start;
                if ((start < end) && (length <= html.Length && (start >= 0) && (end <= html.Length)))
                {
                    returnValue = html.Substring(start, length);
                    returnValue = Regex.Replace(returnValue, "<.*?>", String.Empty);
                }
            }
            return returnValue;
        }

        private string GetSummary(string html)
        {
            int size = html.Length >= 500 ? 500 : html.Length;

            return html.Substring(0, size);
        }
        #endregion
    }
}
