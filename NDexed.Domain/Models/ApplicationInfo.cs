﻿using NDexed.Domain.Models.Matching;
using NDexed.Domain.Models.Patents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models
{
    public class ApplicationInfo :AuditInfo
    {
        public Guid Id { get; set; }
        public Guid OrganizationId { get; set; }
        public string Name { get; set; }
        public string Abstract { get; set; }
        public string Description { get; set; }
        public DateTime FilingDate { get; set; }

        public string Status { get; set; }

        public List<PatentInfo> TopMatches { get; set; }
        public List<MatchInfo> ClaimMatches { get; set; }

        public List<string> TopClassifications { get; set; }
    }
}
