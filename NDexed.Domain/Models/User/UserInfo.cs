﻿using System;
using System.Collections.Generic;
using NDexed.Domain.Models.Security;

namespace NDexed.Domain.Models.User
{
    public enum UserType
    {
        Customer = 0,
        Admin = 1
    }

    public class UserInfo : AuditInfo
    {
        const float DEFAULT_RESULT_SCORE_THRESHOLD = 0.165f;

        private float m_ScoreResultThreshold = float.MinValue;

        public Guid Id { get; set; }
        public UserType UserType { get; set; }
        public Guid OrganizationId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string PasswordHash { get; set; }
        public List<GroupInfo> Membership { get; set; }
        public CustomerInfo ExternalCustomerInfo { get; set; }
        public UserApplicationInfo ApplicationInfo { get; set; }
        public string SearchCredentialsId { get; set; }
        public string SearchCredentialsPassword { get; set; }
        public List<Guid> CaseIds { get; set; }

        public float ScoreResultThreshold
        {   get
            {
                if(m_ScoreResultThreshold == float.MinValue)
                {
                    m_ScoreResultThreshold = DEFAULT_RESULT_SCORE_THRESHOLD;
                }
                return m_ScoreResultThreshold;
            }
            set { m_ScoreResultThreshold = value; }
        }

        public UserInfo()
        {
            CaseIds = new List<Guid>();
        }
    }
}
