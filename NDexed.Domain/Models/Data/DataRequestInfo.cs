﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Data
{
    public class DataRequestInfo :AuditInfo
    {
        public Dictionary<string, object> RequestParameters { get; set; }
    }
}
