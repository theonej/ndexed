﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Data
{
    public class TermInfo : AuditInfo
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public bool Include { get; set; }
    }
}
