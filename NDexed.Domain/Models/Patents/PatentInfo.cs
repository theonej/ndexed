﻿using NDexed.Domain.Models.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Patents
{
    public class PatentInfo :AuditInfo
    {
        public enum Sources
        {
            Internal = 0,
            External = 1
        }

        private string m_Description;

        public string PatentNumber { get; set; }
        public string ApplicationNumber { get; set; }
        public string ArtUnit { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public string Kind { get; set; }
        public string Link { get; set; }
        public string Abstract { get; set; }
        public float AbstractRank { get; set; }
        public string Highlight { get; set; }
        public string Description
        {
            get
            {
                return m_Description == null 
                    ? 
                    string.Empty 
                    :
                    Clean(m_Description);
            }
            set { m_Description = value; }
        }

        public float OverallRank { get; set; }
        public float DescriptionRank { get; set; }
        public List<ClaimInfo> Claims { get; set; }
        public List<CitationInfo> Citations { get; set; }

        public List<DrawingInfo> Drawings { get; set; }
        public float Rank { get; set; }

        public List<CpcInfo> Cpc { get; set; }

        private string Clean(string input)
        {
            return input
                .Replace("\"\"", "")
                .Replace("\n", "")
                .Replace("\r", "");
        }
        
        public Sources Source { get; set; }
        public List<SearchTermInfo> Tags { get; set; }


        public PatentInfo()
        {
            Claims = new List<ClaimInfo>();
            Citations = new List<CitationInfo>();
            Drawings = new List<DrawingInfo>();
            Cpc = new List<CpcInfo>();
        }
    }
}
