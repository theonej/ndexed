﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Patents
{
    public class ClassificationInfo
    {
        public string CPCSection { get; set; }
        public string Class { get; set; }
        public string Subclass { get; set; }
        public string MainGroup { get; set; }
        public string Subgroup { get; set; }
        public string SymbolPositionCode { get; set; }
        public string CPCClassificationValueCode { get; set; }
        public string itemName { get; set; }
    }
}
