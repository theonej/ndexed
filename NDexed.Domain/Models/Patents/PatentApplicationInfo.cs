﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Patents
{
    public class PatentApplicationInfo :PatentInfo
    {
        public bool Granted { get; set; }
    }
}
