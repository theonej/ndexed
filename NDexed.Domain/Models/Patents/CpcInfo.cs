﻿
namespace NDexed.Domain.Models.Patents
{
    public class CpcInfo :  AuditInfo
    {
        public string PatentNumber { get; set; }
        public string Section { get; set; }
        public string Subsection { get; set; }
        public string Group { get; set; }
        public string SubGroup { get; set; }
        public string Category { get; set; }
        public int Sequence { get; set; }
    }
}
