﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Patents
{
    /// <summary>
    /// for the record, I tried to have this inherit from Patent, but the framework
    /// puked on me when I tried to cast it, so fuck it.
    /// </summary>
    public class CitationInfo
    {
        public string PatentNumber { get; set; }
        public string Title { get; set; }
        public string Abstract { get; set; }
        public string CitedPatentNumber { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Category { get; set; }
        public int Sequence { get; set; }

    }
}
