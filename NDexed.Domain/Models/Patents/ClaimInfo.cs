﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Patents
{
    public class ClaimInfo :AuditInfo
    {
        public string PatentNumber { get; set; }
        public string Claim { get; set; }
        public float ClaimRank { get; set; }
        public float Sequence { get; set; }
    }
}
