﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Patents
{
    public class DrawingInfo
    {
        public string Description { get; set; }
        public string Url { get; set; }
        public float DescriptionRank { get; set; }
        public float ImageRank { get; set; }
    }
}
