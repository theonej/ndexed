﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Regulations
{
    public class RegulationInfo : AuditInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Contents { get; set; }
        public string Base64EncodedData { get; set; }
        public double rank { get; set; }

        public string[] Tags { get; set; }
    }
}
