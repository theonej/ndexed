﻿
using System;

namespace NDexed.Domain.Models
{
    public class LogInfo : AuditInfo
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
        public string Source { get; set; }
        public Guid OrganizationId { get; set; }
    }
}
