﻿
using System;

namespace NDexed.Domain.Models.Cases
{
    public class ClientInfo : AuditInfo 
    {
        public Guid Id { get; set; }
        public Guid OrganizationId { get; set; }
        public string Name { get; set; }
        public Address BillingAddress { get; set; }
        public Address MailingAddress { get; set; }
        public string OfficeNumber { get; set; }
        public string MobileNumber { get; set; }
        public string HomeNumber { get; set; }
    }
}
