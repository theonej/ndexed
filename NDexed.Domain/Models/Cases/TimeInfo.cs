﻿using System;

namespace NDexed.Domain.Models.Cases
{
    public enum TimeTypes
    {
        Billable = 0,
        NonBillable = 1,
        NoCharge = 2
    }

    public enum TimeUnits
    {
        Minutes = 0,
        Hours = 1
    }
        
    public class TimeInfo : AuditInfo
    {
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }
        public double Hours { get; set; }
        public int Units { get; set; }
        public TimeUnits Unit { get; set; }
        public TimeTypes Type { get; set; }
        public double Rate { get; set; }
        public string Description { get; set; }
    }
}
