﻿using System;

namespace NDexed.Domain.Models.Cases
{
    public class NoteInfo : AuditInfo
    {
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }
        public string Message { get; set; }
    }
}
