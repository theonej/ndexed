﻿using System;
using System.Collections.Generic;

namespace NDexed.Domain.Models.Cases
{
    public enum CourtTypes
    {
        State =0, 
        Federal =1,
        Tax = 2,
        Bankruptcy = 3,
        Maritime = 4
    }

    public class CaseInfo : AuditInfo
    {
        public Guid Id { get; set; }
        public ClientInfo Client { get; set; }
        public string DocketNumber { get; set; }
        public string Court { get; set; }
        public CourtTypes CourtType { get; set; }
        public string SubjectMatter { get; set; }
        public Guid OrganizationId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ClosedDate { get; set; }
        public DateTime FilingDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Plantif { get; set; }
        public string Defendant { get; set; }
        public string Respondent { get; set; }
        public bool IsDeleted { get; set; }

        public List<Guid> UserIds { get; set; }
    }
}
