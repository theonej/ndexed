﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Prediction
{
    public class PredictionService
    {
        public PredictionService(string endpoint, string modelId)
        {
            Endpoint = endpoint;
            ModelId = modelId;
        }

        public string Endpoint { get; set; }
        public string ModelId { get; set; }

        public Dictionary<string, string> Features { get; set; }
    }
}
