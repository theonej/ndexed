﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Prediction
{
    public class PredictionInfo
    {
        public PredictionInfo()
        {
            Children = new Dictionary<string, PredictionInfo>();
            Definitions = new List<PredictionService>();
            Predictions = new Dictionary<string, float>();
        }

        public PredictionInfo Parent { get; set; }
        public List<PredictionService> Definitions { get; set; }

        public string PredictedLabel { get; set; }
        public string PredictedValue { get; set; }
        public Dictionary<string, float> Predictions { get; set; }
        public Dictionary<string, PredictionInfo> Children { get; set; }
        public string GetPredictedValue()
        {
            var predictedValue = PredictedLabel;
            if (Parent != null)
            {
                predictedValue = string.Format("{0} {1}", Parent.GetPredictedValue(), predictedValue);
            }

            return predictedValue;
        }
    }
}
