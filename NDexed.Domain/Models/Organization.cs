﻿using System;
using System.Collections.Generic;
using NDexed.Domain.Models.User;


namespace NDexed.Domain.Models
{
    public class Organization : AuditInfo
    {
        public Guid OrganizationId { get; set; }
        public string Name { get; set; }
        public string WebsiteUrl { get; set; }
        public string PrimaryEmailAddress { get; set; }
        public Guid? ParentId { get; set; }
        public Address MailingAddress { get; set; }
        public string PhoneNumber { get; set; }
        public Guid AdministratorId { get; set; }
        public List<UserInfo> Users { get; set; }
    }
}
