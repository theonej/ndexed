﻿namespace NDexed.Domain.Models.Search
{
    public class SearchTermInfo
    {
        public string Term { get; set; }
        public string Stem { get; set; }
        public float Score { get; set; }
    }
}
