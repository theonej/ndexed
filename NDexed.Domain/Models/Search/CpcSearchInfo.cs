﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Search
{
    public class CpcSearchInfo
    {
        public List<string> Subsection { get; set; }
        public List<string> Group { get; set; }
        public List<string> Subgroup { get; set; }

        public CpcSearchInfo()
        {
            Subsection = new List<string>();
            Group = new List<string>();
            Subgroup = new List<string>();
        }
    }
}
