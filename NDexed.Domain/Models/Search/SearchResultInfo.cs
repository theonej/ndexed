﻿using NDexed.Domain.Models.Patents;
using System.Collections.Generic;

namespace NDexed.Domain.Models.Search
{
    public class SearchResultInfo
    {
        public List<SearchTermInfo> Terms { get; set; }
       public List<PatentInfo> PatentResults { get; set; }
    }
}
