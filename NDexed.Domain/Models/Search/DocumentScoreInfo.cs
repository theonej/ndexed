﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Search
{
    public class DocumentScoreInfo
    {
        public float Score { get; set; }
        public float[] DocumentScores { get; set; }
    }
}
