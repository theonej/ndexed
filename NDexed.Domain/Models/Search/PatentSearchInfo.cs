﻿using NDexed.Domain.Models.Patents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Search
{
    public class PatentSearchInfo : AuditInfo
    {
        public Guid PatentSearchId { get; set; }
        public Guid UserId { get; set; }
        public string DocketNumber { get; set; }

        public PatentInfo PatentSearchCriteria { get; set; }
        public List<PatentInfo> PatentSearchResults { get; set; }
        public List<PatentInfo> AdditionalSearchResults { get; set; }
        public float PatentabilityPrediction { get; set; }

        public PatentSearchInfo()
        {
            PatentSearchResults = new List<PatentInfo>();
            PatentSearchCriteria = new PatentInfo();
        }
    }
}
