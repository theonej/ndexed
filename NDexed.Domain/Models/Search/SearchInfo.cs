﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Domain.Models.Search
{
    public class SearchInfo : AuditInfo
    {
        public enum SearchType
        {
            All = 0,
            Cpc = 1,
            Terms = 2
        }

        public SearchInfo()
        {
            Count = 10;
        }

        public Guid SearchId { get; set; }
        public Guid PatentSearchId { get; set; }

        public string AccountId { get; set; }
        public string ProviderId { get; set; }
        public string Query { get; set; }
        public int Count { get; set; }
        public int Skip { get; set; }
        public string Endpoint { get; set; }

        public SearchType Type { get; set; }

        public string Included { get; set; }
        public string Excluded { get; set; }
    }
}
