﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Search;
using NDexed.AWS.Repository;
using System.Diagnostics;
using System.Collections.Generic;
using NDexed.Domain.Models.Prediction;
using System.Linq;

namespace Waitless.AWS.Tests.ML
{
    [TestClass]
    public class MLRepositoryTests
    {
        [TestMethod]
        public void GetPredictionForSectionAContent()
        {
            var searchInfo = new PredictionService("https://realtime.machinelearning.us-east-1.amazonaws.com", "ml-2IvKWR4vP8s");
            searchInfo.Features = new Dictionary<string, string>();
            searchInfo.Features.Add("Var1", "Surgical systems and methods are disclosed for safe bi-cortical bone screw placement within a bone segment. Included is a method of measurement to control advancement of instruments and implants to repeatedly obtain bi-cortical screw fixation while minimizing protrusion of the lead end of the screw beyond the distal cortical wall therein reducing incidence of injury to adjacent soft tissues.");
        
            var predictionRepo = new MLRepository();
            var prediction = predictionRepo.Search(searchInfo) as List<PredictionInfo>;

            Debug.WriteLine(prediction[0].PredictedLabel);
            Debug.WriteLine(prediction[0].PredictedValue);
            foreach(var result in prediction[0].Predictions)
            {
                Debug.WriteLine("{0} - {1}", result.Key, result.Value);
            }

            Assert.IsNotNull(prediction);
        }
    }
}
