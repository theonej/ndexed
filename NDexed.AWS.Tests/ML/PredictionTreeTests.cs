﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.AWS.ML;
using NDexed.AWS.Repository;
using NDexed.Domain.Models.Prediction;
using System.Collections.Generic;
using System.Diagnostics;

namespace Waitless.AWS.Tests.ML
{
    [TestClass]
    public class PredictionTreeTests
    {
        [TestMethod]
        public void ExecutePredictionTreeAndGetResults()
        {
            var predictionRepo = new MLRepository();

            var query = new PredictionService("https://realtime.machinelearning.us-east-1.amazonaws.com", "ml-2IvKWR4vP8s");
            query.Features = new Dictionary<string, string>();
            query.Features.Add("Var1", "An energy storage device includes a first conductor having a first surface and a second surface. The energy storage device also includes a second conductor and a separator assembly that encloses the first conductor and that is disposed between the first and second conductors. The separator assembly also includes a first portion that covers the first surface and a second portion that covers the second surface. The first and second portions are attached to one another  and at least one of the first and second portions includes a first sheet and a second sheet that are attached to one another. The first sheet includes a first material  and the second sheet includes a second material that is different from the first material.");
            var root = new PredictionInfo();
            root.Definitions.Add(query);

            //add children 
            var aChild = new PredictionInfo();
            aChild.Definitions.Add(new PredictionService(query.Endpoint, "ml-Kg7L9TUcheW"));
            aChild.Definitions.Add(new PredictionService(query.Endpoint, "ml-jokDF2Be5ao"));
            aChild.Definitions.Add(new PredictionService(query.Endpoint, "ml-xzTCPQKOXSi"));
            aChild.Definitions.Add(new PredictionService(query.Endpoint, "ml-R0fBaL2ZYqq"));
            aChild.Definitions.Add(new PredictionService(query.Endpoint, "ml-fpvnC6wQ6os"));

            root.Children.Add("B", aChild);

            var predictionTree = new PredictionTree(predictionRepo, root);
            var prediction = predictionTree.Predict(query);

            Debug.WriteLine(prediction.GetPredictedValue());
            Debug.WriteLine(prediction.PredictedLabel);
            Debug.WriteLine(prediction.PredictedValue);
            foreach (var result in prediction.Predictions)
            {
                Debug.WriteLine("{0} - {1}", result.Key, result.Value);
            }
        }
    }
}
