﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.AWS.SQS;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NDexed.Messaging.Commands.Document;
using NDexed.Messaging.Handlers;
using NDexed.Elastic.Repository;
using NDexed.CouchDb.Repositories;
using NDexed.AWS.Repository;
using NDexed.Messaging.Messages;
using NDexed.AWS.Messagers;

namespace Waitless.AWS.Tests.SQS
{
    [TestClass]
    public class AwsSqsClientTests
    {
        [TestMethod]
        public void ReadMessageWithEmailInfoFromQueue()
        {
            var termRepo = new CouchTermsRepository();
            var docRepo = new ElasticDocumentRepository(termRepo);
            var opinionRepo = new ElasticOpinionRepository(termRepo);
            var emailRepo = new S3Repository();
            var userRepo = new CouchUserRepository();
            var messager = new AwsEmailMessenger();

            var handler = new DocumentCommandHandler(docRepo, opinionRepo, emailRepo, userRepo, messager);

            var processor = new SqsMessageProcessor(handler);
            processor.ProcessNext();
        }
    }
}
