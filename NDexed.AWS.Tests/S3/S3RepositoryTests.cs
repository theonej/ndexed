﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Waitless.AWS.Tests.TestData;
using System.IO;
using NDexed.Domain.Models.Data;
using System.Collections.Generic;
using System.Configuration;
using NDexed.AWS.Repository;

namespace Waitless.AWS.Tests.S3
{
    [TestClass]
    public class S3RepositoryTests
    {
        [TestMethod]
        public void CreateImageThenGetImageThenDeleteImage()
        {
            var image = TestData.TestData.Working_Schedule;
            var path = Path.GetTempFileName();
            image.Save(path);

            var request = new DataInfo();
            request.Parameters = new Dictionary<string, object>();
            request.Parameters.Add("BucketName", ConfigurationManager.AppSettings["ImageBucket"]);
            request.Parameters.Add("ObjectKey", Guid.NewGuid().ToString());
            request.Parameters.Add("TempFilePath", path);

            var repo = new S3Repository();

            //create image
            var response = repo.Add(request);

            //get image
            var imageResponse = repo.Get(response);

            //remove image
            repo.Remove(request);

            File.Delete(path);
        }
    }
}
