﻿using NDexed.Messaging.Queues;
using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.SQS;
using Amazon.SQS.Model;
using System.Configuration;
using Amazon;

namespace NDexed.AWS.SQS
{
    public class AwsSqsClient : IQueueClient<SqsMessage>
    {
        public bool DeleteMessage(SqsMessage message)
        {
            var deleted = false;

            var client = new AmazonSQSClient(RegionEndpoint.USEast1);
            using (client)
            {
                var request = new DeleteMessageRequest();
                request.ReceiptHandle = message.MessageId;
                request.QueueUrl = GetQueueUrl(message.Title);

                var result = client.DeleteMessage(request);
            }

            return deleted;
        }

        public SqsMessage GetNext(string queueName)
        {
            SqsMessage returnValue = null;

            var client = new AmazonSQSClient(RegionEndpoint.USEast1);
            using (client)
            {
                var request = new ReceiveMessageRequest();
                request.MaxNumberOfMessages = 1;
                request.QueueUrl = GetQueueUrl(queueName);

                var message = client.ReceiveMessage(request);
                if(message != null && message.Messages.Count > 0)
                {
                    returnValue = new SqsMessage();
                    returnValue.Body = message.Messages[0].Body;
                    returnValue.MessageId = message.Messages[0].ReceiptHandle;
                }
            }

            return returnValue;
        }

        public void Publish(SqsMessage message)
        {
            var client = new AmazonSQSClient(RegionEndpoint.USEast1);
            using (client)
            {
                var request = new SendMessageRequest();
                request.MessageBody = message.Body;
                request.QueueUrl = GetQueueUrl(message.Title);

                client.SendMessage(request);
            }
        }

        #region Private Methods

        private string GetQueueUrl(string queueName)
        {
            var queueUrl = ConfigurationManager.AppSettings[queueName];
            if (string.IsNullOrEmpty(queueUrl))
            {
                throw new MissingFieldException(string.Format("No setting was found for {0}.  Please check your configuration", queueName));
            }

            return queueUrl;
        }

        #endregion
    }
}
