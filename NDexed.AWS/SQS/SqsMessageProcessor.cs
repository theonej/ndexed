﻿using NDexed.Messaging.Commands.Document;
using NDexed.Messaging.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuttingEdge.Conditions;
using NDexed.Messaging.Queues;

namespace NDexed.AWS.SQS
{
    public class SqsMessageProcessor : IMessageQueueProcessor<SqsMessage>
    {
        private readonly ICommandHandler<DownloadEmailCommand> m_Handler;

        public SqsMessageProcessor(ICommandHandler<DownloadEmailCommand> handler)
        {
            Condition.Requires(handler).IsNotNull();

            m_Handler = handler;
        }

        public void ProcessNext()
        {
            var client = new AwsSqsClient();

            var published = new SqsMessage();
            published.Title = "email-documents-queue";

            var message = client.GetNext(published.Title);
            if (message != null)
            {
                message.Title = published.Title;

                var command = new DownloadEmailCommand();
                command.MessageData = message;
                
                m_Handler.Handle(command);

                //delete message from sqs
                client.DeleteMessage(message);
            }
        }
    }
}
