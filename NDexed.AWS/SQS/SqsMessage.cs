﻿using NDexed.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.AWS.SQS
{
    public class SqsMessage : IMessageInfo
    {
        public string Body { get; set; }

        public string MessageId{ get; set; }

        public List<string> Recipients { get; set; }

        public string Sender { get; set; }

        public string Title { get; set; }

        public DateTime UtcSent { get; set; }
    }
}
