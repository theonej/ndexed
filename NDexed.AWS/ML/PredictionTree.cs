﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Prediction;

using System.Collections.Generic;
using System.Linq;

namespace NDexed.AWS.ML
{
    public class PredictionTree
    {
        private readonly ISearchableRepository<PredictionService, PredictionInfo> m_PredictionRepository;
        public PredictionInfo Root { get; set; }

        public PredictionTree(ISearchableRepository<PredictionService, PredictionInfo> predictionRepo, PredictionInfo root)
        {
            Condition.Requires(predictionRepo).IsNotNull();
            Condition.Requires(root).IsNotNull();

            m_PredictionRepository = predictionRepo;
            Root = root;
        }
        
        public PredictionInfo Predict(PredictionService query)
        {
            var prediction = m_PredictionRepository.Search(query).First();
            prediction.Children = Root.Children;

            if (prediction.Children.ContainsKey(prediction.PredictedLabel))
            {
                var child = prediction.Children[prediction.PredictedLabel];
                var childPredictions = new List<PredictionInfo>();
                foreach (var definition in child.Definitions)
                {
                    definition.Features = query.Features;

                    var definitionPrediction = Predict(definition);
                    childPredictions.Add(definitionPrediction);
                }

                var aggregatePrediction = new PredictionInfo();
                aggregatePrediction.Parent = prediction;
                aggregatePrediction.PredictedLabel = string.Join(string.Empty, childPredictions.Select(item=>item.PredictedLabel).ToArray());
                
                foreach(var childPrediction in childPredictions)
                {
                    foreach(var item in childPrediction.Predictions)
                    {
                        aggregatePrediction.Predictions.Add(item.Key, item.Value);
                    }
                }

                prediction = aggregatePrediction;
            }

            return prediction;
        }
    }
}
