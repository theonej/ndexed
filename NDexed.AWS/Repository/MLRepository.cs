﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Matching;
using NDexed.Domain.Models.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.MachineLearning;
using Amazon.MachineLearning.Model;
using System.Web;
using NDexed.Domain.Models.Prediction;
using Amazon;

namespace NDexed.AWS.Repository
{
    public class MLRepository : ISearchableRepository<PredictionService, PredictionInfo>
    {
        public IEnumerable<PredictionInfo> Search(PredictionService criteria)
        {
            var client = new AmazonMachineLearningClient(RegionEndpoint.USEast1);
            using (client)
            {
                var request = new PredictRequest();
                request.MLModelId = criteria.ModelId;
                request.PredictEndpoint = criteria.Endpoint;
                request.Record = criteria.Features;

                var response = client.Predict(request);
                if(response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new HttpException(response.HttpStatusCode.ToString());
                }

                var prediction = new PredictionInfo();
                prediction.PredictedLabel = response.Prediction.PredictedLabel;
                prediction.PredictedValue = response.Prediction.PredictedValue.ToString();
                prediction.Predictions = response.Prediction.PredictedScores;

                var returnValue = new List<PredictionInfo>() { prediction };

                return returnValue;
            }
        }
    }
}
