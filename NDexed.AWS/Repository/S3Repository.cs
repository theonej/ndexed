﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Data;
using System;
using System.Collections.Generic;

using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using System.IO;

namespace NDexed.AWS.Repository
{
    public class S3Repository : IRepository<DataRequestInfo, DataInfo>
    {
        public DataRequestInfo Add(DataInfo item)
        {
            var client = new AmazonS3Client(RegionEndpoint.USEast1);
            using (client)
            {
                var request = new PutObjectRequest();
                request.BucketName = GetRequiredRequestParameter(item.Parameters, "BucketName");
                request.Key = GetRequiredRequestParameter(item.Parameters, "ObjectKey");
                request.FilePath = GetRequiredRequestParameter(item.Parameters, "TempFilePath");

                var response = client.PutObject(request);

                var returnValue = new DataRequestInfo();
                returnValue.RequestParameters = item.Parameters;

                return returnValue;
            }
        }

        public DataInfo Get(DataRequestInfo id)
        {
            var client = new AmazonS3Client(RegionEndpoint.USEast1);
            using (client)
            {
                var request = new GetObjectRequest();
                request.BucketName = GetRequiredRequestParameter(id.RequestParameters, "BucketName");
                request.Key = GetRequiredRequestParameter(id.RequestParameters, "ObjectKey");

                try
                {
                    var response = client.GetObject(request);
                    var memororyStream = new MemoryStream();
                    using (memororyStream)
                    {
                        response.ResponseStream.CopyTo(memororyStream);

                        var base64EncodedData = Convert.ToBase64String(memororyStream.ToArray());

                        var returnValue = new DataInfo();
                        returnValue.SerializedData = base64EncodedData;

                        return returnValue;
                    }
                }
                catch (AmazonS3Exception ex)
                {
                    var clone = new FileNotFoundException(ex.Message, ex);
                    throw clone;
                }
            }
        }

        public IEnumerable<DataInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Remove(DataInfo item)
        {
            var client = new AmazonS3Client(RegionEndpoint.USEast1);
            using (client)
            {
                var request = new DeleteObjectRequest();
                request.BucketName = GetRequiredRequestParameter(item.Parameters, "BucketName");
                request.Key = GetRequiredRequestParameter(item.Parameters, "ObjectKey");

                var response = client.DeleteObject(request);
                if(response.HttpStatusCode != System.Net.HttpStatusCode.OK && response.HttpStatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    throw new OperationCanceledException(string.Format("Failed to delete object with code: {0}", response.HttpStatusCode));
                }
            }
        }

        #region Private Methods

        private string GetRequiredRequestParameter(Dictionary<string, object> parameters, string name)
        {
            if(!parameters.ContainsKey(name))
            {
                throw new MissingFieldException(string.Format("The request parameter dictionary does not contain key {0}", name));
            }

            return parameters[name].ToString();
        }

        #endregion
    }
}
