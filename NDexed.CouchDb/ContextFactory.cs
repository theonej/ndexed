﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Web;

namespace NDexed.CouchDb
{
    internal static class HttpExtensions
    {
        internal static void CheckForError(this HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                string errorMessage = response.Content.ReadAsStringAsync().Result;
                throw new HttpException((int)response.StatusCode, errorMessage);
            }
        }
    }

    internal static class ContextFactory
    {
        internal const string USER_DATABASE = "users";
        internal const string CASE_USERS_DATABASE = "case_users";
        internal const string CASE_DATABASE = "cases";
        internal const string TIME_DATABASE = "time";
        internal const string NOTE_DATABASE = "note";
        internal const string CLIENT_DATABASE = "client";
        internal const string ORGANIZATION_DATABASE = "organization";
        internal const string TERMS_DATABASE = "terms";
        internal const string CLASSIFICATION_DATABASE = "classification";
        internal const string APPLICATION_DATABASE = "application";
        internal const string PATENT_SEARCH_DATABASE = "patent_searches";
        internal const string SEARCH_HISTORY_DATABASE = "search_history";

        internal static HttpClient InitializeClient(string databaseName)
        {
            string baseUrl = ConfigurationManager.AppSettings["couchBaseUrl"];
            if (string.IsNullOrEmpty(baseUrl))
            {
                throw new ConfigurationErrorsException("Base Url for CouchDB not set");
            }

            var client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);

            bool databaseExists = DatabaseExists(databaseName, client);
            if (!databaseExists)
            {
                CreateDatabase(databaseName, client);
            }
            return client;
        }

        #region Private Methods

        private static bool DatabaseExists(string databaseName, HttpClient client)
        {
            bool databaseExists = true;
            try
            {
                HttpResponseMessage response = client.GetAsync(databaseName).Result;
                response.CheckForError();
            }
            catch (HttpException)
            {
                databaseExists = false;
            }

            return databaseExists;
        }

        private static void CreateDatabase(string databaseName, HttpClient client)
        {
            var content = new StringContent(string.Empty);

            HttpResponseMessage response = client.PutAsync(databaseName, content).Result;
            response.CheckForError();
        }

        #endregion
    }
}
