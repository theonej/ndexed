﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.User;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NDexed.CouchDb.Repositories
{
    public class CouchUserRepository : CouchRepositoryBase,  
                                       IRepository<UserInfo, UserInfo>,
                                       ISearchableRepository<UserInfo, UserInfo>,
                                       ISearchableRepository<Guid, UserInfo>
    {
        public UserInfo Get(UserInfo id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.USER_DATABASE, id.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.USER_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<UserInfo>(content);

                return itemObject;
            }
        }

        public UserInfo Add(UserInfo item)
        {
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof (UserInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.USER_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.USER_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.USER_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }
         
            return item;
        }

        public void Remove(UserInfo item)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.USER_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.USER_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.USER_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.DeleteAsync(documentLocation).Result;
                response.CheckForError();
            }
        }

        public IEnumerable<UserInfo> GetAll()
        {
            var users = new List<UserInfo>();

            string documentLocation = string.Format("{0}/_all_docs", ContextFactory.USER_DATABASE);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.USER_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var criteria = new UserInfo
                    {
                        Id = Guid.Parse(row.id.ToString())
                    };

                    var user = Get(criteria);
                    users.Add(user);
                }
                return users;
            }
        }

        public IEnumerable<UserInfo> Search(UserInfo criteria)
        {
            var clients = new List<UserInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.USER_DATABASE, "_design/user_email/_view/user_email", criteria.EmailAddress.ToLower());

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.USER_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var userInfo = JsonConvert.DeserializeObject<UserInfo>(row.doc.ToString());
                    clients.Add(userInfo);
                }
                return clients;
            }

        }

        public IEnumerable<UserInfo> Search(Guid criteria)
        {
            var clients = new List<UserInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.USER_DATABASE, "_design/org_users/_view/org_users", criteria);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.USER_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var clientInfo = JsonConvert.DeserializeObject<UserInfo>(row.value.ToString());
                    clients.Add(clientInfo);
                }
                return clients;
            }
        }
    }
}
