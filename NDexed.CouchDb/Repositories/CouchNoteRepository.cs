﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NDexed.CouchDb.Repositories
{
    public class CouchNoteRepository : CouchRepositoryBase, 
                                       IRepository<Guid, NoteInfo>,
                                       ISearchableRepository<Guid, NoteInfo>
    {
        public NoteInfo Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.NOTE_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.NOTE_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<NoteInfo>(content);

                return itemObject;
            }
        }

        public Guid Add(NoteInfo item)
        {
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof (NoteInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.NOTE_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.NOTE_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.NOTE_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.Id;
        }

        public void Remove(NoteInfo item)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.NOTE_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.NOTE_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.NOTE_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.DeleteAsync(documentLocation).Result;
                response.CheckForError();
            }
        }

        public IEnumerable<NoteInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<NoteInfo> Search(Guid criteria)
        {
            var notes = new List<NoteInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.NOTE_DATABASE, "_design/case_note/_view/case_note", criteria);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.NOTE_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var note = new NoteInfo
                    {
                        Id = row.doc.Id,
                        CaseId = row.doc.CaseId,
                        Message = row.doc.Message,
                        CreatedBy = row.doc.CreatedBy,
                        CreatedDateTime = row.doc.CreatedDateTime
                    };
                    notes.Add(note);
                }
                return notes;
            }
        }
    }
}
