﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.CouchDb.Repositories
{
    public class CouchTermsRepository : CouchRepositoryBase, IRepository<Guid, TermInfo>
    {
        public Guid Add(TermInfo item)
        {
            //get all terms
            var terms = GetAll();

            var values = terms.Select(term => term.Value);

            //if the current term does not exist, add it
            if(!values.Contains(item.Value))
            {
                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                HttpContent content = new ObjectContent(typeof(TermInfo), item, formatter, header);

                string documentLocation = string.Format("{0}/{1}", ContextFactory.TERMS_DATABASE, item.Id);

                HttpClient client = ContextFactory.InitializeClient(ContextFactory.TERMS_DATABASE);
                using (client)
                {
                    string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.TERMS_DATABASE);
                    if (!string.IsNullOrEmpty(revisionNumber))
                    {
                        documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                    }
                    var response = client.PutAsync(documentLocation, content).Result;
                    response.CheckForError();
                }

                return item.Id;
            }else
            {
                var matchingTerm = terms.First(term => term.Value == item.Value);
                return matchingTerm.Id;
            }
        }

        public TermInfo Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TermInfo> GetAll()
        {
            var terms = new List<TermInfo>();

            string documentLocation = string.Format("{0}/_design/terms/_view/terms", ContextFactory.TERMS_DATABASE);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.TERMS_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var termJson = JsonConvert.SerializeObject(row.value);
                    var term = JsonConvert.DeserializeObject<TermInfo>(termJson);
                    terms.Add(term);
                }
                return terms;
            }
        }

        public void Remove(TermInfo item)
        {
            throw new NotImplementedException();
        }
    }
}
