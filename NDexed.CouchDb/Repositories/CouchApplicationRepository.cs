﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace NDexed.CouchDb.Repositories
{
    public class CouchApplicationRepository : CouchRepositoryBase, 
                                              IRepository<Guid, ApplicationInfo>
    {
        public Guid Add(ApplicationInfo item)
        {
            if(item.Id == Guid.Empty)
            {
                item.Id = Guid.NewGuid();
            }

            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof(ApplicationInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.APPLICATION_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.APPLICATION_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.APPLICATION_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.Id;
        }

        public ApplicationInfo Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.APPLICATION_DATABASE, id);

            var client = ContextFactory.InitializeClient(ContextFactory.APPLICATION_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<ApplicationInfo>(content);

                return itemObject;
            }
        }

        public IEnumerable<ApplicationInfo> GetAll()
        {
            var applications = new List<ApplicationInfo>();

            string documentLocation = string.Format("{0}/_all_docs", ContextFactory.APPLICATION_DATABASE);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.APPLICATION_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var application = Get(Guid.Parse(row.id.ToString()));
                    applications.Add(application);
                }
                return applications;
            }
        }

        public void Remove(ApplicationInfo item)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.APPLICATION_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.APPLICATION_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.APPLICATION_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.DeleteAsync(documentLocation).Result;
                response.CheckForError();
            }
        }
        
    }
}
