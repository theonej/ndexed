﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NDexed.CouchDb.Repositories
{
    public class CouchOrganizationRepository : CouchRepositoryBase,
                                        IRepository<Guid, Organization>
    {
        public Organization Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.ORGANIZATION_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.ORGANIZATION_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<Organization>(content);

                return itemObject;
            }
        }

        public Guid Add(Organization item)
        {
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof(Organization), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.ORGANIZATION_DATABASE, item.OrganizationId);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.ORGANIZATION_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.OrganizationId, client, ContextFactory.ORGANIZATION_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.OrganizationId;
        }

        public void Remove(Organization item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Organization> GetAll()
        {
            var organizations = new List<Organization>();

            string documentLocation = string.Format("{0}/_all_docs", ContextFactory.ORGANIZATION_DATABASE);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.ORGANIZATION_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var organization = Get(Guid.Parse(row.id.ToString()));
                    organizations.Add(organization);
                }
                return organizations;
            }
        }
    }
}
