﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Patents;

namespace NDexed.CouchDb.Repositories
{
    public class CouchClassificationRepository : CouchRepositoryBase,
                                       ISearchableRepository<ClassificationInfo, ClassificationInfo>
    {
        public IEnumerable<ClassificationInfo> Search(ClassificationInfo criteria)
        {
            var classifications = new List<ClassificationInfo>();

            var args = new string[5];
            args[0] = criteria.CPCSection;
            args[1] = criteria.Class;
            args[2] = criteria.Subclass;
            args[3] = criteria.MainGroup;

            var queryKey = string.Format("{0}{1}{2}{3}", args);
            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.CLASSIFICATION_DATABASE, "_design/itemNameBy/_view/subGroup", queryKey);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CLASSIFICATION_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var @classification = JsonConvert.DeserializeObject<ClassificationInfo>(row.doc.ToString());
                    classifications.Add(@classification);
                }
                return classifications;
            }
        }
    }
}
