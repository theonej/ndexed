﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NDexed.CouchDb.Repositories
{
    public abstract class CouchRepositoryBase
    {
        protected static string GetRevisionNumber(Guid id, HttpClient client, string database)
        {
            string revisionNumber = null;
            try
            {
                var documentLocation = string.Format("{0}/{1}", database, id);
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                var itemObject = JsonConvert.DeserializeObject<JObject>(content);

                revisionNumber = itemObject["_rev"].ToString();
            }
            catch (HttpException ex)
            {
                if (ex.GetHttpCode() != (int)HttpStatusCode.NotFound)
                {
                    throw;
                }
            }

            return revisionNumber;
        }
    }
}
