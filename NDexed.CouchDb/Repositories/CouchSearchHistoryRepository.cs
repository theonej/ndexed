﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.CouchDb.Repositories
{
    public class CouchSearchHistoryRepository : CouchRepositoryBase,
                                              IRepository<Guid, SearchInfo>,
                                              ISearchableRepository<PatentSearchInfo, SearchInfo>
    {
        public Guid Add(SearchInfo item)
        {
            if(item.SearchId == Guid.Empty)
            {
                item.SearchId = Guid.NewGuid();
                item.CreatedDateTime = DateTime.Now;
            }

            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof(SearchInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.SEARCH_HISTORY_DATABASE, item.SearchId);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.SEARCH_HISTORY_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.SearchId, client, ContextFactory.SEARCH_HISTORY_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.SearchId;
        }

        public SearchInfo Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.SEARCH_HISTORY_DATABASE, id);

            var client = ContextFactory.InitializeClient(ContextFactory.SEARCH_HISTORY_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<SearchInfo>(content);

                return itemObject;
            }
        }

        public IEnumerable<SearchInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Remove(SearchInfo item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SearchInfo> Search(PatentSearchInfo criteria)
        {
            var searchHistory = new List<SearchInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.SEARCH_HISTORY_DATABASE, "_design/searches/_view/by_search_id", criteria.PatentSearchId);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.SEARCH_HISTORY_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var patentSearch = JsonConvert.DeserializeObject<SearchInfo>(row.doc.ToString());
                    searchHistory.Add(patentSearch);
                }
                return searchHistory;
            }
        }
    }
}
