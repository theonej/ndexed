﻿using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace NDexed.CouchDb.Repositories
{
    public class PatentSearchInfoRepository : CouchRepositoryBase,
                                              IRepository<Guid, PatentSearchInfo>,
                                              ISearchableRepository<Guid, PatentSearchInfo>
    {
        public Guid Add(PatentSearchInfo item)
        {
            if(item.CreatedDateTime == DateTime.MinValue)
            {
                item.CreatedDateTime = DateTime.Now;
                item.CreatedBy = item.UserId.ToString();
            }

            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof(PatentSearchInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.PATENT_SEARCH_DATABASE, item.PatentSearchId);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.PATENT_SEARCH_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.PatentSearchId, client, ContextFactory.PATENT_SEARCH_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.PatentSearchId;
        }

        public PatentSearchInfo Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.PATENT_SEARCH_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.PATENT_SEARCH_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<PatentSearchInfo>(content);

                return itemObject;
            }
        }

        public IEnumerable<PatentSearchInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Remove(PatentSearchInfo item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PatentSearchInfo> Search(Guid criteria)
        {
            var patentSearches = new List<PatentSearchInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.PATENT_SEARCH_DATABASE, "_design/patent_search/_view/by_user", criteria);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.PATENT_SEARCH_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var patentSearch = JsonConvert.DeserializeObject<PatentSearchInfo>(row.doc.ToString());
                    patentSearches.Add(patentSearch);
                }
                return patentSearches.OrderByDescending(item=>item.CreatedDateTime).ToList();
            }
        }
    }
}
