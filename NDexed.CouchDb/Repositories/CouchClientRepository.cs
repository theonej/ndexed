﻿
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NDexed.CouchDb.Repositories
{
    public class CouchClientRepository : CouchRepositoryBase, 
                                         IRepository<Guid, ClientInfo>,
                                         ISearchableRepository<Guid, ClientInfo>
    {
        public ClientInfo Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.CLIENT_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CLIENT_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<ClientInfo>(content);

                return itemObject;
            }
        }

        public Guid Add(ClientInfo item)
        {
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof(ClientInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.CLIENT_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CLIENT_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.CLIENT_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.Id;
        }

        public void Remove(ClientInfo item)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.CLIENT_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CLIENT_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.CLIENT_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.DeleteAsync(documentLocation).Result;
                response.CheckForError();
            }
        }

        public IEnumerable<ClientInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ClientInfo> Search(Guid criteria)
        {
            var clients = new List<ClientInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.CLIENT_DATABASE, "_design/organization_client/_view/organization_client", criteria);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CLIENT_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var clientInfo = JsonConvert.DeserializeObject<ClientInfo>(row.value.ToString());
                    clients.Add(clientInfo);
                }
                return clients;
            }
        }
    }
}
