﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NDexed.Domain.Models.Data;

namespace NDexed.CouchDb.Repositories
{
    public class CouchCaseRepository : CouchRepositoryBase, 
                                       IRepository<Guid, CaseInfo>,
                                       ISearchableRepository<Guid, CaseInfo>
    {
        public CaseInfo Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.CASE_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CASE_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<CaseInfo>(content);

                return itemObject;
            }
        }

        public Guid Add(CaseInfo item)
        {
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof (CaseInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.CASE_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CASE_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.CASE_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.Id;
        }

        public void Remove(CaseInfo item)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.CASE_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CASE_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.CASE_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.DeleteAsync(documentLocation).Result;
                response.CheckForError();
            }
        }

        public IEnumerable<CaseInfo> GetAll()
        {
            var cases = new List<CaseInfo>();

            string documentLocation = string.Format("{0}/_all_docs", ContextFactory.CASE_DATABASE);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CASE_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var @case = Get(Guid.Parse(row.id.ToString()));
                    cases.Add(@case);
                }
                return cases;
            }
        }

        public IEnumerable<CaseInfo> Search(Guid criteria)
        {
            var cases = new List<CaseInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.CASE_DATABASE, "_design/org_cases/_view/org_cases", criteria);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.CASE_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var @case = JsonConvert.DeserializeObject<CaseInfo>(row.doc.ToString());
                    cases.Add(@case);
                }
                return cases;
            }
        }
    }
}
