﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NDexed.CouchDb.Repositories
{
    public class CouchTimeRepository : CouchRepositoryBase,  IRepository<Guid, TimeInfo>,
                                       ISearchableRepository<Guid, TimeInfo>
    {
        public TimeInfo Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.TIME_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.TIME_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<TimeInfo>(content);

                return itemObject;
            }
        }

        public Guid Add(TimeInfo item)
        {
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof (TimeInfo), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.TIME_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.TIME_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.TIME_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }

            return item.Id;
        }

        public void Remove(TimeInfo item)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.TIME_DATABASE, item.Id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.TIME_DATABASE);
            using (client)
            {
                string revisionNumber = GetRevisionNumber(item.Id, client, ContextFactory.TIME_DATABASE);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.DeleteAsync(documentLocation).Result;
                response.CheckForError();
            }
        }

        public IEnumerable<TimeInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TimeInfo> Search(Guid criteria)
        {
            var times = new List<TimeInfo>();

            string documentLocation = string.Format("{0}/{1}?key=\"{2}\"&include_docs=true", ContextFactory.TIME_DATABASE, "_design/case_time/_view/case_time", criteria);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.TIME_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var time = new TimeInfo
                    {
                        Id = row.doc.Id,
                        CaseId = row.doc.CaseId,
                        Unit = row.doc.Unit,
                        Type = row.doc.Type,
                        Rate = row.doc.Rate,
                        Description = row.doc.Description,
                        CreatedBy = row.doc.CreatedBy,
                        CreatedDateTime = row.doc.CreatedDateTime,
                        Hours = row.doc.Hours
                    };
                    times.Add(time);
                }
                return times;
            }
        }
    }
}
