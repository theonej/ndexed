﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.DataAccess.Query;
using NDexed.Elastic.Repository;
using System.Collections.Generic;
using NDexed.DataAccess.Response;
using NDexed.CouchDb.Repositories;

namespace NDexed.Elasticsearch.Tests.Repositories
{
    [TestClass]
    public class ElasticOpinionRepositoryTests
    {
        [TestMethod]
        public void SearchScotusOpinions()
        {
            var searchQuery = new OpinionSearchQuery();
            searchQuery.Jurisdictions = new string[] { "scotus" };
            searchQuery.PageNumber = 0;
            searchQuery.PageSize = 20;
            searchQuery.SearchTerm = "Amendment";

            var repo = new ElasticOpinionRepository(new CouchTermsRepository());

            var response = repo.Search(searchQuery) as IList<OpinionSearchResponse>;

            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Count);
            Assert.IsNotNull(response[0]);
            Assert.AreEqual("scotus", response[0].Jurisdiction);
            Assert.AreEqual(20, response[0].TotalDocuments);
            Assert.AreEqual(response[0].Opinions.Count, response[0].TotalDocuments);

            var query = new OpinionDetailQuery()
            {
                Id = response[0].Opinions[0].id,
                Jurisdiction = response[0].Jurisdiction
            };
            var opinionDetails = repo.Get(query);
            Assert.IsNotNull(opinionDetails);
            Assert.AreNotEqual(string.Empty, opinionDetails.plain_text);
        }
    }
}
