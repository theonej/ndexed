﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Search;
using NDexed.Elastic.Repository;
using System.Linq;

namespace NDexed.Elasticsearch.Tests.Repositories
{
    [TestClass]
    public class ElasticPatentApplicationRepositoryTests
    {
        [TestMethod]
        public void SearchPatnetApplicationText()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 1;
            searchInfo.Query = "CROSS-REFERENCE TO RELATED APPLICATION This is a divisional application of application Ser. No. 12/892,055, filed Sep. 28, 2010, now U.S. Pat. No. 7,955,945, which is incorporated herein by reference. ";
            searchInfo.Skip = 0;


            var patentRepo = new ElasticPatentApplicationRepository();
            var applications = patentRepo.Search(searchInfo).ToList();

            Assert.IsNotNull(applications);
            Assert.AreEqual(searchInfo.Count, applications.Count);
        }
    }
}
