﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using NDexed.Elastic.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Elasticsearch.Tests.Repositories
{
    [TestClass]
    public class ElasticPatentRepositoryTests
    {
        [TestMethod]
        public void SearchClaimsAndGetRelatedPatents()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 10;
            searchInfo.Endpoint = "claims";
            searchInfo.Query = "2. Apparatus according to claim 1, in which the opening in the front surface of the valve and the spray outlet opening in the nozzle member are circular and coaxially, the diameter of the opening in the valve being smaller that the diameter of the outlet opening to allow free flow of the spray material from the first annular chamber into the atomizing fluid stream.";
            searchInfo.Skip = 0;

            var repo = new ElasticClaimsSearchRepository();
            var response = repo.Search(searchInfo);
            Assert.IsNotNull(response);

            var patentNumbers = response.Select(claim => claim.PatentNumber)
                                  .Distinct().ToArray();
            var patentRepo = new ElasticPatentRepository();
            var patents = patentNumbers.Select(patentNumber=>patentRepo.Get(patentNumber)).ToList();

            Assert.IsNotNull(patents);
            Assert.AreEqual(patentNumbers.Length, patents.Count);
        }

        [TestMethod]
        public void SearchPatents()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 1;
            searchInfo.Query = "BACKGROUND FIELD OF INVENTION This invention relates to optical surfaces commonly referred to as Fresnel surfaces. Fresnel surfaces are commonly used to direct and/or focus light in desirable ways and have remained largely unchanged since their invention nearly 200 years ago. Such surfaces commonly consist of either rigid prism surfaces arranged concentrically on an often flat surface or rigid curved surfaces arranged concentrically often on a flat surface. Fresnel optical devices utilize either diffraction and/or refraction, or reflection to direct light as desired. The general advantages of Fresnel optics include the performance simulation of optical lenses, prisms, and mirrors with a significant reduction of material, thickness and consequently dramatically lighter weight and less bulky optics. BACKGROUND-DESCRIPTION OF PRIOR ART Heretofore";
            searchInfo.Skip = 0;


            var patentRepo = new ElasticPatentRepository();
            var patents = patentRepo.Search(searchInfo).ToList();

            Assert.IsNotNull(patents);
            Assert.AreEqual(searchInfo.Count, patents.Count);
        }


        [TestMethod]
        public void SearchPatentsByTerms()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 1;
            searchInfo.Included = "optical surfaces";
            searchInfo.Excluded = "glasses";
            searchInfo.Skip = 0;
            searchInfo.Type = SearchInfo.SearchType.Terms;

            var patentRepo = new ElasticPatentRepository();
            var patents = patentRepo.Search(searchInfo).ToList();

            Assert.IsNotNull(patents);
            Assert.AreEqual(searchInfo.Count, patents.Count);
        }
    }
}
