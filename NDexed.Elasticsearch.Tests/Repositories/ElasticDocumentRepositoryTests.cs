﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.DataAccess.Query;
using NDexed.Elastic.Repository;
using NDexed.Domain.Models.Documents;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NDexed.DataAccess.Response;
using NDexed.CouchDb.Repositories;

namespace NDexed.Elasticsearch.Tests.Repositories
{
    [TestClass]
    public class ElasticDocumentRepositoryTests
    {
        [TestMethod]
        public void GetAllDocumentsAndUpdateTheirOrganizationId()
        {
            //var docRepo = new ElasticDocumentRepository(new CouchTermsRepository());
            //var caseRepo = new CouchDb.Repositories.CouchCaseRepository();

            //var docs = docRepo.GetAll().ToList();

            //foreach(var doc in docs)
            //{
            //    var @case = caseRepo.Get(doc.CaseId);
            //    doc.OrganizationId = @case.OrganizationId;

            //    docRepo.Add(doc);
            //}
        }

        [TestMethod]
        public void GetAKnownDocumentWithAttachment()
        {
            var documentId = Guid.Parse("a20f9ca0-33bf-493c-9870-8a61f78ba516");

            var docRepo = new ElasticDocumentRepository(new CouchTermsRepository());

            var document = docRepo.Get(documentId);
            Assert.IsNotNull(document);

            Assert.IsNotNull(document.Contents);
        }

        [TestMethod]
        public void GetUnindexedDocumentsThenFindMatches()
        {
            var caseId = Guid.Parse("999487f3-86e4-4876-b383-32661068d09a");

            var docRepo = new ElasticDocumentRepository(new CouchTermsRepository());

            var document = new DocumentInfo();
            document.Id = Guid.NewGuid();
            document.Indexed = false;
            document.Name = "TestDocument";
            document.CaseId = caseId;

            var body = " held required to be included in a life insurance company's assets and gross premium income, as well as in its reserves, for purposes of computing its federal";
            var bytes = Encoding.ASCII.GetBytes(body);
            document.Base64EncodedData = Convert.ToBase64String(bytes);
            document.ContentType = "text/plain";
            document.CreatedBy = "J.Henry";
            document.CreatedDateTime = DateTime.Now;
            document.Type = DocumentTypes.Private;

            bool addDocument = false;
            
            if (addDocument)
            {
                 docRepo.Add(document);
            }
            else
            {

                var query = new IndexedDocumentQuery();
                query.CaseId = caseId;
                query.Indexed = false;

                var docResults = docRepo.Search(query) as List<DocumentInfo>;
                Assert.IsNotNull(docResults);

                Assert.AreEqual(document.Base64EncodedData, docResults[0].Base64EncodedData);

                var searchQuery = new OpinionSearchQuery();
                searchQuery.Jurisdictions = new string[] { "scotus" };
                searchQuery.PageNumber = 0;
                searchQuery.PageSize = 20;
                searchQuery.SearchTerm = docResults[0].Contents;

                var repo = new ElasticOpinionMatchingRepository(new CouchTermsRepository());

                var response = repo.Search(searchQuery) as IList<OpinionSearchResponse>;
                Assert.IsNotNull(response);
            }
    
        }
    }
}
