﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Elastic.Repository;
using NDexed.Domain.Models.Search;

namespace NDexed.Elasticsearch.Tests.Repositories
{
    [TestClass]
    public class ElasticClaimsSearchRepositoryTests
    {
        [TestMethod]
        public void FindClaimsByQuery()
        {
            var searchInfo = new SearchInfo();
            searchInfo.Count = 10;
            searchInfo.Endpoint = "claims";
            searchInfo.Query = "2. Apparatus according to claim 1, in which the opening in the front surface of the valve and the spray outlet opening in the nozzle member are circular and coaxially, the diameter of the opening in the valve being smaller that the diameter of the outlet opening to allow free flow of the spray material from the first annular chamber into the atomizing fluid stream.";
            searchInfo.Skip = 0;

            var repo = new ElasticClaimsSearchRepository();
            var response = repo.Search(searchInfo);
            Assert.IsNotNull(response);
        }
    }
}
