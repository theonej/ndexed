﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDexed.Elastic.Repository;
using System.Linq;
using NDexed.Domain.Models.Search;
using System.Collections.Generic;

namespace NDexed.Elasticsearch.Tests.Repositories
{
    [TestClass]
    public class ElasticCpcRepositoryTests
    {
        [TestMethod]
        public void SearchPatentsByCpcSection()
        {
            var section = "E";
            var criteria = new SearchInfo();
            criteria.Query = section;
            criteria.Skip = 0;
            criteria.Count = 10;

            var repo = new ElasticCpcRepository();
            var results = repo.Search(criteria).ToList();

            Assert.AreNotEqual(0, results.Count);
        }
        [TestMethod]
        public void SearchPatentsByCpcSubsection()
        {
            var subsection = "E21";
            var criteria = new SearchInfo();
            criteria.Query = subsection;
            criteria.Skip = 0;
            criteria.Count = 10;

            var repo = new ElasticCpcRepository();
            var results = repo.Search(criteria).ToList();

            Assert.AreNotEqual(0, results.Count);
        }
        [TestMethod]
        public void SearchPatentsByCpcGroup()
        {
            var subGroupId = "E21D";
            var criteria = new SearchInfo();
            criteria.Query = subGroupId;
            criteria.Skip = 0;
            criteria.Count = 10;

            var repo = new ElasticCpcRepository();
            var results = repo.Search(criteria).ToList();

            Assert.AreNotEqual(0, results.Count);
        }
        [TestMethod]
        public void SearchPatentsByCpcSubGroup()
        {
            var subGroupId = "E21D23/0427";
            var criteria = new SearchInfo();
            criteria.Query = subGroupId;
            criteria.Skip = 0;
            criteria.Count = 10;

            var repo = new ElasticCpcRepository();
            var results = repo.Search(criteria).ToList();

            Assert.AreNotEqual(0, results.Count);
        }


        [TestMethod]
        public void GetCpcTitles()
        {
            var criteria = new CpcSearchInfo();
            criteria.Subsection = new List<string>() {
                "A62",
                "B65",
                "C03",
                "E01"
            };

            criteria.Group = new List<string>() {
                "A01B",
                "F16M",
                "C09C"
            };

            criteria.Subgroup = new List<string>()
            {
                "A01K89/011221",
                "A01K97/01",
                "A01N43/40",
                "B23B31/408",
                "B23B51/0466",
                "B23B2231/2016",
                "B29K2623/0666",
                "B29K2455/00",
                "B29K2481/00",
                "B29D99/0071",
                "B29D2030/0674",
                "B29C2949/78319"
            };

            var repo = new ElasticCpcRepository();
            var results = repo.Search(criteria).ToList();

            Assert.AreNotEqual(0, results.Count);
        }
    }
}
