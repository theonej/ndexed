﻿ndexed.services.clientServices = function ($http, $q, authorization, baseApiUrl) {
    return{
        get: function (organizationId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'organization/' + organizationId + '/client/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        getDetails: function (clientId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'clientDetails/' + clientId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        update:function(clientDetails){
            var deferred = $q.defer();

            var url = baseApiUrl + '/clientDetails/' + clientDetails.clientId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: clientDetails
            };

            $http.put(url, clientDetails, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        add: function (clientInfo) {
            var deferred = $q.defer();
            
            var url = baseApiUrl + 'organization/' + clientInfo.organizationId + '/client/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: clientInfo
            };

            $http.post(url, clientInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        remove: function (clientInfo) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'organization/' + clientInfo.organizationId + '/client/' + clientInfo.id;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};


ndexed.factory('clientServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.clientServices]);