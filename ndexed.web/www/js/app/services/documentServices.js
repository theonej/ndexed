﻿ndexed.services.documentServices = function ($http, $q, authorization, baseApiUrl) {
    return {
        get: function (caseId) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'case/' + caseId + '/document/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        remove:function(documentId){
            var deferred = $q.defer();

            var url = baseApiUrl + 'document/' + documentId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        search: function (caseId, term, size, page) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'case/' + caseId + '/documentSearch/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var criteria = {
                term: term,
                pageNumber: page,
                pageSize: size,
                caseId: caseId
            };

            var config = {
                url: url,
                headers: headers,
                data: criteria
            };

            $http.post(url, criteria, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        getDocumentOpinionMatches: function (jurisdictions, docId, size) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'DocumentOpinionMatches/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var command = {
                jurisdictions: jurisdictions,
                documentId: docId,
                maxCount: size
            };

            var config = {
                url: url,
                headers: headers,
                data: command
            };

            $http.post(url, command, config)
                .success(function (response) {
                    deferred.resolve(response);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        getDocumentMatches: function (docId, size) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'DocumentMatches/' + docId + '?resultCount=' + size;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (response) {
                    deferred.resolve(response);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        getDetail: function (caseId, id) {
            var deferred = $q.defer();

            //var url = searchBaseUrl + resourceUri + '?format=json';
            var url = baseApiUrl + 'case/' + caseId + '/documentDetails/' + id;
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        addCaseDocument: function (caseId, sourceDocumentId, type, tags) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'caseDocuments/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var command = {
                caseId:caseId,
                sourceDocumentId: sourceDocumentId,
                type: type,
                tags:tags
            };

            var config = {
                url: url,
                headers: headers,
                data: command
            };

            $http.post(url, command, config)
                .success(function (response) {
                    deferred.resolve(response);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        update: function (document) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'document/' + document.Id;

            var command = {
                documentId:document.id,
                documentName:document.name,
            };
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: command
            };

            $http.put(url, command, config)
                .success(function (response) {
                    deferred.resolve(response);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        downloadZip: function (createZipCommand) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'documentZip/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: createZipCommand,
                responseType: 'arraybuffer'
            };

            $http.post(url, createZipCommand, config)
                .success(function (response) {
                    console.log(blob);

                    var blob = new Blob([response], { type: 'application/zip' });
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = createZipCommand.outputFileName;
                    link.click();
                    deferred.resolve(response)
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('documentServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.documentServices]);