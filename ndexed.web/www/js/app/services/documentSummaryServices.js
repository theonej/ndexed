﻿ndexed.services.documentSummaryServices = function($http, $q, authorization, baseApiUrl) {
    return{
        get: function (caseId) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'case/' + caseId + '/documentSummary/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('documentSummaryServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.documentSummaryServices]);