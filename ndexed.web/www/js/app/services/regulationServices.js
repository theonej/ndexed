﻿ndexed.services.regulationServices = function ($http, $q, authorization, baseApiUrl) {
    return {
        getDocumentMatches: function (docId, size) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'RegulationMatches/' + docId + '?resultCount=' + size;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (response) {
                    deferred.resolve(response);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('regulationServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.regulationServices]);