﻿ndexed.services.caseUsersServices = function ($http, $q, authorization, baseApiUrl, base64Encoding) {
    return {
        addCaseUsers: function (command) {
            var url = baseApiUrl + 'case/' + command.caseId + '/caseusers/add';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                data: command,
                headers: headers
            };

            var deferred = $q.defer();

            $http.post(url, command, config)
                .success(function (token) {
                    deferred.resolve(token);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        removeCaseUsers: function (command) {
            var url = baseApiUrl + 'case/' + command.caseId + '/caseusers/remove';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                data: command,
                headers: headers
            };

            var deferred = $q.defer();

            $http.post(url, command, config)
                .success(function (token) {
                    deferred.resolve(token);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    }
};


ndexed.factory('caseUsersServices', ['$http', '$q', 'authorization', 'baseApiUrl', 'base64Encoding', ndexed.services.caseUsersServices]);