﻿ndexed.services.cpcServices = function ($http, $q, authorization, baseApiUrl, base64Encoding) {
    return {
        getCpcTitles: function (criteria) {
            var url = baseApiUrl + 'cpc';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                data: criteria,
                headers: headers
            };

            var deferred = $q.defer();

            $http.post(url, criteria, config)
                .success(function (token) {
                    deferred.resolve(token);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    }
};


ndexed.factory('cpcServices', ['$http', '$q', 'authorization', 'baseApiUrl', 'base64Encoding', ndexed.services.cpcServices]);