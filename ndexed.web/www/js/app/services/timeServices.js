﻿ndexed.services.timeServices = function ($http, $q, authorization, baseApiUrl) {
    return {
        get:function(caseId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'case/' + caseId + '/time/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        add: function (timeInfo) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'case/' + timeInfo.caseId + '/time/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: timeInfo
            };

            $http.post(url, timeInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        remove: function (timeInfo) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'case/' + timeInfo.caseId + '/time/' + timeInfo.id;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('timeServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.timeServices]);