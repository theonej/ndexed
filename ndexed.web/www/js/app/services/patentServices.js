﻿ndexed.services.patentServices = function ($http, $q, $sce, searchBaseUrl, authorization) {

    return {
        getPatent: function (patentId) {
            var deferred = $q.defer();

            //var url = searchBaseUrl + resourceUri + '?format=json';
            var url = searchBaseUrl + '/patent/' + patentId;
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        getCitationContents: function (patentId) {
            var deferred = $q.defer();

            var url = searchBaseUrl + '/citationContents/' + patentId;
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        searchPatents: function (searchInfo) {
            var deferred = $q.defer();

            var url = searchBaseUrl + '/patent/';
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: searchInfo
            };

            $http.post(url, searchInfo, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },


        searchPatentApplications: function (searchInfo) {
            var deferred = $q.defer();

            var url = searchBaseUrl + '/patentapplication/';
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: searchInfo
            };

            $http.post(url, searchInfo, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};
ndexed.factory('patentServices', ['$http', '$q', '$sce', 'searchBaseUrl', 'authorization', ndexed.services.patentServices]);