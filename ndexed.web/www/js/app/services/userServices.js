﻿ndexed.services.userServices = function ($http, $q, authorization, baseApiUrl, base64Encoding) {
    var _currentUser = null;

    function getAuthHeaderValue(user) {
        var headerValue = user.emailAddress + ':' + user.password;

        headerValue = base64Encoding.encode(headerValue);

        return 'Basic ' + headerValue;
    }

    return {
        authenticate:function(user) {
            var url = baseApiUrl + 'authorization/';

            var headers = {
                'Authorization': getAuthHeaderValue(user)
            };

            var config = {
                url: url,
                headers: headers
            };

            var deferred = $q.defer();

            $http.get(url, config)
                .success(function(token) {
                    deferred.resolve(token);
                })
                .error(function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        },
        get: function () {

            var deferred = $q.defer();
            if (_currentUser) {
                deferred.resolve(_currentUser);
            } else {
                var url = baseApiUrl + 'user/';

                var headers = {
                    'NDexedAuthToken': authorization.getAuthToken()
                };

                var config = {
                    url: url,
                    headers: headers
                };


                $http.get(url, config)
                    .success(function(user) {
                        _currentUser = user;

                        deferred.resolve(user);
                    })
                    .error(function(err) {
                        deferred.reject(err);
                    });
            }
            return deferred.promise;
        },
        getUserDetails: function (userId) {

            var deferred = $q.defer();

            var url = baseApiUrl + 'user/getById/' + userId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        create:function(user) {
            var url = baseApiUrl + 'authorization/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var command = {
                userName: user.userName,
                emailAddress: user.emailAddress,
                organizationId:user.organizationId
            };

            var config = {
                url: url,
                data:command,
                headers: headers
            };

            var deferred = $q.defer();

            $http.post(url, command, config)
                .success(function (token) {
                    deferred.resolve(token);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        update: function (user) {
            var url = baseApiUrl + 'user/update';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                data: user,
                headers: headers
            };

            var deferred = $q.defer();

            $http.put(url, user, config)
                .success(function (token) {
                    deferred.resolve(token);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        setPassword: function (user) {
            var url = baseApiUrl + 'user/setPassword';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var command = {
                userId: user.id,
                password: user.password
            };

            var config = {
                url: url,
                data: command,
                headers: headers
            };

            var deferred = $q.defer();

            $http.post(url, command, config)
                .success(function (token) {
                    deferred.resolve(token);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        resetPassword: function (user) {
            var url = baseApiUrl + 'authorization';

            var command = {
                emailAddress:user.emailAddress
            };

            var config = {
                url: url,
                data: command
            };

            var deferred = $q.defer();

            $http.put(url, command, config)
                .success(function (token) {
                    deferred.resolve(token);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        deleteUser: function (userId) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'user/delete/' + userId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(function () {
                    deferred.resolve();
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        search: function (searchTerm) {

            var deferred = $q.defer();

            var url = baseApiUrl + 'userSearch?searchTerm=' + searchTerm;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };


            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('userServices', ['$http', '$q', 'authorization', 'baseApiUrl', 'base64Encoding', ndexed.services.userServices]);