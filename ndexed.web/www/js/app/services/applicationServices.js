﻿ndexed.services.applicationServices = function ($http, $q, authorization, baseApiUrl) {
    return {
        get: function () {
            var deferred = $q.defer();
            var url = baseApiUrl + 'application/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        getById: function (applicationId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'application/' + applicationId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        add: function (application) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'application/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: application
            };

            $http.post(url, timeInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        create: function (application) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'application/'

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: application
            };

            $http.post(url, application, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        remove: function (applicationId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'application/' + applicationId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('applicationServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.applicationServices]);