﻿ndexed.services.predictionServices = function ($http, $q, $sce, searchBaseUrl, authorization) {

    return {
        predict: function (modelId, contents) {
            var deferred = $q.defer();

            var url = searchBaseUrl + '/prediction?modelId=' + modelId + '&contents=' + contents;
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        similarity: function (contents1, contents2) {
            var deferred = $q.defer();

            var url = searchBaseUrl + '/documentSimilarity';
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var command = {
                "document1Contents": contents1,
                "document2Contents": contents2
            };

            var config = {
                url: url,
                headers: headers,
                data:command
            };

            $http.post(url, command, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('predictionServices', ['$http', '$q', '$sce', 'searchBaseUrl', 'authorization', ndexed.services.predictionServices]);