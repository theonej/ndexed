﻿ndexed.services.noteServices = function ($http, $q, authorization, baseApiUrl) {
    return {
        get:function(caseId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'case/' + caseId + '/note/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        add: function (noteInfo) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'case/' + noteInfo.caseId + '/note/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: noteInfo
            };

            $http.post(url, noteInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        remove: function (noteInfo) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'case/' + noteInfo.caseId + '/note/' + noteInfo.id;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('noteServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.noteServices]);