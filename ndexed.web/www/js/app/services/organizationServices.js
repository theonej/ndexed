﻿ndexed.services.organizationServices = function ($http, $q, authorization, baseApiUrl) {
    return{
        get: function(organizationId) {
            var deferred = $q.defer();

            var url = baseApiUrl + 'organization/' + organizationId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function(user) {
                    deferred.resolve(user);
                })
                .error(function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        update: function(organization) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'organization/' + organization.organizationId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: organization
            };

            $http.put(url, organization, config)
                .success(function(user) {
                    deferred.resolve(user);
                })
                .error(function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('organizationServices', ['$http', '$q', 'authorization', 'baseApiUrl', ndexed.services.organizationServices]);