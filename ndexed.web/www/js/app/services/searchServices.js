﻿ndexed.services.searchServices = function ($http, $q, $sce, searchBaseUrl, authorization) {
    const CROSS_REFERENCE_URL = 'https://bc6ifgxy3h.execute-api.us-east-1.amazonaws.com/prod/ndexed-cross-reference';
    const CPC_URL = 'https://pdhonb4sdk.execute-api.us-east-1.amazonaws.com/prod/cpc';

    return {
        crossReference(previousSearches){
            var deferred = $q.defer();

            var url = CROSS_REFERENCE_URL;
            var headers = {
                //'NDexedAuthToken': authorization.getAuthToken()
            };

            var data = {
                body:
                    { previous_searches: previousSearches }
            };

            var config = {
                url: url,
                headers: headers,
                data: data
            };

            $http.post(url, data, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        searchCPC(criteria) {
            var deferred = $q.defer();

            var url = CPC_URL;
            var headers = {
                //'NDexedAuthToken': authorization.getAuthToken()
            };

            var data = {
                body:
                    {
                        cpcs: criteria.cpcs,
                        take: criteria.count,
                        skip:criteria.skip
                    }
            };

            var config = {
                url: url,
                headers: headers,
                data: data
            };

            $http.post(url, data, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        refineSearch: function (patentSearchId) {
            var deferred = $q.defer();

            var url = searchBaseUrl + '/search/' + patentSearchId + '/refine';
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: patentSearchId
            };

            $http.put(url, patentSearchId, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        searchProviders: function (searchInfo) {
            var deferred = $q.defer();

            var url = searchBaseUrl + '/search';
            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: searchInfo
            };

            $http.post(url, searchInfo, config)
                .success(function (results) {
                    deferred.resolve(results);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        get: function (id) {
            var deferred = $q.defer();
            var url = searchBaseUrl + '/search/' + id;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        getList: function (userId) {
            var deferred = $q.defer();
            var url = searchBaseUrl + '/user/' + userId + '/search/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        add: function (patentSearchInfo) {
            var deferred = $q.defer();
            var url = searchBaseUrl + '/user/' + patentSearchInfo.userId + '/search/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: patentSearchInfo
            };

            $http.put(url, patentSearchInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        getHistoryList: function (searchId) {
            var deferred = $q.defer();
            var url = searchBaseUrl + '/searchHistory/' + searchId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data:searchId
            };

            $http.post(url, searchId, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        getHistory: function (historyId) {
            var deferred = $q.defer();
            var url = searchBaseUrl + '/searchHistory/' + historyId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('searchServices', ['$http', '$q', '$sce', 'searchBaseUrl', 'authorization', ndexed.services.searchServices]);