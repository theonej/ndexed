﻿ndexed.services.caseServices = function($http, $q, authorization, baseApiUrl, base64Encoding) {
    return{
        getAll: function(organizationId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'organization/' + organizationId + '/case/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function(user) {
                    deferred.resolve(user);
                })
                .error(function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        get: function (caseId) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'case/' + caseId;

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        create: function (caseInfo) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'organization/' + caseInfo.organizationId + '/case/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data:caseInfo
            };

            $http.post(url, caseInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        update: function (caseInfo) {
            var deferred = $q.defer();
            var url = baseApiUrl + 'organization/' + caseInfo.organizationId + '/case/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            var config = {
                url: url,
                headers: headers,
                data: caseInfo
            };

            console.log(caseInfo);

            $http.put(url, caseInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        deleteCase: function (caseInfo) {
            var deferred = $q.defer();
            
            var url = baseApiUrl + 'organization/' + caseInfo.organizationId + '/case/';

            var headers = {
                'NDexedAuthToken': authorization.getAuthToken()
            };

            caseInfo.isDeleted = true;

            var config = {
                url: url,
                headers: headers,
                data: caseInfo
            };

            $http.put(url, caseInfo, config)
                .success(function (user) {
                    deferred.resolve(user);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('caseServices', ['$http', '$q', 'authorization', 'baseApiUrl', 'base64Encoding', ndexed.services.caseServices]);