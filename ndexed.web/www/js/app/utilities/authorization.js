﻿ndexed.utilities.authorization = function ($cookies, $location, $window) {

    return {
        getAuthToken: function () {
            var authToken = $location.search().token;

            if (!authToken) {
                authToken = $window.localStorage.getItem('authToken') || '';
            } else {
                this.setAuthToken(authToken);
            }

            return authToken;
        },

        setAuthToken: function (token) {
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + 1);

            //$cookies.put('authToken', token);
            $window.localStorage.setItem('authToken', token);

            var authToken = $window.localStorage.getItem('authToken');
        }
    };
};

ndexed.factory('authorization', ['$cookies', '$location', '$window', ndexed.utilities.authorization]);