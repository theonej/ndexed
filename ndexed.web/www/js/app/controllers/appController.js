﻿ndexed.controllers.appController = function ($scope, $rootScope, ngProgressLite) {
    ngProgressLite.start();

    $scope.startLoading = function() {
        ngProgressLite.start();
    };

    $scope.doneLoading = function() {

        ngProgressLite.done();
    };

    $rootScope.$on('error', function(event, message) {
        $scope.doneLoading();
        toastr.error(message);
    });

    $rootScope.$on('info', function (event, message) {
        $scope.doneLoading();
        toastr.info(message);
    });

    $rootScope.$on('success', function (event, message) {
        $scope.doneLoading();
        toastr.success(message);
    });

    $rootScope.$on('request-load-user', function(event) {
        $rootScope.$broadcast('load-user');
    });
};


ndexed.controller('appController', ['$scope', '$rootScope', 'ngProgressLite', ndexed.controllers.appController]);