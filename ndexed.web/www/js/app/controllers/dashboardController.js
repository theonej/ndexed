﻿ndexed.controllers.dashboardController = function($scope, $q, userServices, clientServices, caseServices, organizationServices) {

    $scope.user = null;
    $scope.clients = null;
    $scope.cases = null;
    $scope.organization = null;

    $scope.initialize = function() {
        $scope.startLoading();
        
        var userPromise = userServices.get();
        userPromise.then(function (user) {

            $scope.user = user;

            $scope.loadSummary();
        });
        
    };

    $scope.loadSummary = function() {
        var promises = [
            clientServices.get($scope.user.organizationId),
            caseServices.getAll($scope.user.organizationId),
            organizationServices.get($scope.user.organizationId)
        ];

        var resolved = $q.all(promises);
        resolved.then(function (data) {

            $scope.clients = data[0];
            $scope.cases = data[1];
            $scope.organization = data[2];

            $scope.doneLoading();
        });
    };

    $scope.toggleAddClient = function () {
        $scope.showAddClient = !$scope.showAddClient;
    };

    $scope.loadClients = function () {
        $scope.startLoading();

        if ($scope.showAddClient) {
            $scope.showAddClient = false;
        }

        var promise = clientServices.get($scope.user.organizationId);
        promise.then(function (clients) {

            $scope.clients = clients;
            $scope.doneLoading();
        });
    };

    $scope.toggleAddCase = function () {
        $scope.showAddCase = !$scope.showAddCase;
    };

    $scope.loadCases = function () {
        $scope.startLoading();

        if ($scope.showAddCase) {
            $scope.showAddCase = false;
        }

        var casePromise = caseServices.getAll($scope.user.organizationId);

        casePromise.then(function (cases) {
            $scope.cases = cases;
            $scope.doneLoading();
        });
    };
};

ndexed.controller('dashboardController', ['$scope', '$q', 'userServices', 'clientServices', 'caseServices', 'organizationServices', ndexed.controllers.dashboardController]);