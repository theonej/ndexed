﻿ndexed.controllers.cases.caseCalendarController = function ($scope, $routeParams, caseServices, documentSummaryServices) {
   
    $scope.initialize = function () {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: '2017-05-12',
            navLinks: false, 
            editable: true,
            eventLimit: true,
            selectable:true,
            events: [
            ],
            dayClick: function (date, jsEvent, view) {
                console.log([date, event, view]);

            }
        });
    };


    $scope.daySelected = function (date, event, view) {
        console.log([date, event, view]);
    };
};

ndexed.controller('caseCalendarController', ['$scope', '$routeParams', 'caseServices', ndexed.controllers.cases.caseCalendarController]);