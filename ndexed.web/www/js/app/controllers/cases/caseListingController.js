﻿ndexed.controllers.cases.caseListingController = function($scope, userServices, caseServices) {

    $scope.cases = null;
    $scope.organizationId = null;
    $scope.filterCriteria = {
        name:''
    };

    $scope.filteredCases = null;

    $scope.courtTypes = [
        { value: 0, text: 'State' },
        { value: 1, text: 'Federal' },
        { value: 2, text: 'Tax' },
        { value: 3, text: 'Bankruptcy' },
        { value: 4, text: 'Maritime' }
    ];

    $scope.initialize = function () {

        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.organizationId = user.organizationId;

            $scope.loadCases();
        });
    };

    $scope.getTypeById = function(courtType) {
        var type = null;

        for (var index = 0; index < $scope.courtTypes.length; index++) {
            var item = $scope.courtTypes[index];
            if (item.value === courtType) {
                type = item.text;
                break;
            }
        }
        return type;
    };

    $scope.toggleAddCase = function() {
        $scope.showAddCase = !$scope.showAddCase;

        if ($scope.showAddCase) {
            $('#docketNumberInput').focus();
        }
    };

    $scope.loadCases = function () {
        if ($scope.showAddCase) {
            $scope.showAddCase = false;
        }
        var casePromise = caseServices.getAll($scope.organizationId);

        casePromise.then(function (cases) {
            $scope.cases = cases;
            $scope.filterCases();
            $scope.doneLoading();
        });
    };

    $scope.showDeleteConfirmation = function(caseInfo) {
        $scope.cases.forEach(function(caseItem) {
            caseItem.showConfirmDelete = false;
        });

        caseInfo.showConfirmDelete = true;
    };

    $scope.deleteCase = function(caseInfo) {
        $scope.startLoading();

        var deletePromise = caseServices.deleteCase(caseInfo);
        deletePromise.then($scope.loadCases);
    };

    $scope.focusCaseFilter = function() {
        $scope.filterEnabled = true;
    };

    $scope.blurCaseFilter = function () {
        $scope.filterEnabled = false;
    };

    $scope.handleKeyPress = function (event) {
        if (event.keyCode === 13 && $scope.filterEnabled) {

            $scope.filterCases();
        }
    };

    $scope.filterCases = function() {
        var filteredCases = [];

        if ($scope.filterCriteria.name) {
            var criteria = $scope.filterCriteria.name.toLowerCase();

            $scope.cases.forEach(function(caseInfo) {
                var docketNumber = caseInfo.docketNumber.toLowerCase();

                if (docketNumber.indexOf(criteria) >= 0) {
                    filteredCases.push(caseInfo);
                }
            });
        } else {
            filteredCases = $scope.cases;
        }

        $scope.filteredCases = filteredCases;
    };
};

ndexed.controller('caseListingController', ['$scope', 'userServices', 'caseServices', ndexed.controllers.cases.caseListingController]);