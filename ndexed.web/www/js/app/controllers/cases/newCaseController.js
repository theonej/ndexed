﻿ndexed.controllers.cases.newCaseController = function($scope, $location, userServices, caseServices, clientServices) {

    $scope.user = null;
    $scope.clients = null;

    $scope.initialize = function() {
        $scope.caseInfo = {
            id: '',
            docketNumber: '',
            court: '',
            courtType: '',
            subjectMatter: '',
            organizationId: '',
            startDate: '',
            closedDate: ''
        };

        var userPromise = userServices.get();
        userPromise.then(function(user) {
            $scope.user = user;

            var clientPromise = clientServices.get(user.organizationId);
            clientPromise.then(function(clients) {
                $scope.clients = clients;
            });
        });
    };

    $scope.createCase = function() {
        $scope.startLoading();

        $scope.caseInfo.organizationId = $scope.user.organizationId;

        var createPromise = caseServices.create($scope.caseInfo);

        createPromise.then(function (caseId) {
            $scope.user.caseIds.push(caseId);
            var permissionPromise = userServices.update($scope.user);
            permissionPromise.then(function () {
                $location.path('/cases/' + caseId + '/details');
            });
        });
    };
};

ndexed.controller('newCaseController', ['$scope', '$location', 'userServices', 'caseServices', 'clientServices', ndexed.controllers.cases.newCaseController]);