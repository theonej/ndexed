﻿ndexed.controllers.cases.caseDetailsController = function ($scope, $routeParams, caseServices, documentSummaryServices) {
    $scope.caseId = null;
    $scope.caseInfo = null;

    $scope.courtTypes = [
        { value: 0, text: 'State' },
        { value: 1, text: 'Federal' },
        { value: 2, text: 'Tax' },
        { value: 3, text: 'Bankruptcy' },
        { value: 4, text: 'Maritime' }
    ];

    $scope.sections = [
        { name: 'Summary', templateUrl: 'html/cases/details/summary.html', selected: true, icon:'newspaper-o' },
        { name: 'Documents', templateUrl: 'html/cases/details/documents.html', selected: false, icon: 'file-pdf-o' },
        { name: 'Time', templateUrl: 'html/cases/details/time.html', selected: false, icon: 'clock-o' },
        { name: 'Notes', templateUrl: 'html/cases/details/notes.html', selected: false, icon: 'file-text-o' },
        { name: 'Users', templateUrl: 'html/cases/details/users.html', selected: false, icon: 'users' },
        { name: 'Search', templateUrl: 'html/cases/details/search.html', selected: false, icon: 'search' }
    ];

    $scope.detailsTemplate = $scope.sections[0].templateUrl;

    $scope.$on('case-users-added', function (event, message) {
        $scope.loadCase();
    });

    $scope.initialize = function () {

        $scope.loadCase();
    };

    $scope.loadCase = function () {
        $scope.startLoading();

        $scope.caseId = $routeParams.caseId;

        var casePromise = caseServices.get($scope.caseId);

        casePromise.then(function (caseInfo) {
            $scope.caseInfo = caseInfo;

            $scope.doneLoading();
        });
    };

    $scope.selectSectionByName = function(sectionName) {
        $scope.sections.forEach(function (section) {
            if (section.name === sectionName) {
                $scope.selectSection(section);
            }
        });
    };

    $scope.selectSection = function(selectedSection) {
        $scope.sections.forEach(function(section) {
            section.selected = false;
        });

        selectedSection.selected = true;
        $scope.detailsTemplate = selectedSection.templateUrl;
    };

    $scope.getTypeById = function (courtType) {
        var type = null;

        for (var index = 0; index < $scope.courtTypes.length; index++) {
            var item = $scope.courtTypes[index];
            if (item.value === courtType) {
                type = item.text;
                break;
            }
        }
        return type;
    };
};

ndexed.controller('caseDetailsController', ['$scope', '$routeParams', 'caseServices', ndexed.controllers.cases.caseDetailsController]);