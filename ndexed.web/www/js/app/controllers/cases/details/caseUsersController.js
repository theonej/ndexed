﻿ndexed.controllers.cases.details.caseUsersController = function ($scope, $routeParams, $q, userServices, caseServices, caseUsersServices) {
    $scope.showFlyIn = false;
    $scope.searchTerm = '';
    $scope.searchResults = null;

    $scope.hasChanges = false;

    $scope.templates = {
        addUser: 'html/cases/details/addUser.html'
    };
    $scope.initialize = function () {
        if (!$scope.caseInfo) {
            throw { message: 'This controller expects to be in a hierarchy with a case' };
        }

        $scope.caseInfo.users = $scope.caseInfo.users || [];
        $scope.getUserDetails();
    };

    $scope.toggleShowAddUser = function () {
        $scope.showFlyIn = !$scope.showFlyIn;
        if ($scope.showFlyIn) {
            $scope.flyInTemplate = $scope.templates.addUser;
        }
    };

    $scope.getUserDetails = function () {

        $scope.startLoading();

        var promises = $scope.caseInfo.userIds.map(function (userId) {
            return userServices.getUserDetails(userId);
        });

        $q.all(promises).then(function (response) {
            $scope.caseInfo.users = response;
            $scope.doneLoading();
        });
    };

    $scope.search = function (searchTerm) {
        if (searchTerm) {
            $scope.startLoading();

            $scope.searchResults = null;

            var promise = userServices.search(searchTerm);
            promise.then(function (response) {

                $scope.searchResults = response;
                $scope.doneLoading();

            });
        }
    };

    $scope.addUser = function (user) {
 
        var existingUserIds = $scope.caseInfo.users.map(function (user) {
            return user.id
        });
        if (existingUserIds.indexOf(user.id) == -1) {
            $scope.caseInfo.users.push(user);

            //create add users command
            var command = {
                caseId: $scope.caseInfo.id
            };

            command.userIds = $scope.caseInfo.users.map(function (user) {
                return user.id
            });

            $scope.startLoading();

            var promise = caseUsersServices.addCaseUsers(command);
            promise.then(function (response) {

                $scope.$emit('success', 'User Added to Matter');
                $scope.doneLoading();
                $scope.toggleShowAddUser();
            });
        }
    };

    $scope.removeUser = function (user) {

        user.showConfirmDelete = false;

        //create remove users command
        var command = {
            caseId: $scope.caseInfo.id
        };

        command.userIds = [user.id];

        $scope.startLoading();

        var promise = caseUsersServices.removeCaseUsers(command);
        promise.then(function (response) {
            var existingUserIds = $scope.caseInfo.users.map(function (exitingUser) {
                return exitingUser.id
            });

            var index = existingUserIds.indexOf(user.id);
            console.log([existingUserIds, user.id, index]);

            $scope.caseInfo.users.splice(index, 1);

            $scope.$emit('success', 'User Removed from Matter');
            $scope.doneLoading();
        });
    };

    $scope.updateCase = function () {
        console.log($scope.caseInfo);

        var promise = caseServices.update($scope.caseInfo);
        
        return promise;
    };

    $scope.getSelectedUsers = function () {
        var users = $scope.searchResults || [];
        var selectedUsers = users.filter(function (user) {
            return user.selected;
        });

        return selectedUsers;
    };

    $scope.showDeleteConfirmation = function (user) {
        user.showConfirmDelete = true;
    };
}

ndexed.controller('caseUsersController', ['$scope', '$routeParams', '$q', 'userServices', 'caseServices', 'caseUsersServices', ndexed.controllers.cases.details.caseUsersController]);