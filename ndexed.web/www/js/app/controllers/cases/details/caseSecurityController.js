﻿ndexed.controllers.cases.details.caseSecurityController = function($scope, userServices, organizationServices) {
    $scope.user = null;
    $scope.organization = null;

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function(user) {
            $scope.user = user;

            $scope.loadOrganization();
        });
    };

    $scope.loadOrganization = function() {
        var promise = organizationServices.get($scope.user.organizationId);
        promise.then(function(organization) {
            $scope.organization = organization;

            $scope.doneLoading();
        });
    };
};

ndexed.controller('caseSecurityController', ['$scope', 'userServices', 'organizationServices', ndexed.controllers.cases.details.caseSecurityController]);