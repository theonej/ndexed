﻿ndexed.controllers.cases.details.caseSearchController = function ($scope, $sce, $routeParams, userServices, searchServices, documentServices, authorization, baseApiUrl, base64Encoding) {
    $scope.user = null;
    $scope.results = null;
    $scope.caseId = $routeParams.caseId;
    $scope.uploader = {};
    $scope.relatedEntityType = 'Opinions';

    $scope.totalDocuments = 0;

    $scope.searchCriteria = {
        term: '',
        page: 0,
        size: 20,
        tags:[]
    };

    $scope.documentTags = [];

    $scope.detail = null;

    $scope.searchEnabled = false;

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function(user) {
            $scope.user = user;
            $scope.doneLoading();
        });
    };

    $scope.onEntityTypeChanged = function () {
       
        $scope.totalDocuments = 0;
        $scope.results = [];
        $scope.documentTags = [];
    };

    $scope.search = function (page) {
        var handlers = {
            'Opinions': $scope.searchOpinions,
            'Documents': $scope.searchDocuments,
            'Regulations': $scope.searchRegulations
        };

        var handler = handlers[$scope.relatedEntityType];

        handler(page);
    };

    $scope.searchOpinions = function (page) {
        if ($scope.searchCriteria.term != '') {
            $scope.startLoading();

            if (page) {
                $scope.searchCriteria.page = page;
            }
            var searchPromise = searchServices.search($scope.searchCriteria, $scope.user);
            searchPromise.then(function (responses) {
                console.log(responses);

                /*this should be an array with the format of
                    - jurisdiction
                    - total documents
                    - opinions []
                */

                $scope.totalDocuments = 0;
                $scope.results = [];
                responses.forEach(function (response) {
                    $scope.totalDocuments += response.totalDocuments;

                    var opinions = response.opinions;
                    opinions.forEach(function (opinion) {
                        opinion.jurisdiction = response.jurisdiction;
                        opinion.trustedHtml = $sce.trustAsHtml(opinion.summary);
                        $scope.addDocumentTags(opinion);
                        $scope.results.push(opinion);
                    });
                });

                $scope.doneLoading();
            });
        }
    };

    $scope.searchRegulations = function (page) {
        if ($scope.searchCriteria.term != '') {
            $scope.startLoading();

            if (page) {
                $scope.searchCriteria.page = page;
            }
            var searchPromise = searchServices.searchRegulations($scope.searchCriteria);
            searchPromise.then(function (responses) {
                console.log(responses);

                /*this should be an array with the format of
                    - usCodes
                    - total documents
                    - opinions []
                */

                $scope.totalDocuments = 0;
                $scope.results = [];
                responses.forEach(function (response) {
                    $scope.totalDocuments += response.totalDocuments;

                    var regulations = response.regulations;
                    regulations.forEach(function (regulation) {
                        regulation.usCaseName = response.usCaseName;
                        $scope.addDocumentTags(regulation);

                        $scope.results.push(regulation);
                    });
                });

                $scope.doneLoading();
            });
        }
    };

    $scope.searchDocuments = function (page) {
        
        if ($scope.searchCriteria.term != '') {
            $scope.startLoading();

            $scope.searchCriteria.caseId = $scope.caseId;

            if (page || page === 0) {
                $scope.searchCriteria.page = page;
            }

            console.log($scope.searchCriteria);

            var searchPromise = searchServices.searchDocuments($scope.searchCriteria);
            searchPromise.then(function (responses) {
                /*this should be an array with the format of
                    - total documents
                    - documents []
                */

                $scope.totalDocuments = 0;
                $scope.results = [];
                responses.forEach(function (response) {
                    $scope.totalDocuments += response.totalDocuments;

                    var documents = response.results;
                    documents.forEach(function (document) {
                        $scope.addDocumentTags(document);
                       
                        $scope.results.push(document);
                    });
                });

                $scope.doneLoading();
            });
        }
    };

    $scope.addDocumentTags = function (document) {
        //add document tags to all tags
        if (document.tags) {

            var currentTags = $scope.documentTags.map(function (tag) {
                return tag.value;
            });

            document.tags.forEach(function (tag) {
                if (currentTags.indexOf(tag) === -1) {
                    var tagObject = {
                        selected: false,
                        value: tag
                    };
                    $scope.documentTags.push(tagObject);
                }
            });
        }
    };

    $scope.focusSearch = function () {
        $scope.searchEnabled = true;
    };

    $scope.blurSearch = function () {
        $scope.searchEnabled = false;
    };

    $scope.handleKeyPress = function (event) {
        if (event.keyCode === 13 && $scope.searchEnabled) {

            $scope.search();
        }
    };

    $scope.getDetail = function (item) {
        var handlers = {
            'Opinions': $scope.getOpinionDetail,
            'Documents': $scope.getDocumentDetail,
            'Regulations': $scope.getRegulationDetail
        };

        var handler = handlers[$scope.relatedEntityType];

        handler(item);
    };

    $scope.getOpinionDetail = function (opinion) {
        $scope.startLoading();

        var searchPromise = searchServices.getDetail(opinion.id, opinion.jurisdiction, $scope.user);
        searchPromise.then(function (detail) {

            detail.trustedHtml = $sce.trustAsHtml(detail.html || detail.plain_text || detail.html_lawbox);

            $scope.detail = detail;

            $scope.showModal = true;

            $scope.doneLoading();
        });
    };

    $scope.getRegulationDetail = function (regulation) {
        $scope.startLoading();

        var searchPromise = searchServices.downloadRegulationDetail($scope.caseId, regulation.id);
        searchPromise.then(function (downloadResponse) {
            $scope.detail = regulation;
            $scope.detail.binaryData = downloadResponse.binaryData;

            $scope.showModal = true;

            $scope.doneLoading();
        });
    };

    $scope.getDocumentDetail = function (document) {
        $scope.startLoading();

        var searchPromise = searchServices.downloadDocumentDetail($scope.caseId, document.id);
        searchPromise.then(function (downloadResponse) {
            $scope.detail = document;
            $scope.detail.binaryData = downloadResponse.binaryData;
            $scope.detail.url = $scope.getDocumentUri(document);
            $scope.detail.viewUrl = $scope.getDocumentViewUri(document);
            $scope.showModal = true;

            $scope.detail.trustedHtml = $sce.trustAsHtml(document.contents);
            console.log($scope.detail);

            $scope.doneLoading();
        });
    };

    $scope.toggleModal = function() {

        $scope.showModal = !$scope.showModal;
    };

    $scope.addDocument = function (document) {
        var handlers = {
            'Opinions': $scope.saveSubDetailToCaseFile,
            'Documents': $scope.addDocumentToCaseFile,
            'Regulations': $scope.addRegulationToCaseFile
        };

        var handler = handlers[$scope.relatedEntityType];
        handler(document);
    };

    $scope.addDocumentToCaseFile = function (document) {
        $scope.startLoading();

        var tags = $scope.getTagsFromSearch();
        
        var addPromise = documentServices.addCaseDocument($scope.caseId, document.id, 0/*document*/, tags);

        addPromise.then(function (detail) {
            $scope.showModal = false;

            $scope.$emit('success', 'Document Added to Matter');
            $scope.doneLoading();
        });
    };

    $scope.addRegulationToCaseFile = function (document) {
        $scope.startLoading();

        var tags = $scope.getTagsFromSearch();

        var addPromise = documentServices.addCaseDocument($scope.caseId, document.id, 1/*regulation*/, tags);
        addPromise.then(function (detail) {
            $scope.showModal = false;

            $scope.$emit('success', 'Regulation Added to Matter');
            $scope.doneLoading();
        });
    };

    $scope.saveSubDetailToCaseFile = function (detail) {

        if ($scope.uploader.flow) {

            $scope.startLoading();

            var documentName = detail.parties + '.html';

            var blob = new Blob([angular.toJson(detail.html)], { type: "text/plain" });
            blob.name = documentName;
            try {
                $scope.uploader.flow.addFile(blob);
            } catch (e) {
            }
            $scope.upload();
        }
    };

    $scope.getUploadSettings = function () {
        var settings = {
            documentName: 'flowFileName',
            caseId: 'caseId',
            chunkNumber: 'flowChunkNumber',
            totalChunks: 'flowTotalChunks',
            documentType: 1,//private,
            tags: 'tags'
        };

        return settings;
    };

    $scope.getTagsFromSearch = function () {
        var tags = [];

        if ($scope.searchCriteria.term) {
            tags = $scope.searchCriteria.term.split(' ');
        }

        tags = tags.reduce(function (result, current) {
            if (result.indexOf(current) == -1) {
                result.push(current);
            }
            return result;
        }, []);

        return tags;
    };

    $scope.upload = function () {
        $scope.uploader.flow.opts.headers = {
            'NDexedAuthToken': authorization.getAuthToken(),
            "DocumentSettings": JSON.stringify($scope.getUploadSettings())
        };

        $scope.uploader.flow.opts.method = 'POST';
        $scope.uploader.flow.opts.testChunks = false;
        $scope.uploader.flow.opts.progressCallbacksInterval = 0;
        $scope.uploader.flow.opts.target = baseApiUrl + 'case/' + $scope.caseId + '/document/';
        $scope.uploader.flow.opts.query = {
            caseId: $scope.caseId,
            tags: $scope.getTagsFromSearch()
        };

        $scope.uploader.flow.upload();
    };

    $scope.uploadComplete = function () {
        $scope.uploader.flow.files = [];
        $scope.showModal = false;
        $scope.$emit('success', 'File Added to Case');
        $scope.doneLoading();
    };

    $scope.onFileUploadError = function (message, $file) {
        console.log(message);
        $file.error = message;
    };

    $scope.onFileUploadSuccess = function (message, $file) {
        $file.done = true;
    };

    $scope.onFileProgress = function ($file) {
        $file.percentComplete = $file.progress(false);
    };

    $scope.getPageCount = function() {
        var pageCount = 0;

        if ($scope.totalDocuments) {
            pageCount = Math.ceil($scope.totalDocuments / $scope.searchCriteria.size);
        }

        return pageCount;
    };

    $scope.getPages = function() {
        var pages = [];

        if ($scope.totalDocuments) {

            var pageCount = Math.ceil($scope.totalDocuments / $scope.searchCriteria.size);

            var initialPage = $scope.searchCriteria.page - 8;
            if (initialPage < 0) {
                initialPage = 0;
            }

            var maxCount = (pageCount > 10) ? 10 : pageCount;
            for (var index = 0; index < maxCount; index++) {
                pages[index] = initialPage++;
            }
        }

        return pages;
    };

    $scope.getRegulationUri = function (regulation) {
        var token = authorization.getAuthToken();

        var url = baseApiUrl + '/case/' + $scope.caseId + '/regulation/' + regulation.id + '?authToken=' + base64Encoding.encode(token);

        return url;
    };

    $scope.getDocumentUri = function (document) {
        var token = authorization.getAuthToken();

        var url = baseApiUrl + '/case/' + $scope.caseId + '/document/' + document.id + '?authToken=' + base64Encoding.encode(token);

        return url;
    };

    $scope.getDocumentViewUri = function (document) {
        var token = authorization.getAuthToken();

        var url = baseApiUrl + 'case/' + $scope.caseId + '/document/' + document.id + '?authToken=' + base64Encoding.encode(token);
        url = 'https://view.officeapps.live.com/op/embed.aspx?src=' + url;

        return $sce.trustAsResourceUrl(url);
    };

    $scope.documentIsPdf = function (document) {
        return document.name.indexOf('.pdf') > -1;
    };


    $scope.documentIsHtml = function (document) {
        console.log(document);
        return document.name.indexOf('.htm') > -1 || document.name.indexOf('.html') > -1;
    };

    $scope.toggleFilter = function (tag) {
        var tagIndex = $scope.searchCriteria.tags.indexOf(tag.value);
        if (tagIndex == -1) {
            $scope.searchCriteria.tags.push(tag.value);
        } else {
            $scope.searchCriteria.tags.splice(tagIndex, 1);
        }
        tag.selected = !tag.selected;
        console.log(tag);

        $scope.search(0);
    };
};

ndexed.controller('caseSearchController', ['$scope', '$sce', '$routeParams', 'userServices', 'searchServices', 'documentServices', 'authorization', 'baseApiUrl', 'base64Encoding', ndexed.controllers.cases.details.caseSearchController]);