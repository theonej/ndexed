﻿ndexed.controllers.cases.details.caseTimeController = function ($scope, $routeParams, timeServices) {
    $scope.caseId = $routeParams.caseId;

    $scope.times = null;

    $scope.time = {
        caseId:$scope.caseId
    };

    $scope.initialize = function() {
        $scope.getCaseTimes();
    };

    $scope.addTime = function() {
        var promise = timeServices.add($scope.time);
        promise.then(function () {

            $scope.time = {
                caseId: $scope.caseId,
                hours:0
            };

            $scope.$emit('success', 'Time Added to Matter');
            $scope.$emit('time-added');
            $scope.getCaseTimes();
        });
    };

    $scope.getCaseTimes = function () {

        $scope.startLoading();

        var promise = timeServices.get($scope.caseId);
        promise.then(function(times) {
            $scope.times = times;

            $scope.doneLoading();
        });
    };

    $scope.showDeleteConfirmation = function(time) {
        $scope.times.forEach(function (existingTime) {
            existingTime.showConfirmDelete = false;
        });

        time.showConfirmDelete = true;
    };

    $scope.deleteTime = function (time) {

        $scope.startLoading();

        var promise = timeServices.remove(time);
        promise.then(function () {
            $scope.getCaseTimes();
        });
    };
};

ndexed.controller('caseTimeController', ['$scope', '$routeParams', 'timeServices', ndexed.controllers.cases.details.caseTimeController]);