﻿ndexed.controllers.cases.details.caseNoteController = function ($scope, $routeParams, noteServices) {
    $scope.caseId = $routeParams.caseId;
    $scope.notes = null;
    $scope.note = {
        caseId:$scope.caseId
    };
    $scope.initialize = function() {
        $scope.getNotes();
    };

    $scope.getNotes = function() {
        $scope.startLoading();

        var promise = noteServices.get($scope.caseId);
        promise.then(function(notes) {
            $scope.notes = notes;

            $scope.doneLoading();
        });
    };

    $scope.addNote = function () {
        var promise = noteServices.add($scope.note);
        promise.then(function () {

            $scope.note = {
                caseId: $scope.caseId
            };

            $scope.getNotes();
        });
    };

    $scope.showDeleteConfirmation = function (note) {
        $scope.notes.forEach(function (existingNote) {
            existingNote.showConfirmDelete = false;
        });

        note.showConfirmDelete = true;
    };

    $scope.deleteNote = function (note) {

        $scope.startLoading();

        var promise = noteServices.remove(note);
        promise.then(function () {
            $scope.getNotes();
        });
    };
};

ndexed.controller('caseNoteController', ['$scope', '$routeParams', 'noteServices', ndexed.controllers.cases.details.caseNoteController]);