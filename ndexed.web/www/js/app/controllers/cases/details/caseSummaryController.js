﻿ndexed.controllers.cases.details.caseSummaryController = function ($scope, documentSummaryServices, timeServices) {

    $scope.documentSummary = null;
    $scope.showFlyIn = null;
    $scope.flyInTemplate = '';

    $scope.templates = {
        addDocument: 'html/cases/details/addDocuments.html',
        addTime: 'html/cases/details/addTime.html'
    };

    $scope.$on('time-added', function (event, message) {
        $scope.toggleShowFlyIn();
        $scope.refreshTimeView();
    });

    $scope.initialize = function () {
        if (!$scope.caseId) {
            throw {message:'This controller expects to be in a hierarchy with a caseId'};
        }

        $scope.refreshDocumentSummary();
        $scope.refreshTimeView();
    };

    $scope.showAddDocument = function() {
        $scope.flyInTemplate = $scope.templates.addDocument;
        $scope.showFlyIn = true;
    };

    $scope.showAddTime = function () {
        $scope.flyInTemplate = $scope.templates.addTime;
        $scope.showFlyIn = true;
    };

    $scope.toggleShowFlyIn = function() {
        $scope.showFlyIn = !$scope.showFlyIn;
    };

    $scope.addToSummary = function(documentCount) {
        $scope.documentSummary.documentCount += documentCount;
    };

    $scope.refreshDocumentSummary = function () {
        var promise = documentSummaryServices.get($scope.caseId);
        promise.then(function (summary) {
            $scope.documentSummary = summary;
        });
    };

    $scope.refreshTimeView = function() {
        var promise = timeServices.get($scope.caseId);
        promise.then(function (times) {
            var totalHours = 0;
            for (var index = 0; index < times.length; index++) {
                var time = times[index];
                var hours = parseFloat(time.hours);
                totalHours += hours;
            }

            $scope.totalHours = totalHours;
        });
    };
};

ndexed.controller('caseSummaryController', ['$scope', 'documentSummaryServices', 'timeServices', ndexed.controllers.cases.details.caseSummaryController]);