﻿ndexed.controllers.cases.details.caseDocumentsController = function ($scope, $routeParams, $sce, baseApiUrl, authorization, documentServices, regulationServices, searchServices, base64Encoding) {
    
    $scope.template = null;
    $scope.caseId = $routeParams.caseId;
    $scope.uploader = {};
    $scope.documents = null;
    $scope.page = 0;
    $scope.size = 10;
    $scope.selectedDocument = null;
    $scope.relatedEntityType = 'View';
    $scope.action = 'showDocumentDetails';
    $scope.detail = null;
    $scope.showSubModal = false;
    $scope.tags = [];
    $scope.tagInput = {value:''};

    $scope.subSections = [
        {name:'View', selected:true, icon:'file-text'},
        { name: 'Documents', selected: false, icon: 'file-word-o' },
        { name: 'Opinions', templateUrl: '', selected: false, icon: 'commenting-o' },
        { name: 'Regulations', templateUrl: '', selected: false, icon: 'balance-scale' },
        { name: 'Tags', templateUrl: '', selected: false, icon: 'tags' }
    ];

    $scope.initialize = function () {
        $scope.loadDocuments();
    };


    $scope.onFileAdded = function($file) {
        $file.percentComplete = 0.00;
    };

    $scope.loadDocuments = function () {

        $scope.startLoading();

        var promise = documentServices.get($scope.caseId);
        promise.then(function(documents) {
            $scope.documents = documents;

            $scope.doneLoading();
        });
    };

    $scope.getDocumentUri = function(document) {
        var token = authorization.getAuthToken();

        var url = baseApiUrl + 'case/' + $scope.caseId + '/document/' + document.id + '?authToken=' + base64Encoding.encode(token);
        
        return url;
    };

    $scope.getDocumentViewUri = function (document) {
        var token = authorization.getAuthToken();

        var url = baseApiUrl + 'case/' + $scope.caseId + '/document/' + document.id + '?authToken=' + base64Encoding.encode(token);
        url = 'https://view.officeapps.live.com/op/embed.aspx?src=' + url;

        return $sce.trustAsResourceUrl(url);
    };

    $scope.getRegulationUri = function (regulation) {
        var token = authorization.getAuthToken();

        var url = baseApiUrl + 'case/' + $scope.caseId + '/regulation/' + regulation.id + '?authToken=' + base64Encoding.encode(token);

        return url;
    };

    $scope.getPage = function(page) {
        $scope.page = page;
        $scope.search();
    };

    $scope.search = function () {

        $scope.startLoading();

        var size = $scope.size;
        var page = $scope.page;
        var term = $scope.searchCriteria.name;

        var promise = documentServices.search($scope.caseId, term, size, page);

        promise.then(function (response) {
            $scope.documentOpinionMatches = [];
            $scope.searchResponse = response;

            $scope.doneLoading();
        });
    };

    $scope.getMatches = function (document) {
        var handlers = {
            'Opinions': $scope.getDocumentOpinionMatches,
            'Documents': $scope.getDocumentMatches,
            'Regulations': $scope.getRegulationMatches
        };

        var handler = handlers[$scope.relatedEntityType];
        if (handler) {
            handler(document);
        }
    };

    $scope.getDocumentOpinionMatches = function (document) {

        $scope.startLoading();

        console.log($scope.detail);

        var size = $scope.size;
        var jurisdictions = ["scotus"];//pull from checkbox list
        var docId = document.id;

        var promise = documentServices.getDocumentOpinionMatches(jurisdictions, docId, size);

        var matches = [];
        promise.then(function (responses) {

            responses.forEach(function (response) {
                var opinions = response.opinions;
                opinions.forEach(function (opinion) {
                    opinion.parties = opinion.parties.substr(0, 50) + '...';
                    opinion.jurisdiction = response.jurisdiction;
                    opinion.trustedHtml = $scope.getTrustedHtml(opinion.summary);
                });

                matches = matches.concat(opinions);
            });

            $scope.detail.matchingOpinions = matches.slice(0, 5);

            console.log(['got opinion matches', $scope.detail.matchingOpinions]);
            $scope.doneLoading();
        });
    };

    $scope.getDocumentMatches = function (document) {
        $scope.startLoading();

        var size = $scope.size;
        var docId = document.id;

        var promise = documentServices.getDocumentMatches(docId, size);

        var matches = [];
        promise.then(function (responses) {

            responses.forEach(function (response) {
                var documents = response.results;
                documents.forEach(function (document) {
                    document.trustedHtml = $scope.getTrustedHtml(document.contents);
                });

                matches = matches.concat(documents);
            });

            $scope.detail.matchingDocuments = matches.slice(0, 5);
            console.log(['got document matches', $scope.detail.matchingDocs]);
            $scope.doneLoading();
        });
    };

    $scope.getRegulationMatches = function (document) {
        $scope.startLoading();

        $scope.resetSelectedDocument(document);

        var size = $scope.size;
        var docId = document.id;

        var promise = regulationServices.getDocumentMatches(docId, size);

        var matches = [];

        promise.then(function (responses) {

            responses.forEach(function (response) {
                var regulations = response.regulations;
                regulations.forEach(function (regulation) {
                    regulation.trustedHtml = $scope.getTrustedHtml(regulation.contents);
                });


                matches = matches.concat(regulations);
            });


            $scope.detail.matchingRegulations = matches.slice(0, 5);
            $scope.doneLoading();
        });
    }

    $scope.resetSelectedDocument = function (document) {

        var selectedDocument = $scope.getSelectedDocument();
        if (selectedDocument != null) {
            selectedDocument.matchingOpinions = [];
            selectedDocument.matchingDocuments = [];
            selectedDocument.matchingRegulations = [];
        }
        $scope.selectedDocumentId = document.id;
    };

    $scope.getSelectedDocument= function(){
        var selectedDocument = null;

        var matchingDocs = $scope.documents.filter(function (doc) {
            return doc.id === $scope.selectedDocumentId;
        });

        if (matchingDocs.length === 1) {
            selectedDocument = matchingDocs[0];
        }

        return selectedDocument;
    };

    $scope.focusDocumentSearch = function () {
        $scope.searchEnabled = true;
    };

    $scope.blurDocumentSearch = function () {
        $scope.searchEnabled = false;
    };

    $scope.handleKeyPress = function (event) {
        if (event.keyCode === 13 && $scope.searchEnabled) {

            $scope.search();
        }
    };

    $scope.getDocumentContents = function (contents) {
        return $sce.trustAsHtml(contents);
    };

    $scope.getPages = function () {
        if ($scope.searchResponse) {
            var documentCount = $scope.searchResponse.totalDocuments;

            var totalPages = Math.ceil(documentCount / $scope.size);

            var pages = [];
            for (var index = 0; index < totalPages; index++) {
                pages.push(index);
            }

            return pages;
        }
    };

    $scope.getDocumentDetail = function (id, caseId) {
        $scope.startLoading();

        $scope.action = 'showDocumentDetails';

        var searchPromise = documentServices.getDetail(caseId, id);
        searchPromise.then(function (detail) {
            detail.caseId = caseId;
            detail.id = id;

            detail.url = $scope.getDocumentUri(detail);
            detail.viewUrl = $scope.getDocumentViewUri(detail);
            $scope.detail = detail;
            $scope.detail.trustedHtml = $scope.getTrustedHtml(detail.contents);

            var downloadPromise = searchServices.downloadDocumentDetail(caseId, id);
            downloadPromise.then(function (downloadResponse) {

                $scope.detail.binaryData = downloadResponse.binaryData;
                $scope.tags = $scope.detail.tags || [];
                
                $scope.showModal = true;

                $scope.doneLoading();
            });
        });
    };

    $scope.getTrustedHtml = function (content) {
        var tokensToRemove = [
            "\\n    ",
            "\"\""
        ];

        for (var index = 0; index < tokensToRemove.length; index++) {
            content = content.split(tokensToRemove[index]).join("");
        }

        return $sce.trustAsHtml(content);
    }

    $scope.getSubDetail = function (match) {
        var handlers = {
            'Opinions': $scope.getOpinionSubDetail,
            'Documents': $scope.getDocumentSubDetail,
            'Regulations': $scope.getRegulationSubDetail
        };

        var handler = handlers[$scope.relatedEntityType];
        if (handler) {
            handler(match);
        }
    };

    $scope.getDocumentSubDetail = function (document) {
        $scope.startLoading();

        $scope.action = 'showDocumentDetails';

        var searchPromise = searchServices.downloadDocumentDetail(document.caseId, document.id);
        searchPromise.then(function (downloadResponse) {
        
            $scope.subDetail = document;
            $scope.subDetail.binaryData = downloadResponse.binaryData;
            $scope.subDetail.url = $scope.getDocumentUri(document);
            $scope.subDetail.viewUrl = $scope.getDocumentViewUri(document);
            $scope.showSubModal = true;

            $scope.doneLoading();

        });
    };

    $scope.getOpinionSubDetail = function (opinion) {
        $scope.startLoading();

        var searchPromise = searchServices.getDetail(opinion.id, opinion.jurisdiction, $scope.user);
        searchPromise.then(function (detail) {

            detail.trustedHtml = $scope.getTrustedHtml(detail.html || detail.plain_text || detail.html_lawbox);
            
            $scope.subDetail = detail;
            $scope.showSubModal = true;

            $scope.doneLoading();
        });
    };

    $scope.getRegulationSubDetail = function (regulation) {
        $scope.startLoading();

        var searchPromise = searchServices.downloadRegulationDetail($scope.caseId, regulation.id);
        searchPromise.then(function (downloadResponse) {

            $scope.subDetail = regulation;
            $scope.subDetail.binaryData = downloadResponse.binaryData;
            
            $scope.showSubModal = true;

            $scope.doneLoading();
        });
    };


    $scope.toggleModal = function () {

        $scope.showModal = !$scope.showModal;
    };

    $scope.hideModal = function () {

        $scope.showModal = false;
    };

    $scope.toggleSubModal = function () {
        $scope.showSubModal = !$scope.showSubModal;
    }

    $scope.addDocument = function (document) {
        var handlers = {
            'Opinions': $scope.saveSubDetailToCaseFile,
            'Documents': $scope.addDocumentToCaseFile,
            'Regulations': $scope.addRegulationToCaseFile
        };

        var handler = handlers[$scope.relatedEntityType];
        if (handler) {
            handler(document);
        }
    };

    $scope.addDocumentToCaseFile = function (document) {
        $scope.startLoading();

        var addPromise = documentServices.addCaseDocument($scope.caseId, document.id, 0/*document*/, $scope.tags);
        addPromise.then(function (detail) {
            $scope.showModal = false;

            $scope.$emit('success', 'Document Added to Matter');
            $scope.doneLoading();

            $scope.loadDocuments();
        });
    };


    $scope.addRegulationToCaseFile = function (document) {
        $scope.startLoading();

        var addPromise = documentServices.addCaseDocument($scope.caseId, document.id, 1/*regulation*/, $scope.tags);
        addPromise.then(function (detail) {
            $scope.showModal = false;

            $scope.$emit('success', 'Regulation Added to Matter');
            $scope.doneLoading();

            $scope.loadDocuments();
        });
    };


    $scope.saveSubDetailToCaseFile = function (detail) {
        
        if ($scope.uploader.flow) {

            $scope.startLoading();

            var documentName = detail.parties + '.html';

            var blob = new Blob([angular.toJson(detail.html)], { type: "text/plain" });
            blob.name = documentName;
            try {
                $scope.uploader.flow.addFile(blob);
            } catch (e) {
            }
            $scope.upload();
        }
    };

    $scope.getUploadSettings = function () {
        var settings = {
            documentName: 'flowFileName',
            caseId: 'caseId',
            chunkNumber: 'flowChunkNumber',
            totalChunks: 'flowTotalChunks',
            documentType: 1,//private,
            tags:'tags'
        };

        return settings;
    };

    $scope.upload = function () {
        $scope.uploader.flow.opts.headers = {
            'NDexedAuthToken': authorization.getAuthToken(),
            "DocumentSettings": JSON.stringify($scope.getUploadSettings())
        };

        $scope.uploader.flow.opts.method = 'POST';
        $scope.uploader.flow.opts.testChunks = false;
        $scope.uploader.flow.opts.progressCallbacksInterval = 0;
        $scope.uploader.flow.opts.target = baseApiUrl + 'case/' + $scope.caseId + '/document/';
        $scope.uploader.flow.opts.query = {
            caseId: $scope.caseId,
            tags:$scope.tags
        };

        $scope.uploader.flow.upload();
    };

    $scope.uploadComplete = function () {
        $scope.uploader.flow.files = [];
        $scope.tags = [];
        $scope.showModal = false;
        $scope.showSubModal = false;
        $scope.$emit('success', 'File Added to Matter');
        $scope.doneLoading();

        $scope.loadDocuments();
    };

    $scope.onFileUploadError = function (message, $file) {
        console.log(message);
        $file.error = message;
    };

    $scope.onFileUploadSuccess = function (message, $file) {
        $file.done = true;
    };

    $scope.onFileProgress = function ($file) {
        $file.percentComplete = $file.progress(false);
    };

    $scope.selectItem = function (selectedSection) {
        $scope.subSections.forEach(function (section) {
            section.selected = false;
        });

        $scope.relatedEntityType = selectedSection.name;

        selectedSection.selected = true;
    };

    $scope.documentIsPdf = function (document) {
        return document.name.indexOf('.pdf') > -1;
    };


    $scope.documentIsHtml = function (document) {
        return document.name.indexOf('.htm') > -1 || document.name.indexOf('.html') > -1;
    };

    $scope.handleKeyPress = function (event) {
      
        if (event.keyCode === 13) {
            $scope.addTag($scope.tagInput.value);
        }
    };

    $scope.addTag = function (tag) {
        console.log(tag);
        if (tag) {
            if ($scope.tags.indexOf(tag) == -1) {
                $scope.tags.push(tag);
                $scope.tagInput = { value: '' };
            }
        }
    };

    $scope.removeTag = function (tag) {
        var index = $scope.tags.indexOf(tag);
        $scope.tags.splice(index, 1);
    };

    $scope.showDeleteConfirmation = function (document) {
        document.showConfirmDelete = true;
    };

    $scope.removeDocument = function (document) {
        document.showConfirmDelete = false;

        $scope.startLoading();

        var promise = documentServices.remove(document.id);
        promise.then(function (result) {
            $scope.$emit('success', 'Document Removed from Matter');
            $scope.doneLoading();

            $scope.loadDocuments();
        });
    };

    $scope.editDocument = function (document) {
        document.editing = true;
    };

    $scope.updateDocument = function (document) {
        document.editing = false;
        $scope.startLoading();

        var promise = documentServices.update(document);
        promise.then(function (result) {
            $scope.$emit('success', 'Document Updated Successfully');
            $scope.doneLoading();

            $scope.loadDocuments();
        });
    };

    $scope.toggleCreatePackage = function(){
        $scope.creatingPackage = !$scope.creatingPackage;
    };

    $scope.downloadDocumentZip = function () {
        var command = {
            outputFileName:'package.zip',
            documentIds: $scope.getSelectedDocumentIds()
        };

        $scope.startLoading();

        var promise = documentServices.downloadZip(command);
        promise.then(function (result) {
            $scope.doneLoading();
        });
    };

    $scope.getSelectedDocumentIds = function () {
        var selectedIds = $scope.documents.map(function (document) {
            if (document.selected) {
                return document.id;
            }
        });

        return selectedIds;
    }
};

ndexed.controller('caseDocumentsController', ['$scope', '$routeParams', '$sce', 'baseApiUrl', 'authorization', 'documentServices', 'regulationServices', 'searchServices', 'base64Encoding', ndexed.controllers.cases.details.caseDocumentsController]);