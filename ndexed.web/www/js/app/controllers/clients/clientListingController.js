﻿ndexed.controllers.clients.clientListingController = function ($scope, userServices, clientServices) {
    $scope.organizationId = null;

    $scope.filterCriteria = {
        name: ''
    };

    $scope.filteredClients = null;

    $scope.initialize = function() {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.organizationId = user.organizationId;

            $scope.loadClients();
        });
    };

    $scope.loadClients = function() {
        $scope.startLoading();

        var promise = clientServices.get($scope.organizationId);
        promise.then(function(clients) {

            $scope.clients = clients;
            $scope.filterClients();

            $scope.doneLoading();
        });
    };

    $scope.toggleAddClient = function() {
        $scope.showAddClient = !$scope.showAddClient;
    };

    $scope.focusClientFilter = function () {
        $scope.filterEnabled = true;
    };

    $scope.blurClientFilter = function () {
        $scope.filterEnabled = false;
    };

    $scope.handleKeyPress = function (event) {
        if (event.keyCode === 13 && $scope.filterEnabled) {

            $scope.filterClients();
        }
    };

    $scope.filterClients = function () {
        var filteredClients = [];

        if ($scope.filterCriteria.name) {
            var criteria = $scope.filterCriteria.name.toLowerCase();

            $scope.clients.forEach(function (client) {
                if (client.name) {
                    var name = client.name.toLowerCase();

                    if (name.indexOf(criteria) >= 0) {
                        filteredClients.push(client);
                    }
                }
            });
        } else {
            filteredClients = $scope.clients;
        }

        $scope.filteredClients = filteredClients;
    };

    $scope.showDeleteConfirmation = function (client) {
        $scope.clients.forEach(function (item) {
            item.showConfirmDelete = false;
        });

        client.showConfirmDelete = true;
    };

    $scope.deleteClient = function (client) {
        $scope.startLoading();

        var deletePromise = clientServices.remove(client);
        deletePromise.then($scope.loadClients);
    };
};

ndexed.controller('clientListingController', ['$scope', 'userServices', 'clientServices', ndexed.controllers.clients.clientListingController]);