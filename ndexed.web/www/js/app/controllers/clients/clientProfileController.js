﻿ndexed.controllers.clients.clientProfileController = function ($scope, $routeParams, clientServices) {

    $scope.clientId = null;
    $scope.editing = false;
    $scope.details = null;
    $scope.templateUrl = 'html/clients/view.html';

    $scope.templates = {
        view: 'html/clients/view.html',
        edit: 'html/clients/edit.html'
    };

    $scope.initialize = function () {

        $scope.startLoading();

        $scope.clientId = $routeParams.clientId;

        clientServices.getDetails($scope.clientId)
        .then(function (response) {

            $scope.details = response;

            $scope.doneLoading();
        });
    };

    $scope.toggleEdit = function () {
        $scope.editing = !$scope.editing;

        if ($scope.editing) {
            $scope.templateUrl = $scope.templates.edit;
        } else {
            $scope.templateUrl = $scope.templates.view;
        }
    };

    $scope.save = function () {
        $scope.startLoading();

        clientServices.update($scope.details)
        .then(function (response) {

            $scope.$emit('success', 'Client Updated');

            $scope.doneLoading();

            $scope.toggleEdit();
        });
    };
};


ndexed.controller('clientProfileController', ['$scope', '$routeParams', 'clientServices', ndexed.controllers.clients.clientProfileController]);