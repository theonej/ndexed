﻿ndexed.controllers.clients.newClientController = function($scope, userServices, clientServices) {
    $scope.user = null;
    $scope.client = {};

    $scope.initialize = function () {
        var userPromise = userServices.get();

        userPromise.then(function (user) {
            $scope.user = user;

            $scope.client = {
                organizationId: user.organizationId
            };
        });
    };

    $scope.createClient = function () {
        
        var promise = clientServices.add($scope.client);

        promise.then(function () {

            $scope.$emit('success', 'New Case Added');
            $scope.toggleAddClient();
            $scope.loadClients();

            $scope.client = {
                organizationId: $scope.user.organizationId
            };
        });
    };
};

ndexed.controller('newClientController', ['$scope', 'userServices', 'clientServices', ndexed.controllers.clients.newClientController]);