﻿ndexed.controllers.headerController = function ($scope, $rootScope, $location, userServices) {
    $scope.currentUser = null;

    $scope.items = [
        { text: 'Projects', icon: '', path: '/search-list', selected: true }
    ];

    $rootScope.$on("$routeChangeSuccess", function(event, next, current) {
        $scope.setSelectedItem();
    });

    $rootScope.$on('load-user', function() {
        $scope.initialize();
    });

    $scope.initialize = function () {
        userServices.get().then(function(user) {
            $scope.currentUser = user;
        });

        $scope.setSelectedItem();
    };

    $scope.$on('load-user', function() {
        userServices.get().then(function (user) {
            $scope.currentUser = user;
        });

        $scope.setSelectedItem();
    });

    $scope.setSelectedItem = function () {

        $scope.items.forEach(function (item) {
            item.selected = false;
            var path = $location.path();
            if (path.indexOf(item.path) >= 0) {
                item.selected = true;
            }
        });

    };

    $scope.selectItem = function(selectedItem) {
        $scope.items.forEach(function(item) {
            item.selected = false;
        });

        selectedItem.selected = true;

        $location.path(selectedItem.path);
    };
};

ndexed.controller('headerController', ['$scope', '$rootScope', '$location', 'userServices', ndexed.controllers.headerController]);