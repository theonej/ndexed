﻿ndexed.controllers.applications.applicationListingController = function ($scope, $sce, $routeParams, userServices, applicationServices) {
    $scope.user = null;
    $scope.results = null;
    $scope.applicationId = $routeParams.applicationId;
    $scope.applications = null;

    $scope.showAddApplication = false;

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;

            var applicationsPromise = applicationServices.get(user.organizationId);
            applicationsPromise.then(function (applications) {
                $scope.applications = applications;

                $scope.doneLoading();
            });
        });
    };

    $scope.toggleAddApplication = function () {
        $scope.showAddApplication = !$scope.showAddApplication;
    };
};

ndexed.controller('applicationListingController', ['$scope', '$sce', '$routeParams', 'userServices', 'applicationServices', ndexed.controllers.applications.applicationListingController]);