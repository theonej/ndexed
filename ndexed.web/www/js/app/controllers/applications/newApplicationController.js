﻿ndexed.controllers.applications.newApplicationController = function ($scope, $location, userServices, applicationServices, clientServices) {

    $scope.user = null;
    $scope.clients = [];

    $scope.initialize = function () {
        $scope.applicationInfo = {
            name: '',
            description: '',
            filingDate: '',
        };

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;

            var clientPromise = clientServices.get(user.organizationId);
            clientPromise.then(function (clients) {
                $scope.clients = clients;
            });
        });
    };

    $scope.createApplication = function () {
        $scope.startLoading();

        $scope.applicationInfo.organizationId = $scope.user.organizationId;

        var createPromise = applicationServices.create($scope.applicationInfo);

        createPromise.then(function (applicationId) {
            $scope.message = "Your application is being created.  you will be notified via email when it is ready";
            $scope.$emit("info", "Your application is being created.  you will be notified via email when it is ready");
            $scope.doneLoading();
            $scope.toggleAddApplication();
            //$location.path('/applications/' + applicationId + '/details');
        });
    };
};

ndexed.controller('newApplicationController', ['$scope', '$location', 'userServices', 'applicationServices', 'clientServices', ndexed.controllers.applications.newApplicationController]);