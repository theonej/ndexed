﻿ndexed.controllers.applications.applicationController = function ($scope, $sce, $routeParams, $location, userServices, applicationServices) {
    $scope.user = null;
    $scope.results = null;
    $scope.applicationId = $routeParams.applicationId;
   
    $scope.apiKey = "AIzaSyC3kXOQJYvGoQHzSjo6iOqu8nbvbYqL5l8";
    $scope.calendarAPIId = "233687887466-b5sfqj5ncmie933sjlsvcs665gcujccq.apps.googleusercontent.com";
    $scope.discoveryDocs = [
                            "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest",
                            "https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"
                           ];
    $scope.scopes = 'https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/drive.metadata';

    //$scope.discoveryDocs = [
    //                        "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"
    //];
    //$scope.scopes = 'https://www.googleapis.com/auth/calendar';

    $scope.calendarAuthorized = null;
    $scope.displayCalendarMessage = true;
    $scope.showAddEvent = false;

    $scope.files = [];

    $scope.newEvent = {
        "sendNotifications": true,
        "reminders": {
            'useDefault': false,
            'overrides': [
              { 'method': 'email', 'minutes': 7 * 60 * 24 },
              { 'method': 'email', 'minutes': 3 * 60 * 24},
              { 'method': 'email', 'minutes': 1 * 60 * 24}
            ]
        }
    };

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;
            $scope.newEvent.attendees = [
                {
                    "id": user.id,
                    "email": user.emailAddress,
                    "displayName": user.userName,
                    "organizer": user.emailAddress,
                    "self": true,
                    "optional": false
                }
            ];

            applicationServices.getById($scope.applicationId).then(function (application) {
                $scope.application = application;
                $scope.initializeClient();
            });
        });
    };

    $scope.initializeClient = function () {
        gapi.load('client:auth2', $scope.initializeGAPIClient);
    };

    $scope.initializeGAPIClient = function () {
        var options = {
            apiKey: $scope.apiKey,
            clientId: $scope.calendarAPIId,
            discoveryDocs: $scope.discoveryDocs,
            scope:$scope.scopes
        };
        var promise = gapi.client.init(options).then(function () {
            var authInstance = gapi.auth2.getAuthInstance();
            authInstance.isSignedIn.listen($scope.onSignedIn);

            var initialStatus = authInstance.isSignedIn.get();
            $scope.onSignedIn(initialStatus);
        });

        $scope.doneLoading();
    };

    $scope.onSignedIn = function (signInStatus) {
        $scope.calendarAuthorized = signInStatus;
        if (signInStatus === true) {
            $scope.displayCalendarMessage = false;

            $scope.getCalendars();

            $scope.getFiles();
        }

        $scope.$apply();
    };

    $scope.onAuthClick = function (event) {
        $scope.events = [];
        gapi.auth2.getAuthInstance().signIn();
    };

    $scope.onSignoutClick = function () {
        gapi.auth2.getAuthInstance().signOut();
        $scope.events = [];
    };

    $scope.showCalendarMessage = function () {

        return $scope.calendarAuthorized === false && $scope.showCalendarMessage;
    };

    $scope.toggleShowAddEvent = function () {
        $scope.showAddEvent = !$scope.showAddEvent;
    };

    $scope.addCalendar = function () {

        $scope.startLoading();

        var options = {
            summary:$scope.getCalendarName()
        };

        gapi.client.calendar.calendars.insert(options).then(function (response) {
            if (!response.status === 200) {
                $scope.$emit('error', response);
            } else {
                $scope.$emit('info', 'Calendar Created');
                $scope.calendarExists = true;
                $scope.getCalendars();
            }
        });
    };

    $scope.addOneYearEvent = function () {
        var currentDate = new Date();
        currentDate.setFullYear(currentDate.getFullYear() + 1)

        var oneYearEvent = {
            summary:'On Year Reminder',
            description:'A reminder that the patent was disclosed one year ago.  Application needs to be submitted.',
            start: {
                dateTime: currentDate
            },
            end: {
                dateTime: currentDate
            },
            attendees: $scope.newEvent.attendees,
            reminders: $scope.newEvent.reminders,
            sendNotifications: $scope.newEvent.sendNotifications
        };

        $scope.addEvent(oneYearEvent);
    };

    $scope.addCommunicationReceivedEvent = function () {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() + 1);

        var actionRequiredEvent = {
            summary: 'Action Required',
            description: 'A reminder that a communication was received from the patent office.  Action is required to keep the application current.',
            start: {
                dateTime: currentDate
            },
            end: {
                dateTime: currentDate
            },
            attendees: $scope.newEvent.attendees,
            reminders: $scope.newEvent.reminders,
            sendNotifications: $scope.newEvent.sendNotifications
        };

        $scope.addEvent(actionRequiredEvent);
    };

    $scope.addNewEvent = function(){
        $scope.addEvent($scope.newEvent);
    };

    $scope.addEvent = function (eventOptions) {

        $scope.startLoading();

        var options = {

        };
        gapi.client.calendar.calendarList.list(options).then(function (response) {
            var calendars = response.result.items;

            var applicationCalendars = calendars.filter(function (calendar) {
                return calendar.summary === $scope.getCalendarName();
            });


            if (applicationCalendars.length > 0) {
                eventOptions.calendarId = applicationCalendars[0].id;
                console.log(eventOptions);

                gapi.client.calendar.events.insert(eventOptions).then(function (response) {
                    $scope.showAddEvent = false;

                    if (!response.status === 200) {
                        $scope.$emit('error', response)
                    }
                    $scope.getRelatedEvents(applicationCalendars[0].id);
                })
            }
        });
    };

    $scope.getCalendars = function () {
        var options = {
           
        };
        gapi.client.calendar.calendarList.list(options).then(function (response) {
            var calendars = response.result.items;
            
            var applicationCalendars = calendars.filter(function (calendar) {
                return calendar.summary === $scope.getCalendarName();
            });

            if (applicationCalendars.length > 0) {

                $scope.calendarExists = true;

                var calendarId = applicationCalendars[0].id;
                $scope.getRelatedEvents(calendarId);
            } else {
                $scope.calendarExists = false;
                $scope.$apply();
            }

            $scope.doneLoading();
        });
    };

    $scope.getRelatedEvents = function (calendarId) {
        var options = {
            calendarId: calendarId
        };

        console.log(options);

        gapi.client.calendar.events.list(options).then(function (response) {
            $scope.events = response.result.items;
            console.log($scope.events);
            $scope.$apply();

            $scope.doneLoading();
        });
    };

    $scope.getCalendarName = function () {
        var applicationIdString = $scope.application.id.substring(0, 8);
        
        return $scope.application.name + ' (' + applicationIdString + ')';
    };

    $scope.getMatchLink = function (match) {
        return match.data.link || match.data.formattedUrl;
    }

    $scope.goToSearch = function () {
        var location = "/search/" + $scope.application.id;
        $location.path(location);
    };

    $scope.deleteEvent = function (eventId) {
       
        gapi.client.calendar.calendarList.list({}).then(function (response) {
            var calendars = response.result.items;

            var applicationCalendars = calendars.filter(function (calendar) {
                return calendar.summary === $scope.getCalendarName();
            });

            if (applicationCalendars.length > 0) {
                var calendarId = applicationCalendars[0].id;
                var options = {
                    calendarId: calendarId,
                    eventId: eventId
                };

                gapi.client.calendar.events.delete(options).then(function (response) {
                    $scope.getRelatedEvents(calendarId);
                });
            }
        });
    };

    $scope.getFiles = function () {
        var options = {
            'pageSize': 10,
            "q": "mimeType='application/vnd.google-apps.folder' and name='" + $scope.getCalendarName() + "'"
        };

        gapi.client.drive.files.list(options).then(function (response) {
            if (response.result.files.length > 0) {
                $scope.folder = response.result.files[0];
            }
        });
    }
};

ndexed.controller('applicationController', ['$scope', '$sce', '$routeParams', '$location', 'userServices', 'applicationServices', ndexed.controllers.applications.applicationController]);