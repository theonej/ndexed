﻿ndexed.controllers.search.searchHistoryController = function ($scope, $routeParams, $location, userServices, searchServices) {
    $scope.searchHistory = null;
    $scope.user = null;
    $scope.showCrossReferencedResults = false;
    $scope.crossReferencedResults = [];
    $scope.crossReferenceResultsTemplate = 'html/search/results/crossReferencedResults.html';

    $scope.initialize = function () {
        $scope.startLoading();

        $scope.searchId = $routeParams.searchId;
        if (!$scope.searchId) {
            $location.path('/search-list')
        }

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;

            $scope.loadSearchHistory();
        });
    };

    $scope.loadSearchHistory = function () {
        searchServices.getHistoryList($scope.searchId)
                .then(function (results) {
                    $scope.searchHistory = results;
                    $scope.doneLoading();
                });
    };

    $scope.selectSearch = function (historyId) {

        $location.path('/search/' + $scope.searchId + '/history/' + historyId);
    };


    $scope.crossReferenceSearches = function () {
        var previousSearches = $scope.searchHistory.filter((item) => {
            return item.selected;
        }).map((result) =>{
            return result.query;
        });

        if (previousSearches.length > 1) {
            $scope.startLoading();

            searchServices.crossReference(previousSearches)
            .then((response) => {
                console.log(response);
                var results = JSON.parse(response.body);
                console.log(results);
                results = (results[0] || { results: [] }).results.map((result) => {
                    
                    result.overallRank = result.score;
                    result.descriptionRank = result.score;
                    result.maxClaimScore = 0.00;
                    result.highlight = result.abstract;

                    result.claims.map((claim) => {
                        claim.showClaim = true;
                        if (claim.claimRank > result.maxClaimScore) {
                            result.maxClaimScore = claim.claimRank;
                        }
                    });

                    return result;
                });

                $scope.crossReferencedResults = results;
                $scope.showCrossReferencedResults = true;
                console.log('showing ' + $scope.crossReferencedResults.length + ' results');
                $scope.doneLoading();
            })
        }
    };

    $scope.setHistoryItemResults = function (query, results) {
        results = results.map((result) => {
            result.overallRank = result.score;
            result.descriptionRank = result.score;
            result.maxClaimScore = result.score;

            return result;
        });

        var historyItem = $scope.searchHistory.filter((item)=>{
            return item.query === query;
        })[0];

        historyItem.results = results;
        console.log(historyItem);
    };

    $scope.toggleCrossReferencedResults = function () {
        $scope.showCrossReferencedResults = !$scope.showCrossReferencedResults;
    };
};

ndexed.controller('searchHistoryController', ['$scope', '$routeParams', '$location', 'userServices', 'searchServices', ndexed.controllers.search.searchHistoryController]);
