﻿ndexed.controllers.search.image.reverseImageSearchController = function ($scope, $sce, $routeParams, $q, baseApiUrl, authorization, documentServices, userServices, searchServices) {
    $scope.user = null;
    $scope.results = null;
    $scope.caseId = $routeParams.caseId;
    $scope.uploader = {};

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;
            $scope.doneLoading();
        });
    };


    $scope.upload = function () {

        $scope.startLoading();

        $scope.pageResults = [];
        $scope.fullImageResults = [];
        $scope.partialImageResults = [];

        $scope.uploader.flow.opts.headers = {
            'NDexedAuthToken': authorization.getAuthToken(),
            "ImageSettings": JSON.stringify($scope.getUploadSettings())
        };

        $scope.uploader.flow.opts.method = 'POST';
        $scope.uploader.flow.opts.testChunks = false;
        $scope.uploader.flow.opts.progressCallbacksInterval = 0;
        $scope.uploader.flow.opts.target = baseApiUrl + 'image/';
        $scope.uploader.flow.opts.query = {
        };

        $scope.uploader.flow.upload();
    };

    $scope.onFileUploadSuccess = function (message, $file) {
        var results = JSON.parse(message);
        $scope.totalResults = results.length;

        $scope.pageResults = results.filter(function (result) {
            return result.matchType === 'Page';
        });
        $scope.fullImageResults = results.filter(function (result) {
            return result.matchType === 'FullImage';
        });
        $scope.partialImageResults = results.filter(function (result) {
            return result.matchType === 'PartialImage';
        });

        $scope.pageResults = $scope.pageResults.map(function (result) {
            result.trustedHtml = $sce.trustAsHtml(result.contents);
            return result;
        });

        console.log([
            results,
            $scope.pageResults,
            $scope.fullImageResults,
            $scope.partialImageResults
        ]);
        $file.done = true;
    };
    $scope.uploadComplete = function (e) {
        $scope.uploader.flow.files = [];
        $scope.tags = [];
        $scope.showModal = false;
        $scope.showSubModal = false;
        $scope.$emit('success', 'Search Completed');
        $scope.doneLoading();

        console.log($scope.uploader);
        //this is where we will call the reverse image search, with the result of the call
    };

    $scope.getUploadSettings = function () {
        var settings = {
            imageName: 'flowFileName',
            chunkNumber: 'flowChunkNumber',
            totalChunks: 'flowTotalChunks',
        };

        return settings;
    };
};

ndexed.controller('reverseImageSearchController', ['$scope', '$sce', '$routeParams', '$q',  'baseApiUrl', 'authorization', 'documentServices', 'userServices', 'searchServices', ndexed.controllers.search.image.reverseImageSearchController]);