﻿ndexed.controllers.search.providerSearchController = function ($scope, $sce, $routeParams, $q, $location, userServices, searchServices, predictionServices, applicationServices, patentServices, cpcServices) {
    $scope.user = null;
    $scope.results = null;
    $scope.caseId = $routeParams.caseId;
    $scope.uploader = {};

    $scope.totalDocuments = 0;
    $scope.providerId = '';
    $scope.predictedLabel = '';
    $scope.predictions = [];
    $scope.nextPredictions = [];
    $scope.breadcrumbs = [];
    $scope.predictionTemplate = 'html/search/predictions/prediction.html';
    $scope.imageTemplate = 'html/search/image/reverseImageSearch.html';
    $scope.descriptionTemplate = 'html/search/results/details.html';
    $scope.cpcTemplate = 'html/search/results/cpc.html';
    $scope.termsTemplate = 'html/search/results/terms.html';
    $scope.resultTemplate = 'html/search/results/resultTemplate.html';
    $scope.showPredictionView = false;
    $scope.showCpc = false;
    $scope.showTerms = false;
    $scope.showImageView = false;
    $scope.loadingMore = false;
    $scope.showSelectedResult = false;
    $scope.selectedResult = null;
    $scope.searchId = null;
    $scope.cpcs = [];
    $scope.terms = [];
    $scope.searchViewTemplate = '';
    $scope.statsTemplate = $scope.cpcTemplate;
    $scope.selectedStatsView = 'cpc';

    $scope.showSave = true;

    $scope.cpcFieldOptions = [
        { name: 'Section', field: 'section' },
        { name: 'Sub Section', field: 'subsection' },
        { name: 'Group', field: 'group' },
        { name: 'Sub Group', field: 'subGroup' }
    ];
    $scope.cpcField = $scope.cpcFieldOptions[2];

    $scope.providers = [
        { name: '', providerId: 0, selected: true }
    ];

    $scope.providerId = 0;

    $scope.sortTypes = [
        {type:'Overall', selected:true},
        {type:'Description', selected:false},
        {type:'Max Claim', selected:false}
    ];

    $scope.searchViews = [
        { view: 'search', template: 'html/search/views/searchView.html', selected: true },
        { view: 'stats', template: 'html/search/views/statsView.html', selected: false },
        { view: 'saved', template: 'html/search/views/savedView.html', selected: false },
        { view: 'history', template: 'html/search/views/historyView.html', selected: false }
    ];

    $scope.searchCriteria = {
        query: '',
        skip: 0,
        accountId: 'AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o',
        count: 10
    };

    $scope.initialize = function () {

        $scope.searchViewTemplate = $scope.getActiveView();

        $scope.searchId = $routeParams.searchId;
        if (!$scope.searchId) {
            $location.path('/search-list')
        }

        $scope.searchCriteria.patentSearchId = $scope.searchId;

        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;
        });

        if ($routeParams.historyId) {
            searchServices.getHistory($routeParams.historyId)
                .then(function (history) {
                    $scope.searchCriteria.query = history.query;
                });
        } else {
            $scope.doneLoading();
        }
    };

    $scope.selectProvider = function (name) {
        $scope.providers = $scope.providers.map(function (provider) {

            provider.selected = false;
            if (provider.name === name) {
                provider.selected = true;
                $scope.providerId = provider.providerId;
            }

            return provider;
        });

        $scope.search(0);
    };

    $scope.searchMore = function () {
        $scope.loadingMore = true;
        $scope.search($scope.searchCriteria.skip + 10);
    };

    $scope.search = function (skip) {
        switch ($scope.providerId) {
            case (0):
                return $scope.searchPatents(skip);
            case (1):
                return $scope.searchCpcOnly(skip);
            default:
                break;
        };
    };

    $scope.searchPatents = function (skip) {
        if ($scope.searchCriteria.query != '') {

            $scope.providerId = 0;

            $scope.startLoading();

            if (skip || skip === 0) {
                $scope.searchCriteria.skip = skip;
            }

            if (skip === 0) {
                $scope.results = [];
            }
            $scope.searchCriteria.type = 'All';

            var searchPromise = patentServices.searchPatents($scope.searchCriteria);
            searchPromise.then(function (result) {
                var termScores = result.terms.map((term) => {
                    return term.score;
                });

                var min = Math.min(...termScores);
                var max = Math.max(...termScores);

                $scope.terms = result.terms.map((term) =>{
                    term.searchStatus = 'Ignore';
                    term.score = $scope.normalize(min, max, term.score);
                    return term;
                });

                $scope.processResults(result.patentResults);
            });
        }
    };

    $scope.processResults = function (matches) {
        var deferred = $q.defer();

        //deduplicate
        var identifiers = $scope.results.map(function (item) {
            return item.patentNumber || item.applicationNumber;
        });

        var filteredMatches = matches.filter(function (match) {
            return identifiers.indexOf(match.patentNumber || match.applicationNumber) < 0;
        });

        var promises = filteredMatches.map(function (match) {
            var claims = match.claims;

            if (match.cpc.length > 0) {
                match.mainCpc = match.cpc[0].subGroup;
            }
            return $scope.getDocumentSimilarity(match);
        });

        $q.all(promises).then(function (results) {
            results.sort(function (a, b) {
                return b.overallRank - a.overallRank;
            });

            $scope.results = $scope.results.concat(results);

            $scope.doneLoading();

            $scope.cpcField = $scope.cpcFieldOptions[2];
            $scope.cpcRanking($scope.cpcField.field);

            $scope.loadingMore = false;

            deferred.resolve($scope.results);
        });

        return deferred.promise;
    };

    $scope.sortBy = function (type) {
        console.log(type);

        $scope.sortTypes = $scope.sortTypes.map((item) => {
            item.selected = false;

            return item;
        });

        type.selected = true;

        switch (type.type) {
            case ('Max Claim'):
                $scope.results.sort(function (a, b) {
                    return b.maxClaimScore - a.maxClaimScore;
                });
                break;
            case ('Overall'):
                $scope.results.sort(function (a, b) {
                    return b.overallRank - a.overallRank;
                });
                break;
            case ('Description'):
                $scope.results.sort(function (a, b) {
                    return b.overallRank - a.overallRank;
                });
                break;
            case ('mainCpc'):
                $scope.results.sort(function (a, b) {
                    return b.mainCpc - a.mainCpc;
                });
                break;
            default:
                break;
        }
    };
    $scope.getDocumentSimilarity = function (match) {
        var deferred = $q.defer();

        match.loading = true;

        var claimScores = match.claims.map(function (claim) {
            return claim.claimRank;
        });

        match.maxClaimScore = Math.max(...claimScores);
        match.minClaimScore = Math.min(...claimScores);

        if (match.claims.length === 0) {
            match.maxClaimScore = 0;
            match.minClaimScore = 0;
        }
        deferred.resolve(match);

        return deferred.promise;
    };

    $scope.toggleShowClaims = function (match) {
        match.showClaims = !match.showClaims;
    };

    $scope.toggleShowClaim = function (claim) {
        claim.showClaim = !claim.showClaim;
    };


    $scope.toggleShowCitations = function (match) {
        match.showCitations = !match.showCitations;
    };

    $scope.toggleShowCpc = function (match) {
        match.showCpc = !match.showCpc;
    };


    $scope.toggleShowCitation = function (citation) {
        citation.showCitation = !citation.showCitation;
        if (citation.showCitation && !citation.similarityScore) {
            $scope.scoreCitation(citation);
        }
    };

    $scope.getClaimScoreClass = function (similarityScore) {
        var className = 'low-score';
        if (similarityScore > .65) {
            className = 'high-score';
        } else if (similarityScore > .25) {
            className = 'medium-score';
        }

        return className;
    };

    $scope.scoreCitation = function (citation) {
        citation.loading = citation.showCitation;

        var promise = patentServices.getCitationContents(citation.citedPatentNumber);
        promise
            .then(function (result) {
                citation.contents = result.description;

                predictionServices.similarity($scope.searchCriteria.query, citation.contents)
                .then(function (result) {
                    citation.similarityScore = result[0];
                })
            })
            .finally(function (err) {
                citation.loading = false;
                deferred.resolve(match);
            });
    };

    $scope.togglePredictionView = function () {
        $scope.showPredictionView = !$scope.showPredictionView;
        if ($scope.showPredictionView) {
            $scope.showImageView = false;
            $scope.$broadcast('predict');
        }
    };

    $scope.toggleImageView = function () {
        $scope.showImageView = !$scope.showImageView;
        if ($scope.showImageView) {
            $scope.showPredictionView = false;
        }
    };

    $scope.showResultDescription = function (result) {
        $scope.selectedResult = result;
        $scope.showSelectedResult = true;
    };

    $scope.toggleDescriptionView = function () {
        $scope.showSelectedResult = !$scope.showSelectedResult;
    };

    $scope.toggleCpcView = function () {
        $scope.showCpc = !$scope.showCpc;
    };

    $scope.toggleTermsView = function () {
        $scope.showTerms = !$scope.showTerms;
    };

    $scope.cpcRanking = function (field) {
        if ($scope.results) {
            var resultCps = $scope.results.map(function (result) {
                
                return result.cpc;
            });

            if (resultCps.length > 0) {
                var reduced = resultCps.reduce(function (accumulator, currentValue, currentIndex, array) {
                    return accumulator.concat(currentValue);
                }, [])

                var itemValues = reduced.map(function (cpcItem) {
                    return cpcItem[field];
                });

                var cpcs = [...new Set(itemValues)];

                var cpcCounts = [];

                for (var cpcIndex = 0; cpcIndex < cpcs.length; cpcIndex++) {
                    var cpc = cpcs[cpcIndex];
                    var count = reduced.filter(function (item) {
                        return item[field] === cpc;
                    }).length;

                    cpcItem = {
                        cpc: cpc,
                        count: count,
                        sequence: reduced.filter(function (item) {
                            return item[field] === cpc;
                        })[0].sequence
                    };

                    cpcCounts.push(cpcItem);

                }

                var counts = cpcCounts.map((item) => { return item.count });
                var maxCount = Math.max(...counts);
                var minCount = Math.min(...counts);
                var step = 100 / maxCount;

                var total = .0001;
                if (counts.length > 0) {
                    total = counts.reduce(function (a, b) { return a + b; });
                }

                $scope.cpcs = cpcCounts.map((item) =>{
                        item.score = $scope.normalize(minCount, maxCount, item.count);
                        item.searchStatus = 'Ignore';
                        return item;
                    })
                    .sort(function (a, b) {
                    return b.count - a.count;
                });

                var criteria = {};

                criteria[field] = [...new Set(itemValues)];

                cpcServices.getCpcTitles(criteria)
                    .then((results) => {
                        var titles = results[0];

                        $scope.cpcs = $scope.cpcs.map((cpc) => {
                            cpc.title = titles[cpc.cpc.toLowerCase()];
                            return cpc;
                        });
                    });
            }
        }
    };

    $scope.saveResult = function (result) {
        if ($scope.savingResult) {
            $scope.$emit('info', 'Please wait for previous save to complete before adding more references');
        } else {
            console.log(result);
            $scope.addResults([result]);
        }
    };

    $scope.addResults = function (resultstoAdd) {
        $scope.savingResult = true;
        $scope.startLoading();

        searchServices.get($scope.searchId)
        .then((result) => {

            if (!result.patentSearchResults) {
                result.patentSearchResults = [];
            }

            //filter out results that are already associated
            var existingIds = result.patentSearchResults.map((item) => {
                return item.patentNumber || item.applicationNumber;
            });

            var internalResults = result.patentSearchResults
                                                .concat(resultstoAdd.filter((item) => {
                                                    return existingIds.indexOf(item.patentNumber || item.applicationNumber) < 0;
                                                }))
                                                .filter((item) => {
                                                    return item.source === 0
                                                })
                                                .map((item) =>{
                                                    return {
                                                        "patentNumber": item.patentNumber,
                                                        "applicationNumber": item.applicationNumber,
                                                        "overallRank": item.overallRank,
                                                    };
                                                });

            var externalResults = result.patentSearchResults
                                                .concat(resultstoAdd.filter((item) => {
                                                    return existingIds.indexOf(item.patentNumber || item.applicationNumber) < 0;
                                                }))
                                                .filter((item) => {
                                                    return item.source === 1
                                                });

            result.patentSearchResults = internalResults.concat(externalResults);

            console.log(["result.patentSearchResults", result.patentSearchResults]);

            searchServices.add(result)
            .then(() => {
                $scope.$emit('success', 'Results saved successfully');
                $scope.savingResult = false;
                $scope.doneLoading();
            });
        });
    };

    $scope.showScores = function (result) {
        if (result.showScores === undefined) {
            result.showScores = true;
        }
        return result.showScores;
    };

    $scope.getDisplayNumber = function (result) {

        var returnValue = null;

        if (result.patentNumber) {
            var cleaned = result.patentNumber.replace(/\D/g, '');

            var number = parseInt(cleaned);

            var returnValue = "US " + number.toLocaleString();

            return returnValue;
        }
        return  result.applicationNumber;
    };

    $scope.selectSearchView = function (searchView) {
        $scope.searchViews = $scope.searchViews.map((item) => {
            item.selected = false;

            return item;
        });

        searchView.selected = true;

        $scope.searchViewTemplate = $scope.getActiveView();
    };

    $scope.getActiveView = function () {
        var selectedItems = $scope.searchViews.filter((item) => { return item.selected === true; });
        return selectedItems[0].template;
    };

    $scope.searchByCpc = function () {

        $scope.selectSearchView($scope.searchViews[0]);

        var includedSearchCpcs = $scope.cpcs.filter((cpc) => {
            return cpc.searchStatus === 'Include';
        }).map((cpc) =>{
            return cpc.cpc;
        });

        $scope.results = [];
        $scope.searchCriteria.query = includedSearchCpcs.join('\n');

        $scope.searchViewTemplate = 'html/search/views/cpcSearchView.html';
    };

    $scope.searchCpcOnly = function (skip) {
        if ($scope.searchCriteria.query != '') {
            $scope.providerId = 1;

            $scope.startLoading();

            if (skip || skip === 0) {
                $scope.searchCriteria.skip = skip
            }

            if (skip === 0) {
                $scope.results = [];
            }
            $scope.searchCriteria.type = 'Cpc';

            $scope.startLoading();

            const cpcs = $scope.searchCriteria.query.split('\n');
            $scope.searchCriteria.cpcs = cpcs;

            var searchPromise = searchServices.searchCPC($scope.searchCriteria);
            searchPromise.then(function (result) {
                console.log(result);

                //$scope.terms = result.terms;
                var results = JSON.parse(result.body).map((result) =>{
                    result.showScores = false;
                    result.showCpcStats = true;
                    result.highlight = result.abstract;

                    return result;
                }).sort((a, b) =>{
                    return b.cpcsMatched - a.cpcsMatched;
                });

                $scope.processResults(results);
            });
        }
    };

    $scope.searchByTerms = function () {
        $scope.selectSearchView($scope.searchViews[0]);

        var includedTerms = $scope.terms.filter((term) => {
            return term.searchStatus === 'Include';
        }).map((term) => {
            return term.term;
        });

        var excludedTerms = $scope.terms.filter((term) => {
            return term.searchStatus === 'Exclude';
        }).map((term) => {
            return term.term;
        });

        $scope.results = [];
        $scope.searchCriteria.included = includedTerms.join('\n');
        $scope.searchCriteria.excluded = excludedTerms.join('\n');

        $scope.searchCriteria.query = ($scope.searchCriteria.included.replace('\n', ' ') + ' ' + $scope.searchCriteria.excluded.replace('\n', ' '));
        $scope.searchCriteria.type = 'Terms';

        $scope.searchViewTemplate = 'html/search/views/termsSearchView.html';
    };

    $scope.searchTerms = function (skip) {
        if (skip || skip === 0) {
            $scope.searchCriteria.skip = skip;
        }

        if (skip === 0) {
            $scope.results = [];
        }

        $scope.searchCriteria.query = ($scope.searchCriteria.included.replace('\n', ' ') + ' ' + $scope.searchCriteria.excluded.replace('\n', ' '));
        $scope.searchCriteria.type = 'Terms';

        $scope.startLoading();
        var searchPromise = patentServices.searchPatents($scope.searchCriteria);
        searchPromise.then(function (result) {
            console.log(result);
            $scope.terms = result.terms;

            $scope.processResults(result.patentResults);
        });

        searchServices.search
    };

    $scope.selectStatsView = function (statType) {
        $scope.selectedStatsView = statType;
        switch (statType) {
            case ('cpc'):
                $scope.statsTemplate = $scope.cpcTemplate;
                break;
            case ('terms'):
                $scope.statsTemplate = $scope.termsTemplate;
                break;
            default:
                break;
        }
    };


    $scope.normalize = function (min, max, current) {
        return ((current - min) / (max - min)).toFixed(2);
    };
};

ndexed.controller('providerSearchController', ['$scope', '$sce', '$routeParams', '$q', '$location', 'userServices', 'searchServices', 'predictionServices', 'applicationServices', 'patentServices', 'cpcServices', ndexed.controllers.search.providerSearchController]);