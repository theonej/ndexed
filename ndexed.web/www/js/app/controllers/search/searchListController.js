﻿ndexed.controllers.search.searchListController = function ($scope, $routeParams, $location, userServices, searchServices) {
    $scope.searchList =  null;
    $scope.user = null;

    $scope.showAddSearch = false;
    $scope.addSearchTemplate = 'html/search/new.html';
    $scope.newSearch = {
        patentSearchCriteria: {}
    };

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;

            $scope.loadSearches();
        });
    };

    $scope.loadSearches = function () {
        searchServices.getList($scope.user.id)
                .then(function (results) {
                    $scope.searchList = results;
                    $scope.doneLoading();
                });
    };

    $scope.toggleAddSearch = function () {
        $scope.showAddSearch = !$scope.showAddSearch;
    };

    $scope.createSearch = function () {
        $scope.startLoading();
        $scope.newSearch.userId = $scope.user.id;

        var addPromise = searchServices.add($scope.newSearch);
        addPromise.then(function (result) {
            $scope.$emit('success', 'Your project was created succesfully');
            $scope.toggleAddSearch();
            $scope.doneLoading();

            $location.path('/search/' + result);
        });
    };

    $scope.selectSearch = function (searchId) {

        $location.path('/search/' + searchId);
    };
};

ndexed.controller('searchListController', ['$scope', '$routeParams', '$location', 'userServices', 'searchServices', ndexed.controllers.search.searchListController]);
