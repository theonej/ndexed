﻿ndexed.controllers.search.savedResultsController = function ($scope, $timeout, $sce, $routeParams, $q, $location, userServices, searchServices, predictionServices, applicationServices, patentServices) {
    ndexed.controllers.search.providerSearchController.apply(this, [$scope, $sce, $routeParams, $q, $location, userServices, searchServices, predictionServices, applicationServices, patentServices]);

    $scope.user = null;
    $scope.search = null;
    $scope.resultTemplate = 'html/search/results/resultTemplate.html';
    $scope.searchCriteria.query = '';
    $scope.history = null;
    $scope.message = null;
    $scope.showRefine = true;
    $scope.results = null;
    $scope.additionalResults = null;

    $scope.initialize = function () {
        
        $scope.results = [];
        $scope.showSave = false;

        $scope.startLoading();
        $scope.searchId = $routeParams.searchId;
        if (!$scope.searchId) {
            $location.path('/search-list')
        }

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;

            searchServices.getHistoryList($scope.searchId)
            .then((history) => {
                $scope.history = history;
                $scope.searchCriteria = history[0];

                $scope.loadSearch();
            })

        });
    };

    $scope.reprocessResults = function () {

        $timeout(function () {
            console.log($scope.search);

            var promises = [
                $scope.processResults($scope.search.patentSearchResults),
                $scope.processResults($scope.search.additionalSearchResults || [])
            ]
            $q.all(promises).then((resultArray) => {
                console.log(resultArray);

                $scope.results = resultArray[0];
                $scope.additionalResults = resultArray[1];
                $scope.doneLoading();
            })
        });
    };


    $scope.processResults = function (matches) {
        var deferred = $q.defer();

        //deduplicate
        var identifiers = $scope.results.map(function (item) {
            return item.patentNumber || item.applicationNumber;
        });

        var promises = (matches||[]).map(function (match) {
            var claims = match.claims;

            if (match.cpc.length > 0) {
                match.mainCpc = match.cpc[0].subGroup;
            }
            match.showScores = false;
            match.highlight = (match.description || "").substring(0, Math.min(match.description.length, 500));

            return $scope.getDocumentSimilarity(match);
        });

        $q.all(promises).then(function (results) {
            $scope.results = results;

            $scope.doneLoading();

            $scope.cpcField = $scope.cpcFieldOptions[0];
            $scope.cpcRanking($scope.cpcField.field);

            $scope.loadingMore = false;

            deferred.resolve($scope.results);
        });

        return deferred.promise;
    };

    $scope.loadSearch = function () {

        $scope.startLoading();

        searchServices.get($scope.searchId)
                .then((result) => {
                    $scope.search = result;

                    $scope.reprocessResults();
                });
    };


    $scope.removeResult = function (result) {
        if ($scope.savingResult) {
            $scope.$emit('info', 'Please wait for previous save to complete before adding more references');
        } else {
            $scope.savingResult = true;
            $scope.startLoading();

            $scope.search.patentSearchResults = $scope.results.filter((item) => {
                return (item.patentNumber || item.applicationNumer) != (result.patentNumber || result.applicationNumer);
            });
            
            $scope.results = $scope.search.patentSearchResults;

            searchServices.add($scope.search)
                .then(() => {
                    $scope.$emit('success', 'Results saved successfully');
                    $scope.savingResult = false;
                    $scope.doneLoading();
                });
        }
    };

    $scope.refineSearch = function () {
        $scope.startLoading();

        searchServices.refineSearch($scope.searchId)
        .then(() => {
            $scope.message = 'Search refinement has been initiated.  You will be alerted via email if any additional results are found';
            console.log($scope.message)
            $scope.doneLoading();
        });
    };

    $scope.clearMessage = function () {
        $scope.message = null;
        $scope.showRefine = false;
    };

    $scope.showRefineButton =  function(){
        return $scope.showRefine;
    };
};

ndexed.controller('savedResultsController', ['$scope', '$timeout', '$sce', '$routeParams', '$q', '$location', 'userServices', 'searchServices', 'predictionServices', 'applicationServices', 'patentServices', ndexed.controllers.search.savedResultsController]);
