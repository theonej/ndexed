﻿ndexed.controllers.account.setPasswordController = function($scope, $location, userServices) {
    $scope.user = null;

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function (user) {
            $scope.user = user;

            $scope.doneLoading();
        });
    };

    $scope.setPassword = function () {

        $scope.displayValidationMessage = false;

        var valid = $scope.validate();
        if (valid) {
            var setPasswordPromise = userServices.setPassword($scope.user);
            
            setPasswordPromise.then(function () {
                $scope.$emit('request-load-user');

                $location.path('/search');
            });
        } else {
            $scope.displayValidationMessage = true;
        }
    };

    $scope.validate = function() {
        var valid = true;

        if ($scope.user.password != $scope.user.passwordConfirmation) {
            valid = false;
        }

        return valid;
    };
};

ndexed.controller('setPasswordController', ['$scope', '$location', 'userServices', ndexed.controllers.account.setPasswordController]);