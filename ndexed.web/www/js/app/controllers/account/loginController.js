﻿ndexed.controllers.account.loginController = function ($scope, $location, authorization, userServices) {
    $scope.user = {};

    $scope.initialize = function() {
        $scope.doneLoading();
    };

    $scope.login = function() {
        var promise = userServices.authenticate($scope.user);

        promise.then(function(authToken) {
            authorization.setAuthToken(authToken);
            $scope.$emit('load-user');
            $location.path('/search');
        });
    };
};

ndexed.controller('loginController', ['$scope', '$location', 'authorization', 'userServices', ndexed.controllers.account.loginController]);