﻿ndexed.controllers.account.organization.newUserController = function($scope, userServices) {
    $scope.newUser = {};

    $scope.addUser = function() {
        $scope.newUser.userName = $scope.newUser.firstName + ' ' + $scope.newUser.lastName;
        $scope.newUser.organizationId = $scope.user.organizationId;

        var userPromise = userServices.create($scope.newUser);

        userPromise.then(function () {
            $scope.$emit('success', 'New User Added');
            $scope.getOrganization();
            $scope.toggleShowAddUser();
        });
    };
};

ndexed.controller('newUserController', ['$scope', 'userServices', ndexed.controllers.account.organization.newUserController]);