﻿ndexed.controllers.account.registerController = function($scope, $location, userServices) {
    $scope.user = {};

    $scope.register = function() {
        var promise = userServices.create($scope.user);

        promise.then(function() { $location.path('/confirmRegistration'); });
    };
};

ndexed.controller('registerController', ['$scope', '$location', 'userServices', ndexed.controllers.account.registerController]);