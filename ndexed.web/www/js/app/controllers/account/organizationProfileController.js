﻿ndexed.controllers.account.organizationProfileController = function($scope, userServices, organizationServices) {
    $scope.organization = null;
    $scope.user = null;
    $scope.canEdit = false;
    $scope.editing = false;
    $scope.templateUrl = 'html/account/organization/view.html';

    $scope.templates = {
        view: 'html/account/organization/view.html',
        edit: 'html/account/organization/edit.html',
        addUser:'html/account/organization/addUser.html'
    };

    $scope.initialize = function () {
        $scope.startLoading();

        var userPromise = userServices.get();
        userPromise.then(function(user) {
            $scope.user = user;
            console.log($scope.user);

            $scope.getOrganization();
        });
    };

    $scope.getOrganization = function() {
        var organizationPromise = organizationServices.get($scope.user.organizationId);
        organizationPromise.then(function (organization) {
            $scope.organization = organization;

            if (organization.administratorId === $scope.user.id) {
                $scope.canEdit = true;
            }
            $scope.doneLoading();
        });
    };

    $scope.toggleEdit = function() {
        $scope.editing = !$scope.editing;

        if ($scope.editing) {
            $scope.templateUrl = $scope.templates.edit;
        } else {
            $scope.templateUrl = $scope.templates.view;
        }
    };

    $scope.save = function() {
        var promise = organizationServices.update($scope.organization);
        promise.then(function() {
            $scope.$emit('success', 'Organization Profile Updated');
            $scope.toggleEdit();
        });
    };

    $scope.toggleShowAddUser = function() {
        $scope.showFlyIn = !$scope.showFlyIn;
        if ($scope.showFlyIn) {
            $scope.flyInTemplate = $scope.templates.addUser;
        }
    };

    $scope.showDeleteConfirmation = function (user) {
        $scope.organization.users.forEach(function (userItem) {
            userItem.showConfirmDelete = false;
        });

        user.showConfirmDelete = true;
    };

    $scope.deleteUser = function(userId) {
        $scope.startLoading();

        var userPromise = userServices.deleteUser(userId);
        userPromise.then(function () {
            $scope.$emit('success', 'User Deleted');

            $scope.getOrganization();
        });
    };
};

ndexed.controller('organizationProfileController', ['$scope', 'userServices', 'organizationServices', ndexed.controllers.account.organizationProfileController]);