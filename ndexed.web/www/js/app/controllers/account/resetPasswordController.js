﻿ndexed.controllers.account.resetPasswordController = function ($scope, $location, userServices) {
    $scope.user = {};

    $scope.resetPassword = function () {

        $scope.displayValidationMessage = false;

        var resetPasswordPromise = userServices.resetPassword($scope.user);

        resetPasswordPromise.then(function () {
            $scope.$emit('info', 'An email has been sent to you with instructions on how to reset your password');
            $location.path('/login');
        });
    };
};

ndexed.controller('resetPasswordController', ['$scope', '$location', 'userServices', ndexed.controllers.account.resetPasswordController]);