﻿/// <reference path="ndexed.js" />

angular.module('d3', [])
  .factory('d3Service', [function () {
      var d3;
      // insert d3 code here
      return d3;
  }]);

var ndexed = angular.module('ndexed', ['ngRoute', 'ngCookies', 'ngProgressLite', 'flow', 'd3', 'ngMaterial', 'ngAnimate']);

ndexed.controllers = {};
ndexed.controllers.account = {};
ndexed.controllers.account.organization = {};
ndexed.controllers.cases = {};
ndexed.controllers.cases.details = {};
ndexed.controllers.clients = {};
ndexed.controllers.search = {};
ndexed.controllers.applications = {};
ndexed.controllers.search.predictions = {};
ndexed.controllers.search.image = {};
ndexed.services = {};
ndexed.utilities = {};
ndexed.directives = {};
ndexed.directives.search = {};

var environment = "prod";

if (environment === "dev") {
    ndexed.value('baseApiUrl', 'http://localhost:49555/api/');
    ndexed.value('searchBaseUrl', 'http://localhost:49555/api');
} else {
    ndexed.value('baseApiUrl', 'https://app.contextum-ml.com/rest/api/');
    ndexed.value('searchBaseUrl', 'https://app.contextum-ml.com/rest/api');
}
/*
ndexed.value('baseApiUrl', 'http://localhost:49555/api/');
ndexed.value('searchBaseUrl', 'http://localhost:49555/api');
*/

/*
ndexed.value('baseApiUrl', 'https://app.contextum-ml.com/rest/api/');
ndexed.value('searchBaseUrl', 'https://app.contextum-ml.com/rest/api');
*/

ndexed.filter("sanitize", ['$sce', function ($sce) {
    return function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    }
}]);
