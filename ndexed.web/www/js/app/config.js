﻿
ndexed.config([
    '$routeProvider',
    '$httpProvider',
    function ($routeProvider, $httpProvider) {

        //routing
        $routeProvider.
             when('/login', {
                 templateUrl: 'html/account/login.html',
                 controller: 'loginController'
             }).
             when('/resetPassword', {
                 templateUrl: 'html/account/resetPassword.html',
                 controller: 'resetPasswordController'
             }).
             when('/register', {
                 templateUrl: 'html/account/register.html',
                 controller: 'registerController'
             }).
             when('/confirmRegistration', {
                 templateUrl: 'html/account/confirmRegistration.html'
             }).
             when('/setPassword', {
                 templateUrl: 'html/account/setPassword.html',
                 controller: 'setPasswordController'
             }).
             when('/admin', {
                 templateUrl: 'html/account/organizationProfile.html',
                 controller: 'organizationProfileController'
             }).
             when('/dashboard', {
                 templateUrl: 'html/dashboard.html',
                 controller: 'dashboardController'
             }).
             when('/search-list', {
                 templateUrl: 'html/search/listing.html',
                 controller: 'searchListController'
             }).
             when('/search/:searchId/history', {
                 templateUrl: 'html/search/history.html',
                 controller: 'searchHistoryController'
             }).
             when('/search/:searchId/saved', {
                 templateUrl: 'html/search/results/saved.html',
                 controller: 'savedResultsController'
             }).
             when('/search/:searchId/history/:historyId', {
                 templateUrl: 'html/search/search.html',
                 controller: 'providerSearchController'
             }).
             when('/search/:searchId', {
                 templateUrl: 'html/search/search.html',
                 controller: 'providerSearchController'
             }).
            otherwise({
                redirectTo: '/search-list'
            });

        //global error handler
        $httpProvider.interceptors.push(['$q', '$location', '$rootScope', function ($q, $location, $rootScope) {

            return {
                'request': function (config) {
                    return config || $q.when(config);
                },

                'response': function (response) {
                    return response || $q.when(response);
                },

                'responseError': function(rejection) {
                    if (rejection.status === 401) {
                        $location.path('/login');
                    } else {
                        $rootScope.$emit('error', rejection.data.message || rejection.exceptionMessage);
                    }
                    var result = $q.reject(rejection);

                    return result;
                }

            };

        }]);
    }
]);

