﻿ndexed.directives.search.cpcDirective = function (d3Service) {
    return {
        restrict: 'E',
        templateUrl:'html//search/results/cpcTable.html',
        scope: {
            cpcs: '=',
            options: '=',
        },
        link: function (scope, element, attrs) {

            scope.$watch('cpcs', (prev, current) => {
                var counts = scope.cpcs.map((item) => { return item.count });
                var maxCount = Math.max(...counts);
                var step = 100 / maxCount;

                var total = .0001;
                if (counts.length > 0) {
                    total = counts.reduce(function (a, b) { return a + b; });
                }

                scope.cpcItems = scope.cpcs.map((item) => {

                    return {
                        text: item.cpc,
                        percent: ((item.count / total) * 100).toFixed(2)
                    }
                });
            });
            
        }
    };
};

ndexed.directive('cpcDirective', ['d3Service', ndexed.directives.search.cpcDirective]);