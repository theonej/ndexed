﻿ndexed.directives.search.termDirective = function (d3Service) {
    return {
        restrict: 'E',
        templateUrl: 'html//search/results/termChart.html',
        scope: {
            terms: '='
        },
        link: function (scope, element, attrs) {

            scope.$watch('terms', (prev, current) => {

                var svg = d3.select("#termChart");

                var x = 0;
                var y = 0;

                var scores = scope.terms.map((item) => { return item.score });
                var maxScore = Math.max(...scores);
                var step = 100 / maxScore;

                var total = .0001;
                if (scores.length > 0) {
                    total = scores.reduce(function (a, b) { return a + b; });
                }

                var items = scope.terms.map((item) => {

                    y += 50;
                    return {
                        x: 0,
                        y: y,
                        width: item.score * step,
                        score:item.score,
                        height: 35,
                        text: item.term,
                        opacity: ((item.score * step) / 100) + .3,
                        percent: ((item.score / total) * 100).toFixed(2)
                    }
                });

                var svg = d3.select("#termChart");
                svg.attr("height", (items.length * 50) + "px")
                svg.selectAll("rect").remove();
                svg.selectAll("text").remove();

                svg.selectAll("rect")
					.data(items)
					.enter()
					.append("rect")
					.style("fill", function (d) { return "#f15a24"; })
                    .style("opacity", function (d) { return d.opacity; })
					.attr("x", function (d) { return d.x; })
					.attr("y", function (d) { return d.y; })
					.attr("width", function (d) { return (d.width / 10) + "%"; })
					.attr("height", function (d) { return d.height; })
			        .transition().delay(100).duration(500)
			        .attr("width", function (d) { return d.width + "%"; });

                svg.selectAll("text")
					.data(items)
					.enter()
					.append("text")
					.style("fill", "#fff")
                    .style('opacity', ".5")
					.attr("x", function (d) { return d.x + 15; })
					.attr("y", function (d) { return d.y + 25; })
					.text(function (d) { return d.text + "  (" + d.score + ")"; })
                    .transition()
                    .duration(100)
                    .style('opacity', "1");
                
            });
        }
    };
};

ndexed.directive('termDirective', ['d3Service', ndexed.directives.search.termDirective]);