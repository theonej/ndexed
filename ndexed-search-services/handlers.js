﻿const cross_reference = require('./cross-reference');
const cpc = require('./cpc');
const terms = require('./terms');
const scores = require('./scores');

exports.cross_reference_lambda_handler = function (event, context, callback) {
    console.log('beginning cross reference search');
    var previous_searches = event.previous_searches || event.body.previous_searches;

    cross_reference.cross_reference(previous_searches)
        .then((results) => {
            var response = {
                statusCode: 200,
               headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(results),
                isBase64Encoded: false,
            };

            callback(null, response);
        })
        .catch((err) => {
            var response = {
                statusCode: 500,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(err),
                isBase64Encoded: false,
            };
            console.log(['handler error', response]);

            callback(err, null);
        });
};

exports.cpc_search_lambda_handler = function (event, context, callback) {
    var cpcs = event.cpcs || event.body.cpcs;
    var take = event.take || event.body.take || 10;
    var skip = event.skip || (event.body || {skip:0}).skip || 0;

    console.log(event);
    console.log([cpcs, take, skip]);

    cpc.cpc_search(cpcs, take, skip)
        .then((results) => {
            var response = {
                statusCode: 200,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(results),
                isBase64Encoded: false,
            };

            callback(null, response);
        })
        .catch((err) => {
            var response = {
                statusCode: 500,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(err),
                isBase64Encoded: false,
            };
            console.log(['handler error', response]);

            callback(err, null);
        });
};

exports.terms_search_lambda_handler = function (event, context, callback) {
    var criteria = event.criteria || event.body.criteria;
    
    terms.terms_search(criteria)
        .then((results) => {
            var response = {
                statusCode: 200,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(results),
                isBase64Encoded: false,
            };

            callback(null, response);
        })
        .catch((err) => {
            console.error(err);
            var response = {
                statusCode: 500,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(err),
                isBase64Encoded: false,
            };
            console.log(['handler error', response]);

            callback(err, null);
        });
};


exports.scores_search_lambda_handler = function (event, context, callback) {
    var criteria = event.criteria || event.body.criteria;

    scores.get_scores(criteria)
        .then((results) => {
            var response = {
                statusCode: 200,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(results),
                isBase64Encoded: false,
            };

            callback(null, response);
        })
        .catch((err) => {
            console.error(err);
            var response = {
                statusCode: 500,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(err),
                isBase64Encoded: false,
            };
            console.log(['handler error', response]);

            callback(err, null);
        });
};