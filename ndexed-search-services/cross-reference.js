﻿const q = require('q');
const elastic = require('./clients/elastic-search-client');
const patent_client = require('./clients/patent_client.js');
const scorer = require('./clients/score-client');
const query_factory = require('./query/queryFactory');

module.exports = {
    cross_reference: function (previous_searches) {
        var deferred = q.defer();
        
        var promises = previous_searches.map((search) => {
            return getPreviousSearchPatentNumbers(search);
        });

        q.all(promises)
            .then((unique_patent_number_lists) => {
                return getCommonResults(unique_patent_number_lists);
            })
            .then((common) => {
                if (common.length < 1) {
                    deferred.resolve(common);
                } else {
                    return scoreResults(common, previous_searches);
                }
            })
            .then((scored_results) => {
                deferred.resolve(scored_results);
            })
            .catch((err) => {
                deferred.reject(err);
            });
        return deferred.promise;
    }
};

function getPreviousSearchPatentNumbers(previous_search) {
    var deferred = q.defer();

    var query_promises = [
        getClaimsQueries(previous_search),
        getSummaryQueries(previous_search),
        getApplicationQueries(previous_search)
    ];

    q.all(query_promises)
        .then((results) => {
            const unique_patent_numbers = getUniquePatentNumbers(results);

            deferred.resolve(unique_patent_numbers);
        })
        .catch((err) => {
            deferred.reject(err);
        });

    return deferred.promise;
}

function getUniquePatentNumbers(searchResults) {
    var flat_list = [].concat.apply([], searchResults);
    
    var patent_numbers = flat_list.map((result) => {
        return result.patent_id;
    });

    var unique = new Set(patent_numbers);
    
    return [...unique];
}
/*
    for each previous search, score all documents against that search
*/
function scoreResults(results, previous_searches) {
    var deferred = q.defer();

    //call the scoring code to score results
    var promises = previous_searches.map((previous_search) => {
        //would use splice, but it will persist changes
        var score_documents= [
            { contents: previous_search }
        ].concat(results);
        
        return scorer.score_results(score_documents, 1);
    });

    q.all(promises)
        .then((scored_results) => {
            var scored_search_results = previous_searches.map((search, index) => {
          
                return {
                    search: search,
                    results:scored_results[index]
                }
            });
            deferred.resolve(scored_search_results);
        })
        .catch((err) => {
            deferred.reject(err);
        });

    return deferred.promise;
};

function getCommonResults(unique_patent_number_lists) {
    var deferred = q.defer();
    
    var patent_numbers = [].concat.apply([], unique_patent_number_lists);

    //now cross reference results to find common items
    var common_patent_numbers = patent_numbers.filter((item) => {
        return patent_numbers.filter((patent_number) => {
            return item === patent_number;
        }).length === unique_patent_number_lists.length//this means it must be in all lists
    });

    var unique_common_numbers = new Set(common_patent_numbers);

    console.log('getting unique patents');
    var patent_promises = [...unique_common_numbers].map((patent_id) => {
        return patent_client.get_patent(patent_id);
    });

    q.all(patent_promises)
        .then((patents) => {
            deferred.resolve(patents);
        })
        .catch((err) => {
            deferred.reject(err);
        });

    return deferred.promise;
};

function getClaimsQueries(search) {
    var deferred = q.defer();

    var query = query_factory.getClaimsQuery(search);

    elastic.query_post(query)
        .then((results) => {
            var contents = results.map((result) => {
                result.contents = result.claim;
                delete result.claim;
                return result;
            });
            deferred.resolve(contents);
        })
        .catch((err) => {
            deferred.reject(err);
        });

    return deferred.promise;
};

function getSummaryQueries(search) {
    var deferred = q.defer();

    var query = query_factory.getSummaryQuery(search);

    elastic.query_post(query)
        .then((results) => {
            var contents = results.map((result) => {
                result.contents = result.summary;
                delete result.summary;
                return result;
            });
            deferred.resolve(contents);
        })
        .catch((err) => {
            deferred.reject(err);
        });

    return deferred.promise;
};


function getApplicationQueries(search) {
    var deferred = q.defer();

    var query = query_factory.getApplicationQuery(search);

    elastic.query_post(query)
        .then((results) => {
            var contents = results.map((result) => {
                result.contents = result.text;
                delete result.text;
                return result;
            });
            deferred.resolve(contents);
        })
        .catch((err) => {
            deferred.reject(err);
        });
    return deferred.promise;
};