﻿const q = require('q');
const elastic = require('./clients/elastic-search-client');
const patentClient = require('./clients/patent_client');

module.exports = {
    cpc_search:function (cpcs, take, skip) {
        var deferred = q.defer();

        //search by cpcs
        searchCpcs(cpcs, take, skip)
            .then((results) => {
                const patentIds = getPatentIds(results);

                const paternPromises = patentIds.map((patentId) => {
                    return patentClient.get_patent(patentId);
                });

                return q.all(paternPromises);
            })
            .then((patents) => {
                patents = patents.map((patent) => {
                        patent.cpcsSearched = cpcs.length;
                        patent.cpcsMatched = getCpcMatchCount(cpcs, patent);
                        
                        return patent;
                });

                deferred.resolve(patents);
            })
            .catch((err) => {
                console.log(err);
                deferred.reject(err);
            });
        return deferred.promise;
    }
};

function getCpcMatchCount(cpcs, patent) {
    
    var patentCpcs = patent.cpc.map((cpc) => {
        return [cpc.section, cpc.subSection, cpc.group, cpc.subGroup];
    });

    var flat = new Set([].concat.apply([], patentCpcs));

    var filtered = [...flat].filter((item) => {
        return cpcs.indexOf(item) >= 0;
    });

    return filtered.length;
};

function getPatentIds(cpcSearchResults) {
    var patentIds = cpcSearchResults.map((result) => {
        return result.patent_id;
    });

    var unique = new Set(patentIds);

    return [...unique];
};

function searchCpcs(cpcs, take, skip) {
    var deferred = q.defer();

    var promises = cpcs.map((cpc) => {
        var request = {
            query: {
                multi_match: {
                    query: cpc,
                    fields: ["section_id", "subsection_id", "group_id", "subgroup_id"]
                }
            }
        };

        var query = {
            request: request,
            index: 'n-dexed-cpc',
            type: 'cpc',
            take: take,
            skip: skip
        };

        return elastic.query_post(query);
    });
    
    q.all(promises)
        .then((resultArrays) => {
            deferred.resolve([].concat.apply([], resultArrays));
        })
        .catch((err) => {
            deferred.reject(err);
        });
    
    return deferred.promise;
};


