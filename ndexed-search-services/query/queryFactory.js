﻿module.exports = {
    getClaimsQuery: function (search) {

        return {
            index: 'n-dexed',
            type: 'claims',
            request: {
                query: {
                    common: {
                        claim: {
                            query: search,
                            cutoff_frequency: 0.001
                        }
                    }
                }
            }
        }
    },

    getSummaryQuery: function (search) {

        return {
            index: 'n-dexed-summary',
            type: 'summary',
            request: {
                query: {
                    common: {
                        summary: {
                            query: search,
                            cutoff_frequency: 0.001
                        }
                    }
                }
            }
        }
    },

    getApplicationQuery: function (search) {
        return {
            index: 'n-dexed-application-text',
            type: 'application-text',
            request: {
                query: {
                    common: {
                        text: {
                            query: search,
                            cutoff_frequency: 0.001
                        }
                    }
                }
            }
        }
    }
}