﻿const q = require('q');
const elastic = require('./clients/elastic-search-client');
const scorer = require('./clients/score-client');
const query_factory = require('./query/queryFactory');

module.exports = {
    get_scores: function (criteria) {
        var deferred = q.defer();

        var searchPromises = [
            searchSummaries(criteria),
            searchClaims(criteria),
            searchApplications(criteria)
        ];

        q.all(searchPromises)
            .then((responses) => {
                var contents = [].concat.apply([], responses).map((item) => {
                    return { contents: item }
                });
                var score_documents = [
                    { contents: criteria.query }
                ].concat(contents);
                
                return scorer.score_results(score_documents, 1);
            })
            .then((scored_docs) => {
                const scores = scored_docs.map((scored_doc) => {
                    return scored_doc.score;
                }).sort((a, b) => {
                    return b - a;
                 });

                deferred.resolve(scores);
            })
            .catch((err) => {
                deferred.reject(err);
            })

        return deferred.promise;
    }
};

function searchSummaries(criteria) {
    var deferred = q.defer();

    var query = query_factory.getSummaryQuery(criteria.query);

    elastic.query_post(query)
        .then((results) => {
            var contents = results.map((result) => {
                return result.summary;
            });
            deferred.resolve(contents);
        })
        .catch((err) => {
            deferred.reject(err);
        });

    return deferred.promise;
}

function searchClaims(criteria) {
    var deferred = q.defer();

    var query = query_factory.getClaimsQuery(criteria.query);

    elastic.query_post(query)
        .then((results) => {
            var contents = results.map((result) => {
                return result.claim;
            });
            deferred.resolve(contents);
        })
        .catch((err) => {
            deferred.reject(err);
        });

    return deferred.promise;
}
function searchApplications(criteria) {
    var deferred = q.defer();

    var query = query_factory.getApplicationQuery(criteria.query);

    elastic.query_post(query)
        .then((results) => {
            var contents = results.map((result) => {
                return result.text;
            });
            deferred.resolve(contents);
        })
        .catch((err) => {
            deferred.reject(err);
        });
    return deferred.promise;
}