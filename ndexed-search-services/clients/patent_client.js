﻿const q = require('q');
const got = require('got');

const BASE_URL = process.env.ELASTICSEARCH_BASE_URL || 'http://172.30.0.233:9200';
module.exports = {
    get_patent: function (patent_id) {
        return get_patent_info(patent_id);
    }
};

function get_patent_info(patent_id) {
    var deferred = q.defer();

    var promises = [
        get_patent(patent_id),
        get_summary(patent_id),
        get_claims(patent_id),
        get_citations(patent_id),
        get_cpc(patent_id)
    ];

    q.all(promises)
        .then((results) => {
            var patent = results[0];
            patent.description = results[1];
            patent.contents = patent.description;
            patent.claims = results[2];
            patent.citations = results[3];
            patent.cpc = results[4];

            deferred.resolve(patent);
        })
        .catch((err) => {
            deferred.reject(err);
        });
    return deferred.promise;
}

function get_patent(patent_id) {
    var deferred = q.defer();

    var config = {
        json: true
    };

    var url = BASE_URL + '/n-dexed-patent/patent/_search?q=number:' + patent_id;

    got(url, config)
        .then((response) => {
            const hit = response.body.hits.hits[0] || { _source: '' };
            var patent = hit._source;
            patent.patentNumber = patent_id;
            patent.link = 'http://pdfpiw.uspto.gov/.piw?PageNum=0&docid=' + patent_id;

            deferred.resolve(patent);
        })
        .catch((err) => {
            console.log(['elastic err', err]);
            deferred.reject(err);
        });

    return deferred.promise;
}

function get_summary(patent_id) {
    var deferred = q.defer();

    var config = {
        json: true
    };

    var url = BASE_URL + '/n-dexed-summary/summary/_search?q=patent_id:' + patent_id;
    
    got(url, config)
        .then((response) => {
            const hit = response.body.hits.hits[0] || {_source:''};
            var patent = hit._source;
            deferred.resolve(patent.summary);
        })
        .catch((err) => {
            console.log(['elastic err', err]);
            deferred.reject(err);
        });

    return deferred.promise;
}

function get_claims(patent_id) {
    var deferred = q.defer();

    var config = {
        json: true
    };

    var url = BASE_URL + '/n-dexed/claims/_search?q=patent_id:' + patent_id + '&size=100';

    got(url, config)
        .then((response) => {
            var claims = response.body.hits.hits.map((hit) => {
                return {
                    claim: hit._source.claim,
                    patentNumber: hit._source.patent_id,
                    sequence:hit._source.sequence
                }
            });
            deferred.resolve(claims);
        })
        .catch((err) => {
            console.log(['elastic err', err]);
            deferred.reject(err);
        });

    return deferred.promise;
}
function get_citations(patent_id) {
    var deferred = q.defer();

    var config = {
        json: true
    };

    var url = BASE_URL + '/n-dexed-citations/citations/_search?q=patent_id:' + patent_id + '&size=100';

    got(url, config)
        .then((response) => {
            var citations = response.body.hits.hits.map((hit) => {
                const data = hit._source;
                return {
                    patentNumber:patent_id,
                    title:'',
                    abstract:'',
    
                    citedPatentNumber:data.citation_id,
                    country:data.country,
                    name:data.name,
                    sequence:data.sequence,
                    category:data.category
                }
            });
            deferred.resolve(citations);
        })
        .catch((err) => {
            console.log(['elastic err', err]);
            deferred.reject(err);
        });

    return deferred.promise;
}
function get_cpc(patent_id) {
    var deferred = q.defer();

    var config = {
        json: true
    };

    var url = BASE_URL + '/n-dexed-cpc/cpc/_search?q=patent_id:' + patent_id + '&size=100';

    got(url, config)
        .then((response) => {
            var cpc = response.body.hits.hits.map((hit) => {
                const data = hit._source;
                return {
                    patentNumber:data.patent_id,
                    section:data.section_id,
                    subSection:data.subsection_id,
                    group:data.group_id,
                    subGroup:data.subgroup_id,
                    sequence:data.sequence
                }
            });
            deferred.resolve(cpc);
        })
        .catch((err) => {
            console.log(['elastic err', err]);
            deferred.reject(err);
        });

    return deferred.promise;
}