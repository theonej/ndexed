﻿const q = require('q');
const got = require('got');
const util = require('util');

const BASE_URL = process.env.SCORING_RESULTS_URL || 'http://172.30.0.233:5000';
module.exports = {
    score_results: function (documents, source_document_count) {

        var deferred = q.defer();

        scoreContents(documents, source_document_count)
            .then((results) => {
                var sourceDocuments = documents.slice(0, source_document_count).map((document) => {
                    return document.contents;
                });
                
                var claims_promises = documents.filter((item) => {
                    return item.claims; 
                }).map((document) => {
                    return scoreClaims(sourceDocuments, document.claims);
                });

                return q.all(claims_promises);
            })
            .then((claims_results) => {
                results = documents.splice(source_document_count).map((document, index) => {
                    document.claims = claims_results[index];
                    return document;
                });


                deferred.resolve(results);
            })
            .catch((err) => {
                console.log(['scoring error', err]);
                deferred.reject(err);
            });

        return deferred.promise;
    }
};

function scoreContents(documents, source_document_count) {
    var deferred = q.defer();

    var config = {
        json: true,
        body: {
            documents: JSON.stringify(documents.map((document) => {
                return document.contents;
            })),
            source_document_count: source_document_count
        }
    };
    
    var url = BASE_URL + '/documents/scores';
   
    got.post(url, config)
        .then((response) => {
            var scores = response.body[0][0][0];

            var scored_results = documents.slice(source_document_count, documents.length)
                .map((item, index) => {
                    item.score = scores[index];
                    return item;
                });

            deferred.resolve(scored_results);
        })
        .catch((err) => {
            console.log(['error', err]);
            deferred.reject(err);
        });

    return deferred.promise;
};

function scoreClaims(sourceDocuments, claims) {
    var deferred = q.defer();

    var claims_documents = sourceDocuments.concat(claims.map((claims) => {
        return claims.claim.replace(/\\/g, '');
    }));

    var config = {
        json: true,
        body: {
            documents: JSON.stringify(claims_documents),
            source_document_count: sourceDocuments.length
        }
    };
    
    var url = BASE_URL + '/documents/scores';

    got.post(url, config)
        .then((response) => {
            var scores = response.body[0][0][0];
            var scored_results = claims.map((item, index) => {
                    item.claimRank = scores[index];
                
                    return item;
                });

            deferred.resolve(scored_results);
        })
        .catch((err) => {
            console.log(['claims error', err]);
            deferred.reject(err);
        });

    return deferred.promise;
}