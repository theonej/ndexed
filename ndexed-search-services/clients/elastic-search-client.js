﻿const q = require('q');
const got = require('got');

const BASE_URL = process.env.ELASTICSEARCH_BASE_URL || 'http://172.30.0.233:9200';
module.exports = {
    query_post: function (query) {
        console.log('querying elastic at ' + BASE_URL);
        var deferred = q.defer();
        
        var config = {
            json: true,
            body: query.request
        };

        var take = query.take || 10;
        var skip = query.skip || 0;

        var url = BASE_URL + '/' + query.index + '/' + query.type + '/_search?size=' + take + '&from=' + skip; 

        //console.log(['elastic request', config]);

        got.post(url, config)
            .then((response) => {
                var source = response.body.hits.hits.map((hit) => {
                    return hit._source;
                });
                deferred.resolve(source);
            })
            .catch((err) => {
                console.log(['elastic err', err]);
                deferred.reject(err);
            });

        return deferred.promise;
    }
};