﻿const PROTO_PATH = '../ndexed-protocols/search-service.proto';
const grpc = require('grpc');
const proto_descriptor = grpc.load(PROTO_PATH);
const search = proto_descriptor.search;

function crossReference(call, callback) {

    const cross_reference = require('./cross-reference');
    var previous_searches = call.request.previous_searches;

    cross_reference.cross_reference(previous_searches)
        .then((results) => {
            var searchResults = results.map((result) => {
                return result.results;
            });

            var flat = formatResults(searchResults);

            callback(null, flat);
        })
        .catch((err) => {
            console.error(err);
            callback(err, null);
        });
};

function cpcSearch(call, callback) {
    const cpc = require('./cpc');
    
    const cpcs = call.request.cpcs;
    const take = call.request.take;
    const skip = call.request.skip;

    cpc.cpc_search(cpcs, take, skip)
        .then((results) => {
   
            var flat = formatResults(results);
            
            callback(null, flat);
        })
        .catch((err) => {
            console.error(err);
            callback(err, null);
        });
}


function termsSearch(call, callback) {
    const terms = require('./terms');
    console.log(call);
    const criteria = call.request.criteria;


    terms.terms_search(criteria)
        .then((results) => {

            var flat = formatResults(results);

            callback(null, flat);
        })
        .catch((err) => {
            console.error(err);
            callback(err, null);
        });
}

function formatResults(results) {
    return [].concat.apply([], results).map((result) => {
        //remove these one you define the types in the .proto file

        delete result.contents;
        delete result.claims;
        delete result.cpc;
        delete result.citations;
        delete result.cpcsSearched;
        delete result.cpcsMatched;

        return result;
    });
}

function initializeServer() {
    const services = {
        crossReference: crossReference,
        cpcSearch: cpcSearch,
        termsSearch:termsSearch
    };

    const server = new grpc.Server();

    server.addService(search.service, services);

    return server;
};

var server = initializeServer();
server.bind('0.0.0.0:8000', grpc.ServerCredentials.createInsecure());
server.start();
console.log('server running on 0.0.0.0:8000');

