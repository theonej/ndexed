﻿const q = require('q');
const elastic = require('./clients/elastic-search-client');
const patentClient = require('./clients/patent_client');

module.exports = {
    terms_search: function (criteria) {
        var deferred = q.defer();

        const queryPromises = [
            searchSummaryTerms(criteria),
            searchClaimTerms(criteria)
        ];
        q.all(queryPromises)
            .then((results) => {
                const flat = [].concat.apply([], results);
                const patentIds = getPatentIds(flat);

                const paternPromises = patentIds.map((patentId) => {
                    return patentClient.get_patent(patentId);
                });

                return q.all(paternPromises);
            })
            .then((patents) => {
                deferred.resolve(patents);
            })
            .catch((err) => {
                deferred.reject(err);
            });

        return deferred.promise;
    }
};

function searchSummaryTerms(criteria) {
    const request = getSummaryTermsRequest(criteria);

    const query = {
        request: request,
        index: 'n-dexed-summary',
        type: 'summary',
        take: criteria.take,
        skip: criteria.skip
    };

    return elastic.query_post(query);
};

function searchClaimTerms(criteria) {
    const request = getClaimTermsRequest(criteria);

    const query = {
        request: request,
        index: 'n-dexed',
        type: 'claims',
        take: criteria.take,
        skip: criteria.skip
    };

    return elastic.query_post(query);
};

function getSummaryTermsRequest(criteria) {

    var must = criteria.included.map((included_term) => {
        return {
            match: {
                summary: included_term
            }
        }
    });

    var must_not = criteria.excluded.map((excluded_term) => {
        return {
            match: {
                summary: excluded_term
            }
        }
    });

    var request = {
        query: {
            bool: {
                must: must,
                must_not: must_not
            }
        }
    };

    return request;
};

function getClaimTermsRequest(criteria) {

    var must = criteria.included.map((included_term) => {
        return {
            match: {
                claim: included_term
            }
        }
    });

    var must_not = criteria.excluded.map((excluded_term) => {
        return {
            match: {
                claim: excluded_term
            }
        }
    });

    var request = {
        query: {
            bool: {
                must: must,
                must_not: must_not
            }
        }
    };

    return request;
};

function getPatentIds(searchResults) {
    var patentIds = searchResults.map((result) => {
        return result.patent_id;
    });

    var unique = new Set(patentIds);

    return [...unique];
};