﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.DocumentProcessing.iTextSharp
{
    public class DocumentConverter
    {
        public string ConvertHtmlToBase64Pdf(string html)
        {

            var document = new Document();
            using (document)
            {
                var stream = new MemoryStream();
                using (stream)
                {
                    var writer = PdfWriter.GetInstance(document, stream);
                    
                    var worker = new HTMLWorker(document);
                    using (worker)
                    {
                        
                        document.Open();
                        worker.StartDocument();

                        var reader = new StringReader(html);
                        using (reader)
                        {
                            worker.Parse(reader);
                        }

                        worker.EndDocument();
                        worker.Close();
                        document.Close();
                    }

                    var bytes = stream.ToArray();
                    var base64Pdf = Convert.ToBase64String(bytes);

                    return base64Pdf;
                }
            }
        }

    }
}
