﻿using Microsoft.Office.Interop.Word;
using System;
using System.IO;

namespace NDexed.DocumentProcessing.OfficeInterop
{
    public class DocumentConverter
    {
        public string ConvertBase64WordToBase64Pdf(string base64EncodedWordDoc)
        {
            var filePath = GetDocContents(base64EncodedWordDoc);

            var pdfBytes = File.ReadAllBytes(filePath);
            var base64PdfString = Convert.ToBase64String(pdfBytes);

            File.Delete(filePath);

            return base64PdfString;
        }

        #region Private Methods

        private string GetDocContents(string base64Word)
        {
            var tempFilePath = Path.GetTempFileName();

            SaveDocFile(base64Word, tempFilePath);

            var word = new Application();
            
            object miss = System.Reflection.Missing.Value;
            object path = tempFilePath;
            object readOnly = false;
            var docs = word.Documents.Open(ref path, 
                                            ref miss, 
                                            ref readOnly, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss, 
                                            ref miss);

            object outputPath = tempFilePath.Replace(".tmp", ".pdf");
            object format = WdSaveFormat.wdFormatPDF;

            docs.SaveAs2(ref outputPath, ref format);
            docs.Close();

            File.Delete(tempFilePath);

            return outputPath.ToString();
        }

        private void SaveDocFile(string base64Word, string path)
        {
            var bytes = Convert.FromBase64String(base64Word);

            var fileStream = File.Create(path);
            using (fileStream)
            {
                fileStream.Write(bytes, 0, bytes.Length);

            }
        }

        #endregion
    }
}
