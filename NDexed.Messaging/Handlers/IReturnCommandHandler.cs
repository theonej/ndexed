﻿using NDexed.Messaging.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Handlers
{
    public interface IReturnCommandHandler<TCommand, TReturn> where TCommand : ICommand
    {
        TReturn Handle(TCommand command);
    }
}
