﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Query;
using NDexed.DataAccess.Repositories;
using NDexed.DataAccess.Response;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Documents;
using NDexed.Messaging.Commands.Document;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using MimeKit;
using System.IO;
using NDexed.Domain.Models.User;
using System.Text;
using System.Linq;
using NDexed.Messaging.Messages;
using System.Configuration;
using System.IO.Compression;
using NDexed.Domain.Models;
using NDexed.Messaging.Commands.Applications;
using NDexed.Messaging.Handlers.Applications;

namespace NDexed.Messaging.Handlers
{
    public class DocumentCommandHandler : ICommandHandler<FindMatchingDocumentOpinionsCommand>,
                                          ICommandHandler<UpdateDocumentCommand>,
                                          ICommandHandler<DownloadEmailCommand>,
                                          ICommandHandler<CreateDocumentZipCommand>
    {
        private readonly IRepository<Guid, DocumentInfo> m_DocumentRepo;
        private readonly ISearchableRepository<OpinionSearchQuery, OpinionSearchResponse> m_searchRepo;
        private readonly IRepository<DataRequestInfo, DataInfo> m_EmailRepository;
        private readonly ISearchableRepository<UserInfo, UserInfo> m_UserRepo;
        private readonly IMessager m_Messager;

        public DocumentCommandHandler(IRepository<Guid, DocumentInfo>  docRepo, 
                                      ISearchableRepository<OpinionSearchQuery, OpinionSearchResponse> searchRepo,
                                      IRepository<DataRequestInfo, DataInfo> emailRepository,
                                      ISearchableRepository<UserInfo, UserInfo> userRepo,
                                      IMessager messager)
        {
            Condition.Requires(docRepo).IsNotNull();
            Condition.Requires(searchRepo).IsNotNull();
            Condition.Requires(emailRepository).IsNotNull();
            Condition.Requires(userRepo).IsNotNull();
            Condition.Requires(messager).IsNotNull();

            m_DocumentRepo = docRepo;
            m_searchRepo = searchRepo;
            m_EmailRepository = emailRepository;
            m_UserRepo = userRepo;
            m_Messager = messager;
        }

        public void Handle(DownloadEmailCommand command)
        {
            if(command ==null || command.MessageData == null)
            {
                throw new NullReferenceException("The command and command message must be provided");
            }


            try
            {

                var request = GetDataRequest(command);
                var data = m_EmailRepository.Get(request);
                data.Parameters = request.RequestParameters;

                var message = GetMimeMessage(data);

                //lookup the user by sender Id
                var user = GetUser(message);

                var importCommand = new ImportApplicationCommand();
                importCommand.ApplicationName = message.Subject;
                importCommand.Abstract = message.GetTextBody(MimeKit.Text.TextFormat.Text);
                importCommand.EmailAddress = user.EmailAddress;

                var handler = new ImportApplicationHandler(m_Messager);
                handler.Handle(importCommand);

                /*
                //parse the subject line to see if it contains a matter ID
                //var caseId = GetCaseId(message.Subject);

                //create a document from the email
                var emailDocument = GetDocument(message, user);
                m_DocumentRepo.Add(emailDocument);

                //get attachments and create documents from them
                var attachments = GetAttachments(message, user);
                foreach (var attachment in attachments)
                {
                    m_DocumentRepo.Add(attachment);
                }

                //delete item from s3
                */
                m_EmailRepository.Remove(data);
            }
            catch (MissingMemberException ex)
            {
                //do not delete from s3.  Send an alert.
                SendExceptionmessage(ex);
            }
            catch(FileNotFoundException ex)
            {
                SendExceptionmessage(ex);
            }
        }

        public void Handle(UpdateDocumentCommand command)
        {
            var document = m_DocumentRepo.Get(command.DocumentId);
            if (document == null)
            {
                throw new NullReferenceException(string.Format("No document was found for ID {0}", command.DocumentId));
            }

            document.Name = command.DocumentName;
            document.UpdatedBy = command.UpdatedByUser;
            document.UpdatedDateTime = DateTime.Now;

            m_DocumentRepo.Add(document);
        }

        public void Handle(FindMatchingDocumentOpinionsCommand command)
        {
            var document = m_DocumentRepo.Get(command.DocumentId);
            if(document == null)
            {
                throw new NullReferenceException(string.Format("No document was found for ID {0}", command.DocumentId));
            }

            var query = new OpinionSearchQuery();
            query.Jurisdictions = command.Jurisdictions.ToArray();
            query.PageNumber = 0;
            query.PageSize = command.MaxCount;
            query.SearchTerm = document.Contents;

            m_searchRepo.Search(query);
        }

        public void Handle(CreateDocumentZipCommand command)
        {
            //get all documents
            var documents = new List<DocumentInfo>();
            foreach (var docId in command.DocumentIds)
            {
                var doc = m_DocumentRepo.Get(docId);
                documents.Add(doc);
            }

            //create zip archive
            var archive = ZipFile.Open(command.OutputFileName, ZipArchiveMode.Create);
            using (archive)
            {
                var fileNames = new List<string>();

                foreach (var document in documents)
                {
                    var tempFileName = Path.GetTempFileName();
                    fileNames.Add(tempFileName);
                    SaveDocument(tempFileName, document.Base64EncodedData);

                    archive.CreateEntryFromFile(tempFileName, document.Name, CompressionLevel.Fastest);

                }
                
                foreach (var fileName in fileNames)
                {
                    File.Delete(fileName);
                }
            }
        }

        #region Private Methods

        private void SaveDocument(string path, string base64EncodedData)
        {
            var bytes = Convert.FromBase64String(base64EncodedData);
            SaveDocument(path, bytes);
        }


        private void SaveDocument(string path, byte[] bytes)
        {
            var fileStream = File.Create(path);
            using (fileStream)
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }
        }

        private void SendExceptionmessage(Exception ex)
        {
            var message = m_Messager.CreateMessage();
            message.Recipients = GetRecipients().ToList();
            message.Sender = "support@n-dexed.com";
            message.Title = ex.Message;
            message.Body = ex.StackTrace;

            m_Messager.SendMessage(message);
        }

        private string[] GetRecipients()
        {
            var recipientList = ConfigurationManager.AppSettings["ExceptionEmailRecipients"];
            if (string.IsNullOrEmpty(recipientList))
            {
                recipientList = "support@n-dexed.com";
            }

            return recipientList.Split(';');
        }

        private List<DocumentInfo> GetAttachments(MimeMessage message, UserInfo user)
        {
            var returnValue = new List<DocumentInfo>();

            if (message.Attachments != null)
            {
                foreach (var attachment in message.Attachments)
                {
                    var document = GetDocument(message, attachment, user);
                    returnValue.Add(document);
                }
            }

            return returnValue;
        }

        private DocumentInfo GetDocument(MimeMessage message, MimeEntity attachment, UserInfo user)
        {
            var document = new DocumentInfo();
            document.CreatedBy = user.UserName;
            document.OrganizationId = user.OrganizationId;
            document.CaseId = GetCaseId(message.Subject);
            document.Base64EncodedData = GetBase64EncodedData(attachment);

            document.Name = attachment.ContentDisposition.FileName;

            document.Id = Guid.NewGuid();
            document.Tags = new string[]
            {
                "email attachment",
                message.From[0].Name
            };

            return document;
        }

        private string GetAttachmentName(HeaderList headers)
        {
            var returnValue = "undefined";

            var header = headers["Content-Disposition"];
            if (header != null)
            {
                returnValue = header.Replace("attachment; filename=\"", string.Empty);
                returnValue = header.Replace("\"", string.Empty);
            }

            return returnValue;
        }

        private DocumentInfo GetDocument(MimeMessage message, UserInfo user)
        {
            var emailDocument = new DocumentInfo();

            emailDocument.CreatedBy = user.UserName;
            emailDocument.OrganizationId = user.OrganizationId;
            emailDocument.CaseId = GetCaseId(message.Subject);
            emailDocument.Base64EncodedData = GetBase64EncodedData(message);
            emailDocument.Name = message.Subject + ".msg";
            emailDocument.Id = Guid.NewGuid();
            emailDocument.Tags = new string[]
            {
                "email",
                message.From[0].Name
            };

            return emailDocument;
        }

        private string GetBase64EncodedData(MimeMessage message)
        {
            var bytes = Encoding.UTF8.GetBytes(message.TextBody);

            var base64String = Convert.ToBase64String(bytes);

            return base64String;
        }


        private string GetBase64EncodedData(MimeEntity attachment)
        {
            var stream = new MemoryStream();
            using (stream)
            {
                var part = (MimePart)attachment;
                part.ContentObject.DecodeTo(stream);

                stream.Position = 0;
                var bytes = stream.ToArray();

                var base64String = Convert.ToBase64String(bytes);

                return base64String;
            }
        }

        private Guid GetCaseId(string subject)
        {
            var returnValue = Guid.Empty;

            if (subject.Contains("Matter: "))
            {
                string caseName = subject.Replace("Matter: ", "").Trim();

                //lookup case by name
            }

            return returnValue;
        }

        private UserInfo GetUser(MimeMessage message)
        {
            var from = message.From.FirstOrDefault() as MailboxAddress;
            if (from == null)
            {
                throw new MissingMemberException("Could not identify the sender of the message");
            }

            var criteria = new UserInfo();
            criteria.EmailAddress = from.Address;

            var users = m_UserRepo.Search(criteria) as List<UserInfo>;

            if (users == null || users.Count == 0)
            {
                throw new MissingMemberException(string.Format("Email address {0} is not a registered user", criteria.EmailAddress));
            }

            return users[0];
        }

        private MimeMessage GetMimeMessage(DataInfo data)
        {
            var bytes = Convert.FromBase64String(data.SerializedData);
            var memoryStream = new MemoryStream(bytes);
            using (memoryStream)
            {
                memoryStream.Position = 0;
                var message = MimeMessage.Load(memoryStream);

                return message;
            }
        }
        private DataRequestInfo GetDataRequest(DownloadEmailCommand command)
        {
            var messageInfo = (JObject)JsonConvert.DeserializeObject(command.MessageData.Body);
            var messageDetails = GetRequiredObject(messageInfo, "Message");
            var receipt = GetRequiredObject(messageDetails, "receipt");
            var action = GetRequiredObject(receipt, "action");

            var bucket = GetRequiredString(action, "bucketName");
            var key = GetRequiredString(action, "objectKey");

            var request = new DataRequestInfo();
            request.RequestParameters = new Dictionary<string, object>();
            request.RequestParameters.Add("BucketName", bucket);
            request.RequestParameters.Add("ObjectKey", key);

            return request;
        }

        private JObject GetRequiredObject(JObject parent, string name)
        {
            var detailString = GetRequiredString(parent, name);

            return (JObject)JsonConvert.DeserializeObject(detailString);
        }

        private string GetRequiredString(JObject parent, string name)
        {
            var detail = parent[name];
            if (detail == null)
            {
                throw new NullReferenceException(string.Format("Could not find the required object {0}.  Cannot proceed", name));
            }

            return detail.ToString();
        }

        #endregion
    }
}
