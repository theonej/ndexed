﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Cases;
using NDexed.Domain.Models.User;
using NDexed.Messaging.Commands.CaseUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Handlers
{
    public class CaseUsersCommandHandler : ICommandHandler<AddCaseUsersCommand>,
                                           ICommandHandler<DeleteCaseUsersCommand>
    {
        private readonly IRepository<Guid, CaseInfo> m_CaseRepo;
        private readonly IRepository<UserInfo, UserInfo> m_UserRepo;

        public CaseUsersCommandHandler(IRepository<Guid, CaseInfo> caseRepo,
                                       IRepository<UserInfo, UserInfo> userRepo)
        {
            Condition.Requires(caseRepo).IsNotNull();
            Condition.Requires(userRepo).IsNotNull();

            m_CaseRepo = caseRepo;
            m_UserRepo = userRepo;
        }

        public void Handle(DeleteCaseUsersCommand command)
        {
            var @case = GetCase(command.CaseId);

            foreach (Guid userId in command.UserIds)
            {
                @case.UserIds.Remove(userId);

                var user = GetUser(userId);

                user.CaseIds.Remove(command.CaseId);

                m_UserRepo.Add(user);
            }

            m_CaseRepo.Add(@case);
        }

        public void Handle(AddCaseUsersCommand command)
        {
            var @case = GetCase(command.CaseId);

            foreach (Guid userId in command.UserIds)
            {
                if(@case.UserIds.IndexOf(userId) == -1)
                {
                    @case.UserIds.Add(userId);
                }

                var user = GetUser(userId);

                if (user.CaseIds.IndexOf(command.CaseId) == -1)
                {
                    user.CaseIds.Add(command.CaseId);
                }

                m_UserRepo.Add(user);
            }

            m_CaseRepo.Add(@case);
        }

        #region private methods

        private CaseInfo GetCase(Guid caseId)
        {
            var @case = m_CaseRepo.Get(caseId);
            if (@case == null)
            {
                throw new KeyNotFoundException(string.Format("No case was found for id {0}", caseId));
            }

            if(@case.UserIds == null)
            {
                @case.UserIds = new List<Guid>();
            }

            return @case;
        }

        private UserInfo GetUser(Guid userId)
        {
            var criteria = new UserInfo()
            {
                Id = userId
            };

            var user = m_UserRepo.Get(criteria);
            if (user == null)
            {
                throw new KeyNotFoundException(string.Format("No user was found for id {0}", userId));
            }

            if(user.CaseIds == null)
            {
                user.CaseIds = new List<Guid>();
            }
            return user;
        }
        #endregion
    }
}
