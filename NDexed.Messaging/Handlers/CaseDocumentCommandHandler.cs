﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Documents;
using NDexed.Domain.Models.Regulations;
using NDexed.Messaging.Commands.Document;
using System;
using System.Linq;
using System.Web;

namespace NDexed.Messaging.Handlers
{
    public class CaseDocumentCommandHandler : ICommandHandler<AddCaseDocumentCommand>
    {
        private readonly IRepository<Guid, DocumentInfo> m_DocumentRepo;
        private readonly IRepository<Guid, RegulationInfo> m_RegulationRepo;
        private readonly IRepository<Guid, TermInfo> m_TermRepo;

        public CaseDocumentCommandHandler(IRepository<Guid, DocumentInfo> documentRepo,
                                          IRepository<Guid, RegulationInfo> regulationRepo,
                                          IRepository<Guid, TermInfo> termRepo)
        {
            Condition.Requires(documentRepo).IsNotNull();
            Condition.Requires(regulationRepo).IsNotNull();
            Condition.Requires(termRepo).IsNotNull();

            m_DocumentRepo = documentRepo;
            m_RegulationRepo = regulationRepo;
            m_TermRepo = termRepo;
        }

        public void Handle(AddCaseDocumentCommand command)
        {
            var document = new DocumentInfo
            {
                Id = Guid.NewGuid(),
                CaseId = command.CaseId,
                OrganizationId = command.OrganizationId,
                Tags = GetTags(command.Tags)
            };

            //find the document, based on type
            switch (command.Type)
            {
                case (AddCaseDocumentCommand.SourceDocumentType.Document):
                    {
                        var sourceDocument = m_DocumentRepo.Get(command.SourceDocumentId);

                        //add to the source document tags
                        sourceDocument.Tags = AddToTags(sourceDocument.Tags, document.Tags);
                        m_DocumentRepo.Add(sourceDocument);

                        document.Base64EncodedData = sourceDocument.Base64EncodedData;
                        document.Name = sourceDocument.Name;
                        document.Size = sourceDocument.Size;
                        document.Type = sourceDocument.Type;
                        document.CreatedBy = sourceDocument.CreatedBy;
                        document.CreatedDateTime = DateTime.Now;
                        document.ContentType = sourceDocument.ContentType;
                    }
                    break;
                case (AddCaseDocumentCommand.SourceDocumentType.Regulation):
                    {
                        var sourceDocument = m_RegulationRepo.Get(command.SourceDocumentId);

                        //add to the source document tags
                        sourceDocument.Tags = AddToTags(sourceDocument.Tags, document.Tags);
                        m_RegulationRepo.Add(sourceDocument);

                        document.Base64EncodedData = sourceDocument.Base64EncodedData;
                        document.Name = sourceDocument.Name;
                        document.Size = sourceDocument.Base64EncodedData.Length;
                        document.Type = DocumentTypes.Private;
                        document.CreatedBy = sourceDocument.CreatedBy;
                        document.CreatedDateTime = DateTime.Now;
                        document.ContentType = MimeMapping.GetMimeMapping(sourceDocument.Name);
                    }
                    break;
                default:
                    {
                        throw new NotSupportedException(string.Format("document type {0} is not supported", command.Type));
                    }
            }

            m_DocumentRepo.Add(document);
        }

        #region private Methods

        private string[] AddToTags(string[] existingTags, string[] newTags)
        {
            var returnValue = GetTags(newTags);
            if(existingTags != null)
            {
                var combinedTags = existingTags.Concat(returnValue).ToArray();
                returnValue = combinedTags.Distinct().ToArray();
            }
            return returnValue;
        }

        private string[] GetTags(string[] newTags)
        {
            string[] tags = new string[] { };
            
            if (newTags != null)
            {
                tags = newTags;
            }

            var termsToRemove = m_TermRepo.GetAll().Select(term => term.Value);
            tags = tags.Where(tag => !termsToRemove.Contains(tag) && !string.IsNullOrEmpty(tag)).ToArray();

            return tags;
        }
        
        #endregion
    }
}
