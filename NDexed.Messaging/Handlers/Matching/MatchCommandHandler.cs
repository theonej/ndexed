﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Data;
using NDexed.Domain.Models.Matching;
using NDexed.Messaging.Commands.Matching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NDexed.Messaging.Handlers.Matching
{
    public class MatchCommandHandler : IReturnCommandHandler<FindMatchesCommand, List<MatchInfo>>,
                                       IReturnCommandHandler<GetMatchClassificationCommand, List<string>>,
                                       ICommandHandler<ExtractMatchTermsAndClassesCommand>
    {
        private readonly ISearchableRepository<FindMatchesCommand, MatchInfo> m_MatchRepo;
        private readonly IRepository<Guid, TermInfo> m_TermRepo;

        public MatchCommandHandler(ISearchableRepository<FindMatchesCommand, MatchInfo> matchRepository,
                                   IRepository<Guid, TermInfo> termRepo)
        {
            Condition.Requires(matchRepository).IsNotNull();
            Condition.Requires(termRepo).IsNotNull();

            m_MatchRepo = matchRepository;
            m_TermRepo = termRepo;
        }

        public void Handle(ExtractMatchTermsAndClassesCommand command)
        {
            int totalRecords = 0;

            var findCommand = new FindMatchesCommand();
            findCommand.MatchType = "patents";
            findCommand.PageNumber = 0;
            findCommand.PageSize = 100;
            findCommand.SearchTerms = new string[] { "a" };

            var termFile = File.CreateText(command.TermOutputFile);
            var classFile = File.CreateText(command.ClassOutputFile);
            var subClassFile = File.CreateText(command.SubClassOutputFile);

            try
            {
                while (totalRecords < command.TotalRecordsToGet)
                {
                    //get a batch of matches
                    var matches = m_MatchRepo.Search(findCommand);

                    //remove words in the terms repo
                    var termsToRemove = m_TermRepo.GetAll().Where(term => !term.Include).Select(item => item.Value).ToList();
                    foreach (var match in matches)
                    {
                        string stringContents = match.Data.applicationContents.ToString();
                        var words = stringContents.Replace('\n', ' ').Split(' ').ToList();
                        words.Remove(" ");

                        //for each record, get unique words
                        var termsLine = string.Join(",", words.Distinct());


                        //get classes
                        var classifyCommand = new GetMatchClassificationCommand();
                        classifyCommand.Match = match;

                        var classifications = Handle(classifyCommand).FirstOrDefault();
                        if (classifications != null)
                        {
                            var parts = classifications.Split(' ');

                            if (parts.Length > 1)
                            {
                                var className = parts[0];
                                var subClass = parts[1];

                                termFile.WriteLine(termsLine);
                                classFile.WriteLine(className);
                                subClassFile.WriteLine(subClass);

                                totalRecords++;
                            }
                        }

                    }

                    findCommand.PageNumber++;
                }
            }
            finally
            {
                termFile.Dispose();
                classFile.Dispose();
                subClassFile.Dispose();
            }
        }

        public List<string> Handle(GetMatchClassificationCommand command)
        {
            var returnValue = new List<string>();

            string applicationText = command.Match.Data.applicationContents.ToString();

            var startString = @"Current CPC Class:";
            var endString = "International Class:";
            var startIndex = applicationText.IndexOf(startString) + startString.Length;
            var endIndex = applicationText.IndexOf(endString);

            if (applicationText.IndexOf(startString) > 0 && endIndex > startIndex)
            {
                var classificationsString = applicationText.Substring(startIndex, endIndex - startIndex);
                var array = classificationsString.Replace('\n', ' ').Trim().Split(';');

                if (classificationsString.Contains("ily"))
                {

                }
                var classifications = array.Select(item => item.Trim()).ToList();

                returnValue =  classifications;
            }

            return returnValue;
        }
        
        

        public List<MatchInfo> Handle(FindMatchesCommand command)
        {
            //prune search terms

            //search for results
            var matches = m_MatchRepo.Search(command);

            //rank results

            return matches.ToList();
        }
    }
}
