﻿using NDexed.CouchDb.Repositories;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models;
using NDexed.Domain.Models.Documents;
using NDexed.Domain.Models.Matching;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Prediction;
using NDexed.Domain.Models.Search;
using NDexed.Messaging.Commands.Applications;
using NDexed.Messaging.Messages;
using NDexed.Search.Analyzers;
using NDexed.Search.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Handlers.Applications
{
    public class ImportApplicationHandler : ICommandHandler<ImportApplicationCommand>
    {
        private const float THRESHOLD = 0.01f;

        private readonly ISearchableRepository<SearchInfo, PatentInfo> m_SearchRepo;
        private readonly ISearchableRepository<SearchInfo, MatchInfo> m_ClaimSearchRepo;
        private readonly ISearchableRepository<SearchInfo, MatchInfo> m_VisionRepo;
        private readonly ISearchableRepository<PredictionService, PredictionInfo> m_PredictionRepo;
        private readonly IRepository<Guid, ApplicationInfo> m_ApplicationRepo;
        private readonly IMessager m_Messager;

        public ImportApplicationHandler(IMessager messager)
        {
            m_SearchRepo = new GoogleRepository();
            m_ClaimSearchRepo = new GoogleClaimsRepository();
            m_ApplicationRepo = new CouchApplicationRepository();
            m_Messager = messager;
        }
        public void Handle(ImportApplicationCommand command)
        {
            //find matching web results
            var matches = FindWebResults(command.Abstract);
            matches = ScoreMatches(command.Abstract, matches);

            //find matching claims

            //find classifications

            //find vision results

            //score each vision result

            //find application, if it exists
            var existingApplication = m_ApplicationRepo
                                                .GetAll()
                                                .FirstOrDefault(
                                                    application => application.Name == command.ApplicationName
                                                );
            //if it does not exist, create it
            if(existingApplication == null)
            {
                AddApplication(command, matches, null);
            }
            else
            {
                UpdateApplication(existingApplication, matches, null);
            }
            //send email notification that application has been created
            var confirmation = GetConfirmationMessage(command);
            m_Messager.SendMessage(confirmation);
        }

        private Guid AddApplication(ImportApplicationCommand command, List<PatentInfo> matches, List<MatchInfo> claimsMatches)
        {
            var newApplication = new ApplicationInfo();
            newApplication.Name = command.ApplicationName;
            newApplication.Abstract = command.Abstract;
            newApplication.CreatedBy = command.EmailAddress;
            newApplication.CreatedDateTime = DateTime.Now;
            newApplication.FilingDate = DateTime.Now;
            newApplication.Id = command.Id;
            newApplication.Status = "Open";
            newApplication.TopMatches = matches;
            newApplication.ClaimMatches = claimsMatches;

            m_ApplicationRepo.Add(newApplication);

            return newApplication.Id;
        }

        private void UpdateApplication(ApplicationInfo existingApplication, List<PatentInfo> matches, List<MatchInfo> claimsMatches)
        {
            existingApplication.Status = "Open";
            existingApplication.TopMatches = matches;
            existingApplication.ClaimMatches = claimsMatches;

            m_ApplicationRepo.Add(existingApplication);
        }

        private List<PatentInfo> FindWebResults(string abstractContents)
        {
            var accountId = ConfigurationManager.AppSettings["GoogleSearchAccountId"];

            var providers = new Dictionary<string, string>();
            providers.Add("google", "000375274410958331611:ealp1cwjuyo");
            providers.Add("uspto", "000375274410958331611:tkd-ybnspku");
            providers.Add("espacenet", "000375274410958331611:wi3vx2vobea");
            providers.Add("WIPO", "000375274410958331611:q3uxlfvyjhq");

            var searchInfos = providers.Select(provider => new SearchInfo()
            {
                AccountId = accountId,
                ProviderId = provider.Value, 
                Query = abstractContents
            }).ToList();

            var results = searchInfos.SelectMany(query => m_SearchRepo.Search(query)).ToList();

            return results;
        }

        #region Private Methods

        private List<PatentInfo> ScoreMatches(string abstractContents, List<PatentInfo> matches)
        {
            var scoredMatches = matches.Select(match => GetMatchSimilarity(abstractContents, match)).ToList();

            return scoredMatches.Where(match=>match.Rank > THRESHOLD).OrderByDescending(match=>match.Rank).ToList();
        }
        
        private PatentInfo GetMatchSimilarity(string abstractContents, PatentInfo match)
        {
            var request = new DocumentSimilarityRequest();
            request.Document1Contents = abstractContents;
            request.Document2Contents = match.Description;

            match.Rank = (float)DocumentSimilarityAnalyzer.GetDocumentSimilarity(request)[0];
            
            return match;
        }

        private List<PatentInfo> ScoreClaimsMatches(string abstractContents, List<PatentInfo> matches)
        {
            var scoredMatches = matches.Select(match => GetClaimSimilarity(abstractContents, match)).ToList();

            return scoredMatches.Where(match => match.Rank > THRESHOLD).OrderByDescending(match => match.Rank).ToList();
        }
        private PatentInfo GetClaimSimilarity(string abstractContents, PatentInfo match)
        {
            if (match.Description!= null)
            {
                    var request = new DocumentSimilarityRequest();
                    request.Document1Contents = abstractContents;
                    request.Document2Contents = match.Description;

                    match.Rank += (float)DocumentSimilarityAnalyzer.GetDocumentSimilarity(request)[0];
                
            }

            return match;
        }

        private EmailMessage GetConfirmationMessage(ImportApplicationCommand command)
        {
            var message = new EmailMessage();

            message.Body = string.Format("Your application {0} has been imported.  Please log in to view the results", command.ApplicationName);
            message.MessageId = string.Format("Application {0} imported", command.ApplicationName);
            message.Title = message.MessageId;
            message.Recipients = new List<string>();
            message.Recipients.Add(command.EmailAddress);
            message.Sender = ConfigurationManager.AppSettings["SupportEmailAddress"];

            return message;
        }

        #endregion
    }
}
