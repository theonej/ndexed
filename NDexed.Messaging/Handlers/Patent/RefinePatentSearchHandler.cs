﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Documents;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using NDexed.Domain.Models.User;
using NDexed.Messaging.Commands;
using NDexed.Messaging.Messages;
using NDexed.Messaging.Resources;
using NDexed.Search.Analyzers;
using NDexed.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NDexed.Messaging.Handlers.Patent
{
    public class RefinePatentSearchHandler : IReturnCommandHandler<RefinePatentSearchCommand, List<PatentInfo>>
    {
        private readonly IRepository<Guid, PatentSearchInfo> m_PatentSearchRepository;
        private readonly IReturnCommandHandler<PatentSearchCommand, SearchResultInfo> m_SearchCommandHandler;
        private readonly IRepository<UserInfo, UserInfo> m_UserRepo;
        private readonly IMessager m_Messager;

        public RefinePatentSearchHandler(IRepository<Guid, PatentSearchInfo> patentSearchRepo,
                                         IReturnCommandHandler<PatentSearchCommand, SearchResultInfo> searchCommandHandler,
                                         IRepository<UserInfo, UserInfo> userRepo,
                                         IMessager messager)
        {
            Condition.Requires(patentSearchRepo).IsNotNull();
            Condition.Requires(searchCommandHandler).IsNotNull();
            Condition.Requires(userRepo).IsNotNull();
            Condition.Requires(messager).IsNotNull();

            m_PatentSearchRepository = patentSearchRepo;
            m_SearchCommandHandler = searchCommandHandler;
            m_UserRepo = userRepo;
            m_Messager = messager;
        }

        public List<PatentInfo> Handle(RefinePatentSearchCommand command)
        {
            var searchInfo = m_PatentSearchRepository.Get(command.PatentSearchInfoId);

            var existingRecordIds = searchInfo.PatentSearchResults
                                              .Select(item => GetIdentifier(item))
                                              .ToList();

            //get user record so that we can use the custom user selection threshold

            var queries = GetSearchCriteria(searchInfo);

            //execute parallel searches for all of the saved results
            var searchTasks = queries.Select(query => GetSearchTask(query, searchInfo));

            //get any common search results
            var results = searchTasks.Select(task => task.Result)
                                            .SelectMany(result=>result.PatentResults)
                                            .ToList();

            var recurringItemIds = results.Select(patent => GetIdentifier(patent))
                                     .Distinct()
                                     .Select(item => new
                                     {
                                         Id = item,
                                         Count = results.Where(patent => GetIdentifier(patent) == item).ToList().Count
                                     })
                                     .Where(item => item.Count > 1)
                                     .Select(item=>item.Id)
                                     .ToList();

            var filteredResults = results
                                    .Where(result => recurringItemIds.Contains(GetIdentifier(result)))
                                    .Where(result=>!existingRecordIds.Contains(GetIdentifier(result)))
                                    .GroupBy(item=>GetIdentifier(item))
                                    .Select(group=>group.First())
                                    .ToList();

            //use the user-level threshold for selecting matches
            var user = GetUser(searchInfo.UserId);

            var scoredResults = filteredResults
                                    .Select(item => ScoreResult(searchInfo.PatentSearchResults, item))
                                    .Where(item=>item.OverallRank >= user.ScoreResultThreshold)
                                    .OrderByDescending(item=>item.OverallRank)
                                    .ToList();

            searchInfo.AdditionalSearchResults = scoredResults;
            m_PatentSearchRepository.Add(searchInfo);

            SendNotificationEmail(searchInfo, user);

            return scoredResults;
        }

        private UserInfo GetUser(Guid id)
        {
            var criteria = new UserInfo();
            criteria.Id = id;

            return m_UserRepo.Get(criteria);
        }

        private PatentInfo ScoreResult(List<PatentInfo> source, PatentInfo target)
        {
            var sourceDescriptions = source.Select(item => item.Description)
                                           .ToList();

            var sourceClaims = source.SelectMany(item => item.Claims)
                                     .Select(item => item.Claim)
                                     .ToList();

            var sourceDocuments = sourceDescriptions.Concat(sourceClaims)
                                                    .Select(item=>item.Replace("\n", ""))
                                                    .ToList();
            var targetDocs = target.Claims
                                   .Select(item => item.Claim)
                                   .Select(item => item.Replace("\n", ""))
                                   .ToList();

            targetDocs.Add(target.Description);
            
            var score = DocumentSimilarityAnalyzer.GetSourceDocumentScores(sourceDocuments, targetDocs);
            target.Rank = score[0];

            return target;
        }

        private string GetIdentifier(PatentInfo patent)
        {
            return patent.PatentNumber == null ? patent.ApplicationNumber : patent.PatentNumber;
        }

        /// <summary>
        /// This code does a document similarity comparison against all search results description and all claims
        /// The highest ranking claims are then returned as search criteria for the next phase in refinement
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        private Task<SearchResultInfo> GetSearchTask(string searchCriteria, PatentSearchInfo searchInfo)
        {
            var command = new PatentSearchCommand();
            command.Criteria = new SearchInfo();
            command.Criteria.Query = searchCriteria;
            command.Criteria.PatentSearchId = searchInfo.PatentSearchId;
            command.Criteria.AccountId = "AIzaSyAaRcXv0NeGNp1aOEcFC5cEZevh31ehx8o";
            command.Criteria.ProviderId = "000375274410958331611:ealp1cwjuyo";

            return Task<SearchResultInfo>.Factory.StartNew(() => m_SearchCommandHandler.Handle(command));
        }

        private List<string> GetSearchCriteria(PatentSearchInfo searchInfo)
        {
            var savedResults = searchInfo.PatentSearchResults;
            
            var documentLists = GetDocumentLists(savedResults);

            //get matrix of scores for each search results
            float[][][] resultScores = documentLists.Select(dict => GetDocumentDictionaryScores(dict)).ToArray();

            /*
            //now marry scores back to documents, by order
                [0][0][0] = first result, first array of values, first entry in array -- corresponds to the document score against itself
                [0][0][1] = first result, first array of values, second entry in array -- should be the second item in the first document dictionary 
            */

            var scoresAndQueries = documentLists.Zip(resultScores, (queries, scores)=> new {
                queries= queries,
                scores=scores[0]
            })
            .ToList();

            var scoredQueries = scoresAndQueries.Select(item => item.queries.Zip(item.scores, (query, score) => new
                                                {
                                                    query = query,
                                                    score = score
                                                }).ToList());

            Dictionary<string, float> aggregatedResults = new Dictionary<string, float>();
            foreach(var scoredQuerySet in scoredQueries)
            {
                foreach(var item in scoredQuerySet)
                {
                    if(!aggregatedResults.ContainsKey(item.query))
                    {
                        aggregatedResults.Add(item.query, 0.0f);
                    }

                    aggregatedResults[item.query] += item.score;
                }
            }
            var scoredSearchTerms = aggregatedResults.Where(item => item.Value < 1)
                                    .OrderByDescending(item=>item.Value)
                                    .Take(3)
                                    .Select(item=>item.Key)
                                    .ToList();

            return scoredSearchTerms;
        }

        private float[] Add(float[] first, float[] second)
        {
            return first.Zip(second, (a, b) => a + b).ToArray();
        }

        private float[][] GetDocumentDictionaryScores(List<string> documentList)
        {
            var command = new DocumentSimilarityRequest();

            command.DocumentContents = documentList
                                                .Select(item=>item.Replace("\n", ""))
                                                .ToList();

            var results = DocumentSimilarityAnalyzer.GetDocumentsSimilarity(command);

            return results;
        }

        private List<List<string>> GetDocumentLists(List<PatentInfo> savedResults)
        {
            var claimsList = GetClaimsList(savedResults);
            
            List<List<string>> documentLists = new List<List<string>> ();
            foreach(var result in savedResults)
            {
                var clone = claimsList.Select(item => item).ToList();
                clone.Insert(0, result.Description);
                
                documentLists.Add(clone);
            }

            return documentLists;
        }

        private List<string> GetClaimsList(List<PatentInfo> savedResults)
        {
            var claimsDocDictionary = savedResults
                                        .SelectMany(item => item.Claims)
                                        .Select(claim => claim.Claim).ToList();
                                        

            return claimsDocDictionary;
        }

        private void SendNotificationEmail(PatentSearchInfo searchInfo, UserInfo user)
        {
            //construct an email address alerting the user that additional search results have been found
            var message = GetRefinementMessage(searchInfo, user);
            m_Messager.SendMessage(message);
        }

        private IMessageInfo GetRefinementMessage(PatentSearchInfo searchInfo, UserInfo user)
        {
            EmailMessage message = new EmailMessage();
            message.Recipients = new List<string>() { user.EmailAddress };
            message.Sender = ConfigurationManager.AppSettings["SupportEmailAddress"];
            message.Title = "Refined Search Results";
            message.UtcSent = DateTime.UtcNow;
            message.MessageId = Guid.NewGuid().ToString();
            message.Body = GetUserRegistrationEmailBody(searchInfo, user);
            
            return message;
        }

        private string GetUserRegistrationEmailBody(PatentSearchInfo searchInfo, UserInfo user)
        {
            string url = ConfigurationManager.AppSettings["AppUrl"];

            List<Tuple<string, string>> tagValues = new List<Tuple<string, string>>();
            tagValues.Add(new Tuple<string, string>("{{AppUrl}}", url));
            tagValues.Add(new Tuple<string, string>("{{ResultCount}}", searchInfo.AdditionalSearchResults.Count.ToString()));
            tagValues.Add(new Tuple<string, string>("{{SearchName}}", searchInfo.PatentSearchCriteria.Title));
            tagValues.Add(new Tuple<string, string>("{{DocketNumber}}", searchInfo.DocketNumber));

            string fileContents = Templates.RefinedSearchResults;
            foreach (Tuple<string, string> tagValue in tagValues)
            {
                fileContents = fileContents.Replace(tagValue.Item1, tagValue.Item2);
            }

            return fileContents;
        }

    }
}
