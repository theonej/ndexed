﻿using CuttingEdge.Conditions;
using NDexed.DataAccess.Repositories;
using NDexed.Domain.Models.Patents;
using NDexed.Domain.Models.Search;
using NDexed.Domain.Models.User;
using NDexed.Messaging.Commands;
using NDexed.Search.Analyzers;
using NDexed.Search.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Handlers.Patent
{
    public class PatentSearchHandler : IReturnCommandHandler<PatentSearchCommand, SearchResultInfo>,
                                       ICommandHandler<SaveSearchInfoCommand> 
    {
        const float TERM_SELECTION_THRESHOLD = .2f;
        
        private readonly IRepository<string, PatentInfo> m_PatentRepo;
        private readonly IRepository<string, PatentApplicationInfo> m_PatentApplicationRepo;
        private readonly ISearchableRepository<SearchInfo, ClaimInfo> m_ClaimRepo;
        private readonly IRepository<Guid, SearchInfo> m_SearchHistoryRepository;
        private readonly ISearchableRepository<SearchInfo, PatentInfo> m_PatentSearchRepo;
        private readonly ISearchableRepository<SearchInfo, PatentInfo> m_GoogleSearchRepo;
        private readonly ISearchableRepository<SearchInfo, PatentApplicationInfo> m_ApplicationSearchRepo;
        private readonly ISearchableRepository<SearchInfo, CpcInfo> m_CpcSearchRepo;
        private readonly IRepository<Guid, PatentSearchInfo> m_PatentSearchInfoRepo;
        private readonly IRepository<UserInfo, UserInfo> m_UserRepo;

        public PatentSearchHandler(IRepository<string, PatentInfo> patentRepo,
                                   IRepository<string, PatentApplicationInfo> patentApplicationRepo,
                                   ISearchableRepository<SearchInfo, PatentInfo> patentSearchRepo,
                                   ISearchableRepository<SearchInfo, PatentApplicationInfo> applicationSearchRepo,
                                   ISearchableRepository<SearchInfo, ClaimInfo> claimRepo,
                                   IRepository<Guid, SearchInfo> searchHistoryRepo,
                                   ISearchableRepository<SearchInfo, CpcInfo> cpcSearchRepo,
                                   IRepository<Guid, PatentSearchInfo> patentSearchInfoRepo,
                                   IRepository<UserInfo, UserInfo> userRepo)
        {
            Condition.Requires(patentRepo).IsNotNull();
            Condition.Requires(claimRepo).IsNotNull();
            Condition.Requires(searchHistoryRepo).IsNotNull();
            Condition.Requires(patentSearchRepo).IsNotNull();
            Condition.Requires(applicationSearchRepo).IsNotNull();
            Condition.Requires(cpcSearchRepo).IsNotNull();
            Condition.Requires(patentApplicationRepo).IsNotNull();
            Condition.Requires(patentSearchInfoRepo).IsNotNull();
            Condition.Requires(userRepo).IsNotNull();

            m_PatentRepo = patentRepo;
            m_PatentApplicationRepo = patentApplicationRepo;
            m_ClaimRepo = claimRepo;
            m_SearchHistoryRepository = searchHistoryRepo;
            m_PatentSearchRepo = patentSearchRepo;
            m_ApplicationSearchRepo = applicationSearchRepo;
            m_CpcSearchRepo = cpcSearchRepo;
            m_PatentSearchInfoRepo = patentSearchInfoRepo;
            m_UserRepo = userRepo;

            m_GoogleSearchRepo = new GoogleRepository();
        }

        public SearchResultInfo Handle(PatentSearchCommand command)
        {
            if (command.Criteria.PatentSearchId == Guid.Empty)
            {
                throw new InvalidOperationException("You must specify the search Id for this query");
            }


            var list = Search(command.Criteria);

            if (list.Count > 0)
            {
                //perform document score lookups
                list = list.Select(
                                result => Task<PatentInfo>.Factory
                                                            .StartNew(() => ScoreResult(command.Criteria, result))
                                                            .Result)
                            .ToList();
            }

            var returnValue = GetSearchResultInfo(command.Criteria.Query, list);

            

            returnValue.PatentResults = returnValue.PatentResults
                                                   .Select(item => Highlight(returnValue.Terms, item))
                                                   .ToList();

            return returnValue;
        }

        private List<PatentInfo> Search(SearchInfo criteria)
        {
            switch (criteria.Type)
            {
                case (SearchInfo.SearchType.All):
                    return SearchAll(criteria);
                case (SearchInfo.SearchType.Cpc):
                    return SearchCpc(criteria);
                case (SearchInfo.SearchType.Terms):
                    return SearchTerms(criteria);
                default:
                    throw new NotSupportedException(string.Format("Search Type {0} is not supported", criteria.Type));
            }
        }

        private List<PatentInfo> SearchAll(SearchInfo criteria)
        {
            var patentSearchTask = Task<List<PatentInfo>>.Factory.StartNew(() => SearchPatents(criteria));

            var tasks = new Task<List<PatentInfo>>[]
            {
                Task<List<PatentInfo>>.Factory.StartNew(()=>SearchClaims(criteria)),
                Task<List<PatentInfo>>.Factory.StartNew(()=>SearchApplications(criteria)),
                Task<List<PatentInfo>>.Factory.StartNew(()=>SearchGoogle(criteria))
            };

            var results = tasks.Select(task => task.Result).SelectMany(task => task).ToList();

            //deduplicate, prefering patent search records
            var patentSearchIds = patentSearchTask.Result.Select(patent => patent.PatentNumber).ToList();
            var filteredTaskResults = results.Where(result => !patentSearchIds.Contains(result.PatentNumber)).ToList();

            var filteredLists = patentSearchTask.Result
                                              .Concat(filteredTaskResults)
                                              .ToList();

            return filteredLists;
        }

        private SearchResultInfo GetSearchResultInfo(string query, List<PatentInfo> searchResults)
        {
            var descriptionContents = searchResults.Select(item => item.Description)
                                                .ToList();

            var claimContents = searchResults.SelectMany(item => item.Claims)
                                             .Select(claim => claim.Claim)
                                             .ToList();

            var documentContents = descriptionContents;//.Concat(claimContents).ToList();

            var returnValue = new SearchResultInfo()
            {
                PatentResults = searchResults,
                Terms = DocumentSimilarityAnalyzer.GetDocumentsTfIdf(documentContents)
            };

            var searchWords = query.Split(' ').ToList();
            returnValue.Terms = returnValue.Terms
                                         .Where(item => searchWords.Contains(item.Term))
                                         .Where(item=>item.Score > 0)
                                         .ToList();
            
            return returnValue;
        }

        private PatentInfo ScoreResult(SearchInfo criteria, PatentInfo result)
        {
            var sourceDocuments = new List<string>() { criteria.Query };
            var targetDocuments = result.Claims
                                        .Select(item => item.Claim)
                                        .ToList();

            targetDocuments.Insert(0, result.Description);

            var scoreInfo = DocumentSimilarityAnalyzer.GetSourceDocumentsScores(sourceDocuments, targetDocuments);
            result.OverallRank = scoreInfo.Score;
            result.DescriptionRank = scoreInfo.DocumentScores[0];
            result.Claims = result.Claims.Zip(scoreInfo.DocumentScores.Skip(1).ToList(), (claim, score) => {
                claim.ClaimRank = score;
                return claim;
            }).ToList();

            return result;
        }

        private List<PatentInfo> SearchPatents(SearchInfo criteria)
        {
            return m_PatentSearchRepo.Search(criteria).ToList();
        }

        private List<PatentInfo> SearchApplications(SearchInfo criteria)
        {
            return m_ApplicationSearchRepo.Search(criteria)
                                          .Select(item => (PatentInfo)item)
                                          .ToList();
        }

        private List<PatentInfo> SearchGoogle(SearchInfo criteria)
        {
            return m_GoogleSearchRepo.Search(criteria).ToList();
        }
        private List<PatentInfo> SearchClaims(SearchInfo criteria)
        {
            var claims = m_ClaimRepo.Search(criteria);
            m_SearchHistoryRepository.Add(criteria);

            var patentNumbers = claims.Select(claim => claim.PatentNumber)
                                  .Distinct().ToArray();

            var patents = patentNumbers.Select(patentNumber => m_PatentRepo.Get(patentNumber))
                                       .ToList();

            return patents;
        }

        private List<PatentInfo> SearchCpc(SearchInfo criteria)
        {
            var cpcs = criteria.Query.Split('\n');
            var searchTasks = cpcs.Select(cpc =>
            {
                var cpcCriteria = new SearchInfo();
                cpcCriteria.Query = cpc;
                return Task<List<CpcInfo>>.Factory.StartNew(() => m_CpcSearchRepo.Search(cpcCriteria).ToList());
            });

            var cpcResults = searchTasks.Select(task => task.Result).SelectMany(task => task).ToList();

            var patentNumbers = cpcResults.Select(cpc => cpc.PatentNumber)
                                  .Distinct().ToArray();

            var patents = patentNumbers.Select(patentNumber => m_PatentRepo.Get(patentNumber))
                                       .ToList();

            return patents;
        }

        private List<PatentInfo> SearchTerms(SearchInfo criteria)
        {
            var patents = m_PatentSearchRepo.Search(criteria)
                                            .ToList();

            return patents;
        }

        /// <summary>
        /// Highlights all words in teh patent description that also occur in the search query
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="patent"></param>
        /// <returns></returns>
        private PatentInfo Highlight(List<SearchTermInfo> terms, PatentInfo patent)
        {
            var scores = terms.Select(term => term.Score)
                              .OrderByDescending(score => score);

            var max = scores.FirstOrDefault();
            var min = scores.LastOrDefault();

            if (max != float.MinValue && min != float.MinValue)
            {
                var range = max - min;
                var topTerms = terms.Where(term => term.Score >= (range * TERM_SELECTION_THRESHOLD))
                                    .ToList();
                var documentTerms = GetPatentTerms(patent);


                var topStems = topTerms.Select(item => item.Stem)
                                          .ToList();

                //get all document terms that share a stem with a word in the top terms
                patent.Tags = documentTerms.Where(item => topStems.Contains(item.Stem))
                                                         .ToList();
                
                var wordsToReplace = patent.Tags.Select(item => item.Term)
                                        .ToList();

                //the Highlight field shoud be chosen from the description based on the section of the description that scores the highest
                //by scanning over the description in fixed intervals and scoring each one against the search terms
                patent.Highlight = HighlightContents(wordsToReplace, patent.Abstract).Substring(0, Math.Min(patent.Abstract.Length, 500));
                patent.Description = HighlightContents(wordsToReplace, patent.Description);
                patent.Abstract = HighlightContents(wordsToReplace, patent.Abstract);
                patent.Claims = patent.Claims.Select(item=>
                                                            {
                                                                item.Claim = HighlightContents(wordsToReplace, item.Claim);
                                                                return item;
                                                            }
                                                    )
                                             .ToList();
            }

            return patent;
        }
        

        private List<SearchTermInfo> GetPatentTerms(PatentInfo patent)
        {
            var sourceDocuments = new List<string>();
            sourceDocuments.Add(patent.Abstract);
            sourceDocuments.Add(patent.Description);
            sourceDocuments.AddRange(patent.Claims.Select(claim=>claim.Claim));

            return DocumentSimilarityAnalyzer.GetDocumentStems(sourceDocuments);
        }

        private string HighlightContents(List<string> wordsToReplace, string contents)
        {
            var replaced = contents.Split(' ')
                                   .Select(word =>
                                            wordsToReplace.Contains(word)
                                            ?
                                            string.Format("<em>{0}</em>", word)
                                            :
                                            word);

             return string.Join(" ", replaced);
    }

        public void Handle(SaveSearchInfoCommand command)
        {
            var searchInfo = command.SearchInfo;

            if (searchInfo.PatentSearchId == Guid.Empty)
            {
                searchInfo.PatentSearchId = Guid.NewGuid();
            }

            var patentSearchResults = searchInfo.PatentSearchResults
                                                .Where(item=>item.Source == PatentInfo.Sources.Internal)
                                                .Where(item=>!string.IsNullOrEmpty(item.PatentNumber))
                                                .Select(item => m_PatentRepo.Get(item.PatentNumber))
                                                .ToList();

            var applicationSearchResults = searchInfo.PatentSearchResults
                                                .Where(item => item.Source == PatentInfo.Sources.Internal)
                                                .Where(item => string.IsNullOrEmpty(item.PatentNumber) && !string.IsNullOrEmpty(item.ApplicationNumber))
                                                .Select(item => m_PatentApplicationRepo.Get(item.ApplicationNumber))
                                                .ToList();

            var externalSearchResults = searchInfo.PatentSearchResults
                                                .Where(item => item.Source == PatentInfo.Sources.External)
                                                .ToList();

            if (searchInfo.PatentSearchResults.Count > 0)
            {
                //update the users threshold score with the average of the current selection
                var user = GetUser(command.SearchInfo.UserId);

                var averageScore = searchInfo.PatentSearchResults
                                             .Average(item => item.OverallRank);

                user.ScoreResultThreshold = (user.ScoreResultThreshold + averageScore) / 2;

                m_UserRepo.Add(user);
            }

            //now save the results
            searchInfo.PatentSearchResults = patentSearchResults.Concat(applicationSearchResults)
                                                                .Concat(externalSearchResults)
                                                                .ToList();

            m_PatentSearchInfoRepo.Add(searchInfo);

        }

        private UserInfo GetUser(Guid id)
        {
            var criteria = new UserInfo();
            criteria.Id = id;

            return m_UserRepo.Get(criteria);
        }

    }
}
