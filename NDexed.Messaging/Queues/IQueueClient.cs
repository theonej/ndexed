﻿using NDexed.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Queues
{
    public interface IQueueClient<T> where T:IMessageInfo
    {
        T GetNext(string queueName);
        void Publish(T message);

        bool DeleteMessage(T message);
    }
}
