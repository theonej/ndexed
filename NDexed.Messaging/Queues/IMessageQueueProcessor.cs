﻿using NDexed.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Queues
{
    public interface IMessageQueueProcessor<T> where T:IMessageInfo
    {
        void ProcessNext();
    }
}
