﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.CaseUsers
{
    public class AddCaseUsersCommand : ICommand
    {
        public AddCaseUsersCommand()
        {
            UserIds = new List<Guid>();
        }
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }
        public List<Guid> UserIds { get; set; }
    }
}
