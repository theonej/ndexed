﻿using NDexed.Messaging.Messages;
using System;

namespace NDexed.Messaging.Commands.Document
{
    public class DownloadEmailCommand : ICommand
    {
        public Guid Id { get; set; }
        public IMessageInfo MessageData { get; set; }
    }
}
