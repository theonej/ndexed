﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Document
{
    public class UpdateDocumentCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string UpdatedByUser { get; set; }
    }
}
