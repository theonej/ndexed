﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Document
{
    public class CreateDocumentZipCommand :ICommand
    {
        public Guid Id { get; set; }

        public List<Guid> DocumentIds { get; set; }

        public string OutputFileName { get; set; }
    }
}
