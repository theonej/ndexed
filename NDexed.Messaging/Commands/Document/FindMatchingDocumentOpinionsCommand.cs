﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Document
{
    public class FindMatchingDocumentOpinionsCommand :ICommand
    {
        public Guid Id { get; set; }
        public List<string> Jurisdictions { get; set; }
        public Guid DocumentId { get; set; }
        public int MaxCount { get; set; }
    }
}
