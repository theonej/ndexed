﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Document
{
    public class AddCaseDocumentCommand : ICommand
    {
        public enum SourceDocumentType
        {
            Document= 0,
            Regulation = 1,
            Opinion= 2
        }
        public Guid Id { get; set; }
        public Guid CaseId { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid SourceDocumentId { get; set; }

        public SourceDocumentType Type { get; set; }

        public string[] Tags { get; set; }
    }
}
