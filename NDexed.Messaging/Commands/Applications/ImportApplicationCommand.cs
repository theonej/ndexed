﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Applications
{
    public class ImportApplicationCommand : ICommand
    {
        public Guid Id { get; set; }

        public string ApplicationName { get; set; }
        public string EmailAddress { get; set; }
        public string Abstract { get; set; }
    }
}
