﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Matching
{
    public class ExtractMatchTermsAndClassesCommand : ICommand
    {
        public Guid Id { get; set; }
        public int TotalRecordsToGet { get; set; }

        public string TermOutputFile { get; set; }

        public string ClassOutputFile { get; set; }

        public string SubClassOutputFile { get; set; }
    }
}
