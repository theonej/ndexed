﻿using NDexed.Domain.Models.Matching;
using System;

namespace NDexed.Messaging.Commands.Matching
{
    public class GetMatchClassificationCommand : ICommand
    {
        public Guid Id { get; set; }

        public MatchInfo Match { get; set; }
    }
}
