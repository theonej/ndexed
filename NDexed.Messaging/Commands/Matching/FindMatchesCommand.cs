﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Matching
{
    public class FindMatchesCommand : ICommand
    {
        public Guid Id { get; set; }
        
        public string MatchType { get; set; }

        public string[] SearchTerms { get; set; }

        public int PageSize { get; set; }

        public int PageNumber { get; set; }
    }
}
