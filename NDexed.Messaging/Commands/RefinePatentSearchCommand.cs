﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands
{
    public class RefinePatentSearchCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid PatentSearchInfoId { get; set; }
    }
}
