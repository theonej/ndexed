﻿using NDexed.Domain.Models.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands
{
    public class PatentSearchCommand : ICommand
    {
        public Guid Id { get; set; }
        public SearchInfo Criteria { get; set; }
    }
}
