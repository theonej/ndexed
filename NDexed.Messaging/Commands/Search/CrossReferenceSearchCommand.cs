﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDexed.Messaging.Commands.Search
{
    public class CrossReferenceSearchCommand : ICommand
    {
        public Guid Id { get; set; }

        public List<string> PreviousSearches { get; set; }
    }
}
